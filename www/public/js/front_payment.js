(function ($) {
    $(document).ready(function () {
        // Add to Cart

        $('a.inscrease').click(function (e) {
            e.preventDefault();
            $('.quantity-value').text(+($('.quantity-value').text()) + 1);
        });

        $('a.decrease').click(function (e) {
            e.preventDefault();
            var val = $('.quantity-value').text();
            var val = val - 1;
            if(val >= 1){
                $('.quantity-value').text(val);
            }
        });

        $("#button-add-to-cart").on('click', function (e) {
            e.preventDefault();

            var ajaxrequest = random32bit();
            var productId = $(this).data('product-id');
            var quantity = $('.quantity-value').text();

            $.ajax({
                url: '/cart/add/',
                type: 'POST',
                data: {
                    ajaxrequest: random32bit(),
                    productId: productId,
                    quantity: quantity,
                },
                encode: true,

                beforeSend: function () {
                    $('#loader-perform').show();
                },
                complete: function () {
                    $('#loader-perform').hide();
                }
            }).done(function (response) {
                var data = JSON.parse(response);
                $("#product-number").html('(' + data.nb + ')');

                if(data.error == "") {
                    $("#cart-info").show();
                    $("#cart-info span").text(data.message);
                    setTimeout(function () {
                        $("#cart-info").fadeOut();
                    }, 4000);
                }else{
                    $("#cart-info div.message").toggleClass("success error");
                    $("#cart-info").show();
                    $("#cart-info span").text(data.error);
                    setTimeout(function () {
                        $("#cart-info").fadeOut();
                    }, 4000);
                }

            }).fail(function () {
                console.log("Fail");
            }).always(function (e) {
                //e.target.reset();
            });
        });

        //
    });

})(jQuery);
