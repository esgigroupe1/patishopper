**Après un git clone <lien_du_project>**

Changer de branche (ne pas travailler directement sur la branche master)

- git checkout -b feature/<nom-de-la-fonctionnalite> develop

Quand on a finit, on fait :

- git add .

- git commit -m "Le messsage"

- git checkout develop

- git merge feature/<nom-de-la-fonctionnalite>

- git push origin develop

- Faire un pull request sur le lien généré

Pour partager la branche locale (la fonctionnalité), faire un :

- git checkout feature/<nom-de-la-fonctionnalite>

- git push

ou

- git push --set-upstream origin feature/<nom-de-la-fonctionnalite>

Ou aller sur une des branches existantes

**Annuler une modification sur un fichier**

- sudo git checkout -- <nom-du-fichier>

Pour plus de précision, voir le **Workflow**

**Lancer la compilation des ressources**

Faire : 

- npm install
- gulp

**Démarrer l'environnement de dev**

Si c'est la première fois, faire :

- docker-compose up -d

Si l'environnement docker est déjà en place, faire :

- docker-compose start

Adresse web : localhost:3000
Adresse phpmyadmin: localhost:8000

Pour arrêter l'environnement docker, faire : 

- docker-compose stop

Au lieu de :

- docker-compose down 

qui supprime tout les containers et donc la **base de données**

**Commandes utiles pour Docker**

Supprimer tous les containers
- docker rm $(docker ps -aq) -f

Supprimer toutes les images
- docker rmi $(docker images -q) -f

Se connecter en shell interactif
- sudo docker exec -i -t 43f10328e4e9 /bin/bash

**Workflow**
Système de branches : 
- master
- develop
- feature/<nom-de-la-fonctionalite>

Pour se brancher sur une branche distante, faire :

- git checkout -b <nom-de-la-branche>

- git pull origin <nom-de-la-branche>

** Pour supprimer une branche **

Branche locale : 

- git branch -d <nom-de-la-branche>

Branche distante :

- git push origin --delete <nom-de-la-branche>

** Pour mettre à jour la liste des branches distantes en local **

- git config remote.origin.prune true

Pour créer un pont entre le domaine public et votre ordinateur local, installez localtunnel :

- npm install -g localtunnel

- lt --port <port>
