<?php

class Product extends BaseSQL
{
    protected $id = null;
    protected $name;
    protected $slug;
    protected $trash;
    protected $description;
    protected $stock;
    protected $price;

    protected $user_id;
    protected $attachment_id;

    protected $created_at = null;
    protected $updated_at = null;

    protected $relations = [
        'categories' => [
            'type' => 'ManyToMany',
            'table' => 'category',
            'data' => [],
        ],
        'comments' => [
            'type' => 'OneToMany',
            'table' => 'comment',
            'data' => [],
        ],
        'transactions' => [
            'type' => 'OneToMany',
            'table' => 'transaction',
        ],
        'user' => [
            'type' => 'OneToOne',
            'table' => 'user',
        ],
        'attachment' => [
            'type' => 'OneToOne',
            'table' => 'attachment',
        ],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public static function fetchColumns()
    {
        //$all_products = Product::findAll();
        $published_products = Product::getCollections(["trash" => 0]);
        $trash_products = Product::getCollections(["trash|OR" => 1, "trash|IS" => NULL]);

        return [
            // 'all_products' => $all_products,
            'published_products' => $published_products,
            'trash_products' => $trash_products,
        ];
    }

    public static function configForm($product = [], $existing_cats_ids = [])
    {
        $num = !empty($product);

        $suffix_label = '<span class="bar"></span><span class="line-right-left"></span>';

        $checkboxes = "";

        // gets all attached categories if their exists
        if (empty($existing_cats_ids)) {
            if (!empty($product->relations['categories']['data'])) {
                $existing_cats_ids = array_column($product->relations['categories']['data'], 'id'); // // get all cats id
            }
        }

        $categories = Category::select(['type|OR' => 2, 'type' => 3]);
        Helper::sortByLastUpdated($categories);
        foreach ($categories as $key => $category) {
            $checked = (in_array($category->id, $existing_cats_ids)) ? "checked" : "";

            $checkboxes .= '<label class="checkbox"><div class="label">' . $category->name . '</div><input type="checkbox" name="category_' . $category->id . '" value="' . $category->id . '"' . $checked . '>';
            $checkboxes .= '<span class="checker"></span></label>';
        }

        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => ($num > 0) ? "Enregister" : "Ajouter le produit",
                "submit_name" => "",
                "submit_class" => "button",
            ],

            "input" => [

                "name" => [
                    "name" => "name",
                    "type" => "text",
                    "label" => '<p>Nom</p>' . $suffix_label,
                    "placeholder" => "",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "id" => "product_name",
                    "value" => ($num) ? $product->name : "",
                ],

                "slug" => [
                    "name" => "slug",
                    "type" => "text",
                    "label" => "<p>Slug</p>" . $suffix_label,
                    "placeholder" => "",
                    "minString" => 0,
                    "maxString" => 100,
                    "id" => "product_slug",
                    "value" => ($num) ? (empty($product->slug) ? (Helper::uniqueSlug($product->name, $product)) : $product->slug) : "",
                ],

                "price" => [
                    "name" => "price",
                    "type" => "text",
                    "label" => "<p>Prix (en €)</p>" . $suffix_label,
                    "placeholder" => "",
                    "required" => "required",
                    "minString" => 0,
                    "maxString" => 100,
                    "id" => "product_price",
                    "value" => ($num) ? $product->price : "",
                ],

                "stock" => [
                    "name" => "stock",
                    "type" => "text",
                    "label" => "<p>Stock (en quantités)</p>" . $suffix_label,
                    "placeholder" => "",
                    "required" => "required",
                    "minString" => 0,
                    "maxString" => 100,
                    "id" => "product_stock",
                    "value" => ($num) ? $product->stock : "",
                ],

                "featured-image" => [
                    "id" => "media-selected",
                    "name" => "media-id",
                    "type" => "hidden",
                    "label" => "",
                    "placeholder" => "",
                    "value" => ($num) ? $product->attachment_id : "-1",
                ],

                // grouped checkbox like categories
                "checkboxes" => [
                    "type" => "checkbox",
                    "values" => $checkboxes,
                ],
            ],

            "textarea" => [
                "product-content" => [
                    //"label" => "Contenu de l'article",
                    "name" => "text",
                    "class" => "content_product",
                    "value" => ($num) ? $product->description : "",
                ],

            ],
        ];
    }

    public static function configCommentForm() {
        return [
            "config" => [
                "method" => "POST",
                "action" => DIRNAME . "product/comment/add",
                "submit" => "Donner mon avis",
                "submit_name" => "",
                "submit_class" => "button",
                "id" => "product-comment",
            ],

            "textarea" => [
                "comment-content" => [
                    "name" => "text",
                    "class" => "content_product",
                    "placeholder" => "Ecrivez votre avis ici !",
                ],
            ],
        ];
    }

    public static function configTableList($data = [])
    {
        // prepare data to table
        $rowsData = [];
        $products = $data['products'];

        // build headers
        $data['headers'] = [
            '<label class="checkbox"><div class="label">Tout</div><input type="checkbox" name="all"><span class="checker"></span></label>',
            "<p>Nom</p>",
            "<p>Description</p>",
            "<p>Stock</p>",
            "<p>Prix</p>",
            "<p>Etat</p>",
            "<p>Categories</p>",
        ];

        // Sort by last updated date field
        Helper::sortByLastUpdated($products);

        // build column
        foreach ($products as $key => $row) {
            $rowsData[$key]['checkbox'] = '<label class="checkbox"><input class="checkboxItem" type="checkbox" name="action_' . $row->id . '" value="' . $row->id . '"><span class="checker"></span></label>';

            // links to modify, delete, trash
            $title = '<span class="product-title">' . Helper::xss($row->name) . '</span>';
            $title .= '<span class="product-edit">';

            $show_link = '';

            if(!empty($row->slug)) {
                $title .= '<a target="_blank" href="' . DIRNAME . Helper::xss($row->slug) . '">Afficher</a>';
            }
            $title .= '<a href="' . DIRNAME . "admin/product/edit/" . $row->id . '">Modifier</a>';

            if ($row->trash == 0) {
                $title .= '<a class="product-move-to-bin" href="' . DIRNAME . 'admin/product/move-to-bin" data-id="' . $row->id . '">Mettre à la corbeille</a>';
            }

            if ($row->trash == 1) {
                $title .= '<a class="product-remove-from-bin" href="' . DIRNAME . 'admin/product/remove-from-bin" data-id="' . $row->id . '">Restaurer</a>';
            }
            $title .= '<a class="product-delete" href="' . DIRNAME . 'admin/product/delete" data-id="' . $row->id . '">Supprimer définitivement</a>';
            $title .= '</span>';

            $publishedDate = new DateTime($row->created_at); // format date from MYSQL database
            $updatedDate = new DateTime($row->updated_at); // format date from MYSQL database

            $description = ($row->description != 'NULL') ? (Helper::truncate($row->description, 40)) : "Aucune description";

            $categories = array_column($row->relations['categories']['data'], 'name');

            // set all columns
            $rowsData[$key]['nom'] = $title;
            $rowsData[$key]['description'] = $description;
            $rowsData[$key]['stock'] = $row->stock;
            $rowsData[$key]['price'] = $row->price . ' €';
            $rowsData[$key]['status'] = ($row->trash == 1) ? "Non publié" : "Publié";
            $rowsData[$key]['categories'] = (count($categories) > 0) ? implode(', ', $categories) : "-";
            $rowsData[$key]['published'] = $publishedDate->format("d-m-Y") . " à " . $publishedDate->format("H:i:s");
            $rowsData[$key]['updatedDate'] = $updatedDate->format("d-m-Y") . " à " . $updatedDate->format("H:i:s");
        }

        // data column after categories
        array_push($data['headers'], "<p>Date d'ajout</p>", "<p>Dernière mise à jour</p>");

        return ['headers' => $data['headers'], 'rows' => $rowsData , 'id' => 'product-list'];
    }
}
