<main id="user" class="container-fluid">
    <div id="patishopper-user">
        <div class="row">
            <div class="title col-10">
                <h1><?php echo $title;?></h1>
            </div>
            <div class="col-2" id="button">
                <a href="<?php echo DIRNAME; ?>admin/user" class="button right">Retour</a>
            </div>
        </div>

        <hr class="separator">

        <div id="user-form">
            <div id="form">
                <?php
                if ($this->getAssign()['errors']) {
                    foreach ($errors as $key => $value) : ?>
                        <p class="message error">
                            <span><?php echo $value; ?></span>
                        </p>
                        <?php
                    endforeach;

                } else {

                    if ($this->getAssign()['success']) : ?>
                        <p class="message success">
                            <span><?php echo $success; ?></span>
                        </p>
                        <?php
                    endif;
                }
                $this->addModal("form", $config, $errors);
                ?>
            </div>
            <!-- end form -->
        </div>
    </div>
</main>

