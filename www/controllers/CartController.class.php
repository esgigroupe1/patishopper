<?php

class CartController extends Controller
{
    public function addAction($params)
    {
        (!Helper::canAddToChart()) && $this->errors(404);
        if (!empty($params['POST']['productId']) &&
            is_numeric($params['POST']['productId']) && !empty($params['POST']['ajaxrequest'])) {
            $product = Product::findById($params['POST']['productId']);

            $ar = [];
            sleep(1);
            if (!empty($product)) {
                $id = $params['POST']['productId'];

                if (isset($_SESSION['products'][$id])) {
                    $_SESSION['products'][$id]['quantity'] += $params['POST']['quantity'];
                    $ar['message'] = "La quantité du produit existant a été augmentée.";
                } else {
                    $_SESSION['products'][$id] = [
                        'productId' => $id,
                        'name' => $product->name,
                        'price' => $product->price,
                        'quantity' => $params['POST']['quantity'],
                    ];

                    $ar['message'] = "Produit ajouté au panier";
                }
            }
        }

        if (empty($ar['message'])) {
            $ar['error'] = "Une erreur s'est produite.";
        }

        echo json_encode([
            'nb' => isset($_SESSION['products']) ? count($_SESSION['products']) : 0,
            'message' => isset($ar['message']) ? $ar['message'] : "",
            'error' => isset($ar['error']) ? $ar['error'] : "",
        ]);
    }

    public function contentAction($params)
    {
        (!Helper::canAddToChart()) && $this->errors(404);
        $v = new View("cart", "front");

        $v->assign('products', !empty($_SESSION['products']) ? $_SESSION['products'] : []);
    }

    public function checkoutAction($params)
    {
        empty($_SERVER['HTTP_REFERER']) && $this->errors(404);
        $previous_url = parse_url($_SERVER['HTTP_REFERER']);
        if (!empty($previous_url['path'])) {
            $previous_page = $previous_url['path'];
            if (!($previous_page == DIRNAME . "cart/content")) {
                $this->errors(404);
            }
        }
        (!Helper::canOrder()) && header("Location:" . DIRNAME . "authentication");
        $v = new View("checkout", "front");
        $config['comRef'] = "CCCREF01235";
        $_SESSION['comRef'] = $config['comRef'];
        $v->assign("config", $config);
    }

    public function validateAction($params)
    {
        // Oui, je confirme le paiement ...
        if (!empty($params['POST']) && Helper::canAddToChart() &&
            !empty($_SESSION['products']))
        {
            // Get
            $products = $_SESSION['products'];

            // Register Order
            $order = new Order();
            $order->user_id = $_SESSION['id_user'];
            $order->save();

            foreach ($products as $product) {
                // Register all Transaction
                $transaction = new Transaction();
                $transaction->quantity = $product['quantity'];
                $transaction->price = $product['price'];
                $transaction->order_id = $order->id;
                $transaction->product_id = $product['productId'];
                $transaction->save();
            }

            unset($_SESSION['products']);
            $_SESSION['pay_confirm'] = 'done';
            header("Location: " . DIRNAME . "cart/confirmation");
        } else {
            $this->errors(404);
        }
    }

    public function emptyAction($params)
    {
        unset($_SESSION['products']);
        header("Location:" . DIRNAME . "cart/content");
        die();
    }

    public function confirmationAction($params)
    {
        if(empty($_SESSION['products']) && !empty($_SESSION['pay_confirm']) &&
            $_SESSION['pay_confirm'] == 'done'){
            $v = new View("confirmation", "front");
            unset($_SESSION['pay_confirm']);
        }else {
            $this->errors(404);
        }
    }
}
