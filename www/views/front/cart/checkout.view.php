<main id="cart-checkout" class="container">
    <div class="row">
        <h1>Paiement</h1>
    </div> <!-- end row -->

    <hr>

    <?php $form = Form::get(Order::configFormOrder()); ?>
    <div class="row">
        <div class="col-12" id="message-checkout-wrapper">
           <div class="container">
               <div class="row">
                    <?php echo $form['open']; ?>

                    <div class="col-12">
                        <?php echo $form['card-owner']; ?>
                    </div>
                    <div class="col-12">
                        <?php echo $form['card-number']; ?>
                    </div>
                    <div class="col-12">
                        <label id="line">
                            <span>Date d'expiration (mm/aaaa)</span>
                            <div><?php echo $form['card-expired-month']; ?>/<?php echo $form['card-expired-year']; ?></div>
                        </label>
                    </div>
                    <div class="col-12">
                        <?php echo $form['card-code-verification']; ?>
                    </div>
                    <div class="col-12">
                        <?php echo $form['submit']; ?>
                        <a class="button no-background center" href="<?php echo DIRNAME . "cart/content"; ?>">Revenir au panier</a>
                    </div>
                    <?php echo $form['close']; ?>
               </div>

           </div>
        </div> <!-- end col-12 -->
    </div> <!-- end row -->
</main>
