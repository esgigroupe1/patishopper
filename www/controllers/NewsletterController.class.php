<?php

class NewsletterController extends Controller
{
    public function addmeAction($params)
    {
        if (!empty($params['POST']) && !empty($params['POST']['ajaxrequest'])) {

            if (Validator::checkEmail($params['POST']['email'])) {
                $prospect = Prospect::findByEmail($params['POST']['email']);

                if (empty($prospect)) {

                    $prospect = new Prospect();
                    $prospect->setEmail($params['POST']['email']);
                    $prospect->save();
                    echo json_encode([
                        'message' => "Vous êtes maintenant abonné à notre Newsletter.",
                    ]);
                } else {
                    echo json_encode([
                        'message' => "Vous êtes déjà abonné à notre Newsletter",
                    ]);
                }

            } else {
                echo json_encode([
                    'message' => "Saisissez un mail s'il vous plaît !",
                ]);
            }
        } else {
            $this->errors(404);
        }
    }
}
