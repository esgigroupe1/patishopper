<main id="product" class="container-fluid">
    <div class="row">
        <div class="title col-10">
            <h1>Produits</h1>
        </div>
        <div class="col-2" id="button">
            <a href="<?php echo DIRNAME; ?>admin/product/add" class="button right">Ajouter</a>
        </div>
    </div>

    <hr class="separator">

     <form method="POST" id="buttons-products-actions">
        <?php if(!empty($config['rows'])):?>
            <?php $this->addModal("product-selection-pan", $infos); ?>
        <?php endif; ?>

        <div class="row">
            <div class="col-12">
                <?php $this->addModal("table", $config); ?>
            </div>
        </div>
    </form>

</main>
