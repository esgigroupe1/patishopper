<?php

class Contact extends BaseSQL {
    protected $id = null;
    protected $entity;
    protected $name;
    protected $email;
    protected $message;
    protected $status;

    public function __construct(){
		parent::__construct();
	}

	public function setId($id){
		$this->id = $id;
  }
  
  public static function configFormAdd()
  {
      return [

          "config" => [
              "method" => "POST",
              "action" => "",
              "submit" => "Envoyer",
              "class" => "button",
          ],

          "input" => [

            "who" => [
              "name" => "profession",
              "type" => "text",
              "placeholder" => "Votre profession*",
              "required" => "required",
              "minString" => 2,
              "maxString" => 100,
              ],

              "firstname" => [
                  "name" => "firstname",
                  "type" => "text",
                  "placeholder" => "Votre prénom*",
                  "required" => "required",
                  "minString" => 2,
                  "maxString" => 100,
                  "required" => "required"
              ],

              "lastname" => [
                  "name" => "lastname",
                  "type" => "text",
                  "placeholder" => "Votre nom*",
                  "required" => "required",
                  "minString" => 2,
                  "maxString" => 100,
                  "required" => "required"
              ],

              "email" => [
                  "name" => "email",
                  "type" => "email",
                  "placeholder" => "Votre email*",
                  "required" => "required",
              ],

          ],

          "textarea" => [
                  "message" => [
                    "name" => "message",
                    "rows" => "5" ,
                    "cols" => "65",
                    "placeholder" => "Votre message*",
                    "required" => "required"
                ]
          ]
      ];
  }
}