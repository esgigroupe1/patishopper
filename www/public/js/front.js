(function ($) {
    $(document).ready(function () {
        /**
         * Fade Out Error Message
         */

        $(".exit").on('click', function (e) {
            e.preventDefa.slidet();
            $(this).parent().fadeOut("slow");
        });

        // Begin Code
        setInterval(function () {
            moveRight();
        }, 6000);


        var slideCount = $('#home-slider .slide .slide-element').length;
        var slideWidth = $('#home-slider .slide .slide-element').width();
        var slideHeight = $('#home-slider .slide .slide-element').height();
        var sliderUlWidth = slideCount * slideWidth;

        $('#home-slider').css({
            width: slideWidth,
            height: slideHeight
        });

        $('#home-slider .slide').css({
            width: sliderUlWidth,
            marginLeft: -slideWidth
        });

        $('#home-slider .slide .slide-element:last-child').prependTo('#home-slider .slide');

        function moveLeft() {
            $('#home-slider .slide').animate({
                left: +slideWidth
            }, 200, function () {
                $('#home-slider .slide .slide-element:last-child').prependTo('#home-slider .slide');
                $('#home-slider .slide').css('left', '');
            });
        };

        function moveRight() {
            $('#home-slider .slide').animate({
                left: -slideWidth
            }, 200, function () {
                $('#home-slider .slide .slide-element:first-child').appendTo('#home-slider .slide');
                $('#home-slider .slide').css('left', '');
            });
        };

        $('a.control_prev').click(function (e) {
            e.preventDefault();
            moveLeft();
        });

        $('a.control_next').click(function (e) {
            e.preventDefault();
            moveRight();
        });
        // End Code

        $('#newsletter').submit(function (e) {
            e.preventDefault();
            var request;
            // Abort any pending request
            if (request) {
                request.abort();
            }

            var ajax_form_data = $(this).serialize();
            var message_info = $("#message-infos span");
            var message_container = $('#message-infos');

            ajax_form_data += '&ajaxrequest=' + random32bit();

            request = $.ajax({
                url: "/newsletter/addme",
                type: 'POST',
                data: ajax_form_data,
                encode: true,
                beforeSend: function () {
                    $('#newsletter #loader-perform').show();
                },
                complete: function () {
                    $('#newsletter #loader-perform').hide();
                    message_container.fadeTo("slow", 1, function () {
                        // Animation complete.
                    });
                }
            })

            request.done(function (response) {
                var response = JSON.parse(response);
                message_info.show().text(response.message);
                setTimeout(function () {
                    message_container.fadeTo("slow", 0, function () {
                        // Animation complete.
                    });
                }, 2000);

                // next actions
            })

            // something went wrong
            request.fail(function () {
                console.log("Fail");
            })

            // after all this time ?
            request.always(function () {
                // event.target.reset();
            });
        });
    });

})(jQuery);
