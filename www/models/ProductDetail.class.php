<?php

class ProductDetail extends BaseSQL {
	protected $id = null;
	protected $price;
	protected $date;
	protected $product_id;

    public function __construct(){
		parent::__construct();
	}

	public function setId($id){
		$this->id = $id;
	}
}