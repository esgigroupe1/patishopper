<?php
session_start();
require "conf.inc.php";
spl_autoload_register(function ($class) {
    if (file_exists(SITE_DIR . "core/" . $class . ".class.php")) {
        include (SITE_DIR . "core/" . $class . ".class.php");
    } else if (file_exists(SITE_DIR . "models/" . $class . ".class.php")) {
        include (SITE_DIR . "models/" . $class . ".class.php");
    } else if (file_exists(SITE_DIR . "controllers/" . $class . ".class.php")) {
        include (SITE_DIR . "controllers/" . $class . ".class.php");
    }
});

date_default_timezone_set('Europe/Paris');

// /3IW%20classe%201/user/add?id=2
/*
/
/user/add
 */

/*
/3IW classe 1
/3IW%20classe%201/user/add
 */

//   user/add

$uri = substr(urldecode($_SERVER["REQUEST_URI"]), strlen(dirname($_SERVER["SCRIPT_NAME"])));

/*
/
user/add
 */

/*
/3IW classe 1
/user/add
 */

$uri = ltrim($uri, "/");

/*
/
user/add
 */

/*
/3IW classe 1i
user/add
 */

// /user/add?id=2

$uri = explode("?", $uri);
// user/add
$uriExploded = explode("/", $uri[0]);

//Utiliser des conditions ternaires pour mettre la chaine
//"index" si la clÃ© n'existe pas :
$c = (empty($uriExploded[0])) ? "index" : $uriExploded[0];
$a = (empty($uriExploded[1])) ? "index" : $uriExploded[1];

//Controller : NomController
$c = ucfirst(strtolower($c)) . "Controller";
//Action : nomAction
$a = strtolower($a) . "Action";

// user/modify/12/name/skrzypczyk
// $uriExploded[0]=>user
// $uriExploded[1]=>modify
// $uriExploded[2]=>12
// ...

unset($uriExploded[0]);
unset($uriExploded[1]);
// user/modify/12/name/skrzypczyk
// $uriExploded[2]=>12
// ...

$uriExploded = array_values($uriExploded);
// user/modify/12/name/skrzypczyk
// $uriExploded[0]=>12
// ...

$params = [
    "POST" => $_POST,
    "GET" => $_GET,
    "URL" => $uriExploded,
];

include "plugins/token.pgs.php"; // check if token exist in bdd for this user
include "plugins/session.pgs.php"; // session timer for users
//echo '<pre>', print_r($params, true), '</pre>';

if (file_exists("controllers/" . $c . ".class.php")) {

    include "controllers/" . $c . ".class.php";

    if (class_exists($c)) {

        $objC = new $c();

        if (method_exists($objC, $a)) {
            $objC->$a($params);
        } else {
            RequestController::render($params);
            die();
            // die("L'action " . $a . " n'existe pas");
        }
    } else {
        RequestController::render($params);
        die();
        // ("Le controller " . $c . " n'existe pas");
    }
} else {
    RequestController::render($params);
    die();
}
