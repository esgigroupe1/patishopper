(function ($) {
    $(document).ready(function () {
        function checkboxTable() {
            var $all = $('input[name="all"]');
            var $item = $('tbody input[type="checkbox"]');
            var run = function () {
                $item.prop('checked', $(this).is(':checked'));
            }
            // All
            $all.click(run);

            // uncheck all button when uncheck all and vice-versa
            $item.click(function () {
                var input = 'input.checkboxItem';
                // console.log($(input).length);
                // console.log($(input + ':checked').length);

                if ($(input + ':checked').length == $(input).length) {
                    $all.prop('checked', 'checked');
                } else {
                    $all.prop('checked', '');
                }
            });
        }

        function listenFocus() {
            // admin/page/add or admin/article/add
            var inputField = $('input[type=text], textarea');
            if (inputField.val()) {
                inputField.parent().addClass("up");
            }
            inputField.focus(function () {
                $(this).parent().addClass("up");
            }).blur(function () {
                if (!$(this).val()) {
                    $(this).parent().removeClass("up");
                }
            })
        }

        listenFocus();
        checkboxTable();
    });

})(jQuery);
