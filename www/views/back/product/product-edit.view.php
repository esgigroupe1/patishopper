<main id="product-edit" class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h1><?php echo $title; ?></h1>
        </div>
    </div>

    <hr class="separator">

    <?php $form = Form::get($config); ?>
    <?php echo $form['open']; ?>
    <div class="row">
        <div class="col-10">
            <div class="row">
                <div class="col-12">
                    <?php if(!empty($success)): ?>
                        <div class="message success">
                            <span><?php echo $success; ?></span>
                            <a href="#" class="exit" class="fade_exit">&times;</a>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-12 group">
                    <?php echo $form['name']; ?>

                    <?php if(!empty($errors['name'])): ?>
                        <div class="message error">
                            <span><?php echo $errors['name']; ?></span>
                            <a href="#" class="exit" class="fade_exit">&times;</a>
                        </div>
                    <?php endif; ?>
                </div><!-- end col-12 -->

                <div class="col-12 group">
                    <?php  echo $form['slug']; ?>
                </div> <!-- end col-12 -->

                <div id="form-content" class="col-12 group">
                    <?php  echo $form['price']; ?>

                    <?php if(!empty($errors['price'])): ?>
                        <div class="message error">
                            <span><?php echo $errors['price']; ?></span>
                            <a href="#" class="exit" class="fade_exit">&times;</a>
                        </div>
                    <?php endif; ?>
                </div> <!-- end col-12 -->

                <div id="form-content" class="col-12 group">
                    <?php  echo $form['stock']; ?>

                    <?php if(!empty($errors['stock'])): ?>
                        <div class="message error">
                            <span><?php echo $errors['stock']; ?></span>
                            <a href="#" class="exit" class="fade_exit">&times;</a>
                        </div>
                    <?php endif; ?>
                </div> <!-- end col-12 -->

                <div id="form-content" class="col-12 group">
                    <?php  echo $form['product-content']; ?>
                </div> <!-- end col-12 -->
            </div> <!-- end row -->
        </div> <!-- end col-8 -->
        <div class="col-2">
            <div class="wrapper">

                <div id="button-actions">
                    <?php if(!empty($product->slug)) : ?>
                        <a class="button" target="_blank" href="<?php echo DIRNAME . $product->slug ?>">Afficher</a>
                    <?php else: ?>
                        <?php if(!empty($product->id)): ?>
                            <div class="message info">
                                <span>Ce produit n'a pas encore de slug</span>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if(isset($product->trash)) : ?>
                        <?php if($product->trash == 1) : ?>
                            <input class="button" name="restore" type="submit" value="Restaurer">
                        <?php else: ?>
                            <input class="button" name="trash" type="submit" value="Mettre à la corbeille">
                        <?php endif;?>
                    <?php endif; ?>

                    <?php  echo $form['submit']; ?>
                </div>

                <div id="categories-choice">
                    <h3>Choix des catégories</h3>
                    <div class="list">
                        <?php echo $form['checkboxes']; ?>
                    </div>
                </div> <!-- end categories-choice -->

               <div id="featured-image">
                    <h3>Image à la une</h3>
                    <?php echo $form['featured-image']; ?>
                    <div class="image-preview">
                        <?php if (!empty($product->id)): ?>
                            <?php $media = $product->getAttachment(); ?>
                        <?php endif;?>
                        <?php if(!empty($media)): ?>
                            <img id="medias" class="image" src="<?php echo $media->url ?>" title="<?php echo $media->name; ?>">
                        <?php else: ?>
                            <a id="medias" href="#">Sélectionner une image</a>
                        <?php endif; ?>
                    </div> <!-- end image-preview -->
                </div> <!-- end featured-image -->
            </div> <!-- end wrapper -->
        </div> <!-- col-4 -->
    </div><!-- end row -->
    <?php echo $form['close']; ?>
    <?php  $this->addModal("media", $modal); ?>
</main>
