<main id="page-edit" class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h1><?php echo $title; ?></h1>
        </div>
    </div>

    <hr class="separator">

    <?php $form = Form::get($config); // Helper::dump($form); ?>
    <?php echo $form['open']; ?>
    <div class="row">
        <div class="col-10">
            <div class="row">
                <div class="col-12">
                    <?php if(!empty($success)): ?>
                        <div class="message success">
                            <span><?php echo $success; ?></span>
                            <a href="#" class="exit" class="fade_exit">&times;</a>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-12 group">
                    <?php  echo $form['title']; ?>
                </div><!-- end col-12 -->

                <div class="col-12 group">
                    <?php  echo $form['slug']; ?>
                </div> <!-- end col-12 -->

                <div id="form-content" class="col-12 group">
                    <?php  echo $form['post-content']; ?>
                </div> <!-- end col-12 -->
            </div> <!-- end row -->
        </div> <!-- end col-8 -->
        <div class="col-2">
            <div class="wrapper">
                <div id="button-actions">
                    <?php if(!empty($post->slug)) : ?>
                        <a class="button" target="_blank" href="<?php echo DIRNAME . $post->slug ?>">Afficher</a>
                    <?php else: ?>
                        <?php if(!empty($post->id)): ?>
                            <div class="message info">
                                <span>Cette page n'a pas encore de slug</span>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if(isset($post->status)) : ?>
                        <?php if($post->status == 0) : ?>
                            <input class="button" name="publish" type="submit" value="Publier">
                        <?php else: ?>
                            <input class="button" name="draft" type="submit" value="Mettre au brouillon">
                        <?php endif;?>
                    <?php endif; ?>

                    <?php if(isset($post->trash)) : ?>
                        <?php if($post->trash == 1) : ?>
                            <input class="button" name="restore" type="submit" value="Restaurer">
                        <?php else: ?>
                            <input class="button" name="trash" type="submit" value="Mettre à la corbeille">
                        <?php endif;?>
                    <?php endif; ?>

                    <?php  echo $form['submit']; ?>
                </div>
            </div>
        </div> <!-- col-4 -->
    </div><!-- end row -->
    <?php echo $form['close']; ?>
</main>
