<?php
$time = $_SERVER['REQUEST_TIME'];

// duration in sec : 1800 = 30 min
$timeout_duration = 1800;


if (isset($_SESSION['LAST_ACTIVITY']) &&
   ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
    session_unset();
    session_destroy();
    header("Location:".DIRNAME);
}

$_SESSION['LAST_ACTIVITY'] = $time;
