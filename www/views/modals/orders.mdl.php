<?php $orders = $config['orders']; ?>
<div class="container-fluid inside-content" id="orders">
    <div class="row">
        <div class="col-12 title">
            <h1>Mes commandes</h1>
        </div>
    </div> <!-- end row -->

    <div class="row">
        <div class="col-12">
            <?php if(!empty($orders)): ?>
            <table id="orders">
                <thead>
                    <tr>
                        <th>Numéro de commande</th>
                        <th>Date de la commande</th>
                        <th>Etat</th>
                        <th>Détails</th>
                    </tr>
                </thead>
            <?php foreach($orders as $order): ?>

                    <?php
                        $date = new DateTime($order->created_at);
                        $date = $date->format("d-m-Y") . " à " . $date->format("H:i:s");
                    ?>
                    <tbody>
                        <td><?php echo $order->id ?></td>
                        <td><?php echo $date; ?></td>
                        <td><?php echo "Commandé le " . ($order->status == 1) ? "Livré" : "Non livré" ?></td>
                        <td>
                            <a title="Voir les détails" class="see-more" href="<?php echo DIRNAME;?>account/orders/<?php echo $order->id; ?>"></a>
                        </td>
                    </tbody>

            <?php endforeach; ?>
            </table>
            <?php endif; ?>
        </div>
    </div>
</div>
