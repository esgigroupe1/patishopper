<main id="single-product" class="container product-<?php echo $data->id; ?>">
    <div class="row">
        <div class="col-4 col-lg-6 col-sm-12">
            <div class="title-image">
                <?php $media = $data->getAttachment();?>
                <h1><?php echo $data->name ?></h1>
                <?php if (!empty($media)): ?>
                    <img class="featured-image" src="<?php echo $media->url ?>" title="<?php echo $media->name; ?>">
                <?php endif;?>
            </div> <!-- end title-image -->
        </div> <!-- end col-4 -->

        <div class="col-5 col-lg-6 col-sm-6 col-xs-12">
            <div class="description">
                <h2>Description du produit</h2>
                <?php echo $data->description ?>
            </div> <!-- end description -->
        </div> <!-- end col-4 -->

        <div class="col-3 col-lg-6 cart col-lg-12 col-sm-6 col-xs-12">
            <div id="add-to-cart">
            <?php if (Helper::isAllowedToAdmin()): ?>
                <p><a class="button" href="<?php echo DIRNAME; ?>admin/product/edit/<?php echo $data->id; ?>">Modifier ce produit</a></p>
            <?php endif;?>
                <div class="price">
                    <div>Prix</div>
                    <span><?php echo $data->price . " €"; ?></span>
                </div> <!-- end price -->

                <div class="quantity quantity-wrapper">
                    <div>Quantité</div>
                    <div class="quantity-details">
                        <span class="quantity-value">1</span>
                    </div>
                    <div class="quantity-controls">
                        <a class="decrease" title="Diminuer" href="#descrease"></a>
                        <a class="inscrease" title="Augmenter" href="#inscrease"></a>
                    </div>
                </div> <!-- end quantity-wrapper -->

                <p id="add-to-cart-wrapper">
                    <a id="button-add-to-cart" data-product-id="<?php echo $data->id; ?>" class="button center" href="#add-to-cart">Ajouter au panier</a>
                    <span id="img-wrapper">
                        <img id="loader-perform" src="<?php echo DIRNAME; ?>public/images/icons/loader.gif" alt="Ajout du produit au panier" title="Ajout du produit au panier">
                    </span>
                </p>
            </div> <!-- end add-to-cart -->
        </div> <!-- end col-4 -->
    </div> <!-- end row -->

    <div class="row" id="cart-info">
        <div class="col-12 col-xs-12-col-12">
            <div class="message success">
                <span></span>
            </div>
        </div>
    </div>

    <?php $form = Form::get($commentForm);?>
    <?php
        $comments = $data->getComments();
        Helper::sortByLastUpdated($comments);
    ?>

    <div class="row">
        <div class="col-12">
            <h2>Tous les avis sur ce produit</h2>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-6 col-lg-6 comments col-sm-12 col-xs-12">
                        <div class="lasts-3">
                            <?php $hasShow = 0;?>
                            <?php if (!empty($comments)): ?>
                                <?php foreach ($comments as $comment): ?>
                                    <?php if ($comment->status == 1 || (isset($_SESSION['id_user']) && $comment->user_id == $_SESSION['id_user'])): ?>
                                    <?php $hasShow = 1;?>
                                    <div class="single-comment">
                                        <?php
                                            $date = new DateTime($comment->created_at);
                                            $date = $date->format("d-m-Y") . " à " . $date->format("H:i:s");
                                        ?>
                                        <div class="comment-date">
                                            <div>Laissé par <span class="user"><?php echo $comment->getUser()->firstname; ?></span> le <span class="date"><?php echo $date ?></span></div>
                                        </div>
                                        <p class="comment-text"><?php echo Helper::xss($comment->content); ?></p>
                                    </div>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>

                            <?php if ($hasShow == 0): ?>
                                <div id="message-infos" class="message info">
                                    <span>Aucun avis n'a encore été laissé ou approuvé sur ce produit</span>
                                </div>
                            <?php else: ?>
                                <div class="see-all"><span></span></div>
                            <?php endif;?>
                        </div> <!-- end last-3 -->
                    </div>

                    <!-- end col-6 -->
                    <div class="col-6 col-lg-6 post-comment col-sm-12 col-xs-12">
                        <div class="wrapper">
                            <?php if (empty($_SESSION['id_user'])): ?>
                            <div class="connexion-required">
                                <span>Connectez-vous pour laisser un avis sur ce produit en cliquant</span>
                                <a href="<?php echo DIRNAME; ?>authentication">ici</a>
                            </div>
                            <?php else: ?>

                            <div id="message-info" class="message info">
                                <span>Les avis publiés seront visibles par tous après validation par nos équipes</span>
                            </div>
                            <?php echo $form['open'] ?>
                                <?php echo $form['comment-content'] ?>
                                <input type="hidden" name="product_id" value="<?php echo $data->id ?>" />
                                <div class="submit">
                                    <?php echo $form['submit'] ?>
                                    <img id="loader-perform" src="<?php echo DIRNAME; ?>public/images/icons/loader.gif" alt="Enregistrement de votre avis" title="Enregistrement de votre avis">
                                </div>
                            <?php echo $form['close'] ?>
                            <?php endif;?>
                        </div> <!-- end wrapper -->
                    </div><!-- end col-6 -->
                </div> <!-- end row -->
            </div> <!-- end container-fluid -->
        </div> <!-- end col-12 -->
    </div> <!-- end row -->

    <?php $categories = $data->getCategories();?>
    <?php if (!empty($categories)): ?>

    <?php
    $i = rand(0, (count($categories) - 1));
    $rr = $categories[$i]->getProducts();
    $searchedValue = $data->id;
    $related_products = array_filter(
        $rr,
        function ($e) use (&$searchedValue) {
            return $e->id != $searchedValue;
        }
    );
    ?>
    <div class="row">
        <div class="col-12 categories-group">
            <div class="container-fluid">
                <div class="row">
                    <?php if (!empty($related_products)): ?>
                    <div class="col-4 col-sm-12 col-xs-12">
                        <h3>D'autres produits de la même catégorie</h3>
                    </div> <!-- end col-8 -->
                    <?php endif;?>

                    <div class="col-8 col-sm-12 col-xs-12">
                        <div class="categories">
                            <?php foreach ($categories as $category): ?>
                                <a href="<?php echo DIRNAME . 'produits/' . $category->slug; ?>"><?php echo $category->name; ?></a>
                            <?php endforeach;?>
                        </div> <!-- end categories -->
                    </div> <!-- end col-8 -->
                </div> <!-- end row -->
            </div> <!-- end container-fluid -->
        </div> <!-- end col-12 -->

        <?php if (!empty($related_products)): ?>
        <div class="col-12">
            <main id="products-wrapper" class="same-category-products">
                <div class="container no-collapse">
                    <div id="all-products" class="row no-collapse product-in-same-category">
                        <?php foreach ($related_products as $related_product): ?>
                        <div class="col-4 col-lg-6 col-sm-12 product">
                            <a href="<?php echo $related_product->slug; ?>" class="image">
                                <?php $media = $related_product->getAttachment();?>
                                <?php if (!empty($media)): ?>
                                    <img class="featured-image" src="<?php echo $media->url ?>" title="<?php echo $media->name; ?>">
                                <?php endif;?>
                            </a> <!-- end link -->
                            <h3><?php echo $related_product->name; ?></h3>
                            <p><?php echo $related_product->price; ?>€</p>
                        </div>
                        <?php endforeach;?>
                    </div> <!-- put in loop -->
                </div> <!-- end cotainer -->
            </main> <!-- end same-category-products -->
        </div> <!-- end col-12 -->
        <?php endif;?>
    </div> <!-- end row -->
    <?php endif;?>
</main>
