<div class="row selects">
    <div class="col-12">
        <div id="lists">
            <a id="view-all" href="/admin/<?php echo $config['type'];?>/category">Tout(<span><?php echo $config['published']; ?></span>)</a>
        </div>

        <div id="actions">
            <div class="wrapper-select">
                <select name="action-perform" id="perform">
                    <option value="group" selected>Opérations groupées</option>
                    <option value="delete">Supprimer définitivement</option>
                </select>
            </div>
            <input class="button" type="submit" data-type="<?php echo $config['type'];?>" value="Appliquer">

            <img id="loader-perform" src="<?php echo DIRNAME; ?>public/images/icons/loader.gif" alt="Application des modifications" title="Application des modifications">

            <div id="message-info" class="message info">
                <span></span>
                <a href="#" class="exit" class="fade_exit">&times;</a>
            </div>
        </div>
    </div><!-- end col-12 -->
</div><!-- end row selects -->
