<main id="products-wrapper" class="container">
    <h1>Produits</h1>
    <nav id="categories">
        <ul>
            <li><a href="<?php echo DIRNAME .'produits'; ?>">Tous les produits</a></li>
             <?php foreach ($categories as $category) : ?>
                <li><a href="<?php echo DIRNAME . 'produits/' . $category->slug; ?>"><?php echo $category->name; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </nav>
    <div class="container no-collapse">
        <div id="all-products" class="row no-collapse">
        <?php foreach($products as $product): ?>
            <div class="col-4 col-lg-6 col-sm-12 product">
                <a href="<?php echo DIRNAME . $product->slug; ?>" class="image">
                    <?php $media = $product->getAttachment(); ?>
                    <?php if (!empty($media)): ?>
                        <img class="featured-image" src="<?php echo $media->url ?>" title="<?php echo $media->name; ?>">
                    <?php endif;?>
                </a> <!-- end link -->
                <h3><?php echo $product->name; ?></h3>
                <p><?php echo $product->price; ?>€</p>
            </div>
        <?php endforeach; ?>
        </div> <!-- all-products -->
    </div> <!-- end container -->
</main>
