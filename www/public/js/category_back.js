(function ($) {
    $(document).ready(function () {
        // Perform action on link inside line
        $('a[class^="category-"]').on('click', function (e) {
            e.preventDefault();
            var data_to_send = "";
            var urlTo = $(this).attr('href');
            var idTo = $(this).data('id');
            var randomValue = random32bit();
            var line = $(this).closest("tr");

            // alert(urlTo + '/' + idTo);
            $.ajax({
                    url: urlTo + '/' + idTo,
                    type: 'POST',
                    data: {
                        id: idTo,
                        ajaxrequest: randomValue
                    },
                    encode: true,
                })

                .done(function (response) {
                    line.fadeOut("slow");
                    if (response) {
                        var data = JSON.parse(response);
                        $("#view-all span").fadeOut(function () {
                            $(this).text(data.published_categories).fadeIn();
                        });
                    }
                })

                // something went wrong
                .fail(function () {
                    console.log("Fail");
                })

                // after all this time ?
                .always(function () {
                    // event.target.reset();
                });
        });


        // Perform action from actionS apply button
        $('#buttons-category-actions').submit(function (e) {
            e.preventDefault();
            var ajax_form_data = $(this).serialize();
            var message_info = $("#message-info span");
            var post_type = $('input[type="submit"]').data('type');

            ajax_form_data += '&ajaxrequest=' + random32bit();

            $.ajax({
                    url: "/admin/" + post_type + "/category/selection-pan",
                    type: 'POST',
                    data: ajax_form_data,
                    encode: true,
                    beforeSend: function () {
                        $('#loader-perform').show();
                        message_info.hide().text("");
                    },
                    complete: function () {
                        $('#loader-perform').hide();
                    }
                })

                .done(function (response) {
                    var response = JSON.parse(response);
                    message_info.show().text(response.message);
                    setTimeout(function () {
                        message_info.fadeOut();
                    }, 2000);

                    if (response.data) {
                        var data = response.data;

                        $("#view-all span").fadeOut(function () {
                            $(this).text(data.published_categories).fadeIn();
                        });

                        // update table line
                        $('input.checkboxItem:checked').closest('tr').fadeOut("slow");
                    }
                })

                // something went wrong
                .fail(function () {
                    console.log("Fail");
                })

                // after all this time ?
                .always(function () {
                    // event.target.reset();
                });
        });
    });

})(jQuery);
