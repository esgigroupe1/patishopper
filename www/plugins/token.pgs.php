<?php
    if ($c == "AdminController") {
        if(!isset($_SESSION['user_type_id']) || $_SESSION['user_type_id'] >= 3){
            Controller::errors(404);
            exit();
        }
    }
	if (!empty($_SESSION['token']) && !empty($_SESSION['id_user'])) {
		$token = User::select(["token" => $_SESSION['token'], "id" => $_SESSION['id_user']]);

		if(empty($token)) {
			session_destroy();
            header("Location:".DIRNAME."authentication");
            die();
        }
    }
