<?php
class Validator
{

    public static function validate($form, $params)
    {
        $errorsMsg = [];

        foreach ($form["input"] as $name => $config) {
            if (isset($config["confirm"]) && isset($params[$name]) && isset($params[$config["confirm"]])) {
                if (isset($config["confirm"]) && $params[$name] !== $params[$config["confirm"]]) {
                    $errorsMsg[$name] = "Les champs ne sont pas identiques";
                } else if (!isset($config["confirm"])) {
                    if ($config["type"] == "email" && !self::checkEmail($params[$name])) {
                        $errorsMsg[$name] = "Email invalide";

                    } else if ($config["type"] == "password" && !self::checkPwd($params[$name])) {
                        $errorsMsg[$name] = "6-30 caractères (min/maj/nb)";

                    } else if ($config["type"] == "tel" && !self::checkPhone($params[$name])) {
                        $errorsMsg[$name] = "Numéro invalide";

                    } else if ($config["type"] == "text") {
                        if (isset($config["name"])) {
                            if ($config["name"] == "captcha" && self::checkCaptcha($params[$name])) {
                                $errorsMsg[$name] = "Captcha invalide";
                            } elseif ($config["name"] == "postal_code" && !self::checkPostalCode($params[$name])) {
                                $errorsMsg[$name] = "Code postal invalide";
                            }
                        }
                    }
                }
            }

            if(isset($params[$name])) {
                if (isset($config["required"]) && !self::minLength($params[$name], 1)) {
                    $errorsMsg[$name] = "2 caractères minimum";
                }

                if (isset($config["minString"]) && !self::minLength($params[$name], $config["minString"])) {
                    $errorsMsg[$name] = $config["minString"] . " caractères minimum";
                }

                if (isset($config["maxString"]) && !self::maxLength($params[$name], $config["maxString"])) {
                    $errorsMsg[$name] = $config["maxString"] . " caractères maximum";
                }

                if (isset($config["maxString"]) && !self::maxLength($params[$name], $config["maxString"])) {
                    $errorsMsg[] = $name . " doit faire moins de " . $config["maxString"] . " caractères";
                }
            }

        }

        return $errorsMsg;
    }

    public static function checkPostalCode($postal_code)
    {
        return preg_match(" /^[0-9]{5,5}$/ ", $postal_code);
    }

    public static function checkCaptcha($captcha)
    {
        return strcmp(strtolower($captcha), strtolower($_SESSION['captcha']));
    }

    public static function checkEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function checkPhone($phone)
    {
        return preg_match("/(0|\+33)[1-9]([-. ]?[0-9]{2}){4}/", $phone);
    }

    public static function checkPwd($pwd)
    {
        return strlen($pwd) > 6 && strlen($pwd) < 30 && preg_match("/[A-Z]/", $pwd) && preg_match("/[a-z]/", $pwd) && preg_match("/[0-9]/", $pwd);
    }

    public static function minLength($value, $length)
    {
        return strlen(trim($value)) >= $length;
    }
    public static function maxLength($value, $length)
    {
        return strlen(trim($value)) <= $length;
    }

}
