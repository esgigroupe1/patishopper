# CMS Patishopper
Le CMS patishopper est un esolution permettant aux patisseries ou à tout autres entreprises dans le même domaine de pouvoir
créer ou faire évoluer leur site web. 

Le but du CMS permet l'amélioration et la facilitation des différents processus de gestions, de ventes et de communications avec les clients.

## Getting Started
Deux pré-requis sont indispensables pour la création du site:
````
Avoir une base de données
Connexion Internet
````
### Configuration
Base de données
```
Le créateur doit donner certaines informations lors de la configuration:
- Le nom de la base de données 
- Le hôte de la base de données
- Le port de la base de données
- Le driver de la base de données
- Les identifiants de connexion à la base de données (Nom et mot de passe Utilisateurs)
```
Utilisateurs principal
```
- Son adresse mail personnel pour la création de l'administrateur
- Son mot de passe
```
## Déploiement

Pour que votre site soit en ligne, vous devez posséder:
- Un nom de domaine
- Un serveur ou un hébergeur Web
- Un serveur de base de données

### Cas Spécifiques

Ils existent des cas spécifiques dans le CMS:
- Il y a toujours au moins un administrateur dans les utilisateurs
- Un administrateur peut donner le droit d'administrateur à d'autres utilisateurs
- Un administrateur a le droit de rétrograder tous autres utilisateurs, faites attention aux personnes à qui vous donner les droits d'utilisateurs

## Remerciement

Je remercie toute l'équipe de Patishopper pour le développement du CMS;

Membres:
- Jérémy CASTELAIN
- Kaba CONDE
- Jonathan RAKOTONIRINA
- Adam Malick SOW

## Contribution

Nous vous remercions d'avoir utilisé notre CMS, profitez bien lors de la création de votre site Web.

## Contact
Pour toutes questions ou d'éventuelles suggestions d'amélioration de notre CMS,
vous pouvez nous contacter à l'adresse mail suivante: contact.patishopper@gmail.com.