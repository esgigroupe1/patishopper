<?php

class UserType extends BaseSQL {
    protected $id = null;
    protected $role;

    public function __construct(){
		parent::__construct();
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getRole(){
		return $this->role;
	}
}