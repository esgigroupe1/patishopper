<main id="article" class="container-fluid">
    <div class="row">
        <div class="title col-10">
            <h1>Articles</h1>
        </div>
        <div class="col-2" id="button">
            <a href="<?php echo DIRNAME; ?>admin/article/add" class="button right">Ajouter</a>
        </div>
    </div>

    <hr class="separator">

     <form method="POST" id="buttons-posts-actions">
        <?php if(!empty($config['rows'])):?>
            <?php $this->addModal("post-selection-pan", $infos); ?>
        <?php endif; ?>

        <div class="row">
            <div class="col-12">
                <?php $this->addModal("table", $config); ?>
            </div>
        </div>
    </form>

</main>
