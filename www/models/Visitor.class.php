<?php

class Visitor extends BaseSQL
{
    protected $id = null;
    protected $ip;
    protected $time;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    public function setTime($time)
    {
        $this->time = $time;
    }

    public function addVisitor($values) {
        $pdo = $this->getPdo();
        $data = [$values[0], $values[1]];
        
        $visitor = Visitor::select(["ip" => $values[1]]);
        
        if(!empty($visitor))
        {
            //update time for the visitor
            $update_visitor = "UPDATE visitor SET time = ? WHERE ip = ?";
            $query = $pdo->prepare($update_visitor);
            $query->execute($data);
        }
        else 
        {
            //insert the visitor
            $new_visitor = "INSERT INTO visitor(time,ip) VALUES(?,?)";
            $query = $pdo->prepare($new_visitor);
            $query->execute($data);
        }

    }
}