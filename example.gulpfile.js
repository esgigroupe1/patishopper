var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

/*
:nested
:compact
:expanded
:compressed
*/
gulp.task('sass', function () {
    gulp.src('./www/public/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./www/public/css/'));

    gulp.src('./www/public/scss/sitemap.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        //.pipe(sourcemaps.write('./www/'))
        .pipe(gulp.dest('./www/public/css/'));
});
gulp.task('browser-sync', function () {
    browserSync.init({
        injectChanges: true,
        open: 'local',
        host: 'localhost',
        proxy: 'http://localhost:3000/admin',
        port: '4000'
    });
});


gulp.task('build', ['sass'], function () {});

gulp.task('default', ['sass', 'browser-sync'], function () {
    // files to reload on changes
    var files = [
        './www/style.css',
        './www/*',
        './www/public/js/*.js',
        './www/controllers/**/*.php',
        './www/core/**/*.php',
        './www/views/**/*.php'
    ];

    gulp.watch('./www/public/scss/**/*.scss', ['sass']).on('change', browserSync.reload);
    gulp.watch(files).on('change', browserSync.reload);;
});
