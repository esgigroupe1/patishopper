<?php $message = $this->getAssign();?>

<main class="container" id="contact">
    <h1> Nous contacter</h1>
    <div class="container no-collapse">
        <div class="col-12">
        <?php if (!empty($message['mail_success'])):?>
            <div class="message success">
                <?php echo $message['mail_success']; ?>
            </div>
        <? elseif (!empty($message['mail_error'])): ?>
            <div class="message error">
                <?php echo $message['mail_error']; ?>
            </div>
        <?php endif ?>
        <div class="vertical-bloc">
                                <div class="column">
                                    <div class="col-4">
                                        <h2>Des questions ?</h2>
                                        <p>Les questions fréquemment posées par nos collaborateurs et nos utilisateurs  ainsi que leurs réponses sont répertoriées dans notre foire aux questions. 
                                            Les réponses à vos questions y sont peut-être présentes; nous vous invitons à la consulter en cliquant ici :
                                        </p>
                                        <p><a href="#" class="button">FAQ</a></p>
                                    </div>
                                    <div class="col-4">
                                        <h2>Localisation de l'entreprise </h2>
                                        <div class="iframe">
                                            <iframe width="500" height="350" frameborder="0" style="border:0"
                                                src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJZ13iR3Ny5kcR94QVE8EiPsc&key=AIzaSyCETExtrEEtUHKOSlezEGiOEtmgH8-r9E0" allowfullscreen>
                                            </iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="col-6">
                                        <h2>Laissez-nous un message </h2>
                                        <?php $this->addModal("form", $config, $errors);?>
                                    </div>
                                </div>
                        </div>
        </div>
    </div>

</main>
