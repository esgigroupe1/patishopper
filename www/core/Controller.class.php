<?php

abstract class Controller
{
    /**
     * Handle all types of errors and redirect to the right View with  eventual data
     *
     * @param integer $errorType error code to intercept
     * @return void
     */
    public static function errors($errorType)
    {
        // verify error type to show the right templates 404, 500, ...
        $v = new View("error", "front");
        switch ($errorType) {
            case '401':{
                    $v->assign("errorStatus", "401");
                    $v->assign("errorDescription", "Identifiant incorrect");
                    break;
                }
            case '404':{
                    header('HTTP/1.0 404 Not Found', true, 404);
                    $v->assign('errorStatus', '404');

                    $v->assign('errorDescription', 'Oups... ça sent le pâté ! Une erreur est survenue.');
                    $v->assign('errorMessage', 'Désolé, mais cette page n\'existe pas');

                    $products = Product::findAll();

                    Helper::sortByLastUpdated($products);
                    $products = array_slice($products, 0, 6);

                    $v->assign('products', $products);
                    die();
                    break;
                }
            default:{
                    $v->assign("errorStatus", "404");
                    $v->assign("errorDescription", "Cette page n'existe page");
                    break;
                }
        }

        die();
    }

    /**
     * Retrive posts form config with data
     *
     * @param integer $postType whether article or page
     * @param Post $post
     * @return Array posts config with data or empty array
     */
    private function getPostForm($postType, $post = null)
    {
        if ($postType == 1) {
            return Post::configFormContent($post);
        } elseif ($postType == 2) {
            return Post::configFormPage($post);
        }
        return []; // if we get here nothing no data
    }

    /**
     * Associate categories to post if they exists
     *
     * @param Object $object Post or Product to which categories will be associated
     * @param Array $params urls POST data
     * @return void
     */
    protected function addCategories(&$object, $params)
    {
        // get all categories in post
        $categories_ids = Helper::preg_grep_keys('/^category_[0-9]+$/', $params['POST']);

        // associate categories to
        $cats = [];
        if (!empty($categories_ids)) {
            foreach ($categories_ids as $category_id) {
                $cats[] = Category::findById($category_id);
                // check if it's article category or product category
            }
            $object->addCategories($cats);
        }
    }

    /**
     * Show the edit form for posts (article and pages)
     *
     * @param Array $params url parameters
     * @param integer $postType determines which posts to show in edit form
     * @return void
     */
    protected function editPost($params, $postType)
    {
        // check whether it is an article or page
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
            $configPost['postTitle'] = "d'un article";

        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
            $configPost['postTitle'] = "d'une page";
        }

        if (!empty($params['URL'][1]) && is_numeric($params['URL'][1])) {
            (!Helper::isRoute("/admin/" . $configPost['postType'] . "/edit/" . $params['URL'][1])) && $this->errors(404);
            $post = Post::getCollections([
                'id' => $params['URL'][1],
                'post_type_id' => $postType,
            ])[0];

            if (!empty($post) && is_a($post, 'Post')) {
                $v = new View($configPost['postType'] . "-edit", "back");
                $infos = [];
                if (!empty($params["POST"])) {
                    // Verification des informations modifier
                    $errors = Validator::validate($this->getPostForm($postType), $params["POST"]);

                    if (empty($errors)) {
                        $uniqueSlug = (empty($params["POST"]['slug']) ? (Helper::uniqueSlug($params["POST"]['title'], $post)) : Helper::uniqueSlug($params["POST"]['slug'], $post));
                        $post->setTitle($params["POST"]['title']);
                        $post->setSlug($uniqueSlug);

                        if (!empty($params['POST']['media-id'])) {
                            if (is_numeric($params['POST']['media-id'])) {
                                $existingAttachment = Attachment::findById($params['POST']['media-id']);
                                if (!empty($existingAttachment)) {
                                    $post->setAttachmentId($params["POST"]['media-id']);
                                }
                            }
                        }
                        // $post->setUserId($_SESSION['id_user']);

                        if (!empty($_POST['publish']) && $_POST['publish'] == "Publier") {
                            $post->setStatus(1);
                        }
                        if (!empty($_POST['draft']) && $_POST['draft'] == "Mettre au brouillon") {
                            $post->setStatus(0);
                        }

                        if (!empty($_POST['restore']) && $_POST['restore'] == "Restaurer") {
                            $post->setTrash(0);
                        }

                        if (!empty($_POST['trash']) && $_POST['trash'] == "Mettre à la corbeille") {
                            $post->setTrash(1);
                        }

                        $post->setText($params["POST"]['text']);

                        $this->addCategories($post, $params);
                        $post->save();

                        $v->assign("success", "Modification effectuée avec succès");
                    } else {
                        $v->assign("errors", $errors);
                    }
                }

                $v->assign('title', "Modification " . $configPost['postTitle']);
                $v->assign('post', $post);
                $v->assign("config", $this->getPostForm($postType, $post));
                $v->assign("infos", $infos);
                $v->assign("modal", Attachment::makeModal());

                if (!empty($_POST)) {
                    $url = "/admin/" . $configPost['postType'] . "/edit/" . $post->id;
                    header("Location: " . $url);
                }
            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404);
        }
    }

    /**
     * Show the new post form for (articles and pages)
     *
     * @param Array $params urls parameters
     * @param integer $postType post type 1 for article or 2 for page
     * @return void
     */
    protected function addPost($params, $postType)
    {
        // check whether it is an article or page
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
            $configPost['postTitle'] = "d'un article";

        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
            $configPost['postTitle'] = "d'une page";
        }

        (!Helper::isRoute("/admin/" . $configPost['postType'] . "/add")) && $this->errors(404);
        $v = new View($configPost['postType'] . "-edit", "back");

        $post = null;
        $infos = [];
        if (!empty($params["POST"])) {
            // Verification des informations modifier
            $errors = Validator::validate($this->getPostForm($postType), $params["POST"]);
            $post = new Post();

            $uniqueSlug = (empty($params["POST"]['slug']) ? (Helper::uniqueSlug($params["POST"]['title'])) : (Helper::uniqueSlug($params["POST"]['slug'])));

            $post->setTitle($params["POST"]['title']);
            $post->setSlug($uniqueSlug);
            $post->setText($params["POST"]['text']);
            $post->setUserId($_SESSION['id_user']); // user de la session
            $post->setPublishAt((new DateTime())->format('Y-m-d H:i:s'));
            $post->setPostTypeId($postType);

            if (!empty($params['POST']['media-id'])) {
                if (is_numeric($params['POST']['media-id'])) {
                    $existingAttachment = Attachment::findById($params['POST']['media-id']);
                    if (!empty($existingAttachment)) {
                        $post->setAttachmentId($params["POST"]['media-id']);
                    }
                }
            }

            if (empty($errors)) {

                // retrieve categories if they exists
                $this->addCategories($post, $params);

                $post->save();

                header("Location: /admin/" . $configPost['postType'] . "/edit/" . $post->id);
            } else {
                // Helper::dump($errors);
            }
        }
        $v->assign("title", "Ajout " . $configPost['postTitle']);
        $v->assign("config", $this->getPostForm($postType, $post));
        $v->assign("infos", $infos);
        $v->assign("modal", Attachment::makeModal());
    }

    /**
     * Delete definitively post from database
     *
     * @param Array $params urls parameters
     * @param integer $postType integer $postType post type 1 for article or 2 for page
     * @return void
     */
    protected function deletePost($params, $postType)
    {
        // check whether it is an article or page
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
        }

        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {

            (!Helper::isRoute("/admin/" . $configPost['postType'] . "/delete/" . $params['POST']['id'])) && $this->errors(404);
            Post::delete(['id' => $params['POST']['id']]);

            // get all data columns numbers
            extract(Post::fetchColumns($postType));
            echo json_encode(
                [
                    'all' => count($all_posts),
                    'published' => count($published_posts),
                    'draft' => count($draft_posts),
                    'trash' => count($trash_posts),
                ]
            );
        } else {
            $this->errors(404);
        }
    }

    /**
     * Move post (article & page) to bin
     *
     * @param Array $params urls parameters
     * @param integer $postType integer $postType post type 1 for article or 2 for page
     * @return void
     */
    protected function movePostToBin($params, $postType)
    {
        // check whether it is an article or page
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
        }

        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {
            (!Helper::isRoute("/admin/" . $configPost['postType'] . "/move-to-bin/" . $params['POST']['id'])) && $this->errors(404);
            $post = Post::findById($params['POST']['id']);

            if (is_a($post, 'Post') && !is_array($post)) {
                // update information
                $post->setTrash(1);
                $post->save();

                // get new numbers
                extract(Post::fetchColumns($postType));
                echo json_encode(
                    [
                        'all' => count($all_posts),
                        'published' => count($published_posts),
                        'draft' => count($draft_posts),
                        'trash' => count($trash_posts),
                    ]
                );

            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404); // the request is not coming from the right source
        }
    }

    /**
     * Restore posts from bin
     *
     * @param Array $params urls parameters
     * @param integer $postType integer $postType post type 1 for article or 2 for page
     * @return void
     */
    protected function removePostFromBin($params, $postType)
    {
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
        }

        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {
            (!Helper::isRoute("/admin/" . $configPost['postType'] . "/remove-from-bin/" . $params['POST']['id'])) && $this->errors(404);
            $post = Post::findById($params['POST']['id']);
            if (!empty($post)) {
                $post->setTrash(0);
                $post->save();

                // get all data columns numbers
                extract(Post::fetchColumns($postType));
                echo json_encode(
                    [
                        'all' => count($all_posts),
                        'published' => count($published_posts),
                        'draft' => count($draft_posts),
                        'trash' => count($trash_posts),
                    ]
                );
            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404); // the request is not coming from the right source
        }
    }

    /**
     * Manage grouped action of posts (article and page)
     *
     * @param Array $params urls parameters
     * @param integer $postType integer $postType post type 1 for article or 2 for page
     * @return void
     */
    protected function performPostSelectionPan($params, $postType)
    {
        if (!empty($params['POST']['ajaxrequest'])) {
            if (!empty($params['POST']['action-perform'])) {
                sleep(1); // wait loader
                // get all CORRECT that matches exactly to [action_4] pattern checkkox values
                $data_actions = Helper::preg_grep_keys('/^action_[0-9]+$/', $params['POST']);
                if (!empty($data_actions)) {
                    switch ($params['POST']['action-perform']) {
                        case "group":{
                                echo json_encode([
                                    'message' => "Aucune action n'a été choisie",
                                ]);
                                break;
                            }
                        case "trash":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `post` SET trash = 1 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumns($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_posts),
                                        'published' => count($published_posts),
                                        'draft' => count($draft_posts),
                                        'trash' => count($trash_posts),
                                    ],
                                    'message' => "Toutes les pages sélectionnées ont été mises à la corbeille",
                                ]);

                                break;
                            }
                        case "restore":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `post` SET trash = 0 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumns($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_posts),
                                        'published' => count($published_posts),
                                        'draft' => count($draft_posts),
                                        'trash' => count($trash_posts),
                                    ],
                                    'message' => "Toutes les pages sélectionnées ont été rétirées de la corbeille",
                                ]);

                                break;
                            }
                        case "draft":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `post` SET status = 0 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumns($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_posts),
                                        'published' => count($published_posts),
                                        'draft' => count($draft_posts),
                                        'trash' => count($trash_posts),
                                    ],
                                    'message' => "Toutes les pages selectionnées ont été mises au brouillon",
                                ]);

                                break;
                            }
                        case "publish":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `post` SET status = 1 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumns($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_posts),
                                        'published' => count($published_posts),
                                        'draft' => count($draft_posts),
                                        'trash' => count($trash_posts),
                                    ],
                                    'message' => "Toutes les pages selectionnées ont été mises en publication",
                                ]);

                                break;
                            }

                        case "delete":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "DELETE FROM `post` WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumns($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_posts),
                                        'published' => count($published_posts),
                                        'draft' => count($draft_posts),
                                        'trash' => count($trash_posts),
                                    ],
                                    'message' => "Toutes les pages selectionnées ont été supprimées définitivement",
                                ]);

                                break;
                                break;
                            }
                        default:{
                                echo json_encode([
                                    'message' => "Une erreur s'est produite",
                                ]);
                                break;
                            }
                    }
                } else {
                    echo json_encode([
                        'message' => "Aucune page n'a été sélectionnée",
                    ]);
                }

            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404);
        }
    }
    /**
     * Create a new category
     *
     * @param Array $params urls parameters
     * @param integer $type determine whether to edit article category or product category
     * @return void
     */
    protected function addCategory($params, $type)
    {
        // check whether it is an article or page
        $configCategory = [];
        if ($type == 1) { // Article
            $configCategory['type'] = "article";
        } elseif ($type == 2) { // product
            $configCategory['type'] = "product";
        }

        (!Helper::isRoute("/admin/" . $configCategory['type'] . "/category/add")) && $this->errors(404);

        $v = new View("category-edit", "back");

        $category = [];
        $errors = [];
        $infos = [];
        if (!empty($params["POST"])) {
            // Verification des informations modifier
            $errors = Validator::validate(Category::configForm(), $params["POST"]);

            $existingCategories = Category::select(['name' => $params["POST"]['name']]);
            if (!empty($existingCategories)) {
                $errors['title'] = "Cette catégorie existe déjà";
            }

            $category = new Category();
            $category->setName($params["POST"]['name']);
            $category->setSlug($params["POST"]['slug']);
            $category->setDescription($params["POST"]['category_description']);
            $category->setType($type); // 1 ou 2

            if (empty($errors)) {
                $category->save();
                header("Location: /admin/" . $configCategory['type'] . "/category/edit/" . $category->id);
            } else {
                Helper::dump($errors);
            }
        }
        $v->assign("title", "Création d'une nouvelle catégorie");
        $v->assign("infos", $infos);
        $v->assign("errors", $errors);
        $v->assign("config", Category::configForm($category));
    }

    /**
     * Edit an existing category
     *
     * @param Array $params urls parameters
     * @param integer $type determine whether to edit article category or product category
     * @return void
     */
    protected function editCategory($params, $type)
    {
        $configCategory = [];
        if ($type == 1) { // Article
            $configCategory['type'] = "article";
        } elseif ($type == 2) { //
            $configCategory['type'] = "product";
        }

        if (!empty($params['URL'][2]) && is_numeric($params['URL'][2])) {
            (!Helper::isRoute("/admin/" . $configCategory['type'] . "/category/edit/" . $params['URL'][2])) && $this->errors(404);
            $v = new View("category-edit", "back");

            $category = Category::getCollections([
                'id' => $params['URL'][2],
            ])[0];
            $infos = [];

            if (!empty($params["POST"])) {
                // Verification des informations modifier
                $errors = Validator::validate(Category::configForm(), $params["POST"]);

                // check if there is not another category that has same name
                $existingCategories = Category::select([
                    'id|<>' => $params['URL'][2],
                    'name' => $params["POST"]['name'],
                ]);

                if (!empty($existingCategories)) {
                    $errors['title'] = "Cette catégorie existe déjà";
                    $v->assign("errors", $errors);
                }

                if (empty($errors)) {
                    $category->setName($params["POST"]['name']);
                    $category->setDescription($params["POST"]['category_description']);
                    $category->setSlug($params["POST"]['slug']);
                    $category->setType($type); // 1 ou 2 article category or product category
                    $category->save();

                    $v->assign("success", "Modification effectuée avec succès");
                } else {
                    // Helper::dump($errors);
                }
            }

            $v->assign("title", "Modification d'une catégorie");
            $v->assign("infos", $infos);
            $v->assign("config", Category::configForm($category));
        } else {
            $this->errors(404);
        }
    }

    /**
     * Delete definitively category from database
     *
     * @param Array $params urls parameters
     * @param integer $type integer category type 1 for article or 2 for product
     * @return void
     */
    protected function deleteCategory($params, $type)
    {
        // check whether it is an article or page
        $configCategory = [];
        if ($type == 1) { // Article
            $configCategory['type'] = "article";
        } elseif ($type == 2) { // page
            $configCategory['type'] = "product";
        }

        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {

            (!Helper::isRoute("/admin/" . $configCategory['type'] . "/category/delete/" . $params['POST']['id'])) && $this->errors(404);
            Category::delete(['id' => $params['POST']['id']]);

            // get all data columns numbers
            extract(Category::fetchColumns($type));
            echo json_encode(
                [
                    'published_categories' => count($published_categories),
                ]
            );
        } else {
            $this->errors(404);
        }
    }

    /**
     * Manage grouped action of categories (articles and products)
     *
     * @param Array $params urls parameters
     * @param integer $type integer $type category type 1 for article or 2 for product
     * @return void
     */
    protected function performCategorySelectionPan($params, $type)
    {
        if (!empty($params['POST']['ajaxrequest'])) {
            if (!empty($params['POST']['action-perform'])) {
                sleep(1); // wait loader
                // get all CORRECT that matches exactly to [action_4] pattern checkkox values
                $data_actions = Helper::preg_grep_keys('/^action_[0-9]+$/', $params['POST']);
                if (!empty($data_actions)) {
                    switch ($params['POST']['action-perform']) {
                        case "group":{
                                echo json_encode([
                                    'message' => "Aucune action n'a été choisie",
                                ]);
                                break;
                            }
                        case "delete":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "DELETE FROM `category` WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Category::runQuery($sql, $data_actions);

                                extract(Category::fetchColumns($type));
                                echo json_encode([
                                    'data' => [
                                        'published_categories' => count($published_categories),
                                    ],
                                    'message' => "Toutes les catégories selectionnées ont été supprimées définitivement",
                                ]);

                                break;
                            }
                        default:{
                                echo json_encode([
                                    'message' => "Une erreur s'est produite",
                                ]);
                                break;
                            }
                    }
                } else {
                    echo json_encode([
                        'message' => "Aucune catégorie n'a été sélectionnée",
                    ]);
                }

            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404);
        }
    }

    protected function addProduct($params)
    {
        (!Helper::isRoute("/admin/product/add")) && $this->errors(404);
        $v = new View("product-edit", "back");

        $product = [];
        $errors = [];
        if (!empty($params["POST"])) {
            // Verification des informations modifier
            $errors = Validator::validate(Product::configForm(), $params["POST"]);

            // check if there is not another category that has same name
            $existingProducts = Product::select([
                'name' => $params["POST"]['name'],
            ]);

            if (!empty($existingProducts)) {
                $errors['name'] = "Ce produit existe déjà";
            }

            if ($params["POST"]['price'] < 0) {
                $errors['price'] = "Le prix est négatif (doit être > supérieur à 0)";
            } elseif (!is_numeric($params["POST"]['price'])) {
                $errors['price'] = "Le prix n'est pas un entier";
            }

            $product = new Product();

            $uniqueSlug = (empty($params["POST"]['slug']) ? (Helper::uniqueSlug($params["POST"]['name'])) : (Helper::uniqueSlug($params["POST"]['slug'])));

            $product->setName($params["POST"]['name']);
            $product->setSlug($uniqueSlug);
            $product->setPrice($params["POST"]['price']);
            $product->setDescription($params["POST"]['text']);
            $product->setUserId($_SESSION['id_user']); // user de la session
            $product->setStock($params["POST"]['stock']);
            $product->setTrash(0);

            if (!empty($params['POST']['media-id'])) {
                if (is_numeric($params['POST']['media-id'])) {
                    $existingAttachment = Attachment::findById($params['POST']['media-id']);
                    if (!empty($existingAttachment)) {
                        $product->setAttachmentId($params["POST"]['media-id']);
                    }
                }
            }

            if (empty($errors)) {
                // retrieve categories if they exists
                $this->addCategories($product, $params);
                $product->save();

                header("Location: /admin/product/edit/" . $product->id);
            } else {

            }
        }

        $categories_ids = Helper::preg_grep_keys('/^category_[0-9]+$/', $params['POST']);

        $v->assign("title", "Ajout d'un produit");
        $v->assign("config", Product::configForm($product, $categories_ids));
        $v->assign("errors", $errors);
        $v->assign("modal", Attachment::makeModal());
        // $v->assign("infos", $infos);
    }

    protected function editProduct($params)
    {
        if (!empty($params['URL'][1]) && is_numeric($params['URL'][1])) {
            (!Helper::isRoute("/admin/product/edit/" . $params['URL'][1])) && $this->errors(404);
            $product = Product::getCollections([
                'id' => $params['URL'][1],
            ])[0];

            if (!empty($product) && is_a($product, 'Product')) {
                $v = new View("product-edit", "back");

                if (!empty($params["POST"])) {
                    // Verification des informations modifier
                    $errors = Validator::validate(Product::configForm(), $params["POST"]);

                    $existingProducts = Product::select([
                        'id|<>' => $params['URL'][1],
                        'name' => $params["POST"]['name'],
                    ]);

                    if (!empty($existingProducts)) {
                        $errors['name'] = "Ce produit existe déjà";
                    }

                    if ($params["POST"]['price'] < 0) {
                        $errors['price'] = "Le prix est négatif (doit être > supérieur à 0)";
                    } elseif (!is_numeric($params["POST"]['price'])) {
                        $errors['price'] = "Le prix n'est pas un entier";
                    }

                    if ($params["POST"]['stock'] < 0) {
                        $errors['stock'] = "Le stock est négatif (doit être > supérieur à 0)";
                    } elseif (!is_numeric($params["POST"]['stock'])) {
                        $errors['stock'] = "La quantité en stock n'est pas un entier";
                    }

                    $uniqueSlug = (empty($params["POST"]['slug']) ? (Helper::uniqueSlug($params["POST"]['name'])) : (Helper::uniqueSlug($params["POST"]['slug'])));

                    $product->setName($params["POST"]['name']);
                    $product->setSlug($uniqueSlug);
                    $product->setPrice($params["POST"]['price']);
                    $product->setDescription($params["POST"]['text']);
                    $product->setUserId($_SESSION['id_user']); // user de la session
                    $product->setStock($params["POST"]['stock']);

                    if (!empty($params['POST']['media-id'])) {
                        if (is_numeric($params['POST']['media-id'])) {
                            $existingAttachment = Attachment::findById($params['POST']['media-id']);
                            if (!empty($existingAttachment)) {
                                $product->setAttachmentId($params["POST"]['media-id']);
                            }
                        }
                    }

                    if (!empty($_POST['trash']) && $_POST['trash'] == "Mettre à la corbeille") {
                        $product->setTrash(1);
                    }

                    if (!empty($_POST['restore']) && $_POST['restore'] == "Restaurer") {
                        $product->setTrash(0);
                    }

                    if (empty($errors)) {
                        $this->addCategories($product, $params);
                        $uniqueSlug = (empty($params["POST"]['slug']) ? (Helper::uniqueSlug($params["POST"]['name'], $product)) : Helper::uniqueSlug($params["POST"]['slug'], $product));
                        $product->setSlug($uniqueSlug);

                        $product->save();

                        $v->assign("success", "Modification effectuée avec succès");
                    } else {
                        $v->assign("errors", $errors);
                    }
                }

                $v->assign('title', "Modification du produit");
                $v->assign('product', $product);
                $v->assign("config", Product::configForm($product));
                $v->assign("modal", Attachment::makeModal());
                // $v->assign("infos", $infos);
                // if (!empty($_POST)) {
                //     $url = "/admin/product/edit/" . $product->id;
                //     header("Refresh:5;url=" . $url);
                // }
            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404);
        }
    }

    protected function moveProductToBin($params)
    {
        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {
            (!Helper::isRoute("/admin/product/move-to-bin/" . $params['POST']['id'])) && $this->errors(404);
            $product = Product::findById($params['POST']['id']);
            if (!empty($product)) {
                $product->setTrash(1);
                $product->save();

                // get all data columns numbers
                extract(Product::fetchColumns());
                echo json_encode(
                    [
                        'published' => count($published_products),
                        'trash' => count($trash_products),
                    ]
                );
            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404); // the request is not coming from the right source
        }
    }

    protected function removeProductFromBin($params)
    {
        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {
            (!Helper::isRoute("/admin/product/remove-from-bin/" . $params['POST']['id'])) && $this->errors(404);
            $product = Product::findById($params['POST']['id']);
            if (!empty($product)) {
                $product->setTrash(0);
                $product->save();

                // get all data columns numbers
                extract(Product::fetchColumns());
                echo json_encode(
                    [
                        'published' => count($published_products),
                        'trash' => count($trash_products),
                    ]
                );
            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404); // the request is not coming from the right source
        }
    }

    protected function deleteProduct($params)
    {
        // check whether it is an article or page
        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {

            (!Helper::isRoute("/admin/product/delete/" . $params['POST']['id'])) && $this->errors(404);
            Product::delete(['id' => $params['POST']['id']]);

            // get all data columns numbers
            extract(Product::fetchColumns());
            echo json_encode(
                [
                    'published' => count($published_products),
                    'trash' => count($trash_products),
                ]
            );
        } else {
            $this->errors(404);
        }
    }

    protected function performProductSelectionPan($params)
    {
        if (!empty($params['POST']['ajaxrequest'])) {
            if (!empty($params['POST']['action-perform'])) {
                sleep(1); // wait loader
                // get all CORRECT that matches exactly to [action_4] pattern checkkox values
                $data_actions = Helper::preg_grep_keys('/^action_[0-9]+$/', $params['POST']);
                if (!empty($data_actions)) {
                    switch ($params['POST']['action-perform']) {
                        case "group":{
                                echo json_encode([
                                    'message' => "Aucune action n'a été choisie",
                                ]);
                                break;
                            }

                        case "trash":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `product` SET trash = 1 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Product::runQuery($sql, $data_actions);

                                extract(Product::fetchColumns());
                                echo json_encode([
                                    'data' => [
                                        'published' => count($published_products),
                                        'trash' => count($trash_products),
                                    ],
                                    'message' => "Tous les produits selectionnés ont été mis à la corbeille",
                                ]);

                                break;
                            }

                        case "restore":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `product` SET trash = 0 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Product::runQuery($sql, $data_actions);

                                extract(Product::fetchColumns());
                                echo json_encode([
                                    'data' => [
                                        'published' => count($published_products),
                                        'trash' => count($trash_products),
                                    ],
                                    'message' => "Tous les produits selectionnés ont été retirés de la corbeille",
                                ]);

                                break;
                            }

                        case "delete":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "DELETE FROM `product` WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Product::runQuery($sql, $data_actions);

                                extract(Product::fetchColumns());
                                echo json_encode([
                                    'data' => [
                                        'published' => count($published_products),
                                        'trash' => count($trash_products),
                                    ],
                                    'message' => "Tous les produits selectionnés ont été supprimés définitivement",
                                ]);

                                break;
                            }
                        default:{
                                echo json_encode([
                                    'message' => "Une erreur s'est produite",
                                ]);
                                break;
                            }
                    }
                } else {
                    echo json_encode([
                        'message' => "Aucun produit n'a été sélectionné",
                    ]);
                }

            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404);
        }
    }

    protected function registerCommentResponse($params)
    {
        (!Helper::isRoute("/admin/article/comment/add")) && $this->errors(404);
        if (!empty($params['POST']['ajaxrequest'])) {
            if (empty($params['POST']['reponse-comment'])) {
                echo json_encode(
                    [
                        'comment' => "",
                        'message' => "Saisissez votre réponse d'abord SVP !",
                    ]
                );
            } else {
                if (is_numeric($params['POST']['comment_id']) && $params['POST']['comment_id'] > 0 &&
                    is_numeric($params['POST']['post_id']) && $params['POST']['post_id'] > 0) {
                    sleep(2);
                    $post = Post::findById($params['POST']['post_id']);
                    $parent_comment = Comment::findById($params['POST']['comment_id']);

                    if (!empty($parent_comment) && !empty($post)) {
                        $comment = new Comment();
                        $comment->post_id = $params['POST']['post_id']; // have to check if it's numeric
                        $comment->setContent($params['POST']['reponse-comment']);
                        $comment->setUserId($_SESSION['id_user']); // // have to check if it's numeric
                        $comment->setCommentId($params['POST']['comment_id']);
                        $comment->status(1);
                        $comment->save();

                        echo json_encode(
                            [
                                'message' => "Votre réponse a été enregistrée.",
                            ]
                        );
                    } else {
                        $this->errors(404);
                    }
                } else {
                    $this->errors(404);
                }
            }
        } else {
            $this->errors(404);
        }
    }

    protected function approveComment($params, $comType)
    {
        $type = [];
        if ($comType == "article") {
            $type['url'] = "article";
            $type['field'] = "product";
        } elseif ($comType == "product") {
            $type['url'] = "product";
            $type['field'] = "post";
        }

        if (!empty($params['URL'][2]) && is_numeric($params['URL'][2]) && !empty($params['POST']['ajaxrequest'])) {
            (!Helper::isRoute("/admin/" . $type['url'] . "/comment/approve/" . $params['URL'][2])) && $this->errors(404);

            $key = $type['field'] . '_id|IS';
            $comments = Comment::select(['id' => $params['URL'][2], $key => null, 'comment_id|IS' => null]);

            if (!empty($comments)) {
                $comment = $comments[0];
                $comment->setStatus(1);
                $comment->save();
            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404);
        }
    }

    protected function desapproveComment($params, $comType)
    {
        $type = [];
        if ($comType == "article") {
            $type['url'] = "article";
            $type['field'] = "product";
        } elseif ($comType == "product") {
            $type['url'] = "product";
            $type['field'] = "post";
        }
        if (!empty($params['URL'][2]) && is_numeric($params['URL'][2]) && !empty($params['POST']['ajaxrequest'])) {
            (!Helper::isRoute("/admin/". $type['url'] . "/comment/desapprove/" . $params['URL'][2])) && $this->errors(404);

            $key = $type['field'] . '_id|IS';
            $comments = Comment::select(['id' => $params['URL'][2], $key => null, 'comment_id|IS' => null]);

            if (!empty($comments)) {
                $comment = $comments[0];
                $comment->setStatus(0);
                $comment->save();
                echo json_encode([
                    'message' => 'Commentaire desapprouvé'
                ]);
            } else {
                echo json_encode([
                    'message' => 'Une erreur s\'est produite'
                ]);
                $this->errors(404);
            }
        } else {
            echo json_encode([
                'message' => 'Une erreur s\'est produite'
            ]);
            $this->errors(404);
        }
    }

    protected function deleteComment($params, $comType = 'article')
    {
        $type = [];
        if ($comType == "article") {
            $type['url'] = "article";
            $type['field'] = "product";
        } elseif ($comType == "product") {
            $type['url'] = "product";
            $type['field'] = "post";
        }

        if (!empty($params['URL'][2]) && is_numeric($params['URL'][2]) && !empty($params['POST']['ajaxrequest'])
        && !empty($params['POST']['respond'])) {
            (!Helper::isRoute("/admin/" . $type['url'] . "/comment/delete/" . $params['URL'][2])) && $this->errors(404);
            $key = $type['field'] . '_id|IS';

            if ($params['POST']['respond'] == "true") {
                $is_rep_delete = Comment::delete(['comment_id' => $params['URL'][2], $key => null]);

                $is_delete = Comment::delete(['id' => $params['URL'][2], $key => null, 'comment_id|IS' => null]);

                if ($is_delete == 1) {
                    echo json_encode([
                        'message' => 'Commentaire supprimé avec toutes les réponses',
                    ]);
                } else {
                    echo json_encode([
                        'message' => 'Une erreur s\'est produite lors de la suppression du commentaire',
                    ]);
                }
            }

        } else {
            $this->errors(404);
        }
    }
}
