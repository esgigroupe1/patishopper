<?php
    header("Content-type: text/css; charset: UTF-8");
    $theme_master_color = "#F2994A";
?>
@charset "UTF-8";
@import url("https://fonts.googleapis.com/css?family=Nunito");
* {
  margin: 0;
  padding: 0;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

.container {
  width: 100%;
  max-width: 1200px;
  margin: 0 auto;
  overflow: hidden;
}

*[class*="col-"] {
  padding: 0 10px;
}

.container-fluid {
  width: 100%;
}

.container-fluid.collapse .row,
.container.collapse .row {
  margin: 0;
}

.container-fluid.collapse .row [class*="col-"],
.container.collapse .row [class*="col-"] {
  padding: 0;
}

.container-fluid.no-collapse .row,
.container.no-collapse .row {
  margin: 0 -10px;
}

.container-fluid.no-collapse .row [class*="col-"],
.container.no-collapse .row [class*="col-"] {
  padding: 0 10px 0 10px;
}

.container .row,
.container-fluid .row {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
}

.container .row.no-collapse,
.container-fluid .row.no-collapse {
  margin: 0;
}

.container .row > div,
.container-fluid .row > div {
  padding: 0 10px 0 10px;
  background-clip: content-box;
}

.container .row > div.col-1,
.container-fluid .row > div.col-1 {
  -ms-flex-preferred-size: 8.33333%;
      flex-basis: 8.33333%;
}

.container .row > div.col-2,
.container-fluid .row > div.col-2 {
  -ms-flex-preferred-size: 16.66667%;
      flex-basis: 16.66667%;
}

.container .row > div.col-3,
.container-fluid .row > div.col-3 {
  -ms-flex-preferred-size: 25%;
      flex-basis: 25%;
}

.container .row > div.col-4,
.container-fluid .row > div.col-4 {
  -ms-flex-preferred-size: 33.33333%;
      flex-basis: 33.33333%;
}

.container .row > div.col-5,
.container-fluid .row > div.col-5 {
  -ms-flex-preferred-size: 41.66667%;
      flex-basis: 41.66667%;
}

.container .row > div.col-6,
.container-fluid .row > div.col-6 {
  -ms-flex-preferred-size: 50%;
      flex-basis: 50%;
}

.container .row > div.col-7,
.container-fluid .row > div.col-7 {
  -ms-flex-preferred-size: 58.33333%;
      flex-basis: 58.33333%;
}

.container .row > div.col-8,
.container-fluid .row > div.col-8 {
  -ms-flex-preferred-size: 66.66667%;
      flex-basis: 66.66667%;
}

.container .row > div.col-9,
.container-fluid .row > div.col-9 {
  -ms-flex-preferred-size: 75%;
      flex-basis: 75%;
}

.container .row > div.col-10,
.container-fluid .row > div.col-10 {
  -ms-flex-preferred-size: 83.33333%;
      flex-basis: 83.33333%;
}

.container .row > div.col-11,
.container-fluid .row > div.col-11 {
  -ms-flex-preferred-size: 91.66667%;
      flex-basis: 91.66667%;
}

.container .row > div.col-12,
.container-fluid .row > div.col-12 {
  -ms-flex-preferred-size: 100%;
      flex-basis: 100%;
}

@media screen and (max-width: 1200px) {
  .container .row > div.col-lg-1,
  .container-fluid .row > div.col-lg-1 {
    -ms-flex-preferred-size: 8.33333%;
        flex-basis: 8.33333%;
    width: 8.33333%;
  }
  .container .row > div.col-lg-2,
  .container-fluid .row > div.col-lg-2 {
    -ms-flex-preferred-size: 16.66667%;
        flex-basis: 16.66667%;
    width: 16.66667%;
  }
  .container .row > div.col-lg-3,
  .container-fluid .row > div.col-lg-3 {
    -ms-flex-preferred-size: 25%;
        flex-basis: 25%;
    width: 25%;
  }
  .container .row > div.col-lg-4,
  .container-fluid .row > div.col-lg-4 {
    -ms-flex-preferred-size: 33.33333%;
        flex-basis: 33.33333%;
    width: 33.33333%;
  }
  .container .row > div.col-lg-5,
  .container-fluid .row > div.col-lg-5 {
    -ms-flex-preferred-size: 41.66667%;
        flex-basis: 41.66667%;
    width: 41.66667%;
  }
  .container .row > div.col-lg-6,
  .container-fluid .row > div.col-lg-6 {
    -ms-flex-preferred-size: 50%;
        flex-basis: 50%;
    width: 50%;
  }
  .container .row > div.col-lg-7,
  .container-fluid .row > div.col-lg-7 {
    -ms-flex-preferred-size: 58.33333%;
        flex-basis: 58.33333%;
    width: 58.33333%;
  }
  .container .row > div.col-lg-8,
  .container-fluid .row > div.col-lg-8 {
    -ms-flex-preferred-size: 66.66667%;
        flex-basis: 66.66667%;
    width: 66.66667%;
  }
  .container .row > div.col-lg-9,
  .container-fluid .row > div.col-lg-9 {
    -ms-flex-preferred-size: 75%;
        flex-basis: 75%;
    width: 75%;
  }
  .container .row > div.col-lg-10,
  .container-fluid .row > div.col-lg-10 {
    -ms-flex-preferred-size: 83.33333%;
        flex-basis: 83.33333%;
    width: 83.33333%;
  }
  .container .row > div.col-lg-11,
  .container-fluid .row > div.col-lg-11 {
    -ms-flex-preferred-size: 91.66667%;
        flex-basis: 91.66667%;
    width: 91.66667%;
  }
  .container .row > div.col-lg-12,
  .container-fluid .row > div.col-lg-12 {
    -ms-flex-preferred-size: 100%;
        flex-basis: 100%;
    width: 100%;
  }
}

@media screen and (max-width: 960px) {
  .container .row > div.col-md-1,
  .container-fluid .row > div.col-md-1 {
    -ms-flex-preferred-size: 8.33333%;
        flex-basis: 8.33333%;
    width: 8.33333%;
  }
  .container .row > div.col-md-2,
  .container-fluid .row > div.col-md-2 {
    -ms-flex-preferred-size: 16.66667%;
        flex-basis: 16.66667%;
    width: 16.66667%;
  }
  .container .row > div.col-md-3,
  .container-fluid .row > div.col-md-3 {
    -ms-flex-preferred-size: 25%;
        flex-basis: 25%;
    width: 25%;
  }
  .container .row > div.col-md-4,
  .container-fluid .row > div.col-md-4 {
    -ms-flex-preferred-size: 33.33333%;
        flex-basis: 33.33333%;
    width: 33.33333%;
  }
  .container .row > div.col-md-5,
  .container-fluid .row > div.col-md-5 {
    -ms-flex-preferred-size: 41.66667%;
        flex-basis: 41.66667%;
    width: 41.66667%;
  }
  .container .row > div.col-md-6,
  .container-fluid .row > div.col-md-6 {
    -ms-flex-preferred-size: 50%;
        flex-basis: 50%;
    width: 50%;
  }
  .container .row > div.col-md-7,
  .container-fluid .row > div.col-md-7 {
    -ms-flex-preferred-size: 58.33333%;
        flex-basis: 58.33333%;
    width: 58.33333%;
  }
  .container .row > div.col-md-8,
  .container-fluid .row > div.col-md-8 {
    -ms-flex-preferred-size: 66.66667%;
        flex-basis: 66.66667%;
    width: 66.66667%;
  }
  .container .row > div.col-md-9,
  .container-fluid .row > div.col-md-9 {
    -ms-flex-preferred-size: 75%;
        flex-basis: 75%;
    width: 75%;
  }
  .container .row > div.col-md-10,
  .container-fluid .row > div.col-md-10 {
    -ms-flex-preferred-size: 83.33333%;
        flex-basis: 83.33333%;
    width: 83.33333%;
  }
  .container .row > div.col-md-11,
  .container-fluid .row > div.col-md-11 {
    -ms-flex-preferred-size: 91.66667%;
        flex-basis: 91.66667%;
    width: 91.66667%;
  }
  .container .row > div.col-md-12,
  .container-fluid .row > div.col-md-12 {
    -ms-flex-preferred-size: 100%;
        flex-basis: 100%;
    width: 100%;
  }
}

@media screen and (max-width: 720px) {
  .container .row > div.col-sm-1,
  .container-fluid .row > div.col-sm-1 {
    -ms-flex-preferred-size: 8.33333%;
        flex-basis: 8.33333%;
    width: 8.33333%;
  }
  .container .row > div.col-sm-2,
  .container-fluid .row > div.col-sm-2 {
    -ms-flex-preferred-size: 16.66667%;
        flex-basis: 16.66667%;
    width: 16.66667%;
  }
  .container .row > div.col-sm-3,
  .container-fluid .row > div.col-sm-3 {
    -ms-flex-preferred-size: 25%;
        flex-basis: 25%;
    width: 25%;
  }
  .container .row > div.col-sm-4,
  .container-fluid .row > div.col-sm-4 {
    -ms-flex-preferred-size: 33.33333%;
        flex-basis: 33.33333%;
    width: 33.33333%;
  }
  .container .row > div.col-sm-5,
  .container-fluid .row > div.col-sm-5 {
    -ms-flex-preferred-size: 41.66667%;
        flex-basis: 41.66667%;
    width: 41.66667%;
  }
  .container .row > div.col-sm-6,
  .container-fluid .row > div.col-sm-6 {
    -ms-flex-preferred-size: 50%;
        flex-basis: 50%;
    width: 50%;
  }
  .container .row > div.col-sm-7,
  .container-fluid .row > div.col-sm-7 {
    -ms-flex-preferred-size: 58.33333%;
        flex-basis: 58.33333%;
    width: 58.33333%;
  }
  .container .row > div.col-sm-8,
  .container-fluid .row > div.col-sm-8 {
    -ms-flex-preferred-size: 66.66667%;
        flex-basis: 66.66667%;
    width: 66.66667%;
  }
  .container .row > div.col-sm-9,
  .container-fluid .row > div.col-sm-9 {
    -ms-flex-preferred-size: 75%;
        flex-basis: 75%;
    width: 75%;
  }
  .container .row > div.col-sm-10,
  .container-fluid .row > div.col-sm-10 {
    -ms-flex-preferred-size: 83.33333%;
        flex-basis: 83.33333%;
    width: 83.33333%;
  }
  .container .row > div.col-sm-11,
  .container-fluid .row > div.col-sm-11 {
    -ms-flex-preferred-size: 91.66667%;
        flex-basis: 91.66667%;
    width: 91.66667%;
  }
  .container .row > div.col-sm-12,
  .container-fluid .row > div.col-sm-12 {
    -ms-flex-preferred-size: 100%;
        flex-basis: 100%;
    width: 100%;
  }
}

@media screen and (max-width: 540px) {
  .container .row > div.col-xs-1,
  .container-fluid .row > div.col-xs-1 {
    -ms-flex-preferred-size: 8.33333%;
        flex-basis: 8.33333%;
    width: 8.33333%;
  }
  .container .row > div.col-xs-2,
  .container-fluid .row > div.col-xs-2 {
    -ms-flex-preferred-size: 16.66667%;
        flex-basis: 16.66667%;
    width: 16.66667%;
  }
  .container .row > div.col-xs-3,
  .container-fluid .row > div.col-xs-3 {
    -ms-flex-preferred-size: 25%;
        flex-basis: 25%;
    width: 25%;
  }
  .container .row > div.col-xs-4,
  .container-fluid .row > div.col-xs-4 {
    -ms-flex-preferred-size: 33.33333%;
        flex-basis: 33.33333%;
    width: 33.33333%;
  }
  .container .row > div.col-xs-5,
  .container-fluid .row > div.col-xs-5 {
    -ms-flex-preferred-size: 41.66667%;
        flex-basis: 41.66667%;
    width: 41.66667%;
  }
  .container .row > div.col-xs-6,
  .container-fluid .row > div.col-xs-6 {
    -ms-flex-preferred-size: 50%;
        flex-basis: 50%;
    width: 50%;
  }
  .container .row > div.col-xs-7,
  .container-fluid .row > div.col-xs-7 {
    -ms-flex-preferred-size: 58.33333%;
        flex-basis: 58.33333%;
    width: 58.33333%;
  }
  .container .row > div.col-xs-8,
  .container-fluid .row > div.col-xs-8 {
    -ms-flex-preferred-size: 66.66667%;
        flex-basis: 66.66667%;
    width: 66.66667%;
  }
  .container .row > div.col-xs-9,
  .container-fluid .row > div.col-xs-9 {
    -ms-flex-preferred-size: 75%;
        flex-basis: 75%;
    width: 75%;
  }
  .container .row > div.col-xs-10,
  .container-fluid .row > div.col-xs-10 {
    -ms-flex-preferred-size: 83.33333%;
        flex-basis: 83.33333%;
    width: 83.33333%;
  }
  .container .row > div.col-xs-11,
  .container-fluid .row > div.col-xs-11 {
    -ms-flex-preferred-size: 91.66667%;
        flex-basis: 91.66667%;
    width: 91.66667%;
  }
  .container .row > div.col-xs-12,
  .container-fluid .row > div.col-xs-12 {
    -ms-flex-preferred-size: 100%;
        flex-basis: 100%;
    width: 100%;
  }
}

input.input {
  font-family: "Nunito";
  font-size: 1em;
  color: #242222;
  border-radius: 2px;
  padding: 2px;
}

input.input:not([type="submit"]) {
  border: 2px solid #d6d2d2;
}

input.input[type="submit"] {
  background-color: <?php echo $theme_master_color ?>;
  border: 2px solid <?php echo $theme_master_color ?>;
  color: #fff;
  font-weight: bold;
}

input.input[type="submit"]:hover {
  background-color: #fff;
  color: <?php echo $theme_master_color ?>;
}

input.input:focus {
  outline: 0;
  border-color: <?php echo $theme_master_color ?>;
}

input.input::-webkit-input-placeholder {
  /* WebKit browsers */
  font-weight: 500;
}

input.input:-moz-placeholder {
  /* Mozilla Firefox 4 to 18 */
  font-weight: 500;
}

input.input::-moz-placeholder {
  /* Mozilla Firefox 19+ but I'm not sure about working */
  font-weight: 500;
}

input.input:-ms-input-placeholder {
  /* Internet Explorer 10+ */
  font-weight: 500;
}

label.checkbox {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

label.checkbox .label {
  position: absolute;
  left: 50px;
  font-size: 0.7em;
}

label.checkbox input[type="checkbox"] {
  opacity: 0;
  cursor: pointer;
}

label.checkbox input:checked ~ .checker {
  background-color: <?php echo $theme_master_color ?>;
}

label.checkbox input:checked ~ .checker:after {
  display: block;
}

label.checkbox .checker {
  position: absolute;
  top: 0;
  left: 15px;
  height: 24px;
  width: 24px;
  background-color: #eee;
}

label.checkbox .checker:after {
  content: "";
  position: absolute;
  display: none;
}

label.checkbox .checker:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}

label.checkbox:hover input ~ .checker {
  background-color: <?php echo $theme_master_color ?>;
}

.button {
  background-color: #ef7e1a;
  color: #fff;
  padding: 10px;
  border-radius: 5px;
  border: 1px solid <?php echo $theme_master_color ?>;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
  text-align: center;
  cursor: pointer;
  outline: 0;
  margin-bottom: 10px;
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

.button.no-background {
  background: none;
  border: 1px dashed <?php echo $theme_master_color ?>;
  color: <?php echo $theme_master_color ?>;
  margin: auto 10px auto auto;
}

.button.no-background:hover {
  background: none;
  color: <?php echo $theme_master_color ?>;
}

.button.background-light {
  border: none;
  margin: 20px auto 5px !important;
}

.button.background-light:hover {
  background: rgba(239, 126, 26, 0.9);
}

.button.center {
  margin-left: auto;
  margin-right: auto;
}

.button.left {
  margin-right: auto;
}

.button.right {
  margin-left: auto;
}

.button:hover {
  background-color: <?php echo $theme_master_color ?>;
  -webkit-box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
}

.button::-moz-selection {
  background: #ffb7b7;
  /* WebKit/Blink Browsers */
}

.button::selection {
  background: #ffb7b7;
  /* WebKit/Blink Browsers */
}

.button::-moz-selection {
  background: #ffb7b7;
  /* Gecko Browsers */
}

body {
  font-family: 'Nunito', sans-serif;
  font-size: 16px;
  line-height: 1.4em;
}

body::-webkit-scrollbar {
  width: 7px;
}

body::-webkit-scrollbar-thumb {
  border-radius: 50px;
  background: #a49c9c;
}

body::-webkit-scrollbar-track {
  background: #d5d1d1;
}

h1 {
  font-size: 2em;
  margin: 0.5em;
  color: <?php echo $theme_master_color ?>;
}

h2 {
  font-size: 1.5em;
}

h3 {
  font-size: 1.2em;
}

p {
  font-size: 1em;
  line-height: 1.5;
}

a {
  text-decoration: none;
}

ul {
  list-style: none;
}

main {
  width: 100%;
}

.separator {
  border: 1px solid #D5D1D1;
  width: 95%;
  margin-left: 2%;
}

#back-template #view-content {
  min-height: 800px;
}

#back-template #view-content textarea[name="text"] {
  visibility: hidden;
  display: none;
}

#back-template #view-content hr.separator {
  overflow: hidden;
  padding: 0;
  width: 98%;
  text-align: center;
  border: 1px solid rgba(213, 209, 209, 0.9);
  border-radius: 25px;
  margin: 0 auto 20px;
}

#back-template #view-content #button {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#back-template #view-content #button .button {
  margin-left: auto !important;
  margin-right: 15px;
  height: 50px;
  margin-top: 5px;
}

#back-template #site-header header {
  font-weight: bold;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  background-color: #242222;
  color: #fff;
  position: relative;
  min-height: 100px;
  height: 100px;
}

#back-template #site-header header span {
  margin: auto .5em;
}

#back-template #site-header header #back-mobile-navigation {
  display: none;
  height: 50px;
  width: 50px;
  margin: auto 0;
}

#back-template #site-header header #back-mobile-navigation a {
  display: inline-block;
  height: 50px;
  width: 50px;
  background-repeat: no-repeat;
  background-size: 70%;
  background-position: center;
}

#back-template #site-header header #site-info a {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: 25px;
  color: #fff;
}

#back-template #site-header header #informations {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  position: absolute;
  right: 25px;
  top: 25px;
}

#back-template #site-header header #informations > * {
  margin: auto;
}

#back-template #site-header header #informations span,
#back-template #site-header header #informations a > img {
  margin-right: 25px;
}

#back-template #site-header header #informations a.profil {
  width: 30px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#back-template #site-header header #informations a.profil img {
  border-radius: 50%;
  max-width: 50px;
  max-height: 50px;
  -webkit-transition: -webkit-transform .2s ease-in-out;
  transition: -webkit-transform .2s ease-in-out;
  transition: transform .2s ease-in-out;
  transition: transform .2s ease-in-out, -webkit-transform .2s ease-in-out;
}

#back-template #site-header header #informations a.profil img:hover {
  -webkit-transform: scale(1.1, 1.1);
          transform: scale(1.1, 1.1);
}

#back-template #site-header header #informations .dropbtn {
  padding: 10px;
  border: none;
  cursor: pointer;
}

#back-template #site-header header #informations img.dropbtn:hover, #back-template #site-header header #informations img.dropbtn:focus {
  -webkit-transform: scale(1.3);
          transform: scale(1.3);
}

#back-template #site-header header #informations .dropdown {
  position: relative;
  display: inline-block;
}

#back-template #site-header header #informations .dropdown-content {
  display: none;
  top: 50px;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  overflow: auto;
  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 1;
}

#back-template #site-header header #informations .dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

#back-template #site-header header #informations .dropdown-content a:hover {
  color: <?php echo $theme_master_color ?>;
}

#back-template #site-header header #informations .show {
  display: block;
}

#back-template #site-header header p {
  display: -webkit-inline-box;
  display: -ms-inline-flexbox;
  display: inline-flex;
}

#back-template #site-content {
  position: relative;
}

#back-template #site-navigation nav {
  background-image: url("../images/back/nav-background.svg");
  background-repeat: repeat-y;
  background-size: 100%;
  position: relative;
  min-height: 800px;
  height: 100%;
}

#back-template #site-navigation nav div#overlay {
  background-color: rgba(36, 34, 34, 0.9);
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1;
}

#back-template #site-navigation nav div#overlay > ul > li a:hover {
  color: #fff;
  background-color: #242222;
}

#back-template #site-navigation nav div#overlay > ul > li a:hover svg use {
  fill: #fff;
}

#back-template #site-navigation nav ul {
  margin-top: 70px;
  z-index: 2;
  padding: .5em;
}

#back-template #site-navigation nav li {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  font-weight: bold;
  min-height: 55px;
}

#back-template #site-navigation nav li.active-item > a {
  background-color: #242222;
}

#back-template #site-navigation nav li.active-item svg use {
  fill: #fff;
}

#back-template #site-navigation nav li.active-item a {
  color: #fff;
}

#back-template #site-navigation nav li > a {
  padding: .5em;
  color: <?php echo $theme_master_color ?>;
  border-radius: 5px;
  text-decoration: none;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#back-template #site-navigation nav li > a i {
  display: -webkit-inline-box;
  display: -ms-inline-flexbox;
  display: inline-flex;
  width: 35px;
  height: 35px;
  margin-right: 1vw;
}

#back-template #site-navigation nav li > a i svg {
  margin-top: 8px;
}

#back-template #site-navigation nav li > a span {
  line-height: 38px;
}

#back-template #site-navigation nav li > * {
  margin-top: auto;
  margin-bottom: auto;
}

#back-template #site-navigation ul#dropdown-menu {
  display: none;
}

#back-template #site-navigation ul#dropdown-menu.active {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#back-template #site-navigation ul#dropdown-menu i {
  margin-right: 0vw;
  margin-left: 1vw;
}

#back-template #site-navigation ul#dropdown-menu i svg use {
  fill: <?php echo $theme_master_color ?>;
}

#back-template #site-navigation ul#dropdown-menu a {
  color: <?php echo $theme_master_color ?>;
}

#back-template #site-footer footer {
  min-height: 100px;
  max-height: 100px;
  height: 100px;
  width: 100%;
  background-color: #242222;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

#back-template #site-footer footer > * {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

#back-template #site-footer footer p {
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
  margin: 0 25px;
}

#back-template #site-footer footer a,
#back-template #site-footer footer p {
  color: #fff;
  font-weight: bold;
}

#back-template #site-footer footer ul:first-of-type {
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

#back-template #site-footer footer ul:first-of-type li {
  padding: 0 .5em;
}

#back-template #site-footer footer ul:last-of-type {
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
  margin-right: 25px;
}

#back-template #site-footer footer ul:last-of-type li {
  padding: 0 .2em;
}

@media screen and (max-width: 1240px) {
  #back-template #site-navigation.responsive {
    position: absolute;
    left: -100%;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 2;
  }
  #back-template #site-navigation.opened {
    left: 0;
  }
  #back-template #site-header header #back-mobile-navigation {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
  }
  #back-template #site-header header #back-mobile-navigation a {
    background-image: url(../images/front/icons/burger.svg);
  }
  #back-template #site-header header #back-mobile-navigation a.opened {
    background-image: url(../images/front/icons/burger-close.svg);
  }
  #back-template #site-content {
    overflow-x: scroll;
  }
  #back-template #site-content #button .button {
    margin-left: 16px !important;
  }
}

@media screen and (max-width: 560px) {
  #back-template #site-header header #informations span {
    display: none;
  }
}

@media screen and (max-width: 380px) {
  #back-template #site-header header #informations {
    top: 40px;
  }
  #back-template #site-header header #informations > a.profil {
    display: none !important;
  }
}

@media screen and (max-width: 620px) {
  #back-template #site-footer footer {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
    max-height: 200px;
    height: 170px;
  }
  #back-template #site-footer footer ul:last-of-type {
    margin: 0;
  }
}

#back-template #media-back .img {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-bottom: 10px;
}

#back-template #media-back .img img {
  max-width: 250px;
  max-height: 150px;
  width: 250px;
  height: 150px;
  -webkit-transition: opacity 0.2s ease-in-out;
  transition: opacity 0.2s ease-in-out;
}

#back-template #media-back .img img:hover {
  opacity: 0.5;
}

#back-template #media-back .col-3 {
  padding-right: 16px;
}

#back-template #media-back .col-6 {
  padding-right: 15px;
}

#back-template #media-back .message > * {
  margin: auto;
}

#back-template #media-back form {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#back-template #media-back form input[type='submit'] {
  font-size: 1em !important;
  margin-bottom: 15px;
}

#back-template #media-back form .upload {
  text-align: center;
  margin: 7px;
  border: dashed 4px #d5d1d1;
  min-height: 700px;
  position: relative;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#back-template #media-back form .upload input[type='file'] {
  z-index: -5;
  margin-top: 3px;
  margin-left: 3px;
  -webkit-transition: background 0.4s ease;
  transition: background 0.4s ease;
  z-index: 15;
}

#back-template #media-back form .upload input[type='file']:hover {
  background: rgba(221, 221, 221, 0.2);
  cursor: pointer;
}

#back-template #media-back form .upload #select-file,
#back-template #media-back form .upload input[type="file"] {
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
}

#back-template #media-back form .upload #upload-media {
  visibility: hidden;
}

#back-template #media-back form .upload #select-file {
  z-index: 10;
  color: <?php echo $theme_master_color ?>;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#back-template #media-back form .upload #select-file:hover {
  background: rgba(221, 221, 221, 0.2);
  cursor: pointer;
}

#back-template #media-back form .upload #select-file span {
  font-size: 2em;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: auto;
}

#back-template #media-back form .upload .filename:not(:empty):before {
  content: "Fichier sélectionné: ";
}

#back-template #media-back form .upload .message {
  margin-bottom: 15px;
}

#back-template #media-back,
#back-template #article-edit,
#back-template #product-edit,
#back-template #page-edit,
#back-template #settings-back {
  /* Delete a media */
  /* All properties about the Modal */
}

#back-template #media-back #delete_media, #back-template #media-back #message,
#back-template #article-edit #delete_media,
#back-template #article-edit #message,
#back-template #product-edit #delete_media,
#back-template #product-edit #message,
#back-template #page-edit #delete_media,
#back-template #page-edit #message,
#back-template #settings-back #delete_media,
#back-template #settings-back #message {
  display: none;
}

#back-template #media-back .media-chosen,
#back-template #article-edit .media-chosen,
#back-template #product-edit .media-chosen,
#back-template #page-edit .media-chosen,
#back-template #settings-back .media-chosen {
  border: 1px solid #4169E1;
}

#back-template #media-back #message,
#back-template #article-edit #message,
#back-template #product-edit #message,
#back-template #page-edit #message,
#back-template #settings-back #message {
  margin-right: 10px;
  text-align: center;
}

#back-template #media-back #mediaModal .row,
#back-template #article-edit #mediaModal .row,
#back-template #product-edit #mediaModal .row,
#back-template #page-edit #mediaModal .row,
#back-template #settings-back #mediaModal .row {
  margin: 0 0 0 10px;
  text-align: center !important;
  background-color: #fff;
}

#back-template #media-back #mediaModal .row [class^="col-"],
#back-template #article-edit #mediaModal .row [class^="col-"],
#back-template #product-edit #mediaModal .row [class^="col-"],
#back-template #page-edit #mediaModal .row [class^="col-"],
#back-template #settings-back #mediaModal .row [class^="col-"] {
  padding: 0px 8px 8px 0 !important;
}

#back-template #media-back #mediaModal .row [class*=col-] img,
#back-template #article-edit #mediaModal .row [class*=col-] img,
#back-template #product-edit #mediaModal .row [class*=col-] img,
#back-template #page-edit #mediaModal .row [class*=col-] img,
#back-template #settings-back #mediaModal .row [class*=col-] img {
  width: 130px;
  max-width: 250px;
  max-height: 150px;
  -o-object-fit: cover;
     object-fit: cover;
  -o-object-position: center;
     object-position: center;
}

#back-template #media-back #mediaModal .no-collapse img,
#back-template #article-edit #mediaModal .no-collapse img,
#back-template #product-edit #mediaModal .no-collapse img,
#back-template #page-edit #mediaModal .no-collapse img,
#back-template #settings-back #mediaModal .no-collapse img {
  height: 24px;
  margin: 27px 18px;
}

#back-template #media-back #mediaModal .no-collapse input,
#back-template #article-edit #mediaModal .no-collapse input,
#back-template #product-edit #mediaModal .no-collapse input,
#back-template #page-edit #mediaModal .no-collapse input,
#back-template #settings-back #mediaModal .no-collapse input {
  z-index: 1;
  position: absolute;
}

#back-template #media-back .modal,
#back-template #article-edit .modal,
#back-template #product-edit .modal,
#back-template #page-edit .modal,
#back-template #settings-back .modal {
  display: none;
  position: fixed;
  z-index: 1;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  z-index: 10;
  background-color: black;
  background-color: rgba(0, 0, 0, 0.4);
}

#back-template #media-back .modal-content,
#back-template #article-edit .modal-content,
#back-template #product-edit .modal-content,
#back-template #page-edit .modal-content,
#back-template #settings-back .modal-content {
  background-color: #fefefe;
  margin: 5% auto;
  padding: 0;
  border: 1px solid #888;
  width: 90%;
  -webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  -webkit-animation-name: animatetop;
          animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
          animation-duration: 0.4s;
}

#back-template #media-back .close,
#back-template #article-edit .close,
#back-template #product-edit .close,
#back-template #page-edit .close,
#back-template #settings-back .close {
  color: #fff;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

#back-template #media-back .close:hover, #back-template #media-back .close:focus,
#back-template #article-edit .close:hover,
#back-template #article-edit .close:focus,
#back-template #product-edit .close:hover,
#back-template #product-edit .close:focus,
#back-template #page-edit .close:hover,
#back-template #page-edit .close:focus,
#back-template #settings-back .close:hover,
#back-template #settings-back .close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}

#back-template #media-back .media-file img:hover,
#back-template #article-edit .media-file img:hover,
#back-template #product-edit .media-file img:hover,
#back-template #page-edit .media-file img:hover,
#back-template #settings-back .media-file img:hover {
  opacity: 0.5;
}

#back-template #media-back .p-media,
#back-template #article-edit .p-media,
#back-template #product-edit .p-media,
#back-template #page-edit .p-media,
#back-template #settings-back .p-media {
  padding: 8px;
  margin: 1em 0 3em;
}

#back-template #media-back .header-modal,
#back-template #article-edit .header-modal,
#back-template #product-edit .header-modal,
#back-template #page-edit .header-modal,
#back-template #settings-back .header-modal {
  background-color: <?php echo $theme_master_color ?>;
  padding: 7px 16px;
  margin-bottom: 3px;
}

#back-template #media-back .footer-modal,
#back-template #article-edit .footer-modal,
#back-template #product-edit .footer-modal,
#back-template #page-edit .footer-modal,
#back-template #settings-back .footer-modal {
  background-color: <?php echo $theme_master_color ?>;
  padding: 10px 16px;
  margin-top: 3px;
}

#menu-edit .group {
  position: relative;
  margin-top: 20px;
  margin-bottom: 5px;
}

#menu-edit .col-2 .wrapper {
  padding-left: 1em;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#menu-edit .col-2 .wrapper #button-actions {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#menu-edit .col-2 .wrapper #button-actions .button {
  text-align: center;
  margin-bottom: 10px;
}

#menu-edit .col-2 .wrapper .info {
  text-align: center;
}

#menu-edit .col-2 .wrapper p {
  font-size: 1.2em;
  color: <?php echo $theme_master_color ?>;
  font-weight: bold;
  padding-bottom: 0.5em;
}

#menu-edit form {
  margin: 0 18px 0;
  /* ANIMATIONS ================ */
}

#menu-edit form #categories-choice .list {
  overflow-y: scroll;
  height: 230px;
  padding-top: 15px;
  margin-top: 15px;
  margin-bottom: 15px;
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

#menu-edit form #categories-choice .list::-webkit-scrollbar {
  width: 7px;
}

#menu-edit form #categories-choice .list::-webkit-scrollbar-thumb {
  border-radius: 50px;
  background: #a49c9c;
}

#menu-edit form #categories-choice .list::-webkit-scrollbar-track {
  background: #d5d1d1;
}

#menu-edit form #featured-image .image-preview {
  margin: 10px 0 2em 0;
  border: dashed 4px #d5d1d1;
  height: 200px;
  max-width: 300px;
  overflow: hidden;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

#menu-edit form #featured-image .image-preview img {
  width: 100%;
  z-index: 1;
}

#menu-edit form #featured-image .image-preview a {
  z-index: 0;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  width: 100px;
  margin: auto;
  text-align: center;
  color: #d5d1d1;
  font-size: 1em;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
  -webkit-transform: scale(1.5);
          transform: scale(1.5);
}

#menu-edit form #featured-image .image-preview:hover {
  border-color: #8c8181;
}

#menu-edit form #featured-image .image-preview:hover a {
  color: #8c8181;
}

#menu-edit form label {
  position: relative;
  display: block;
}

#menu-edit form label p {
  color: <?php echo $theme_master_color ?>;
  opacity: 1;
  position: absolute;
  pointer-events: none;
  left: 5px;
  top: 15px;
  -webkit-transition: all 0.2s ease-out;
  transition: all 0.2s ease-out;
}

#menu-edit form label.up p {
  top: -20px;
  font-size: 0.8em;
}

#menu-edit form label.up .bar::before, #menu-edit form label.up .bar::after {
  width: 50%;
}

#menu-edit form label.up .line-right-left {
  -webkit-animation: lineRightLeft 0.5s ease;
  animation: lineRightLeft 0.5s ease;
}

#menu-edit form .bar {
  display: block;
  width: 100%;
}

#menu-edit form .bar:before, #menu-edit form .bar:after {
  content: '';
  height: 2px;
  width: 0;
  bottom: 0;
  position: absolute;
  background: <?php echo $theme_master_color ?>;
  transition: 0.2s ease all;
  -moz-transition: 0.2s ease all;
  -webkit-transition: 0.2s ease all;
}

#menu-edit form .bar::before {
  left: 50%;
}

#menu-edit form .bar::after {
  right: 50%;
}

#menu-edit form .line-right-left {
  position: absolute;
  height: 60%;
  width: 100px;
  top: 25%;
  left: 0;
  pointer-events: none;
  opacity: 0.5;
}

#menu-edit form input {
  z-index: 2;
  font-size: 1em;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

#menu-edit form input[type="text"] {
  width: 100%;
  display: inline-block;
  border: none;
  outline: 0;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

#menu-edit form textarea {
  resize: vertical;
  width: 100%;
  border: none;
}

#menu-edit form textarea:focus {
  outline: none;
}

#menu-edit form input,
#menu-edit form textarea {
  padding: 12px 20px;
  font-size: 1em;
  font-family: 'Nunito', sans-serif !important;
}

#menu-edit form input,
#menu-edit form textarea,
#menu-edit form #form-content {
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

#menu-edit form input:focus,
#menu-edit form textarea:focus,
#menu-edit form #form-content:focus {
  -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

@-webkit-keyframes lineRightLeft {
  from {
    background: <?php echo $theme_master_color ?>;
  }
  to {
    width: 0;
    background: transparent;
  }
}

@keyframes lineRightLeft {
  from {
    background: <?php echo $theme_master_color ?>;
  }
  to {
    width: 0;
    background: transparent;
  }
}

#menu form img {
  height: 39px;
  display: none;
}

#menu form #actions {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  height: 38px;
}

#menu form #actions input {
  margin: 0;
  font-family: 'Nunito', sans-serif;
  font-size: 18px;
  padding: 0 10px;
  outline: none;
}

#menu form #actions #loader-perform {
  margin-left: 10px;
}

#menu form #actions #message-info {
  display: none;
  margin: 0;
  margin-left: 7px;
  width: auto;
  min-height: 40px;
  padding: 0 10px;
}

#menu form #actions #message-info span {
  color: #31708f;
  line-height: 38px;
}

#menu form .selects {
  margin: 0 15px 20px;
  /* Reset Select */
  /* Custom Select */
}

#menu form .selects #lists {
  margin-bottom: 0.5em;
}

#menu form .selects #lists a {
  color: <?php echo $theme_master_color ?>;
  font-weight: bold;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

#menu form .selects #lists a:hover {
  color: #242222;
}

#menu form .selects select {
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  appearance: none;
  outline: 0;
  -webkit-box-shadow: none;
          box-shadow: none;
  border: 0 !important;
  background: #eee;
  background-image: none;
  height: 100%;
  margin: 0;
  padding: 0 0 0 .5em;
  color: #000;
  cursor: pointer;
  font-size: 1em;
  width: 275px;
}

#menu form .selects select::-ms-expand {
  display: none;
}

#menu form .selects .wrapper-select {
  min-width: 275px;
  max-width: 275px;
  width: 275px;
  line-height: 2;
  position: relative;
  display: block;
  height: 38px;
  background: #eee;
  overflow: hidden;
  margin-right: 10px;
  /* Arrow */
  /* Transition */
}

#menu form .selects .wrapper-select::after {
  content: '\25BC';
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  padding: 0 1em;
  background: <?php echo $theme_master_color ?>;
  pointer-events: none;
  -webkit-transition: .25s all ease;
  transition: .25s all ease;
}

#menu form .selects .wrapper-select:hover::after {
  color: #000;
}

#menu table th label {
  padding-left: 0;
  margin-bottom: 28px;
}

#menu table th label .checker {
  top: 27px;
}

#menu table th label .label {
  margin-left: -45px;
}

@media screen and (max-width: 1700px) {
  #menu-edit form > .row {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
  }
  #menu-edit .col-2 .wrapper {
    padding-left: 0;
    -webkit-box-orient: vertical;
    -webkit-box-direction: reverse;
        -ms-flex-direction: column-reverse;
            flex-direction: column-reverse;
  }
  #menu-edit .col-2 .wrapper #button-actions {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
  }
}

.titleMenu {
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
  padding: 15px;
  margin-top: 15px;
}

.titleMenu::-webkit-scrollbar {
  width: 7px;
}

.titleMenu::-webkit-scrollbar-thumb {
  border-radius: 50px;
  background: #a49c9c;
}

.titleMenu::-webkit-scrollbar-track {
  background: #d5d1d1;
}

.titleMenu .title-choice {
  padding: 15px;
}

#navigationMenu div.buttonMenu {
  margin: 15px 15px 15px 0;
  padding: 8px 0 8px 20px;
  background-color: #d5d1d1;
  cursor: pointer;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  -webkit-transition: 0.4s;
  transition: 0.4s;
}

#navigationMenu .delete {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
}

#navigationMenu .delete .section-delete {
  cursor: pointer;
  color: <?php echo $theme_master_color ?>;
}

#navigationMenu .inputSection {
  display: none;
}

#navigationMenu .subSection input {
  z-index: 2;
  font-size: 1em;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
  padding: 12px 20px;
  font-family: 'Nunito', sans-serif !important;
}

#navigationMenu .subSection input[type="text"] {
  width: 100%;
  display: inline-block;
  border: none;
  outline: 0;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

#navigationMenu label {
  width: 70%;
  padding: 15px 0 15px 0;
  margin-left: 30px;
  margin-bottom: 30px;
}

#navigationMenu label.upSection p {
  top: -10px;
  font-size: 0.8em;
}

#navigationMenu label.upSection .bar::before, #navigationMenu label.upSection .bar::after {
  width: 50%;
}

#navigationMenu label.upSection .line-right-left {
  -webkit-animation: lineRightLeft 0.5s ease;
  animation: lineRightLeft 0.5s ease;
}

#navigationMenu .title {
  padding-left: 15px;
}

.subItem {
  display: none;
}

.buttonRight {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
}

#back-template #home-content > .row {
  margin: 0 0 0 20px;
  text-align: center !important;
}

#back-template #home-content > .row [class^="col-"] {
  padding: 0 20px 0 0 !important;
  margin-top: 20px;
}

#back-template #home-content > .row .wrapper {
  background-color: #fff;
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

#back-template #home-content > .row:last-of-type .col-6 {
  margin-top: 40px;
}

#back-template #home-content #chart2,
#back-template #home-content #viewers {
  width: 300px;
  height: 280px;
}

#back-template #home-content #chart2 > div,
#back-template #home-content #viewers > div {
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

#back-template #home-content .icon_home {
  padding: 2% 4%;
  width: 60px;
  -o-object-fit: cover;
     object-fit: cover;
  -o-object-position: center;
     object-position: center;
}

#back-template #home-content .col-3 .wrapper {
  padding: 10px;
  height: 175px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#back-template #home-content .col-3 .wrapper .content {
  margin: auto;
}

#back-template #home-content .col-3 > * {
  text-align: center;
}

#back-template #home-content .col-3 p {
  font-size: 0.96em;
}

#back-template #home-content .comment .wrapper {
  position: relative;
  height: 230px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#back-template #home-content .comment .wrapper > ul {
  margin: auto;
  font-size: 0.9em;
}

#back-template #home-content .comment .wrapper > ul li {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  margin-bottom: 5px;
}

#back-template #home-content .comment .wrapper > ul li > * {
  margin: auto;
}

#back-template #home-content #orange-block {
  position: absolute;
  top: -20px;
  left: 0;
  right: 0;
  margin: auto;
  background-color: <?php echo $theme_master_color ?>;
  margin-top: 0;
  width: 75%;
  z-index: 1;
}

#back-template #home-content #orange-block p {
  padding: 10px 5px;
}

@media screen and (max-width: 1410px) {
  #backend-template #home-content .col-3 p {
    font-size: 0.7em;
  }
  #backend-template #home-content .col-3 > * {
    float: none;
    clear: both;
    padding: 11px 9px;
  }
}

@media screen and (max-width: 1240px) {
  #back-template .col-10 {
    -ms-flex-preferred-size: 100%;
        flex-basis: 100%;
  }
}

@media screen and (max-width: 1102px) {
  #backend-template #orange-block {
    width: 87% !important;
  }
  #backend-template #orange-block p {
    font-size: 0.8em;
  }
  #backend-template .comment > ul {
    font-size: 0.7em !important;
  }
  #backend-template .icon_home {
    width: 40px !important;
  }
}

@media screen and (max-width: 821px) {
  #backend-template #home-content .comment {
    margin-top: 3.4%;
  }
}

table {
  font-family: 'Nunito', sans-serif;
  border-collapse: collapse;
  width: 98%;
  min-width: 1034px;
  margin: 0 auto;
  padding: 1em 0;
  -webkit-box-shadow: 0 0 15px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 15px rgba(0, 0, 0, 0.1);
}

table#cart-table {
  width: 100%;
}

table#orders-details {
  margin-top: 10px;
  width: 100%;
}

table#orders {
  width: 100%;
  min-width: auto;
}

table#orders td:nth-child(1),
table#orders th:nth-child(1) {
  max-width: 75px;
  word-wrap: break-word;
}

table#user-list td:nth-child(1),
table#user-list th:nth-child(1) {
  max-width: 175px;
  word-wrap: break-word;
}

table#user-list td:nth-child(3),
table#user-list th:nth-child(3) {
  max-width: 150px;
  word-wrap: break-word;
}

table#user-list td:nth-child(4),
table#user-list th:nth-child(4) {
  max-width: 150px;
  word-wrap: break-word;
}

table#product-list th:nth-child(2),
table#product-list td:nth-child(2) {
  width: 290px;
}

table#product-list th:nth-child(5),
table#product-list td:nth-child(5) {
  width: 130px;
}

table#product-list th:nth-child(5),
table#product-list td:nth-child(5) {
  width: 115px;
}

table#product-list th:nth-child(7),
table#product-list td:nth-child(7) {
  width: 275px;
}

table#article-list th:nth-child(5),
table#article-list td:nth-child(5) {
  width: 290px;
}

table#comment-list .comment-details {
  font-family: 'Nunito', sans-serif;
}

table#comment-list .comment-details td {
  text-align: justify;
}

table#comment-list .comment-details td div {
  width: 100%;
  max-width: 985px;
  margin: auto;
}

table#comment-list .comment-details td div h3 {
  color: <?php echo $theme_master_color ?>;
  margin-bottom: 5px;
  font-size: 1em;
}

table#comment-list .comment-details td div form {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

table#comment-list .comment-details td div form textarea {
  height: 200px;
  width: 100%;
  max-width: 580px;
  outline: none;
  padding: 1em;
  resize: vertical;
  font-family: "Nunito";
  font-size: 1em;
  -webkit-transition: border 0.2s ease-in-out;
  transition: border 0.2s ease-in-out;
  border: 1px solid #d5d1d1;
}

table#comment-list .comment-details td div form .submit {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

table#comment-list .comment-details td div form input {
  width: 90px;
  margin-top: 10px;
}

table#comment-list .comment-details td div form #loader-perform {
  height: 38px;
  width: 37px;
  margin-top: 10px;
  margin-left: 5px;
  display: none;
}

table#comment-list .comment-details td div form #message-info {
  max-width: 580px;
  margin-top: 10px;
  margin-left: 0;
  display: none;
}

table#comment-list .comment-details .close {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-bottom: 5px;
}

table#comment-list td:nth-child(1) {
  text-align: left;
}

table#comment-list td:nth-child(1) div[class^=comment] {
  display: none;
}

table#comment-list td:nth-child(2) {
  text-align: left;
}

table#comment-list td:nth-child(2) a {
  color: #2861ad;
  -webkit-transition: color 0.3s ease-in-out;
  transition: color 0.3s ease-in-out;
}

table#comment-list td:nth-child(2) a:hover {
  color: <?php echo $theme_master_color ?>;
}

table#comment-list th:nth-child(1),
table#comment-list td:nth-child(1) {
  width: 400px;
}

table#comment-list th:nth-child(2),
table#comment-list td:nth-child(2) {
  width: 330px;
}

th {
  font-size: 20px;
  font-weight: bold;
  background-color: #242222;
  color: <?php echo $theme_master_color ?>;
}

thead th *,
thead td * {
  font-size: 0.8em !important;
}

td,
th {
  height: 60px;
  padding: 8px;
}

tr:hover .post-edit,
tr:hover .product-edit,
tr:hover .category-edit,
tr:hover .user-edit,
tr:hover .comment-edit {
  visibility: visible;
}

tr .post-edit,
tr .product-edit,
tr .category-edit,
tr .user-edit,
tr .comment-edit {
  font-size: 0.8em;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}

tr .post-edit a,
tr .product-edit a,
tr .category-edit a,
tr .user-edit a,
tr .comment-edit a {
  margin: 0 5px;
}

tr .post-edit a:visited,
tr .product-edit a:visited,
tr .category-edit a:visited,
tr .user-edit a:visited,
tr .comment-edit a:visited {
  color: <?php echo $theme_master_color ?>;
}

tr:nth-child(odd) td {
  background-color: #f7f7f7;
}

td {
  text-align: center;
}

td span {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

td span.post-edit, td span.product-edit, td span.category-edit, td span.user-edit, td span.comment-edit {
  margin-top: 10px;
  visibility: hidden;
}

.message {
  width: 100%;
  margin: 10px 0;
  min-height: 50px;
  vertical-align: middle;
  border-radius: 5px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding: 12.5px;
  font-weight: bold;
  font-size: 18px;
}

.message.success {
  color: #155724;
  background-color: #d4edda;
  border-color: #c3e6cb;
}

.message.success .exit {
  color: #155724;
}

.message.info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}

.message.info .exit {
  color: #31708f;
}

.message.error {
  color: #721c24;
  background-color: #f8d7da;
  border-color: #f5c6cb;
}

.message.error .exit {
  color: #721c24;
}

.message .exit {
  margin-left: auto;
  text-align: center;
  font-size: 1.5em;
}

.message .exit:hover {
  cursor: pointer;
}

#page-edit .group,
#article-edit .group,
#category-edit .group,
#product-edit .group {
  position: relative;
  margin-top: 20px;
  margin-bottom: 5px;
}

#page-edit .col-2 .wrapper,
#article-edit .col-2 .wrapper,
#category-edit .col-2 .wrapper,
#product-edit .col-2 .wrapper {
  padding-left: 1em;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#page-edit .col-2 .wrapper #button-actions,
#article-edit .col-2 .wrapper #button-actions,
#category-edit .col-2 .wrapper #button-actions,
#product-edit .col-2 .wrapper #button-actions {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#page-edit .col-2 .wrapper #button-actions .button,
#article-edit .col-2 .wrapper #button-actions .button,
#category-edit .col-2 .wrapper #button-actions .button,
#product-edit .col-2 .wrapper #button-actions .button {
  text-align: center;
  margin-bottom: 10px;
}

#page-edit .col-2 .wrapper .info,
#article-edit .col-2 .wrapper .info,
#category-edit .col-2 .wrapper .info,
#product-edit .col-2 .wrapper .info {
  text-align: center;
}

#page-edit .col-2 .wrapper p,
#article-edit .col-2 .wrapper p,
#category-edit .col-2 .wrapper p,
#product-edit .col-2 .wrapper p {
  font-size: 1.2em;
  color: <?php echo $theme_master_color ?>;
  font-weight: bold;
  padding-bottom: 0.5em;
}

#page-edit form,
#article-edit form,
#category-edit form,
#product-edit form {
  margin: 0 18px 0;
  /* ANIMATIONS ================ */
}

#page-edit form #categories-choice .list,
#article-edit form #categories-choice .list,
#category-edit form #categories-choice .list,
#product-edit form #categories-choice .list {
  overflow-y: scroll;
  height: 230px;
  padding-top: 15px;
  margin-top: 15px;
  margin-bottom: 15px;
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

#page-edit form #categories-choice .list::-webkit-scrollbar,
#article-edit form #categories-choice .list::-webkit-scrollbar,
#category-edit form #categories-choice .list::-webkit-scrollbar,
#product-edit form #categories-choice .list::-webkit-scrollbar {
  width: 7px;
}

#page-edit form #categories-choice .list::-webkit-scrollbar-thumb,
#article-edit form #categories-choice .list::-webkit-scrollbar-thumb,
#category-edit form #categories-choice .list::-webkit-scrollbar-thumb,
#product-edit form #categories-choice .list::-webkit-scrollbar-thumb {
  border-radius: 50px;
  background: #a49c9c;
}

#page-edit form #categories-choice .list::-webkit-scrollbar-track,
#article-edit form #categories-choice .list::-webkit-scrollbar-track,
#category-edit form #categories-choice .list::-webkit-scrollbar-track,
#product-edit form #categories-choice .list::-webkit-scrollbar-track {
  background: #d5d1d1;
}

#page-edit form #featured-image .image-preview,
#article-edit form #featured-image .image-preview,
#category-edit form #featured-image .image-preview,
#product-edit form #featured-image .image-preview {
  margin: 10px 0 2em 0;
  border: dashed 4px #d5d1d1;
  height: 200px;
  max-width: 300px;
  overflow: hidden;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

#page-edit form #featured-image .image-preview img,
#article-edit form #featured-image .image-preview img,
#category-edit form #featured-image .image-preview img,
#product-edit form #featured-image .image-preview img {
  width: 100%;
  z-index: 1;
}

#page-edit form #featured-image .image-preview a,
#article-edit form #featured-image .image-preview a,
#category-edit form #featured-image .image-preview a,
#product-edit form #featured-image .image-preview a {
  z-index: 0;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  width: 100px;
  margin: auto;
  text-align: center;
  color: #d5d1d1;
  font-size: 1em;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
  -webkit-transform: scale(1.5);
          transform: scale(1.5);
}

#page-edit form #featured-image .image-preview:hover,
#article-edit form #featured-image .image-preview:hover,
#category-edit form #featured-image .image-preview:hover,
#product-edit form #featured-image .image-preview:hover {
  border-color: #8c8181;
}

#page-edit form #featured-image .image-preview:hover a,
#article-edit form #featured-image .image-preview:hover a,
#category-edit form #featured-image .image-preview:hover a,
#product-edit form #featured-image .image-preview:hover a {
  color: #8c8181;
}

#page-edit form label,
#article-edit form label,
#category-edit form label,
#product-edit form label {
  position: relative;
  display: block;
}

#page-edit form label p,
#article-edit form label p,
#category-edit form label p,
#product-edit form label p {
  color: <?php echo $theme_master_color ?>;
  opacity: 1;
  position: absolute;
  pointer-events: none;
  left: 5px;
  top: 12px;
  -webkit-transition: all 0.2s ease-out;
  transition: all 0.2s ease-out;
}

#page-edit form label.up p,
#article-edit form label.up p,
#category-edit form label.up p,
#product-edit form label.up p {
  top: -20px;
  font-size: 0.8em;
}

#page-edit form label.up .bar::before, #page-edit form label.up .bar::after,
#article-edit form label.up .bar::before,
#article-edit form label.up .bar::after,
#category-edit form label.up .bar::before,
#category-edit form label.up .bar::after,
#product-edit form label.up .bar::before,
#product-edit form label.up .bar::after {
  width: 50%;
}

#page-edit form label.up .line-right-left,
#article-edit form label.up .line-right-left,
#category-edit form label.up .line-right-left,
#product-edit form label.up .line-right-left {
  -webkit-animation: lineRightLeft 0.5s ease;
  animation: lineRightLeft 0.5s ease;
}

#page-edit form .bar,
#article-edit form .bar,
#category-edit form .bar,
#product-edit form .bar {
  display: block;
  width: 100%;
}

#page-edit form .bar:before, #page-edit form .bar:after,
#article-edit form .bar:before,
#article-edit form .bar:after,
#category-edit form .bar:before,
#category-edit form .bar:after,
#product-edit form .bar:before,
#product-edit form .bar:after {
  content: '';
  height: 2px;
  width: 0;
  bottom: 0;
  position: absolute;
  background: <?php echo $theme_master_color ?>;
  transition: 0.2s ease all;
  -moz-transition: 0.2s ease all;
  -webkit-transition: 0.2s ease all;
}

#page-edit form .bar::before,
#article-edit form .bar::before,
#category-edit form .bar::before,
#product-edit form .bar::before {
  left: 50%;
}

#page-edit form .bar::after,
#article-edit form .bar::after,
#category-edit form .bar::after,
#product-edit form .bar::after {
  right: 50%;
}

#page-edit form .line-right-left,
#article-edit form .line-right-left,
#category-edit form .line-right-left,
#product-edit form .line-right-left {
  position: absolute;
  height: 60%;
  width: 100px;
  top: 25%;
  left: 0;
  pointer-events: none;
  opacity: 0.5;
}

#page-edit form input,
#article-edit form input,
#category-edit form input,
#product-edit form input {
  z-index: 2;
  font-size: 1em;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

#page-edit form input[type="text"],
#article-edit form input[type="text"],
#category-edit form input[type="text"],
#product-edit form input[type="text"] {
  width: 100%;
  display: inline-block;
  border: none;
  outline: 0;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

#page-edit form textarea,
#article-edit form textarea,
#category-edit form textarea,
#product-edit form textarea {
  resize: vertical;
  width: 100%;
  border: none;
}

#page-edit form textarea:focus,
#article-edit form textarea:focus,
#category-edit form textarea:focus,
#product-edit form textarea:focus {
  outline: none;
}

#page-edit form input,
#page-edit form textarea,
#article-edit form input,
#article-edit form textarea,
#category-edit form input,
#category-edit form textarea,
#product-edit form input,
#product-edit form textarea {
  padding: 12px 20px;
  font-size: 1em;
  font-family: 'Nunito', sans-serif !important;
}

#page-edit form input,
#page-edit form textarea,
#page-edit form #form-content,
#article-edit form input,
#article-edit form textarea,
#article-edit form #form-content,
#category-edit form input,
#category-edit form textarea,
#category-edit form #form-content,
#product-edit form input,
#product-edit form textarea,
#product-edit form #form-content {
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

#page-edit form input:focus,
#page-edit form textarea:focus,
#page-edit form #form-content:focus,
#article-edit form input:focus,
#article-edit form textarea:focus,
#article-edit form #form-content:focus,
#category-edit form input:focus,
#category-edit form textarea:focus,
#category-edit form #form-content:focus,
#product-edit form input:focus,
#product-edit form textarea:focus,
#product-edit form #form-content:focus {
  -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

@-webkit-keyframes lineRightLeft {
  from {
    background: <?php echo $theme_master_color ?>;
  }
  to {
    width: 0;
    background: transparent;
  }
}

@keyframes lineRightLeft {
  from {
    background: <?php echo $theme_master_color ?>;
  }
  to {
    width: 0;
    background: transparent;
  }
}

#page form img,
#article form img,
#category form img,
#product form img {
  height: 39px;
  display: none;
}

#page form #actions,
#article form #actions,
#category form #actions,
#product form #actions {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  height: 38px;
}

#page form #actions input,
#article form #actions input,
#category form #actions input,
#product form #actions input {
  margin: 0;
  font-family: 'Nunito', sans-serif;
  font-size: 18px;
  padding: 0 10px;
  outline: none;
}

#page form #actions #loader-perform,
#article form #actions #loader-perform,
#category form #actions #loader-perform,
#product form #actions #loader-perform {
  margin-left: 10px;
}

#page form #actions #message-info,
#article form #actions #message-info,
#category form #actions #message-info,
#product form #actions #message-info {
  display: none;
  margin: 0;
  padding: 0;
  margin-left: 7px;
  width: auto;
  min-height: 40px;
  padding: 0 10px;
}

#page form #actions #message-info span,
#article form #actions #message-info span,
#category form #actions #message-info span,
#product form #actions #message-info span {
  color: #31708f;
  line-height: 38px;
}

#page form .selects,
#article form .selects,
#category form .selects,
#product form .selects {
  margin: 0 15px 20px;
  /* Reset Select */
  /* Custom Select */
}

#page form .selects #lists,
#article form .selects #lists,
#category form .selects #lists,
#product form .selects #lists {
  margin-bottom: 0.5em;
}

#page form .selects #lists a,
#article form .selects #lists a,
#category form .selects #lists a,
#product form .selects #lists a {
  color: <?php echo $theme_master_color ?>;
  font-weight: bold;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

#page form .selects #lists a:hover,
#article form .selects #lists a:hover,
#category form .selects #lists a:hover,
#product form .selects #lists a:hover {
  color: #242222;
}

#page form .selects select,
#article form .selects select,
#category form .selects select,
#product form .selects select {
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  appearance: none;
  outline: 0;
  -webkit-box-shadow: none;
          box-shadow: none;
  border: 0 !important;
  background: #eee;
  background-image: none;
  height: 100%;
  margin: 0;
  padding: 0 0 0 .5em;
  color: #000;
  cursor: pointer;
  font-size: 1em;
  width: 275px;
}

#page form .selects select::-ms-expand,
#article form .selects select::-ms-expand,
#category form .selects select::-ms-expand,
#product form .selects select::-ms-expand {
  display: none;
}

#page form .selects .wrapper-select,
#article form .selects .wrapper-select,
#category form .selects .wrapper-select,
#product form .selects .wrapper-select {
  min-width: 275px;
  max-width: 275px;
  width: 275px;
  line-height: 2;
  position: relative;
  display: block;
  height: 38px;
  background: #eee;
  overflow: hidden;
  margin-right: 10px;
  /* Arrow */
  /* Transition */
}

#page form .selects .wrapper-select::after,
#article form .selects .wrapper-select::after,
#category form .selects .wrapper-select::after,
#product form .selects .wrapper-select::after {
  content: '\25BC';
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  padding: 0 1em;
  background: <?php echo $theme_master_color ?>;
  pointer-events: none;
  -webkit-transition: .25s all ease;
  transition: .25s all ease;
}

#page form .selects .wrapper-select:hover::after,
#article form .selects .wrapper-select:hover::after,
#category form .selects .wrapper-select:hover::after,
#product form .selects .wrapper-select:hover::after {
  color: #000;
}

#page table th label,
#article table th label,
#category table th label,
#product table th label {
  padding-left: 0;
  margin-bottom: 28px;
}

#page table th label .checker,
#article table th label .checker,
#category table th label .checker,
#product table th label .checker {
  top: 27px;
}

#page table th label .label,
#article table th label .label,
#category table th label .label,
#product table th label .label {
  margin-left: -45px;
}

@media screen and (max-width: 1700px) {
  #page-edit form > .row,
  #article-edit form > .row,
  #category-edit form > .row,
  #product-edit form > .row {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
  }
  #page-edit .col-2 .wrapper,
  #article-edit .col-2 .wrapper,
  #category-edit .col-2 .wrapper,
  #product-edit .col-2 .wrapper {
    padding-left: 0;
    -webkit-box-orient: vertical;
    -webkit-box-direction: reverse;
        -ms-flex-direction: column-reverse;
            flex-direction: column-reverse;
  }
  #page-edit .col-2 .wrapper #button-actions,
  #article-edit .col-2 .wrapper #button-actions,
  #category-edit .col-2 .wrapper #button-actions,
  #product-edit .col-2 .wrapper #button-actions {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
  }
}

#back-template #settings-back h1 {
  margin: 1px 0.5;
}

#back-template #settings-back hr {
  border: 1px solid #D5D1D1;
  width: 95%;
  margin-left: 2%;
}

#back-template #settings-back .col-12 {
  width: 100%;
  border: 1px solid #D5D1D1;
  margin-left: auto;
  margin-right: auto;
  margin-top: 20px;
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

#back-template #settings-back .col-12 form input:not([type="submit"]) {
  padding: 5px 2px 5px 8px;
  border: 1px solid #d6d2d2;
}

#back-template #settings-back .col-12 form input, #back-template #settings-back .col-12 form select {
  font-family: "Nunito";
  font-size: 1em;
  border-radius: 10px;
}

#back-template #settings-back .col-12 form input:focus {
  border-color: <?php echo $theme_master_color ?>;
}

#back-template #settings-back .col-12 form .button {
  margin-top: 10px;
  margin-bottom: 10px;
}

#back-template #settings-back .col-12 form input[type="radio"] {
  display: inline-block;
  margin-left: 110px;
}

#back-template #settings-back .col-12 form input[type="file"] {
  visibility: hidden;
}

#back-template #settings-back .col-12 .radio_div, #back-template #settings-back .col-12 form input {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
      -ms-flex-direction: row;
          flex-direction: row;
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

#back-template #settings-back .col-12 .p_radio {
  text-align: center;
  margin-bottom: 6px;
  display: block;
}

#back-template #settings-back .col-12 .radio {
  margin-left: 5px;
  margin-right: 5px;
  margin-top: 2px;
  padding: 10px;
  border: 1px solid #d6d2d2;
}

#back-template #settings-back .col-12 .control-group {
  margin-left: 10px;
  margin-bottom: 10px;
}

#back-template #settings-back .col-12 .message {
  width: 60%;
  margin: 1% auto;
}

#back-template #settings-back .col-12 .message p {
  margin: auto;
}

#back-template #settings-back .col-12 #img_modal {
  max-width: 250px;
  max-height: 150px;
}

#user-content div.col-2#button {
  padding-top: 15px;
}

#user-content #button,
#user-content .button {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-top: 0;
}

#user-content .col-2:nth-child(even) {
  padding-left: 10px;
}

#user-content .col-2:nth-child(odd) {
  padding-right: 10px;
}

#user-content #table-user {
  margin-top: 3%;
}

#user-content #table-user tr > td:last-child {
  width: 5%;
}

#user-content #table-user tr {
  background-color: #efefef;
}

#user-content #table-user tr:hover {
  background-color: #d5d1d1;
  cursor: pointer;
}

#user-content #table-user th {
  height: 50px;
  color: #fff;
  background-color: #d5d1d1;
}

#user-content td,
#user-content th {
  text-align: center;
  max-width: 23.75%;
}

#user-content i {
  cursor: pointer;
}

#user-content input[type=search] {
  background: <?php echo $theme_master_color ?> url(http://localhost:3001/public/images/back/icons/search-white.svg) no-repeat 9px center;
  -webkit-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
}

#user-content #searchForm input[type=search] {
  width: 0;
  border-radius: 5px;
  padding: 9px 10px 9px 32px;
  height: 45px;
  cursor: pointer;
  border: 1px solid <?php echo $theme_master_color ?>;
}

#user-content #searchForm input[type=search]:hover {
  background: #fff url(http://localhost:3001/public/images/back/icons/search-orange.svg) no-repeat 9px center;
}

#user-content #searchForm input[type=search]:focus {
  background: #fff url(http://localhost:3001/public/images/back/icons/search-orange.svg) no-repeat 9px center;
  width: 100%;
  padding-left: 35px;
  cursor: auto;
}

@media all and (min-width: 600px) {
  #user-content .twoColumns {
    display: none;
  }
}

@media all and (max-width: 599px) {
  #user-content .fiveColumns {
    display: none;
  }
  #user-content h1 {
    text-align: center;
  }
}

#user-content div.col-2#button {
  padding-top: 15px;
}

#user-content #button,
#user-content .button {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-top: 0;
}

#user-content .col-2:nth-child(even) {
  padding-left: 10px;
}

#user-content .col-2:nth-child(odd) {
  padding-right: 10px;
}

#user-content #table-user {
  margin-top: 3%;
}

#user-content #table-user tr > td:last-child {
  width: 5%;
}

#user-content #table-user tr {
  background-color: #efefef;
}

#user-content #table-user tr:hover {
  background-color: #d5d1d1;
  cursor: pointer;
}

#user-content #table-user th {
  height: 50px;
  color: #fff;
  background-color: #d5d1d1;
}

#user-content td,
#user-content th {
  text-align: center;
  max-width: 23.75%;
}

#user-content i {
  cursor: pointer;
}

#user-content input[type=search] {
  background: <?php echo $theme_master_color ?> url(/public/images/back/icons/search-white.svg) no-repeat 9px center;
  -webkit-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
}

#user-content #searchForm input[type=search] {
  width: 0;
  border-radius: 5px;
  padding: 9px 10px 9px 32px;
  height: 45px;
  cursor: pointer;
  border: 1px solid <?php echo $theme_master_color ?>;
}

#user-content #searchForm input[type=search]:hover {
  background: #fff url(/public/images/back/icons/search-orange.svg) no-repeat 9px center;
}

#user-content #searchForm input[type=search]:focus {
  background: #fff url(/public/images/back/icons/search-orange.svg) no-repeat 9px center;
  width: 100%;
  padding-left: 35px;
  cursor: auto;
}

@media all and (min-width: 600px) {
  #user-content .twoColumns {
    display: none;
  }
}

@media all and (max-width: 599px) {
  #user-content .fiveColumns {
    display: none;
  }
  #user-content h1 {
    text-align: center;
  }
}

#front-template #site-header {
  background-color: #242222;
  padding: 0.3em;
}

#front-template #site-header .container {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#front-template #site-header .container a {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  color: #fff;
}

#front-template #site-header .container a * {
  margin: auto;
}

#front-template #site-header .container a span {
  padding: 0 .5em;
}

#front-template #site-header .container #infos {
  margin-left: auto;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#front-template #site-header .container #infos #links {
  color: #fff;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: auto;
}

#front-template #site-header .container #infos #links span:first-child {
  margin-right: 10px;
}

#front-template #site-header .container #infos #links a.profil {
  width: 30px;
}

#front-template #site-header .container #infos #links a.profil img {
  margin-right: 25px;
}

#front-template #site-header .container #infos img {
  height: 30px;
  width: 30px;
}

#front-template #site-header .container #infos a:hover {
  color: <?php echo $theme_master_color ?>;
}

#front-template #site-header .container #infos a:hover img {
  -webkit-transform: scale(1.2);
          transform: scale(1.2);
}

#front-template #site-header .container #infos #menu a {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  height: 50px;
  width: 50px;
  background-image: url("../images/front/icons/burger.svg");
  background-repeat: no-repeat;
  background-size: 70%;
  background-position: center;
}

#front-template #site-header .container #infos .dropbtn {
  padding: 5px;
  border: none;
  margin-right: 25px;
  cursor: pointer;
}

#front-template #site-header .container #infos img.dropbtn:hover,
#front-template #site-header .container #infos img.dropbtn:focus {
  -webkit-transform: scale(1.3);
          transform: scale(1.3);
}

#front-template #site-header .container #infos .dropdown {
  position: relative;
  display: inline-block;
}

#front-template #site-header .container #infos .dropdown-content {
  display: none;
  top: 50px;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  overflow: auto;
  -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 1;
}

#front-template #site-header .container #infos .dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

#front-template #site-header .container #infos .dropdown-content a:hover {
  color: <?php echo $theme_master_color ?>;
}

#front-template #site-header .container #infos .show {
  display: block;
  padding: 0 !important;
}

#front-template #site-header .container #infos p {
  display: -webkit-inline-box;
  display: -ms-inline-flexbox;
  display: inline-flex;
}

#front-template #site-navigation {
  background-color: #faf9f9;
}

#front-template #site-navigation ul {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

#front-template #site-navigation ul li {
  padding: 1em;
  margin: auto 0;
}

#front-template #site-navigation ul li a {
  color: #242222;
  font-weight: bold;
  font-size: 1.2em;
}

#front-template #site-navigation ul li a:hover {
  color: <?php echo $theme_master_color ?>;
}

#front-template #site-view-content #pagination {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: 3em 0;
}

#front-template #site-view-content #pagination .wrapper {
  margin: auto;
  position: relative;
}

#front-template #site-view-content #pagination .wrapper > a {
  position: absolute;
  height: 35px;
  width: 30px;
  -webkit-mask-repeat: no-repeat;
  background-color: #242222;
  top: 5px;
}

#front-template #site-view-content #pagination .wrapper > a.prev {
  left: -30px;
  -webkit-mask-image: url("../images/front/icons/arrow_left.svg");
  background-position: top left;
}

#front-template #site-view-content #pagination .wrapper > a.next {
  right: -40px;
  -webkit-mask-image: url("../images/front/icons/arrow_right.svg");
  background-position: top right;
}

#front-template #site-view-content #pagination .wrapper .numbers {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

#front-template #site-view-content #pagination .wrapper .numbers a {
  display: table-cell;
  vertical-align: middle;
  height: 45px;
  width: 45px;
  line-height: 45px;
  background-color: #fce9d9;
  border-right: 2px solid <?php echo $theme_master_color ?>;
  text-align: center;
  font-weight: bold;
  color: #242222;
}

#front-template #site-view-content #pagination .wrapper .numbers a.dots {
  background-color: #fff;
}

#front-template #site-view-content #pagination .wrapper .numbers a.current-page {
  background-color: <?php echo $theme_master_color ?>;
}

#front-template #site-footer {
  background-color: #242222;
  color: #fff;
  padding: 1em;
}

#front-template #site-footer form#newsletter {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#front-template #site-footer form#newsletter #img {
  margin-left: 5px;
  display: none;
}

#front-template #site-footer form#newsletter #loader-perform {
  height: 35px;
}

#front-template #site-footer form#newsletter input[name="email"] {
  padding: 7px 10px;
}

#front-template #site-footer #message-infos {
  opacity: 0;
  width: 260px;
}

#front-template #site-footer .row .col-4 {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#front-template #site-footer .row .col-4 p {
  font-size: 0.9em;
}

#front-template #site-footer .row .col-4 ul {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

#front-template #site-footer .row .col-4:first-child {
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  height: 180px;
}

#front-template #site-footer .row .col-4:first-child p {
  margin: 5px 0;
  width: -webkit-max-content;
  width: -moz-max-content;
  width: max-content;
}

#front-template #site-footer .row .col-4:nth-child(2) {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#front-template #site-footer .row .col-4:nth-child(2) ul li {
  margin: 5px;
}

#front-template #site-footer .row .col-4:nth-child(2) ul li img {
  height: 35px;
  width: 35px;
}

#front-template #site-footer .row .col-4:nth-child(2) p {
  margin: auto;
  display: -webkit-inline-box;
  display: -ms-inline-flexbox;
  display: inline-flex;
}

#front-template #site-footer .row .col-4:nth-child(3) ul {
  -webkit-box-align: end;
      -ms-flex-align: end;
          align-items: flex-end;
  margin: 0 auto;
}

#front-template #site-footer .row .col-4:nth-child(3) ul li a {
  color: #fff;
  margin: 0 0.4em;
  font-size: 0.9em;
}

#front-template #site-footer .row .col-4:nth-child(3) ul li a:hover {
  color: <?php echo $theme_master_color ?>;
}

@media screen and (max-width: 680px) {
  #front-template #site-header .container #infos #links {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
  }
  #front-template #site-header .container #infos #menu {
    display: block;
  }
  #front-template #site-navigation {
    display: none;
  }
}

@media screen and (min-width: 680px) {
  #front-template #site-header .container #infos #menu {
    display: none;
  }
}

@media screen and (max-width: 370px) {
  #front-template #site-header .container #site-logo span {
    display: none;
  }
}

#front-home #home-slider {
  position: relative;
  overflow: hidden;
  margin-top: 20px;
  height: 400px;
  border: 3px solid #F2F2F2;
}

#front-home #home-slider .slide {
  width: 1200px;
  height: 100%;
  padding: 0;
  margin: 0;
  list-style: none;
}

#front-home #home-slider .slide-element {
  position: relative;
  padding: 0;
  height: 400px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  width: 100%;
}

#front-home #home-slider .slide-element img {
  height: auto;
  width: 1200px;
  position: absolute;
  -o-object-fit: fill;
     object-fit: fill;
  left: 1200px;
}

#front-home #home-slider .slide-details {
  max-width: 1200px;
  width: 100%;
  height: 150px;
  text-align: center;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  position: absolute;
  left: 1200px;
  bottom: 130px;
  color: #fff;
  background-color: rgba(0, 0, 0, 0.4);
}

#front-home #home-slider .slide-details p {
  font-size: 1.2em;
}

#front-home #home-slider #controls a {
  z-index: 2;
  position: absolute;
  top: 45%;
}

#front-home #home-slider #controls a.control_prev {
  left: 5px;
}

#front-home #home-slider #controls a.control_next {
  right: 5px;
}

#front-home #container-product h2,
#front-home #container-article h2 {
  margin: 1em 0;
  text-align: center;
  color: <?php echo $theme_master_color ?>;
}

#front-home #container-product [class^="col-"] img,
#front-home #container-article [class^="col-"] img {
  height: 200px;
  width: 100%;
}

#front-home #container-product [class^="col-"] .wrapper,
#front-home #container-article [class^="col-"] .wrapper {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  height: 100%;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  color: #242222;
  -webkit-transition: -webkit-box-shadow 0.2s ease-in-out;
  transition: -webkit-box-shadow 0.2s ease-in-out;
  transition: box-shadow 0.2s ease-in-out;
  transition: box-shadow 0.2s ease-in-out, -webkit-box-shadow 0.2s ease-in-out;
  max-height: -webkit-max-content;
  max-height: -moz-max-content;
  max-height: max-content;
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

#front-home #container-product [class^="col-"] .wrapper:hover,
#front-home #container-article [class^="col-"] .wrapper:hover {
  -webkit-box-shadow: 0 0 15px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 15px rgba(0, 0, 0, 0.1);
}

#front-home #container-product .details,
#front-home #container-article .details {
  padding: 5px;
}

#front-home .button {
  margin: 30px auto 5px;
}

main#single-post {
  min-height: 900px;
  padding: 10px;
}

main#single-post h1 {
  margin-left: 0;
  line-height: 1;
}

main#single-post .published-date {
  font-style: italic;
  font-weight: bold;
  margin-bottom: 10px;
}

main#single-post h3 {
  margin-top: 10px;
  color: <?php echo $theme_master_color ?>;
}

main#single-post .featured-image {
  margin-top: 10px;
  width: 100%;
  max-width: 680px;
  height: 340px;
}

main#single-post #admin-message {
  margin: 10px 0 0;
}

main#single-post #admin-message .admin-show {
  max-height: -webkit-fit-content;
  max-height: -moz-fit-content;
  max-height: fit-content;
  margin: auto;
}

main#single-post #admin-message .admin-show span {
  text-align: center;
}

main#single-post #comments .single-comment {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-bottom: 10px;
  margin-top: 20px;
}

main#single-post #comments .single-comment .user-image {
  border: 1px dashed #d5d1d1;
  margin-right: 10px;
}

main#single-post #comments .single-comment .user-image,
main#single-post #comments .single-comment img {
  height: 50px;
  width: 50px;
}

main#single-post #comments .single-comment .comment-detail .comment-date {
  font-weight: bold;
}

main#single-post #comments .single-comment .comment-detail .comment-text {
  text-align: justify;
}

main#single-post #comments #message-infos {
  max-width: -webkit-fit-content;
  max-width: -moz-fit-content;
  max-width: fit-content;
}

main#single-post #comments h2 {
  color: <?php echo $theme_master_color ?>;
  margin: 10px 0 25px;
}

main#single-post #comments .response {
  margin-top: 20px;
  margin-left: -35px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

main#single-post #comment-article {
  margin-top: 20px;
}

main#single-post #comment-article .categories {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}

main#single-post #comment-article .categories a {
  background-color: rgba(242, 153, 74, 0.25);
  padding: 10px;
  border-radius: 10px;
  margin-left: 15px;
  margin-bottom: 15px;
  font-weight: bold;
  color: #242222;
  -webkit-transition: background 0.5s ease;
  transition: background 0.5s ease;
}

main#single-post #comment-article .categories a:hover {
  background-color: rgba(242, 153, 74, 0.4);
  cursor: pointer;
}

main#single-post #comment-article .connexion-required {
  text-decoration: underline;
  font-weight: bold;
  margin: 20px 0;
}

main#single-post #comment-article .connexion-required a:hover {
  color: <?php echo $theme_master_color ?>;
}

main#single-post #comment-article #message-box {
  width: 100%;
  max-width: 555px;
}

main#single-post #comment-article #message-box span {
  text-align: center;
}

main#single-post #comment-article form {
  width: 100%;
  max-width: 420px;
  margin-bottom: 40px;
}

main#single-post #comment-article form textarea {
  margin-top: 10px;
  outline: none;
  padding: 1em;
  resize: vertical;
  font-family: "Nunito";
  font-size: 1em;
  height: 130px;
  -webkit-transition: border 0.2s ease-in-out;
  transition: border 0.2s ease-in-out;
  border: 1px solid #d5d1d1;
  width: 100%;
}

main#single-post #comment-article form input[type="submit"] {
  font-size: 1em;
  width: 120px;
}

main#single-post #comment-article form .submit {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

main#single-post #comment-article form .submit img {
  height: 50px;
  margin-left: 5px;
  display: none;
}

main#articles-wrapper #all-articles .article {
  margin-bottom: 20px;
}

main#articles-wrapper #all-articles article {
  vertical-align: bottom;
  background-color: white;
  -webkit-transition: -webkit-box-shadow 0.2s ease-in-out;
  transition: -webkit-box-shadow 0.2s ease-in-out;
  transition: box-shadow 0.2s ease-in-out;
  transition: box-shadow 0.2s ease-in-out, -webkit-box-shadow 0.2s ease-in-out;
  border-bottom: 0 none;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.46);
          box-shadow: 0 1px 2px rgba(0, 0, 0, 0.46);
}

main#articles-wrapper #all-articles article .details {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-top: 10px;
  -webkit-box-pack: space-evenly;
      -ms-flex-pack: space-evenly;
          justify-content: space-evenly;
  font-style: italic;
  font-weight: bold;
}

main#articles-wrapper #all-articles article .details .comment-number {
  position: relative;
  height: 20px;
  width: 40px;
}

main#articles-wrapper #all-articles article .details span {
  position: absolute;
}

main#articles-wrapper #all-articles article .details span.icon {
  -webkit-mask-repeat: no-repeat;
          mask-repeat: no-repeat;
  background-color: <?php echo $theme_master_color ?>;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  width: 30px;
  height: 30px;
  -webkit-mask-image: url("/public/images/icons/orange/comment.svg");
          mask-image: url("/public/images/icons/orange/comment.svg");
  top: 0;
}

main#articles-wrapper #all-articles article .details span.number {
  left: 25px;
  color: <?php echo $theme_master_color ?>;
}

main#articles-wrapper #all-articles article:hover {
  border-bottom: 0 none;
  -webkit-box-shadow: 0 1px 10px rgba(0, 0, 0, 0.46);
          box-shadow: 0 1px 10px rgba(0, 0, 0, 0.46);
  cursor: pointer;
}

main#articles-wrapper #all-articles article > a {
  display: block;
  width: 100%;
  margin: auto;
  overflow: hidden;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

main#articles-wrapper #all-articles article > a .image {
  height: 215px;
  background-color: rgba(242, 153, 74, 0.25);
  overflow: hidden;
}

main#articles-wrapper #all-articles article > a .image img {
  height: 215px;
  width: 100%;
  -webkit-transition: -webkit-transform 0.3s ease-in;
  transition: -webkit-transform 0.3s ease-in;
  transition: transform 0.3s ease-in;
  transition: transform 0.3s ease-in, -webkit-transform 0.3s ease-in;
}

main#articles-wrapper #all-articles article > a .image img:hover {
  -webkit-transform: scale(1.05);
          transform: scale(1.05);
}

main#articles-wrapper #all-articles article > a .content {
  padding: 10px;
  color: #242222;
}

main#contact .container {
  width: 100%;
  max-width: 1200px;
  margin: 0 auto;
  overflow: hidden;
}

main#contact .col-12 .message {
  width: 90%;
  margin: 0 auto;
}

main#contact .col-12 .vertical-bloc {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  padding: 4%;
}

main#contact .col-12 .vertical-bloc .column:nth-child(2) {
  margin-left: 2%;
}

main#contact .col-12 .vertical-bloc .column {
  margin: 0 auto;
  width: 50%;
}

main#contact .col-12 .vertical-bloc .column div > form {
  margin: 0 auto;
  text-align: center;
}

main#contact .col-12 .vertical-bloc .column div > form p {
  position: relative;
  padding: 10px;
  width: auto;
}

main#contact .col-12 .vertical-bloc .column div > form p:before {
  width: 25px;
  content: "";
  margin: 0 15px 0;
  position: absolute;
  left: 10px;
  bottom: 0;
  top: 0;
}

main#contact .col-12 .vertical-bloc .column div > form input:not([type="submit"]), main#contact .col-12 .vertical-bloc .column div > form textarea {
  padding: 10px 15px 10px 15px;
  border: 2px solid #d6d2d2;
}

main#contact .col-12 .vertical-bloc .column div > form input[type="submit"] {
  padding: 10px;
  font-size: 1.3em;
  font-weight: bold;
  margin: 25px 0 25px 0px;
  width: 97%;
}

main#contact .col-12 .vertical-bloc .column div > form input[type="submit"]:hover {
  cursor: pointer;
}

main#contact .col-12 .vertical-bloc .column div > form input:focus, main#contact .col-12 .vertical-bloc .column div > form textarea:focus {
  outline: 0;
  border-color: <?php echo $theme_master_color ?>;
}

main#contact .col-12 .vertical-bloc .column div > form input {
  width: 100%;
  font-family: "Nunito";
  font-size: 1.2em;
  border-radius: 10px;
}

main#contact .col-12 .vertical-bloc .column div > form textarea {
  color: #838474;
  width: 97%;
  font-family: "Nunito";
  font-size: 1.2em;
  border-radius: 10px;
}

main#contact .col-12 .vertical-bloc .column div > form textarea:focus {
  color: black;
}

main#contact .col-12 .vertical-bloc .column .col-4 {
  background-color: rgba(242, 153, 74, 0.25);
  padding: 2%;
  margin-top: 2%;
}

main#contact .col-12 .vertical-bloc .column .col-4 p {
  text-align: center;
  margin-bottom: 3%;
}

main#contact .col-12 .vertical-bloc .column .col-4 h2 {
  margin-bottom: 2%;
  text-align: center;
}

main#contact .col-12 .vertical-bloc .column .col-4 .iframe iframe {
  display: block;
  margin: 0 auto;
  top: 0;
  left: 0;
  width: 100%;
}

main#contact .col-12 .vertical-bloc .column .col-4 .iframe {
  position: relative;
}

main#contact .col-12 .vertical-bloc .column .col-6 {
  text-align: center;
}

main#contact .col-12 .vertical-bloc .column .col-6 h2 {
  margin-bottom: 3%;
}

main#single-product {
  padding: 10px;
}

main#single-product > .row {
  margin-bottom: 50px;
}

main#single-product > .row:first-of-type h1 {
  margin-left: 0;
  line-height: 1.2;
}

main#single-product > .row:first-of-type h1 + img {
  height: 320px;
  width: 100%;
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
}

main#single-product > .row:first-of-type .cart {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

main#single-product > .row:first-of-type .title-image {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

main#single-product > .row:first-of-type .description {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  height: 100%;
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
  margin-left: 10px;
}

main#single-product > .row:first-of-type .description h2 {
  margin-bottom: 10px;
}

main#single-product > .row:first-of-type .description p {
  font-size: 1.2em;
}

main#single-product > .row:first-of-type #add-to-cart {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  margin-left: auto;
  margin-right: auto;
  padding: 0 50px;
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
}

main#single-product > .row:first-of-type #add-to-cart > * {
  margin-top: 60px;
}

main#single-product > .row:first-of-type #add-to-cart #add-to-cart-wrapper {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

main#single-product > .row:first-of-type #add-to-cart #add-to-cart-wrapper #loader-perform {
  height: 45px;
  display: none;
}

main#single-product > .row:first-of-type #add-to-cart #add-to-cart-wrapper #img-wrapper {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  width: 30px;
}

main#single-product > .row:first-of-type #add-to-cart .price,
main#single-product > .row:first-of-type #add-to-cart .quantity,
main#single-product > .row:first-of-type #add-to-cart .button {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

main#single-product > .row:first-of-type #add-to-cart .price div,
main#single-product > .row:first-of-type #add-to-cart .quantity div,
main#single-product > .row:first-of-type #add-to-cart .button div {
  font-weight: bold;
}

main#single-product > .row:first-of-type #add-to-cart .price div,
main#single-product > .row:first-of-type #add-to-cart .price span,
main#single-product > .row:first-of-type #add-to-cart .quantity div,
main#single-product > .row:first-of-type #add-to-cart .quantity span,
main#single-product > .row:first-of-type #add-to-cart .button div,
main#single-product > .row:first-of-type #add-to-cart .button span {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  font-size: 1.2em;
  margin: auto;
}

main#single-product > .row:first-of-type #add-to-cart .button {
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  margin-right: 5px;
}

main#single-product > .row:first-of-type #add-to-cart .price span {
  background-color: rgba(242, 153, 74, 0.25);
  padding: 10px;
  border-radius: 10px;
  margin-left: 15px;
  font-weight: bold;
}

main#single-product > .row:first-of-type #add-to-cart .quantity {
  position: relative;
}

main#single-product > .row:first-of-type #add-to-cart .quantity-details {
  height: 50px;
  width: 100px;
  border-bottom: 2px solid rgba(242, 153, 74, 0.25);
}

main#single-product > .row:first-of-type #add-to-cart .quantity-controls a {
  position: absolute;
  height: 35px;
  width: 30px;
  -webkit-mask-repeat: no-repeat;
  top: 5px;
  background-color: rgba(242, 153, 74, 0.25);
}

main#single-product > .row:first-of-type #add-to-cart .quantity-controls a.decrease {
  left: -30px;
  -webkit-mask-image: url("../images/front/icons/arrow_left.svg");
  background-position: top left;
}

main#single-product > .row:first-of-type #add-to-cart .quantity-controls a.inscrease {
  right: -40px;
  -webkit-mask-image: url("../images/front/icons/arrow_right.svg");
  background-position: top right;
}

main#single-product > .row:nth-of-type(2) {
  display: none;
}

main#single-product > .row:nth-of-type(2) .message {
  max-width: 550px;
}

main#single-product > .row:nth-of-type(3) {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  height: 100%;
}

main#single-product > .row:nth-of-type(3) .comments {
  padding-top: 20px;
  overflow: hidden;
  height: 340px;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -ms-flex-negative: 1;
      flex-shrink: 1;
  -ms-flex-preferred-size: auto;
      flex-basis: auto;
}

main#single-product > .row:nth-of-type(3) .wrapper {
  margin: auto;
  padding: 10px;
  width: 100%;
  height: 100%;
  max-width: 515px;
}

main#single-product > .row:nth-of-type(3) .wrapper .connexion-required {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  height: 100%;
  font-size: 1.5em;
  line-height: 1.5;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  text-align: center;
}

main#single-product > .row:nth-of-type(3) .wrapper .connexion-required a {
  color: <?php echo $theme_master_color ?>;
}

main#single-product > .row:nth-of-type(3) .wrapper #message-infos {
  display: none;
}

main#single-product > .row:nth-of-type(3) .lasts-3 {
  position: relative;
  height: 320px;
  overflow-y: scroll;
  padding-right: 5px;
}

main#single-product > .row:nth-of-type(3) .lasts-3::-webkit-scrollbar {
  width: 7px;
}

main#single-product > .row:nth-of-type(3) .lasts-3::-webkit-scrollbar-thumb {
  border-radius: 50px;
  background: #a49c9c;
}

main#single-product > .row:nth-of-type(3) .lasts-3::-webkit-scrollbar-track {
  background: #d5d1d1;
}

main#single-product > .row:nth-of-type(3) .lasts-3 .see-all {
  width: 100%;
  position: -webkit-sticky;
  position: sticky;
  bottom: -1px;
  border-top: 20px solid rgba(255, 255, 255, 0.7);
  -o-border-image: linear-gradient(rgba(255, 255, 255, 0.2), #fff) 100% 0;
     border-image: -webkit-gradient(linear, left top, left bottom, from(rgba(255, 255, 255, 0.2)), to(#fff)) 100% 0;
     border-image: linear-gradient(rgba(255, 255, 255, 0.2), #fff) 100% 0;
}

main#single-product > .row:nth-of-type(3) .lasts-3 .see-all span {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  width: 100%;
  height: 44.44px;
  background: -webkit-gradient(linear, left top, left bottom, from(rgba(255, 255, 255, 0.9)), to(#fff)) 100% 0;
  background: linear-gradient(rgba(255, 255, 255, 0.9), #fff) 100% 0;
  padding: 10px 30px 10px 10px;
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
}

main#single-product > .row:nth-of-type(3) .lasts-3 .see-all span:hover {
  text-decoration: underline;
  cursor: pointer;
}

main#single-product > .row:nth-of-type(3) h2 {
  color: <?php echo $theme_master_color ?>;
}

main#single-product > .row:nth-of-type(3) form {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  width: 100%;
  max-width: 515px;
}

main#single-product > .row:nth-of-type(3) form textarea {
  margin-top: 10px;
  outline: none;
  padding: 1em;
  resize: vertical;
  font-family: "Nunito";
  font-size: 1em;
  height: 130px;
  -webkit-transition: border 0.2s ease-in-out;
  transition: border 0.2s ease-in-out;
  border: 1px solid #d5d1d1;
}

main#single-product > .row:nth-of-type(3) form textarea::-webkit-scrollbar {
  width: 7px;
}

main#single-product > .row:nth-of-type(3) form textarea::-webkit-scrollbar-thumb {
  border-radius: 50px;
  background: #a49c9c;
}

main#single-product > .row:nth-of-type(3) form textarea::-webkit-scrollbar-track {
  background: #d5d1d1;
}

main#single-product > .row:nth-of-type(3) form textarea:focus {
  border: 1px solid #c9c4c4;
}

main#single-product > .row:nth-of-type(3) form .submit {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-top: 10px;
}

main#single-product > .row:nth-of-type(3) form .submit input {
  font-size: 1em;
}

main#single-product > .row:nth-of-type(3) form .submit img {
  display: none;
  height: 35px;
  margin-left: 10px;
}

main#single-product > .row:nth-of-type(3) .single-comment {
  list-style: none;
  background: rgba(251, 220, 193, 0.25);
  padding: 1em;
  position: relative;
  font-size: 1em;
  margin: 1em;
  width: 96%;
  max-width: 588px;
  overflow-y: hidden;
}

main#single-product > .row:nth-of-type(3) .single-comment p {
  line-height: 1.7;
  overflow-wrap: break-word;
}

main#single-product > .row:nth-of-type(3) .single-comment .date,
main#single-product > .row:nth-of-type(3) .single-comment .user {
  color: <?php echo $theme_master_color ?>;
  font-weight: bold;
}

main#single-product > .row:nth-of-type(4) .categories-group {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

main#single-product > .row:nth-of-type(4) .categories {
  margin-top: 20px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}

main#single-product > .row:nth-of-type(4) .categories a {
  background-color: rgba(242, 153, 74, 0.25);
  padding: 10px;
  border-radius: 10px;
  margin-left: 15px;
  margin-bottom: 15px;
  font-weight: bold;
  color: #242222;
  -webkit-transition: background 0.5s ease;
  transition: background 0.5s ease;
}

main#single-product > .row:nth-of-type(4) .categories a:hover {
  background-color: rgba(242, 153, 74, 0.4);
  cursor: pointer;
}

main#products-wrapper h1 {
  text-align: center;
}

main#products-wrapper nav#categories {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

main#products-wrapper nav#categories ul {
  margin: auto;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

main#products-wrapper nav#categories ul li {
  padding: 0.5em 0;
}

main#products-wrapper nav#categories ul li a {
  padding: 0 0.5em;
  border-left: 2px solid <?php echo $theme_master_color ?>;
  font-weight: bold;
  color: #242222;
  font-size: 1.3em;
  -webkit-transition: color 0.2s ease;
  transition: color 0.2s ease;
}

main#products-wrapper nav#categories ul li a:hover {
  color: <?php echo $theme_master_color ?>;
}

main#products-wrapper #all-products .product {
  text-align: center;
  margin: 1em 0;
}

main#products-wrapper #all-products .product a.image {
  display: block;
  height: 215px;
  width: 100%;
  background-color: rgba(242, 153, 74, 0.25);
  margin: auto;
  overflow: hidden;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

main#products-wrapper #all-products .product a.image img {
  width: 100%;
  height: 100%;
  -webkit-transition: -webkit-transform 1s ease;
  transition: -webkit-transform 1s ease;
  transition: transform 1s ease;
  transition: transform 1s ease, -webkit-transform 1s ease;
}

main#products-wrapper #all-products .product a.image img:hover {
  -webkit-transform: scale(1.1);
          transform: scale(1.1);
}

main#products-wrapper #all-products .product h3 {
  position: relative;
  padding: 0.5em;
  display: table;
  margin: auto;
}

main#products-wrapper #all-products .product h3::after {
  content: "";
  width: 40px;
  height: 3px;
  position: absolute;
  left: 10px;
  bottom: 7px;
  background-color: <?php echo $theme_master_color ?>;
}

main#products-wrapper #all-products .product p {
  background-color: rgba(242, 153, 74, 0.25);
  display: inline-block;
  padding: 0.2em 0.5em;
  border-radius: 5px;
  font-weight: bold;
}

div.view-content.container {
  max-width: 100%;
}

#view-content {
  padding: 0;
}

#patishopper-connexion {
  padding: 120px 0;
  background-image: url(../images/connexion-background.svg);
  background-repeat: no-repeat;
  background-size: cover;
  opacity: 0.8;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#patishopper-connexion .row:first-child {
  margin: auto;
}

#patishopper-connexion .row .col-12 {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#patishopper-connexion #connexion-form {
  max-width: 445px;
  width: 100%;
  background-color: #ffff;
  margin: auto;
  height: auto;
  border-radius: 10px;
}

#patishopper-connexion #connexion-form span {
  margin: 0 auto;
}

#patishopper-connexion #connexion-form #form {
  padding: 20px;
}

#patishopper-connexion #connexion-form #form h2 {
  color: rgba(130, 130, 130, 0.85);
  padding-bottom: 10px;
}

#patishopper-connexion #connexion-form form a#register {
  display: block;
  width: auto;
  margin: 20px 10px;
  padding: 10px;
  background-color: #cbc6c6;
  border: 2px solid #d5d1d1;
  color: #fff;
  font-size: 1.3em;
  font-weight: bold;
  text-align: center;
  border-radius: 10px;
}

#patishopper-connexion #connexion-form form a#register:hover {
  background-color: #d5d1d1;
  color: #fff;
  -webkit-box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
}

#patishopper-connexion #connexion-form form p {
  position: relative;
  padding: 10px;
  width: auto;
}

#patishopper-connexion #connexion-form form p#forgot-password {
  font-size: .98em;
  text-align: center;
  border-bottom: 1.5px solid #d5d1d1;
}

#patishopper-connexion #connexion-form form p#forgot-password a {
  color: #242222;
}

#patishopper-connexion #connexion-form form p#forgot-password a:hover {
  text-decoration: underline;
}

#patishopper-connexion #connexion-form form p:before {
  content: "";
  margin: 0 15px 0;
  position: absolute;
  left: 10px;
  bottom: 0;
  top: 0;
}

#patishopper-connexion #connexion-form form p:first-child:before {
  width: 25px;
  background: url("../images/icons/username.svg") center/contain no-repeat;
}

#patishopper-connexion #connexion-form form p:nth-child(2):before {
  top: -5px;
  width: 20px;
  background: url("../images/icons/password.svg") center/contain no-repeat;
}

#patishopper-connexion #connexion-form form input {
  font-family: "Nunito";
  font-size: 1em;
  color: #242222;
  border-radius: 10px;
  cursor: pointer;
  width: 100%;
}

#patishopper-connexion #connexion-form form input:not([type="submit"]) {
  padding: 10px 15px 10px 50px;
  border: 2px solid #d6d2d2;
}

#patishopper-connexion #connexion-form form input[type="submit"] {
  color: #fff;
  font-size: 1.3em;
  font-weight: bold;
}

#patishopper-connexion #connexion-form form input:hover {
  -webkit-box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
}

#patishopper-connexion #connexion-form form input:focus {
  outline: 0;
  border-color: <?php echo $theme_master_color ?>;
}

#patishopper-connexion #connexion-form form input::-webkit-input-placeholder {
  /* WebKit browsers */
  font-weight: 500;
}

#patishopper-connexion #connexion-form form input:-moz-placeholder {
  /* Mozilla Firefox 4 to 18 */
  font-weight: 500;
}

#patishopper-connexion #connexion-form form input::-moz-placeholder {
  /* Mozilla Firefox 19+ but I'm not sure about working */
  font-weight: 500;
}

#patishopper-connexion #connexion-form form input:-ms-input-placeholder {
  /* Internet Explorer 10+ */
  font-weight: 500;
}

#patishopper-connexion footer {
  min-height: 60px;
  max-height: 60px;
  height: 100px;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.85);
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  position: absolute;
  bottom: 0;
}

#patishopper-connexion footer > * {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

#patishopper-connexion footer p {
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
  margin: 0 25px;
}

#patishopper-connexion footer p span {
  margin: 0 4px;
}

#patishopper-connexion footer a,
#patishopper-connexion footer p {
  color: #fff;
  font-weight: bold;
}

#patishopper-connexion footer ul:first-of-type {
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

#patishopper-connexion footer ul:first-of-type li {
  padding: 0 .5em;
}

#patishopper-connexion footer ul:last-of-type {
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
  margin-right: 25px;
}

#patishopper-connexion footer ul:last-of-type li {
  padding: 0 .2em;
  margin-top: .2em;
}

@media screen and (max-width: 810px) {
  #patishopper-connexion footer p {
    margin-right: 0;
    width: 20px;
    font-size: .8em;
  }
  #patishopper-connexion footer p span {
    display: none;
  }
  #patishopper-connexion footer p:first-child {
    margin-left: 2px;
  }
  #patishopper-connexion footer ul img {
    height: 30px;
    width: 30px;
  }
  #patishopper-connexion footer ul li {
    font-size: .8em;
  }
}

div#patishopper-user img#captcha {
  max-width: 100%;
  height: auto;
}

div#patishopper-user div#user-form {
  max-width: 445px;
  width: 100%;
  background-color: #ffff;
  margin: auto;
  height: auto;
  border-radius: 10px;
}

div#patishopper-user div#user-form h1 {
  font-size: 2em;
  margin: 0.5em;
  color: <?php echo $theme_master_color ?>;
  text-align: center;
}

div#patishopper-user div#user-form span {
  margin: 0 auto;
}

div#patishopper-user div#user-form div#form > form p {
  position: relative;
  padding: 10px;
  width: auto;
}

div#patishopper-user div#user-form div#form > form p:before {
  width: 25px;
  content: "";
  margin: 0 15px 0;
  position: absolute;
  left: 10px;
  bottom: 0;
  top: 0;
}

div#patishopper-user div#user-form div#form > form div.control-group:first-child > p:before, div#patishopper-user div#user-form div#form > form div.control-group:nth-child(2) > p:before {
  background: url(../images/front/icons/user.svg) center/contain no-repeat;
}

div#patishopper-user div#user-form div#form > form div.control-group:nth-child(3) > p:before {
  width: 20px;
  background: url(../images/front/icons/phone.svg) center/contain no-repeat;
}

div#patishopper-user div#user-form div#form > form div.control-group:nth-child(4) > p:before, div#patishopper-user div#user-form div#form > form div.control-group:nth-child(5) > p:before {
  background: url(../images/front/icons/at.svg) center/contain no-repeat;
}

div#patishopper-user div#user-form div#form > form div.control-group:nth-child(6) > p:before, div#patishopper-user div#user-form div#form > form div.control-group:nth-child(7) > p:before,
div#patishopper-user div#user-form div#form > form div.control-group:nth-child(8) > p:before, div#patishopper-user div#user-form div#form > form div.control-group:nth-child(9) > p:before {
  background: url(../images/front/icons/address.svg) center/contain no-repeat;
}

div#patishopper-user div#user-form div#form > form div.control-group:nth-child(10) > p:before, div#patishopper-user div#user-form div#form > form div.control-group:nth-child(11) > p:before {
  background: url(../images/front/icons/password.svg) center/contain no-repeat;
}

div#patishopper-user div#user-form div#form > form div.control-group:nth-child(12) > p:before {
  background: url(../images/front/icons/arrow-alt-circle-right.svg) center/contain no-repeat;
}

div#patishopper-user div#user-form div#form > form label {
  margin-top: 5px;
  border-radius: 10px;
}

div#patishopper-user div#user-form div#form > form input:not([type="submit"]) {
  padding: 10px 15px 10px 50px;
  border: 2px solid #d6d2d2;
}

div#patishopper-user div#user-form div#form > form input[type="submit"] {
  padding: 10px;
  background-color: <?php echo $theme_master_color ?>;
  border: 2px solid <?php echo $theme_master_color ?>;
  color: #ffff;
  font-size: 1.3em;
  font-weight: bold;
  margin: 25px 0 25px 0px;
}

div#patishopper-user div#user-form div#form > form input[type="submit"]:hover {
  background-color: rgba(242, 152, 74, 0.8);
  color: #ffff;
  cursor: pointer;
}

div#patishopper-user div#user-form div#form > form input:focus {
  outline: 0;
  border-color: <?php echo $theme_master_color ?>;
}

div#patishopper-user div#user-form div#form > form input {
  width: 100%;
  font-family: "Nunito";
  font-size: 1em;
  border-radius: 10px;
}

#error-404 {
  margin-top: 1.5em;
}

#error-404 h1 {
  margin: auto;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: 0.5em auto;
  line-height: 1.2;
  text-align: center;
}

#error-404 .error-message {
  font-size: 1.5em;
  text-align: center;
}

#error-404 hr {
  border: 1px solid rgba(213, 209, 209, 0.9);
  width: 50%;
  margin: 1.5em auto;
}

#error-404 .home-button {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin-top: 1.5em;
}

#cart-content {
  min-height: 700px;
}

#cart-content hr {
  margin: 1px 0 35px 0;
  display: block;
  height: 1px;
  border: 0;
  border-top: 1px solid #ccc;
  padding: 0;
}

#cart-content #message-infos {
  width: 98%;
  max-width: 500px;
  margin: auto;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

#cart-content #message-infos span {
  text-align: center;
}

#cart-content .button-right {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

#cart-content .button-right a {
  margin-right: 0;
}

#cart-content #button-wrapper .button {
  margin-top: 30px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  max-width: 170px;
  max-width: -webkit-max-content;
  max-width: -moz-max-content;
  max-width: max-content;
}

#cart-content .checkout-infos {
  max-width: 300px;
  margin: auto;
  margin-top: 30px;
}

#cart-content .checkout-infos * {
  font-weight: bold;
}

#cart-content .cart-content-table {
  overflow: auto;
}

#cart-checkout {
  min-height: 700px;
}

#cart-checkout #message-checkout-wrapper {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

#cart-checkout #message-checkout-wrapper #messages {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  margin: auto;
  margin-top: 70px;
  max-width: 700px;
}

#cart-checkout #message-checkout-wrapper #messages p {
  margin: auto;
}

#cart-checkout #message-checkout-wrapper .button {
  margin-top: 50px;
  font-size: 1em;
}

#cart-checkout #checkout-form {
  margin: auto;
  width: 100%;
  max-width: 500px;
  padding: 10px;
}

#cart-checkout #checkout-form label {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  margin-top: 10px;
}

#cart-checkout #checkout-form label input {
  padding: 10px;
  border-radius: 10px;
}

#cart-checkout #checkout-form input[type="submit"] {
  font-size: 1em;
  font-family: "Nunito", Helvetica, sans-serif;
}

#cart-confirmation {
  min-height: 500px;
}

#cart-confirmation .title {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  max-width: -webkit-max-content;
  max-width: -moz-max-content;
  max-width: max-content;
  margin: auto;
}

#cart-confirmation .title span {
  margin: 1em 0;
}

#cart-confirmation .content {
  text-align: center;
}

#cart-confirmation .button {
  margin-top: 1.5em;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  width: -webkit-max-content;
  width: -moz-max-content;
  width: max-content;
}

#patishopper-account {
  min-height: 820px;
}

#patishopper-account #content-wrapper .inside-content .title {
  margin: 0;
}

#patishopper-account #content-wrapper .inside-content .title h1 {
  margin-left: 0;
}

#patishopper-account #content-wrapper .inside-content > .row {
  padding: 0 40px;
}

#patishopper-account #menu-select {
  padding: 10px;
}

#patishopper-account #menu-select ul li {
  border-top: 2px solid #d6d2d2;
  border-left: 2px solid #d6d2d2;
  border-right: 2px solid #d6d2d2;
  padding: 10px;
}

#patishopper-account #menu-select ul li:last-of-type {
  border-bottom: 2px solid #d6d2d2;
}

#patishopper-account #menu-select ul li a {
  color: #242222;
  font-size: 1.2em;
}

#patishopper-account #menu-select ul li a:hover, #patishopper-account #menu-select ul li a.active {
  color: <?php echo $theme_master_color ?>;
}

#patishopper-account form #informations {
  max-width: 750px;
  width: 100%;
}

#patishopper-account form p {
  margin-top: 10px;
}

#patishopper-account form p label span {
  font-weight: bold;
}

#patishopper-account form input {
  margin-top: 10px;
  border-radius: 10px;
}

#patishopper-account form input:not([type="submit"]) {
  padding: 10px 20px;
  width: 98%;
}

#patishopper-account form input[type="submit"] {
  font-size: 1em;
  font-family: "Nunito", Helvetica, sans-serif;
}

#patishopper-account #orders .see-more {
  background-position: top left;
  -webkit-mask-image: url("../images/icons/see-more.svg");
  background-color: #242222;
  -webkit-mask-repeat: no-repeat;
          mask-repeat: no-repeat;
  height: 35px;
  width: 30px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: auto;
  -webkit-transition: background-color 0.15s ease-in;
  transition: background-color 0.15s ease-in;
}

#patishopper-account #orders .see-more:hover {
  background-color: <?php echo $theme_master_color ?>;
}

#patishopper-order-details {
  min-height: 820px;
}

#patishopper-order-details h1 {
  line-height: 1.2;
}

#patishopper-order-details .button {
  margin-top: 15px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  width: 275px;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  margin: auto;
}

#patishopper-order-details .details {
  padding: 0 1.2em;
  overflow: auto;
}

#patishopper-order-details .total {
  font-size: 1.2em;
  margin-top: 15px;
  color: <?php echo $theme_master_color ?>;
  font-weight: bold;
}

@media (max-width: 720px) {
  #patishopper-account {
    min-height: 820px;
  }
  #patishopper-account #content-wrapper .inside-content .title {
    margin: 0;
  }
  #patishopper-account #content-wrapper .inside-content .title h1 {
    margin: 10px 5px;
  }
  #patishopper-account #content-wrapper .inside-content > .row {
    padding: 2px 10px;
  }
}

/*# sourceMappingURL=main.css.map */
