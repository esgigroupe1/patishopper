<?php

abstract class Helper
{
    public static function dump($pages)
    {
        echo '<pre>', print_r($pages, true), '</pre>';
    }

    /**
     * Check if current user can edit
     *
     * @return boolean
     */
    public static function isAllowedToAdmin()
    {
        return isset($_SESSION['user_type_id']) ? ($_SESSION['user_type_id'] < 3) : false;
    }

    public static function canOrder() {
        return !empty($_SESSION['user_type_id']) ? ($_SESSION['user_type_id'] >= 3) : false;
    }

    public static function canAddToChart() {
        return self::canOrder() || empty($_SESSION['id_user']);
    }

    public static function isConnected()
    {
        return !empty($_SESSION['id_user']);
    }

    public static function arrayToObject(array $array, $className)
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(serialize($array), ':')
        ));
    }

    public static function objectToArray($instance, $className)
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(strstr(serialize($instance), '"'), ':')
        ));
    }

    public static function camelCaseToSnakeCase($string)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    public static function snakeCaseToCamelCase($string)
    {
        $str = str_replace('_', '', ucwords($string, '_'));
        $str = lcfirst($str);
        return $str;
    }

    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * Check if string ends with certain string
     *
     * @param string $haystack string in which to perform the test
     * @param string $needle string to be tested in
     * @return boolean
     */
    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return $length === 0 ||
            (substr($haystack, -$length) === $needle);
    }

    /**
     * Elimite duplicated entry according to certain field
     *
     * @param array $objects collections to make unique
     * @param string $criteria field to compare
     * @return void
     */
    public static function unique(&$objects, $criteria)
    {
        $method = "get" . ucfirst(Helper::snakeCaseToCamelCase($criteria)) . "()";
        $known = [];
        $objects = array_filter($objects, function ($val) use (&$known, $method) {
            $v = $val->$method();
            $unique = (!in_array($v, $known) || $v == null);
            $known[] = $v;
            return $unique;
        });
    }

    /**
     * Filter collections according to certain condition on field
     *
     * @param array $objects collection to filter
     * @param mixed $field field to use on each object
     * @param mixed $criteria value to use in comparison
     * @return array filtered array
     */
    public static function filter($objects, $field, $criteria)
    {
        return array_filter($objects, function ($object) use ($field, $criteria) {
            return ($object->$field == $criteria);
        });
    }

    /**
     * Check if form config exits and show that value with attribute
     *
     * @param mixed $data
     * @param string $attribute
     * @return void
     */
    public static function showIfExists($data, $attribute)
    {
        return (!empty($data[$attribute])) ? $attribute . '="' . $data[$attribute] . '"' : "";
    }

    /**
     * Check if given route is valid
     *
     * @param string $pattern the route string to check
     * @return boolean
     */
    public static function isRoute($pattern)
    {
        return (strcmp(Helper::getUrl(), $pattern) == 0) ? true : false;
    }

    /**
     * Get the requested url
     *
     * @return void
     */
    public static function getURl()
    {
        return $_SERVER['REDIRECT_URL'];
    }

    /**
     * Get full site url in http or https
     *
     * @return string the full url of the site
     */
    public static function getSiteUrl()
    {
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ||
            $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'];
        return $protocol . $domainName;
    }

    /**
     * A shorter way to run a match on the array's keys rather than the values
     *
     * @param [type] $pattern
     * @param [type] $input
     * @param integer $flags
     * @return void
     */
    public static function preg_grep_keys($pattern, $input, $flags = 0)
    {
        return array_intersect_key($input, array_flip(preg_grep($pattern, array_keys($input), $flags)));
    }

    /**
     * Prevent xss
     *
     * @param string $value field value to prevent from xss
     * @return void
     */
    public static function xss($value)
    {
        return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Determine next slug number to slug for called model
     *
     * @param string $string the slug string to use to generate next slug number
     * @return void
     */
    public static function getSlug($string, $object = null, $class)
    {
        $number = 0;
        $slug = "";
        do {
            $posts = [];
            $last_char = substr($string, -1);
            if (is_numeric($last_char)) {
                $last_char += 1;
                $string = substr($string, 0, -1);
                $number = $last_char;
            } else {
                $number = "1";
            }

            $slug = rtrim($string, '-') . '-' . $number;

            $objects = (is_a($object, $class)) ? ($class::select(["slug" => $slug, "id|<>" => $object->id])) : ($class::select(["slug" => $slug]));
            //Helper::dump($posts);
            (!empty($objects)) && $string = $objects[0]->slug;
        } while (!empty($objects));

        return rtrim($string, '-') . '-' . $number;
    }

    /**
     * Retrieve a unique slug based on existing slugs in database
     *
     * @param string $string slug to check if it is unique
     * @param int $id id of existing to edit
     * @return void
     */
    public static function uniqueSlug($string, $object = null)
    {
        // Replace é by e and so on
        $unwanted_char = ['Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y'];
        $string = strtr($string, $unwanted_char);

        // filter permalink
        $slug = preg_replace('/[^a-z0-9]+/i', "-", strtolower($string));

        // check in controllers directory
        if (file_exists("controllers/" . ucfirst($string) . "Controller.class.php")) {
            $slug = $string . "-1";
        } elseif (in_array($slug, ['articles', 'products'])) {
            $slug = $slug . "-1";
        } else {
            // check in database build query
            $posts = (is_a($object, 'Post')) ? (Post::select(["slug" => $string, "id|<>" => $object->id])) : (Post::select(["slug" => $string]));

            if (empty($posts)) {
                $products = (is_a($object, 'Product')) ? (Product::select(["slug" => $string, "id|<>" => $object->id])) : (Product::select(["slug" => $string]));
                if (empty($products)) {
                    $categories = (is_a($object, 'Category')) ? (Category::select(["slug" => $string, "id|<>" => $object->id])) : (Category::select(["slug" => $string]));

                    if (empty($categories)) {
                        return $slug;
                    } else {
                        $slug = Helper::getSlug($categories[0]->getSlug(), $object, "Category");
                    }
                } else {
                    $slug = Helper::getSlug($products[0]->getSlug(), $object, "Product");
                }
            } else {
                $slug = Helper::getSlug($posts[0]->getSlug(), $object, "Post");

            }
        }
        return $slug;
    }

    /**
     * Add check prefix to array key
     *
     * @param [type] $array
     * @param [type] $prefix
     * @return void
     */
    public static function array_key_prefix(&$array, $prefix)
    {
        $array = array_combine(
            array_map(function ($key) {return ':' . $key;}, array_keys($array)),
            $array
        );
    }

    /**
     * Cut text paragraph with certain length and append dots by default at the end of string
     *
     * @param string $string string to cut
     * @param integer $length length from which to cut text
     * @param string $append string to append at the end
     * @return void
     */
    public static function truncate($string, $length = 100, $append = "&hellip;")
    {
        $string = trim($string);
        return (mb_strlen($string) > $length) ? mb_strimwidth($string, 0, $length, $append) : $string;
    }

    /**
     * Sort objects data by last updated
     *
     * @param Array $objects data to sort
     * @return void
     */
    public static function sortByLastUpdated(&$objects)
    {
        if (!empty($objects)) {
            usort($objects, function ($object_a, $object_b) {
                return $object_a->updated_at < $object_b->updated_at;
            });
        }
    }

    /**
     * Cut the keys of a array on a unique and special caracter to change in objects
     *
     * @param Array $array data to transform
     * @return array
     */
    public static function arrayKeyInArray($tab = [])
    {
        $tab_key_array = [];
        array_shift($tab);
        $tab_not_empty = array_filter($tab);
        foreach($tab_not_empty as $key => $k){
            $index = explode('_', $key);
            $tab_key_array[$index[1]][$index[0]] = $k;
        }
        $array_clean = array_values($tab_key_array);
        return $array_clean;
    }

    public static function getSitemapData()
    {
        $defaults[] = [
            'loc' => Helper::getSiteUrl(),
            'lastmod' => '2018-07-11T18:37:57+00:00',
            'priority' => '1.00',
        ];
        $defaults[] = [
            'loc' => Helper::getSiteUrl() . '/articles',
            'lastmod' => '2018-07-11T18:37:57+00:00',
            'priority' => '0.80',
        ];
        $defaults[] = [
            'loc' => Helper::getSiteUrl() . '/produits',
            'lastmod' => '2018-07-11T18:37:57+00:00',
            'priority' => '0.80',
        ];

        return $defaults;
    }

    public static function charts($table,$date_attribut) {

        //requests to fill highcharts
        switch($table)
        {
            case "visitor":
                $data = Visitor::fillChart($table,$date_attribut);
                break;
            case "order":
                $data = Order::fillChart($table,$date_attribut);
                break;
        }
        $dimanche = []; $lundi = []; $mardi= []; $mercredi = []; $jeudi = []; $vendredi = []; $samedi = [];
        foreach($data as $key => $value) {
            switch($value["jour"]){
                case 1:
                    $dimanche[] = [$value["id"]];
                    break;
                case 2:
                    $lundi[] = [$value["id"]];
                    break;
                case 3: 
                    $mardi[] = [$value["id"]];
                    break;
                case 4:
                    $mercredi[] = [$value["id"]];
                    break;
                case 5:
                    $jeudi[] = [$value["id"]];
                    break;
                case 6:
                    $vendredi[] = [$value["id"]];
                    break;
                case 7:
                    $samedi[] = [$value["id"]];
                    break;
                default:
                    //echo "jour non identifié";
            }
        }

        $data_visitor = [
                        "dimanche" => $dimanche,
                        "lundi" => $lundi,
                        "mardi" => $mardi,
                        "mercredi" => $mercredi,
                        "jeudi" => $jeudi,
                        "vendredi" => $vendredi,
                        "samedi" => $samedi
        ];
        //Replace values by the number of values before sending data
        foreach($data_visitor as $key => $value) {
            if($value != null) {
                $data_visitor[$key] = count($value);
            }
        }

        $myJSON = json_encode($data_visitor);
        echo $myJSON;
    }
}
