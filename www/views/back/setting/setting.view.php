<?php $message = $this->getAssign();?>
<main class="container-fluid" id="settings-back">
    <h1>Paramètres</h1>
    <hr class="separator">
    <div class="container">
        <div class="col-12">
            <?php if(!empty($message['messages_success'])): ?>
                <div class="message success">
                    <p><?php echo $this->data['messages_success'] ?></p>
                </div>
            <?php endif; ?>
            <br>
            <?php $this->addModal("setting", $config); ?>
            <div class="control-group">
                <figure>
                    <figcaption>Logo</figcaption>
                    <img id="img_modal" src="../<?php echo $config["img"]["url"]; ?>" alt="logo"/>
                    <input type="hidden" id="media-selected" name="logo" value="<?php echo $config["img"]["id"]; ?>" />
                </figure>
                <a id="medias" class="button">Ajouter logo</a>
                <?php $this->addModal("media", $medias); ?>
	        </div>

            <p><input type="submit" id="submit-settings" class="<?php echo $config["config"]["submit_class"];?>" value="<?php echo $config["config"]["submit"];?>" name="<?php echo $config["config"]["submit_name"];?>"></p>
            </form>
        </div>
    </div>
</main>
