<div id="patishopper-account" class="container">
   <div class="row">
        <div class="col-3 col-sm-12" id="menu-select">
            <ul>
                <li><a <?php echo ($page == 'informations') ? 'class="active"': ''?> href="<?php echo DIRNAME . "account/informations";?>">Mes informations</a></li>

                <li><a <?php echo ($page == 'orders') ? 'class="active"': ''?> href="<?php echo DIRNAME . "account/orders";?>">Mes commandes</a></li>
            </ul>
        </div> <!-- end col-2 -->

        <div class="col-9 col-sm-12" id="content-wrapper">
            <?php if($page == 'informations'):?>
                <?php $this->addModal("informations", $config); ?>
            <?php elseif($page == 'orders'): ?>
                <?php $this->addModal("orders", $config); ?>
            <?php endif; ?>
        </div> <!-- end col-10 -->
   </div> <!-- end row -->
</div><!-- end container-fluid -->
