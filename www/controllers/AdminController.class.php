<?php
/**
 * Here are all views associed to our application backend
 */
class AdminController extends Controller
{

    public function indexAction($params)
    {
        $v = new View("home", "back");
        $v->setTitle("Accueil");

        //number of orders in process
        $order = Order::select(["status" => "0"], null, "count(*) AS Nb_Orders");
        $v->assign("orders", count($order));

        //people who suscribed
        $users = User::select(["status" => "1", "user_type_id" => "3"]);
        $v->assign("users", count($users));

        //total money generated
        $money = Transaction::findAll();
        $price = 0;
        foreach ($money as $key => $value) {
            $price += $value->quantity * $value->price;
        }
        $v->assign("price", $price);

        //number of received messages
        $messages = Message::findAll();
        $v->assign("messages", count($messages));

        //last comments
        $comments_products = Comment::select(["product_id|IS NOT" => null, "post_id|IS" => null, "comment_id|IS" => null], null, "*", "created_at DESC ", "3");
        $comments_articles = Comment::select(["post_id|IS NOT" => null, "product_id|IS" => null, "comment_id|IS" => null], null, "*", "created_at DESC ", "3");
        $v->assign("comments_products", $comments_products);
        $v->assign("comments_articles", $comments_articles);
    }

    // Two methods to fill dashboard charts
    public function chart_visitorAction()
    {
        Helper::charts("visitor", "time");
    }
    public function chart_ordersAction()
    {
        Helper::charts("order", "created_at");
    }

    public function articleAction($params)
    {
        extract(Post::fetchColumns(1));

        $infos['all'] = count($all_posts);
        $infos['published'] = count($published_posts);
        $infos['draft'] = count($draft_posts);
        $infos['trash'] = count($trash_posts);
        $infos['post_type'] = "article";

        if (empty($params['URL'])) {
            $v = new View("article", "back");

            $users = User::findAll();
            $config = Post::configTableList([
                'pages' => $all_posts,
                'users' => $users,
            ]);
            // passing data to view
            $v->assign('config', $config);
            $v->assign("infos", $infos);
        } else {
            switch ($params['URL'][0]) {
                case 'edit':
                    {
                        $this->editPost($params, 1);
                        break;
                    }
                case 'add':
                    {
                        $this->addPost($params, 1);
                        break;
                    }
                case 'delete':
                    {
                        $this->deletePost($params, 1);
                        break;
                    }
                case 'move-to-bin':
                    {
                        $this->movePostToBin($params, 1);
                        break;
                    }
                case 'remove-from-bin':{
                        $this->removePostFromBin($params, 1);
                        break;
                    }
                case 'trash':
                    {
                        (!Helper::isRoute("/admin/article/trash")) && $this->errors(404);
                        $v = new View("article", "back");
                        $users = User::findAll();
                        $config = Post::configTableList([
                            'pages' => $trash_posts,
                            'users' => $users,
                        ]);
                        // passing data to view
                        $v->assign('config', $config);
                        $v->assign("infos", $infos);
                        break;
                    }

                case 'draft':
                    {
                        (!Helper::isRoute("/admin/article/draft")) && $this->errors(404);

                        $users = User::findAll();
                        $config = Post::configTableList([
                            'pages' => $draft_posts,
                            'users' => $users,
                        ]);
                        $v = new View("article", "back");
                        $v->assign('config', $config);
                        $v->assign("infos", $infos);
                        break;
                    }
                case 'published':
                    {
                        (!Helper::isRoute("/admin/article/published")) && $this->errors(404);
                        $users = User::findAll();
                        $config = Post::configTableList([
                            'pages' => $published_posts,
                            'users' => $users,
                        ]);
                        $v = new View("article", "back");
                        $v->assign('config', $config);
                        $v->assign("infos", $infos);
                        break;
                    }
                case 'selection-pan':{
                        $this->performPostSelectionPan($params, 1);
                        break;
                    }
                case 'category':
                    {
                        extract(Category::fetchColumns(1));
                        $infos['published'] = count($published_categories);
                        $infos['type'] = "article";

                        if (empty($params['URL'][1])) {
                            $v = new View("category", "back");
                            $config = Category::configTableList([
                                'categories' => $published_categories,
                            ], 1);

                            $v->assign('config', $config); // passing data to view
                            $v->assign('infos', $infos);
                        } else {
                            switch ($params['URL'][1]) {
                                case 'add':
                                    {
                                        $this->addCategory($params, 1);
                                        break;
                                    }
                                case 'edit':
                                    {
                                        $this->editCategory($params, 1);
                                        break;
                                    }
                                case 'delete':
                                    {
                                        $this->deleteCategory($params, 1);
                                        break;
                                    }
                                case 'selection-pan':{
                                        $this->performCategorySelectionPan($params, 1);
                                        break;
                                    }
                                default:
                                    {
                                        $this->errors(404);
                                        break;
                                    }
                            }
                        }

                        break;
                    }
                case 'comment':{
                        extract(Comment::fetchColumns(1));
                        $infos['all_comments'] = count($all_comments);
                        $infos['type'] = "article";

                        if (empty($params['URL'][1])) {
                            (!Helper::isRoute("/admin/article/comment")) && $this->errors(404);
                            $v = new View("comment", "back");
                            $config = Comment::configTableList([
                                'comments' => $all_comments,
                            ], 1);

                            $v->assign('config', $config); // passing data to view
                            $v->assign('infos', $infos);
                        } else {
                            switch ($params['URL'][1]) {
                                case 'add':{
                                        $this->registerCommentResponse($params);
                                        break;
                                    }
                                case 'approve':{
                                        $this->approveComment($params, 'article');
                                        break;
                                    }
                                case 'desapprove':{
                                        $this->desapproveComment($params, "article");
                                        break;
                                    }
                                case 'delete':{
                                        $this->deleteComment($params, 'article');
                                        break;
                                    }
                                default:{
                                        $this->errors(404);
                                    }
                            }
                        }
                        break;
                    }
                default:
                    {
                        // 404
                        $this->errors(404);
                        break;
                    }
            }
        }
    }

    public function productAction($params)
    {
        extract(Product::fetchColumns());

        $infos['published'] = count($published_products);
        $infos['trash'] = count($trash_products);

        if (empty($params['URL'])) {
            $v = new View("product", "back");

            $config = Product::configTableList([
                'products' => $published_products,
            ]);
            // passing data to view
            $v->assign('config', $config);
            $v->assign("infos", $infos);
        } else {
            switch ($params['URL'][0]) {
                case 'add':
                    {
                        $this->addProduct($params);
                        break;
                    }
                case 'edit':
                    {
                        $this->editProduct($params);
                        break;
                    }
                case 'trash':{
                        (!Helper::isRoute("/admin/product/trash")) && $this->errors(404);
                        $config = Product::configTableList([
                            'products' => $trash_products,
                        ]);
                        $v = new View("product", "back");
                        $v->assign('config', $config);
                        $v->assign("infos", $infos);
                        break;
                    }
                case 'comment':{
                        extract(Comment::fetchColumns(2));
                        $infos['all_comments'] = count($all_comments);
                        $infos['type'] = "article";

                        if (empty($params['URL'][1])) {
                            (!Helper::isRoute("/admin/product/comment")) && $this->errors(404);
                            $v = new View("comment", "back");
                            $config = Comment::configTableList([
                                'comments' => $all_comments,
                            ], 2);

                            $v->assign('config', $config); // passing data to view
                            $v->assign('infos', $infos);
                        } else {
                            switch ($params['URL'][1]) {
                                case 'approve':{
                                        $this->approveComment($params, 'product');
                                        break;
                                    }
                                case 'desapprove':{
                                        $this->desapproveComment($params, 'product');
                                        break;
                                    }
                                case 'delete':{
                                        $this->deleteComment($params, 'product');
                                        break;
                                    }
                                default:{
                                        $this->errors(404);
                                    }
                            }
                        }
                        break;
                    }
                case 'remove-from-bin':{
                        $this->removeProductFromBin($params);
                        break;
                    }
                case 'move-to-bin':{
                        $this->moveProductToBin($params);
                        break;
                    }
                case 'delete':{
                        $this->deleteProduct($params);
                        break;
                    }
                case 'selection-pan':{
                        $this->performProductSelectionPan($params);
                        break;
                    }
                case 'category':
                    {
                        extract(Category::fetchColumns(2));
                        $infos['published'] = count($published_categories);
                        $infos['type'] = "article";

                        if (empty($params['URL'][1])) {
                            $v = new View("category", "back");
                            $config = Category::configTableList([
                                'categories' => $published_categories,
                            ], 2);

                            $v->assign('config', $config); // passing data to view
                            $v->assign('infos', $infos);
                        } else {
                            switch ($params['URL'][1]) {
                                case 'add':
                                    {
                                        $this->addCategory($params, 2);
                                        break;
                                    }
                                case 'edit':
                                    {
                                        $this->editCategory($params, 2);
                                        break;
                                    }
                                case 'delete':
                                    {
                                        $this->deleteCategory($params, 2);
                                        break;
                                    }
                                case 'selection-pan':{
                                        $this->performCategorySelectionPan($params, 2);
                                        break;
                                    }
                                default:
                                    {
                                        $this->errors(404);
                                        break;
                                    }
                            }
                        }

                        break;
                    }
                default:
                    {
                        $this->errors(404);
                        break;
                    }
            }
        }
    }

    public function pageAction($params)
    {
        extract(Post::fetchColumns(2));

        $infos['all'] = count($all_posts);
        $infos['published'] = count($published_posts);
        $infos['draft'] = count($draft_posts);
        $infos['trash'] = count($trash_posts);
        $infos['post_type'] = "page";

        if (empty($params['URL'])) {
            $v = new View("page", "back");

            $users = User::findAll();
            $config = Post::configTableList([
                'pages' => $all_posts,
                'users' => $users,
            ]);
            // passing data to view
            $v->assign('config', $config);
            $v->assign("infos", $infos);
        } else {
            switch ($params['URL'][0]) {
                case 'edit':{
                        $this->editPost($params, 2);
                        break;
                    }
                case 'add':
                    {
                        $this->addPost($params, 2);
                        break;
                    }

                case 'delete':{
                        // security check to ajax
                        $this->deletePost($params, 2);
                        break;
                    }

                case 'move-to-bin':{
                        $this->movePostToBin($params, 2);
                        break;
                    }
                case 'remove-from-bin':{
                        $this->removePostFromBin($params, 2);
                        break;
                    }
                case 'trash':{
                        // Helper::dump($trash_posts);

                        (!Helper::isRoute("/admin/page/trash")) && $this->errors(404);
                        $users = User::findAll();
                        $config = Post::configTableList([
                            'pages' => $trash_posts,
                            'users' => $users,
                        ]);
                        $v = new View("page", "back");
                        $v->assign('config', $config);
                        $v->assign("infos", $infos);

                        break;
                    }
                case 'published':{
                        (!Helper::isRoute("/admin/page/published")) && $this->errors(404);
                        $users = User::findAll();
                        $config = Post::configTableList([
                            'pages' => $published_posts,
                            'users' => $users,
                        ]);
                        $v = new View("page", "back");
                        $v->assign('config', $config);
                        $v->assign("infos", $infos);
                        break;
                    }
                case 'draft':{
                        (!Helper::isRoute("/admin/page/draft")) && $this->errors(404);

                        $users = User::findAll();
                        $config = Post::configTableList([
                            'pages' => $draft_posts,
                            'users' => $users,
                        ]);
                        $v = new View("page", "back");
                        $v->assign('config', $config);
                        $v->assign("infos", $infos);
                        break;
                    }
                case 'selection-pan':{
                        $this->performPostSelectionPan($params, 2);
                        break;
                    }
                default:
                    {
                        // 404
                        $this->errors(404);
                        break;
                    }
            }
        }
    }

    public function mediaAction($params)
    {
        if (empty($params['URL'])) {
            //on récupère tous les medias en base pour les afficher
            $medias = Attachment::findAll();
            $v = new View("media", "back");
            $v->assign('medias', $medias);
        } else {
            switch ($params['URL'][0]) {
                case 'delete':
                    {
                        $messages = [];
                        (!Helper::isRoute("/admin/media/delete")) && $this->errors(404);
                        $directory = "public/attachments/";

                        $delete = Attachment::delete(['id' => $_POST['media_id']]);
                        if (file_exists($directory . $_POST['media_name'])) {
                            unlink($directory . $_POST['media_name']);
                        }

                        if ($delete) {
                            echo "le fichier a bien été supprimé.";
                        } else {
                            echo "Une erreur est survenue lors de la suppression du fichier.";
                        }
                        break;
                    }
                case 'add':
                    {
                        (!Helper::isRoute("/admin/media/add")) && $this->errors(404);
                        $v = new View("media-edit", "back");

                        $messages = [];
                        if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['upload']) && !empty($_FILES['upload-media'])) {
                            //contrôle sur le fichier chargé
                            $max_size = 5000000;
                            $valid_ext = ["png", "jpg", "jpeg", "pdf", "docx", "svg"];
                            $directory = "public/attachments/";

                            //on récupère les dimensions maximales pour les images en base
                            $width_image_bd = Setting::findByField(['name', 'image_width'], 'value');
                            $height_image_bd = Setting::findByField(['name', 'image_height'], 'value');

                            //si le chargement des fichiers a bien été éffectué
                            if (isset($_FILES['upload-media']['error']) && $_FILES['upload-media']['error'] > 0) {
                                foreach ($_FILES['upload-media']['size'] as $key => $size) {
                                    if ($size < $max_size || $size == $max_size) {
                                        foreach ($_FILES['upload-media']['name'] as $key1 => $name) {
                                            $transfert = false;
                                            $ext_uploadedfile = strtolower(substr(strrchr($name, '.'), 1));
                                            if (in_array($ext_uploadedfile, $valid_ext)) {
                                                foreach ($_FILES['upload-media']['tmp_name'] as $key2 => $tmpname) {
                                                    if ($ext_uploadedfile == "png" || $ext_uploadedfile == "jpg" || $ext_uploadedfile == "jpeg") {
                                                        //on récupère les dimensions de l'image pour les vérifier
                                                        $sizes = getimagesize($tmpname);
                                                        $width = $sizes[0];
                                                        $height = $sizes[1];

                                                        //Vérification
                                                        if ($width > $width_image_bd->value || $height > $height_image_bd->value) {
                                                            $messages = "Dimensions incorrectes, choississez des images entre ($height_image_bd->value × $width_image_bd->value)";
                                                            $v->assign('messages', $messages);
                                                            die();
                                                        } else {
                                                            $transfert = move_uploaded_file($tmpname, $directory . $name);
                                                        }

                                                    } else {
                                                        //on transfert les fichiers
                                                        $transfert = move_uploaded_file($tmpname, $directory . $name);
                                                    }

                                                }

                                                if ($transfert) {
                                                    //Insertion dans la BD
                                                    $media = new Attachment();
                                                    $media->setName($name);
                                                    //attribuer des images par défaut pour les pdfs et words
                                                    switch ($ext_uploadedfile) {
                                                        case 'pdf':
                                                            $media->setUrl(DIRNAME . $directory . "pdf.svg");
                                                            break;
                                                        case 'docx':
                                                            $media->setUrl(DIRNAME . $directory . "docx.svg");
                                                        default:
                                                            $media->setUrl(DIRNAME . $directory . $name);
                                                    }
                                                    $att = Attachment::findByName($name);
                                                    $media_bdd = null;
                                                    if (!empty($media_bdd)) {
                                                        $media_bdd = $att->name;
                                                    }

                                                    if (!empty($media_bdd)) {
                                                        $media_bdd->save();
                                                    } else {
                                                        $media->save();
                                                    }
                                                    $messages = "Le fichier " . $name . " a bien été ajouté à la banque de médias avec succès";
                                                    $v->assign('messages_success', $messages);
                                                }
                                            } else {
                                                $messages = "Type de fichier(s) non valide";
                                                $v->assign('messages', $messages);
                                            }
                                        }
                                    } else {
                                        $messages = "Fichier(s) trop lourd(s) la taille autorisée a été atteinte";
                                        $v->assign('messages', $messages);
                                    }
                                }
                            } else {
                                $messages = "Aucun média n'a été sélectionné";
                                $v->assign('messages', $messages);
                            }
                        }
                        break;
                    }
                default:
                    {
                        // 404
                        $this->errors(404);
                        break;
                    }
            }
        }
    }

    public function userAction($params)
    {
        if (isset($_SESSION['id_user'])) {
            $isConnected = User::findById($_SESSION['id_user']);
            if (!empty($isConnected) && $isConnected->user_type_id == 1) {
                $users = User::findAll();
                $types = UserType::findAll();
                $errors = [];
                if (empty($params['URL'])) {
                    $v = new View("user", "back");
                    $v->assign("users", $users);
                    $v->assign("types", $types);

                } elseif (!empty($params['URL'])) {

                    if (!empty($params['URL'][1]) && is_numeric($params['URL'][1])) {
                        $verify = User::select([
                            'id' => $params['URL'][1],
                        ]);
                        if (!empty($verify)) {
                            $verify = $verify[0];
                        } else {
                            $this->errors(404);
                        }
                    }

                    switch ($params['URL'][0]) {
                        case 'edit':
                        case 'add':
                            {
                                $user = (empty($verify)) ? new User() : $verify;
                                $form = (empty($verify)) ? (User::configFormAdminAdd()) : (User::configFormAdminEdit($user));
                                $title = (!empty($verify)) ? "Modifier un utilisateur" : "Ajouter un utilisateur";
                                $success = [];

                                if (!empty($user)) {

                                    if (!empty($params["POST"])) {
                                        // Verification des informations modifier

                                        $errors = Validator::validate($form, $params["POST"]);
                                        // Helper::dump($errors); die();
                                        if (empty($errors)) {

                                            if (isset($params["POST"]['firstname'])) {
                                                $user->setFirstname($params["POST"]['firstname']);
                                            }
                                            if (isset($params["POST"]['lastname'])) {
                                                $user->setLastname($params["POST"]['lastname']);
                                            }
                                            if (isset($params["POST"]['phone'])) {
                                                $user->setPhone($params["POST"]['phone']);
                                            }
                                            if (isset($params["POST"]['email'])) {
                                                $user->setEmail($params["POST"]['email']);
                                            }
                                            if (isset($params["POST"]['address'])) {
                                                $user->setAddress($params["POST"]['address']);
                                            }
                                            if (isset($params["POST"]['city'])) {
                                                $user->setCity($params["POST"]['city']);
                                            }
                                            if (isset($params["POST"]['country'])) {
                                                $user->setCountry($params["POST"]['country']);
                                            }
                                            if (isset($params["POST"]['postal_code'])) {
                                                $user->setPostalCode(intval($params["POST"]['postal_code']));
                                            }
                                            if (isset($params["POST"]['password'])) {
                                                $user->setPassword($params["POST"]['password']);
                                            }

                                            if (empty($verify)) {$user->setUserTypeId(3);}
                                            $success = "Utilisateur ajouté/modifié avec succès !";
                                            //$user->setStatus($params["POST"]['status']);
                                            $user->setAttachmentId(1);
                                            //$user->setToken();
                                            try {
                                                $user->save();
                                            } catch (Exception $e) {
                                                $errors['exception'] = "Le compte existe déjà !";
                                            }

                                            //header("Location:/admin/user"); ///edit/" . $user->id
                                        }
                                    }
                                    $v = new View("user-edit", "back");
                                    (empty($errors)) && $v->assign("success", $success);
                                    $v->assign('user', $user);
                                    $v->assign("config", $form);
                                    $v->assign("errors", $errors);
                                    $v->assign("title", $title);

                                } else {
                                    $this->errors(404);
                                }
                                break;
                            }
                        case 'delete':
                            {
                                (!Helper::isRoute("/admin/user/delete/" . $params['URL'][1])) && $this->errors(404);
                                (empty(User::findById($params['URL'][1])) && $this->errors(404));
                                if (!empty($verify)) {
                                    $user = $verify;
                                    if ($user->id !== $_SESSION['id_user']) {
                                        $user->setStatus(0);
                                        $user->save();
                                        header("Location:/admin/user");
                                    }
                                }
                                header("Location:/admin/user");
                                break;
                            }
                        case 'active':
                            {
                                (!Helper::isRoute("/admin/user/active/" . $params['URL'][1])) && $this->errors(404);
                                (empty(User::findById($params['URL'][1])) && $this->errors(404));
                                $user = (User::findById($params['URL'][1]));
                                $user->setStatus(1);
                                $user->save();
                                header("Location:/admin/user");
                                break;
                            }
                        case 'upgrade':
                            {
                                (!Helper::isRoute("/admin/user/upgrade/" . $params['URL'][1])) && $this->errors(404);
                                (empty(User::findById($params['URL'][1])) && $this->errors(404));
                                $user = (User::findById($params['URL'][1]));

                                if ($user->user_type_id > 1) {
                                    $user->setUserTypeId($user->user_type_id - 1);
                                    $user->save();
                                }
                                header("Location:/admin/user");
                                break;
                            }
                        case 'downgrade':
                            {
                                (!Helper::isRoute("/admin/user/downgrade/" . $params['URL'][1])) && $this->errors(404);
                                (empty(User::findById($params['URL'][1])) && $this->errors(404));
                                $user = (User::findById($params['URL'][1]));
                                $admins = User::select(['user_type_id' => 1, 'id|<>' => $params['URL'][1]]);

                                if (!empty($admins)) {
                                    if ($user->id !== $_SESSION['id_user']) {
                                        if ($user->user_type_id < 3) {
                                            $user->setUserTypeId($user->user_type_id + 1);
                                            $user->save();
                                        }
                                    }
                                }

                                header("Location:/admin/user");
                                break;
                            }
                        default:
                            {
                                // 404
                                $v = new View("user", "back");
                                break;
                            }
                    }
                }
            } else {
                $this->errors(404);
            }
        }
    }
    // Menu
    public function menuAction($params)
    {
        // Affichage des menus crées

        if (empty($params['URL'])) {
            $v = new View("menu", "back");
            $menus = Menu::findAll();
            $config = Menu::configTableMenuList(['menus' => $menus]);
            $v->assign("config", $config);
        } else {
            // Permet la modification
            if (($params['URL'][0] == "edit" || $params['URL'][0] == "publish") && !empty($params['URL'][1]) && is_numeric($params['URL'][1])) {
                $verify = Menu::select(['id' => $params['URL'][1]])[0];
            }

            $menu = (empty($verify)) ? new Menu() : $verify;
            $form = (empty($verify)) ? Menu::configFormMenu() : Menu::configFormMenu($menu);
            $title = (!empty($verify)) ? "Modifier un menu" : "Ajouter un menu";
            $errors = [];

            if (!empty($params['URL'])) {
                switch ($params['URL'][0]) {
                    case 'edit':
                    case 'add': // Création d'un menu avec ou sans plusieurs sections
                        {
                            if (!empty($menu)) {
                                if (!empty($params["POST"])) {
                                    $param = $params["POST"];
                                    $errors = Validator::validate($form, $param);
                                    // Vérifie que le menu n'existe pas en base; si il n'existe pas c'est un ajout sinon c'est une modification
                                    if ($params['URL'][0] == "edit") {
                                        $existingMenu = Menu::select(['id' => $params['URL'][1], 'title|<>' => $param['title']]);
                                        if (!empty($existingMenu)) {
                                            $errors['title'] = "Ce Menu existe déjà";
                                        }
                                    } elseif ($params['URL'][0] == "add") {
                                        $existingMenu = Menu::select(['title' => $param['title']]);
                                        if (!empty($existingMenu)) {
                                            $errors['title'] = "Ce Menu existe déjà";
                                        }
                                    }
                                    if (empty($errors)) {
                                        //Insertion ou Update d'un Menu
                                        $param_key = Helper::arrayKeyInArray($param);
                                        $menu->setTitle($param['title']);
                                        $menu->setUpdatedAt((new DateTime())->format('Y-m-d H:i:s'));
                                        $menu->save();

                                        foreach ($param_key as $key => $value) {
                                            //Insertion ou Update d'un ou de plusieurs section
                                            $idSection = null;
                                            if (isset($value["idSection"])) {
                                                $existingSection = Section::select(['id' => $value["idSection"], 'menu_id' => $params['URL'][1]]);
                                                $idSection = $existingSection[0];
                                            }
                                            $section = (empty($idSection)) ? new Section() : $idSection;
                                            if (!empty($section) && !empty($value["nameSection"])) {
                                                $section->setRank($key);
                                                $section->setName($value["nameSection"]);
                                                $section->setHref($value["hrefSection"]);
                                                $section->setMenuId($menu->id);
                                                $section->save();
                                            }
                                        }
                                        header("Location:/admin/menu");
                                    }
                                }
                            }

                            $v = new View("menu-edit", "back");
                            $v->assign("errors", $errors);
                            $v->assign("config", $form);
                            $v->assign("title", $title);
                            break;
                        }
                    case 'delete': // Suppression d'un menu ou d'une section
                        {
                            if (!empty($params['POST']['ajaxrequest']) && !empty($params['POST']['id']) && is_numeric($params['POST']['id']) && empty($params['POST']['name'])) {

                                (!Helper::isRoute("/admin/menu/delete/" . $params['POST']['id'])) && $this->errors(404);
                                Section::delete(['menu_id' => $params['POST']['id']]);
                                Menu::delete(['id' => $params['POST']['id']]);

                            } elseif (!empty($params['POST']['ajaxrequest']) && !empty($params['POST']['id']) && is_numeric($params['POST']['id']) && !empty($params['POST']['name'])) {
                                Section::delete(['id' => $params['POST']['id']]);
                            } else {
                                $this->errors(404);
                            }
                            break;
                        }
                    case 'publish': // Publication d'un menu
                        {
                            if (!empty($verify)) {
                                (!Helper::isRoute("/admin/menu/publish/" . $verify->id)) && $this->errors(404);
                                //Met à Non publié tout les menus
                                $menuActif = Menu::select(['status' => 1]);
                                foreach ($menuActif as $actif) {
                                    if ($actif->status == 1) {
                                        $actif->setStatus(0);
                                        $actif->save();
                                    }
                                }
                                // Publie le Menu
                                $menu = Menu::select(['id' => $params['URL'][1]])[0];
                                $menu->setStatus(1);
                                $menu->save();
                                header("Location:/admin/menu");
                            } else {
                                $this->errors(404);
                            }
                            break;
                        }
                    default:{
                            // 404
                            $this->errors(404);
                            break;
                        }
                }
            }
        }
        //
    }

    public function customizationAction($params)
    {
        if (empty($params['URL'])) {
            $v = new View("customization", "back");
        }
        // 404
    }

    public function settingAction($params)
    {
        $v = new View("setting", "back");
        if (empty($params['URL'])) {
            if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['settings_update'])) {
                //récupérer l'url du media en fonction de l'id
                $url = Attachment::findById($_POST["logo"]);

                //récupérer l'id de la page d'acceuil en fonction du titre
                $title_id = Post::findByTitle($_POST['homepage']);

                //update des champs en base
                $tab = ["site_name" => $_POST['sitename'], "slogan" => $_POST['slogan'], "homepage" => $title_id->id, "image_width" => $_POST['width'], "image_height" => $_POST['height'], "theme_back" => $_POST['theme_back'], "logo" => $url->url];

                //enregistrer les informations en base
                foreach ($tab as $key => $value) {

                    $sitename = Setting::findByName($key);
                    if (!empty($sitename)) {
                        $sitename->setName($key);
                        $sitename->setValue($value);
                        $sitename->save();
                    } else {
                        $setting = new Setting();
                        $setting->setName($key);
                        $setting->setValue($value);
                        $setting->save();
                    }
                    $messages = "les paramètres ont bien été mis à jour.";
                    $v->assign('messages_success', $messages);
                }

            }
            //on récupère la taille actuelle autorisée pour les images
            $taille = Setting::findByName('size_image');
            $setting = new Setting();

            //modal management
            $form = $setting->configFormUpdate();
            $form_media = Attachment::makeModal();

            $messages = [];

            $v->assign("config", $form);
            $v->assign("medias", $form_media);

        }

        // 404
    }

    /**
     *
     */
    public function connexionAction($params)
    {
        $v = new View("connexion", "default");
    }

    public function signupAction($params)
    {
        $v = new View("signup", "default");
    }

}
