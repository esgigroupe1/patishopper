<main class="container-fluid" id="">
    <div class="row">
        <div class="title col-10">
            <h1>Menu</h1>
        </div>
        <div class="col-2" id="button">
            <a href="<?php echo DIRNAME; ?>admin/menu/add" class="button right">Ajouter</a>
        </div>
    </div>

    <hr class="separator">

    <form method="POST" id="buttons-posts-actions">
        <div class="row">
            <div class="col-12">
                <?php $this->addModal("table", $config); ?>
            </div>
        </div>
    </form>

</main>
