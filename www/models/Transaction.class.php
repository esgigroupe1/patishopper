<?php

class Transaction extends BaseSQL
{
    protected $id = null;
    protected $quantity;
    protected $price;
    protected $order_id;
    protected $product_id;

    protected $created_at = null;
    protected $updated_at = null;

    public function __construct()
    {
        parent::__construct();
    }

    protected $relations = [
        'product' => [
            'type' => 'ManyToOne',
            'table' => 'product',
        ],
        'order' => [
            'type' => 'ManyToOne',
            'table' => 'order',
        ],
    ];

}
