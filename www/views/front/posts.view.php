<main id="articles-wrapper" class="container">
    <h1>Nos articles publiés</h1>
    <div class="container no-collapse">
        <div id="all-articles" class="row no-collapse">
        <?php foreach ($posts as $post): ?>
            <?php if(!empty($post->slug)): ?>
            <div class=" col-4 col-lg-6 col-sm-12 article">
                <article>
                    <a href="<?php echo DIRNAME . $post->slug; ?>">
                        <?php $media = $post->getAttachment();?>
                        <?php if (!empty($media)): ?>
                            <div class="image">
                                <img class="thumbnail-image" src="<?php echo $media->url ?>" title="<?php echo $media->name; ?>">
                            </div>
                        <?php endif;?>

                        <div class="content">
                            <h3><?php echo $post->title; ?></h3>
                            <p><?php echo Helper::truncate($post->text, 255); ?></p>
                            <?php
                                $date = new DateTime($post->created_at);
                                $date = $date->format("d-m-Y") . " à " . $date->format("H:i:s");
                            ?>
                            <div class="details">
                                <div class="published-date">Publié le <?php echo $date; ?></div>
                                <div class="comment-number">
                                    <?php
                                        $comments = $post->getComments();
                                        $comments = Helper::filter($comments, 'status', 1);
                                    ?>
                                    <span class="icon"></span>
                                    <span class="number"><?php echo count($comments); ?></span>
                                </div>
                            </div>
                        </div>
                    </a> <!-- end link -->
                </article>
            </div>
            <?php endif; ?>
        <?php endforeach;?>
        </div> <!-- all-products -->
    </div> <!-- end container -->
</main>
