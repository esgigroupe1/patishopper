(function ($) {
    $(document).ready(function () {
        // Perform action on link inside line
        $('a[class^="post-"], a[class^="product-"]').on('click', function (e) {
            e.preventDefault();
            var data_to_send = "";
            var urlTo = $(this).attr('href');
            var idTo = $(this).data('id');
            var randomValue = random32bit();
            var line = $(this).closest("tr");

            // alert(urlTo + '/' + idTo);
            $.ajax({
                    url: urlTo + '/' + idTo,
                    type: 'POST',
                    data: {
                        id: idTo,
                        ajaxrequest: randomValue
                    },
                    // data to send in type POST
                    // dataType: 'json',
                    // what type of data do we expect back from the server
                    encode: true,
                    beforeSend: function () {

                    },
                    complete: function () {
                        //console.log("Ajax request done");
                    }
                })
                .done(function (response) {
                    line.fadeOut("slow");
                    if (response) {
                        var data = JSON.parse(response);
                        $("#view-all span").fadeOut(function () {
                            $(this).text(data.all).fadeIn();
                        });
                        $("#view-published span").fadeOut(function () {
                            $(this).text(data.published).fadeIn();
                        });
                        $("#view-draft span").fadeOut(function () {
                            $(this).text(data.draft).fadeIn();
                        });
                        $("#view-trash span").fadeOut(function () {
                            $(this).text(data.trash).fadeIn();
                        });
                    }
                })

                // something went wrong
                .fail(function () {
                    console.log("Fail");
                })

                // after all this time ?
                .always(function () {
                    // event.target.reset();
                });
        });


        // Perform action from actionS apply button
        $('#buttons-posts-actions, #buttons-products-actions').submit(function (e) {
            e.preventDefault();
            var ajax_form_data = $(this).serialize();
            var message_info = $("#message-info span");
            var message_container = $('#message-info');
            var post_type = $('input[type="submit"]').data('post-type');

            ajax_form_data += '&ajaxrequest=' + random32bit();

            $.ajax({
                    url: "/admin/" + post_type + "/selection-pan",
                    type: 'POST',
                    data: ajax_form_data,
                    encode: true,
                    beforeSend: function () {
                        $('#loader-perform').show();
                        message_container.hide();
                        message_info.hide().text("");
                    },
                    complete: function () {
                        $('#loader-perform').hide();
                        message_container.show();
                        //console.log("Ajax request done");
                    }
                })

                .done(function (response) {
                    var response = JSON.parse(response);
                    message_info.show().text(response.message);
                    setTimeout(function () {
                        //message_container.fadeOut();
                    }, 8000);

                    if (response.data) {
                        var data = response.data;

                        // updata numbers data
                        $("#view-all span").fadeOut(function () {
                            $(this).text(data.all).fadeIn();
                        });
                        $("#view-published span").fadeOut(function () {
                            $(this).text(data.published).fadeIn();
                        });
                        $("#view-draft span").fadeOut(function () {
                            $(this).text(data.draft).fadeIn();
                        });
                        $("#view-trash span").fadeOut(function () {
                            $(this).text(data.trash).fadeIn();
                        });

                        // update table line
                        $('input.checkboxItem:checked').closest('tr').fadeOut("slow");
                    }
                })

                // something went wrong
                .fail(function () {
                    console.log("Fail");
                })

                // after all this time ?
                .always(function () {
                    // event.target.reset();
                });
        });

        // close dropdown element
        $('tbody').on('click', 'a.close', function (e) {
            e.preventDefault();
            $("#comment-list .comment-details").remove();
        });

        // Manage Category Cases
        $('a.following').on('click', function (e) {
            e.preventDefault();

            $("#comment-list .comment-details").remove();
            var text = $(this).closest('.comment-edit').find('div[class^=comment]').text();

            var comment_details = `
            <tr class="comment-details">
                <td colspan="6">
                    <a class="close" href="#">Fermer &times;</a>
                    <div>
                    <h3>Commentaire complet</h3><p>` +
                text +
                `</p>
                </div>
                </td>
            </tr>`;

            $(comment_details).insertAfter($(this).closest('tr')).hide().fadeIn(500);

            // afficher ce text dans un tr
        });

        $('a.response').on('click', function (e) {
            e.preventDefault();
            $("#comment-list .comment-details").remove();

            var post_id = $(this).data('post-id');
            var comment_id = $(this).data('comment-id');
            var comment_details = `
            <tr class="comment-details">
                <td colspan="6">
                    <a class="close" href="#">Fermer &times;</a>
                    <div>
                        <h3>Saisissez votre réponse au commentaire</h3>
                        <form id="response-form" method="POST">
                            <textarea name="reponse-comment"></textarea>

                            <div class="submit">
                                <input class="button" type="submit" value="Répondre">
                                <img id="loader-perform" src="/public/images/icons/loader.gif" alt="Enregistrement de votre avis" title="Enregistrement de votre avis">
                            </div>

                            <div id="message-info" class="message info">
                                <span></span>
                            </div>
                            <input type="hidden" name="post_id" value="` + post_id + `">
                            <input type="hidden" name="comment_id" value="` + comment_id + `">
                        </form>
                    </div>
                </td>
            </tr>`;

            $(comment_details).insertAfter($(this).closest('tr')).hide().fadeIn(500);
        });

        // Give response to comment
        $('#comment-list').on('submit', '#response-form', function (e) {
            e.preventDefault();
            var $textarea = $("#response-form textarea");
            var ajax_form_data = $(this).serialize();
            var message_info = $("#message-info span");
            var message_container = $('#message-info');
            ajax_form_data += '&ajaxrequest=' + random32bit();

            $.ajax({
                    url: "/admin/article/comment/add",
                    type: 'POST',
                    data: ajax_form_data,
                    encode: true,
                    beforeSend: function () {
                        $('#loader-perform').show();
                        message_container.hide();
                        message_info.hide().text("");
                    },
                    complete: function () {
                        $('#loader-perform').hide();
                        message_container.show();
                        //console.log("Ajax request done");
                    }
                })

                .done(function (response) {
                    var response = JSON.parse(response);
                    message_info.show().text(response.message);
                    $textarea.val(''); // empty textarea
                })

                // something went wrong
                .fail(function () {
                    console.log("Fail");
                })

                // after all this time ?
                .always(function () {
                    // event.target.reset();
                });
        });

        // Approve comment

        $('#comment-list a.approve').on('click', function (e) {
            e.preventDefault();

            var idToApprove = $(this).data('id');
            var typeObject = $(this).parent().data('type');

            $.ajax({
                    url: "/admin/" + typeObject + "/comment/approve/" + idToApprove,
                    type: 'POST',
                    data: {
                        ajaxrequest: random32bit()
                    },
                    encode: true,
                })

                .done(function (response) {
                    alert('Commentaire approuvé');
                    location.reload();
                })

                // something went wrong
                .fail(function () {
                    console.log("Fail");
                })
        });

        $('#comment-list a.desapprove').on('click', function (e) {
            e.preventDefault();

            var idToApprove = $(this).data('id');
            var typeObject = $(this).parent().data('type');

            $.ajax({
                    url: "/admin/" + typeObject + "/comment/desapprove/" + idToApprove,
                    type: 'POST',
                    data: {
                        ajaxrequest: random32bit()
                    },
                    encode: true,
                })

                .done(function (response) {
                    var response = JSON.parse(response);
                    alert(response.message);
                    location.reload();
                })

                // something went wrong
                .fail(function () {
                    console.log("Fail");
                })
        });

        $('#comment-list a.delete-comment').on('click', function (e) {
            e.preventDefault();

            var idToDelete = $(this).data('id');
            var typeObject = $(this).parent().data('type');

            respond = confirm("Ceci va supprimer le commentaire et les réponses associées. Etes-vous sûr de vouloir continuer ?");
            $.ajax({
                    url: "/admin/" + typeObject + "/comment/delete/" + idToDelete,
                    type: 'POST',
                    data: {
                        ajaxrequest: random32bit(),
                        respond: respond
                    },
                    encode: true
                })

                .done(function (response) {
                    var response = JSON.parse(response);
                    alert(response.message);
                    location.reload();
                })

                // something went wrong
                .fail(function () {
                    console.log("Fail");
                })
        });
    });

})(jQuery);
