<main id="user" class="container-fluid">
    <div class="row">
        <div class="title col-10">
            <h1>Utilisateurs</h1>
        </div>
        <div class="col-2" id="button">
            <a href="<?php echo DIRNAME; ?>admin/user/add" class="button right">Ajouter</a>
        </div>
    </div>

    <hr class="separator">

    <div class="row">
        <div class="col-12">
            <table id="user-list" class="table-list">
                <tr>
                    <th>Nom</th>
                    <th>Tél</th>
                    <th>Email</th>
                    <th>Adresse</th>
                    <th>Rôle</th>
                    <th>Photo</th>
                    <th>Créé le</th>
                    <th>Mis à jour le</th>
                    <th>Actif</th>

                </tr>

                <?php foreach ($users as $user) : $img = $user->getAttachment()?>

                    <tr>
                        <td>
                            <span class="user-title">
                            <?php
                                echo Helper::xss(strtoupper($user->lastname))." ";
                                echo Helper::xss($user->firstname);
                            ?>
                            </span>
                            <span class="user-edit">
                                <a href="/admin/user/edit/<?php echo $user->id; ?>">Modifier</a>
                                <a href="/admin/user/active/<?php echo $user->id; ?>">Activer</a>
                                <a href="/admin/user/delete/<?php echo $user->id; ?>">Désactiver</a>
                                <a href="/admin/user/upgrade/<?php echo $user->id; ?>">Promouvoir</a>
                                <a href="/admin/user/downgrade/<?php echo $user->id; ?>">Rétrograder</a>
                            </span>

                        </td>

                        <td><?php echo Helper::xss($user->phone); ?></td>
                        <td><?php echo Helper::xss($user->email); ?></td>
                        <td><?php
                                echo Helper::xss($user->address)."<br>";
                                echo Helper::xss($user->city)."<br>";
                                echo Helper::xss($user->country)."<br>";
                                echo Helper::xss($user->postal_code);
                            ?>
                        </td>
                        <td><?php echo Helper::xss($types[$user->user_type_id-1]['role']) ; ?></td>
                        <td><img src="<?php echo Helper::xss($img->url); ?>"</td>
                        <td><?php echo Helper::xss($user->created_at); ?></td>
                        <td><?php echo Helper::xss($user->updated_at); ?></td>
                        <td><?php echo Helper::xss($user->status == true ? 'Oui' : 'Non'); ?></td>

                    </tr>

                <?php endforeach; ?>
            </table>
        </div>
    </div>
</main>
