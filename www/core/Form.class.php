<?php

abstract class Form
{

    public static function get($config)
    {
        // Helper::dump($config); die();
        $form = [];
        $form['open'] =
        '<form ' . Helper::showIfExists($config["config"], "method") . ' '
        . Helper::showIfExists($config["config"], "action")
        . Helper::showIfExists($config["config"], "data-success")
        . Helper::showIfExists($config["config"], "id")
        . Helper::showIfExists($config["config"], "data-error") . '>';

        // Managing all input field
        if (!empty($config["input"])):
            foreach ($config["input"] as $name => $params):

                if (isset($params["type"])) {
                    if ($params["type"] == "text" || $params["type"] == "email" ||
                $params["type"] == "password" || $params["type"] == "tel" || $params["type"] == "hidden" || $params["type"] == "number"):

                        $form[$name] = "";

                        if (!empty($params["label"])):
                            $form[$name] = '<label>' . $params["label"];
                        endif;

                        $form[$name] .= '<input ' .
                        Helper::showIfExists($params, 'type') .
                        Helper::showIfExists($params, 'name') .
                        Helper::showIfExists($params, 'placeholder') .
                        Helper::showIfExists($params, 'id') .
                        Helper::showIfExists($params, 'class') .
                        Helper::showIfExists($params, 'min') .
                        Helper::showIfExists($params, 'max') .
                        Helper::showIfExists($params, 'pattern') .
                        Helper::showIfExists($params, 'value') .
                        Helper::showIfExists($params, 'required') .
                        Helper::showIfExists($params, 'data-success') .
                        Helper::showIfExists($params, 'data-error') . '>';

                        if (!empty($params["label"])):
                            $form[$name] .= '</label>';
                        endif;

                    elseif ($params["type"] == "radio"):
                        if (!empty($params['label'])):
                            $form[$name] = '<label class="' . $params['label'] . ' ">';
                            $form[$name] .= $params['label'];
                            $form[$name] .= '</label>';
                        endif;

                        foreach ($params["options"] as $value => $option):
                            $form[$name] .= '<label class="' . $option['class'] . '">';
                            $form[$name] .= $option["label"];
                            $form[$name] .= '<input type="' . $params["type"] . '"' .
                                'name="' . $params['name'] . '"' .
                                'value="' . $option['value'] . '"' .
                                (isset($option['checked']) ? $option['checked'] : "") . '>';
                            $form[$name] .= '</label>';
                        endforeach;

                    elseif ($params["type"] == "checkbox"):
                        $form[$name] = $params["values"];

                    elseif ($params["type"] == "file"):

                    endif;
                }

                // write all other input field here ...
            endforeach;
        endif;

        // Managing all textarea
        if (!empty($config["textarea"])):
            foreach ($config['textarea'] as $name => $params):
                $form[$name] = "";
                if (!empty($params['label'])):
                    $form[$name] = '<label>' . $params['label'];
                endif;

                $nameText = !empty($params['name']) ? $params['name'] : 'text';

                $form[$name] .= '<textarea name="' . $nameText . '" id="ckeditor" cols="50" rows="10"' . Helper::showIfExists($params, "placeholder") . '>';
                $form[$name] .= (!empty($params["value"])) ? $params["value"] : "";
                $form[$name] .= '</textarea>';

                if (!empty($params['label'])):
                    $form[$name] .= '</label>';
                endif;
            endforeach;
        endif;

        // Managing all select
        if (!empty($config["select"])):
            foreach ($config['select'] as $name => $params):
                $form[$name] = '<label>' . $params['label'] . '</label>';
                $form[$name] .= '<select
										            name="' . $params['name'] . '"
										            class="' . $params['class'] . '">';
                foreach ($params['options'] as $option):
                    $form[$name] .= '<option value="' . $option['value'] . '">' .
                        $option['label'] .
                        '</option>';
                endforeach;
                $form[$name] .= '</select>';
            endforeach;
        endif;

        if ( !empty($config["menu"]) ):
            foreach ($config['menu'] as $name => $params):
                if( isset($params["section"]) && !empty($params["section"]) ):
                    foreach($params["section"] as $key => $value):
                    $form[$key] = '<li class="subSection">';
                    $form[$key] .= '<div class="buttonMenu"><span>'.$params['section'][$key]["titleValue"].'</span>';
                    $form[$key] .= '</div>';
                    $form[$key] .= '<div class="inputSection col-6 group">';
                    $form[$key] .= '<label><p>Titre</p><span class="bar"></span><span class="line-right-left"></span>';
                    $form[$key] .= '<input type="text" class="subTitle name" id="subTitle" name="" value="'.$params['section'][$key]["titleValue"].'"></label>';
                    $form[$key] .= '<label><p>Href</p><span class="bar"></span><span class="line-right-left"></span>';
                    $form[$key] .= '<input type="text" class="subTitle href" id="subTitle" name="" value="'.$params['section'][$key]["hrefSection"].'"></label>';
                    $form[$key] .= '<input type="hidden" value="'.$params["section"][$key]["id_section"].'" class="subTitle id" id="subId">';
                    $form[$key] .= '<div class="delete"><a href="'.DIRNAME.'admin/menu/delete" data-id="'.$params["section"][$key]["id_section"].'" data-name="section" class="section-delete">Supprimer la section</a></div>';
                    $form[$key] .= '</div>';
                    $form[$key] .= '</li>';
                    endforeach;
                endif;
                //Helper::dump($form["section"]);die;
            endforeach;
        endif;

        // Submit button
        $form['submit'] = '<input class="' . $config["config"]["submit_class"] . '" type="submit"' . Helper::showIfExists($config["config"], 'class') .
            'value="' . $config["config"]["submit"] . '"' .
            'name="' . $config["config"]["submit_name"] . '">';

        // Close form
        $form['close'] = '</form>';

        return $form;

    }

    public static function show($config)
    {
        return implode("", self::get($config));
    }

}
