<?php

class Order extends BaseSQL
{
    protected $id = null;
    protected $user_id = null;
    protected $date;
    protected $status = 1;

    protected $created_at = null;
    protected $updated_at = null;

    public function __construct()
    {
        parent::__construct();
    }

    protected $relations = [
        'transactions' => [
            'type' => 'OneToMany',
            'table' => 'transaction',
            'data' => [],
        ],
        'user' => [
            'type' => 'ManyToOne',
            'table' => 'user',
        ],
    ];

    public static function configFormOrder()
    {
        $c = date('Y');
        return [

            "config" => [
                "method" => "POST",
                "action" => DIRNAME . "cart/validate",
                "submit" => "Oui, je confirme mon paiement",
                "submit_class" => "button",
                "submit_name" => "submit",
                "id" => "checkout-form"
            ],

            "input" => [

                "card-owner" => [
                    "class" => 'input',
                    "type" => "text",
                    "placeholder" => "Titulaire de la carte",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "name" => "firstname",
                    "label" => "<span>Titulaire de la carte*</span>",
                    "value" => "Patishopper Card"
                ],

                "card-number" => [
                    "class" => 'input',
                    "type" => "number",
                    "placeholder" => "Votre numéro de carte de crédit",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "name" => "card-number",
                    "pattern" => "[0-9]{4} *[0-9]{6} *[0-9]{5}",
                    "label" => "<span>Numéro de la carte*</span>",
                    "value" => "5412750123450987"
                ],

                "card-expired-month" => [
                    "class" => 'input',
                    "type" => "number",
                    "required" => false,
                    "min" => 1,
                    "max" => 12,
                    "placeholder" => "Mois",
                    "value" => "12"
                ],

                "card-expired-year" => [
                    "class" => 'input',
                    "type" => "number",
                    "min" => $c - 5,
                    "max" => $c + 5,
                    "placeholder" => "Année",
                    "required" => "required",
                    "name" => "card-expired-year",
                    "value" => "2019"
                ],

                "card-code-verification" => [
                    "class" => 'input',
                    "type" => "number",
                    "min" => "0",
                    "placeholder" => "Crytogramme(GCC)",
                    "required" => "required",
                    "name" => "address",
                    "label" => "<span>Votre code à (3) chiffres*</span>",
                    "value" => "366"
                ],
            ],
        ];
    }
}
