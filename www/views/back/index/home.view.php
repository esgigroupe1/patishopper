<main id="home-view">
        <div id="home-content">
                <div id="test"></div>
                <div class="row">
                    <div class="col-6 col-sm-12" id="viewers"></div><!-- end col-6 -->
                    <div class="col-6 col-sm-12" id="chart2"></div><!-- end col-6 -->
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-3 col-sm-6">
                        <div class="wrapper">
                            <div class="content">
                                <img class="icon_home" src="<?php echo DIRNAME . "public/images/back/home/message.png" ?>" alt="New messages">
                                <p><?php echo $messages; ?> message(s).</p>
                            </div>
                            <!-- end content -->
                        </div>
                        <!-- end wrapper -->
                    </div>
                    <!-- end col-3 -->
                    <div class="col-3 col-sm-6">
                        <div class="wrapper">
                            <div class="content">
                                <img class="icon_home" src="<?php echo DIRNAME . "public/images/back/home/orders.png" ?>" alt="New messages">
                                <p><?php echo $orders; ?> commande(s).</p>
                            </div>
                            <!-- end content -->
                        </div>
                        <!-- end wrapper -->
                    </div>
                    <!-- end col-3 -->
                    <div class="col-3 col-sm-6">
                    <div class="wrapper">
                        <div class="content">
                            <img class="icon_home" src="<?php echo DIRNAME . "public/images/back/home/users.png" ?>" alt="New messages">
                                <p><?php echo $users; ?> personne(s) inscrite(s).</p>
                        </div>
                        <!-- end content -->
                    </div>
                    <!-- end wrapper -->
                    </div>
                    <!-- end col-3 -->

                    <div class="col-3 col-sm-6">
                        <div class="wrapper">
                            <div class="content">
                                <img class="icon_home" src="<?php echo DIRNAME . "public/images/back/home/money.png" ?>" alt="New messages">
                                <p><?php echo $price; ?> euros générés.</p>
                            </div>
                            <!-- end content -->
                        </div>
                        <!-- end wrapper -->
                    </div>
                    <!-- end col-3 -->
                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-6 comment col-sm-12">
                        <div class="wrapper">
                            <div id="orange-block"><p>Derniers commentaire sur les articles </p></div>
                            <ul>
                                <?php foreach($comments_articles as $comment_article): ?>
                                    <?php $user = $comment_article->getUser(); $img = $user->getAttachment(); ?>
                                    <li>
                                        <span><?php echo Helper::truncate(Helper::xss($comment_article->content), 50); ?></span>
                                        <img src="<?php echo $img->url; ?>" alt="<?php echo $img->name; ?>">
                                    </li>
                                <?php endforeach ?> 
                            </ul>
                        </div>
                        <!-- end wrapper -->
                    </div>
                    <!-- end col-6 -->
                    <div class="col-6 comment col-sm-12">
                        <div class="wrapper">
                            <div id="orange-block"><p>Derniers commentaires sur les produits</p></div>
                            <ul>
                                <?php foreach($comments_products as $comment_product): ?>
                                    <?php $user = $comment_product->getUser(); $img = $user->getAttachment(); ?>
                                    <li>
                                        <span><?php echo Helper::truncate(Helper::xss($comment_product->content), 50); ?></span>
                                        <img src="<?php echo $img->url; ?>" alt="<?php echo $img->name; ?>">
                                    </li>
                                <?php endforeach ?> 
                            </ul>
                        </div>
                        <!-- end wrapper -->
                    </div>
                    <!-- end col-6 -->
                </div>
                <!-- end row -->
        </div>
</main>
