<?php

class DisconnectController extends Controller
{
	public function indexAction($params)
	{
		session_destroy();
       		 header("Location:".DIRNAME."authentication");
	}
}
