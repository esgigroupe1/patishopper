(function ($) {
    $(document).ready(function () {
        // Change all img tag that has svg by svg
        var $imgs = $("#overlay img");
        $imgs.each(function (index, element) {

            var $img = jQuery(this);
            var imgURL = $img.attr('src');
            $.get(imgURL, function (data) {
                var $svg = jQuery(data).find('svg');

                // Replace image with new SVG
                $($img).replaceWith($svg);

            }, 'xml');
        });

        // Active menu item
        var $menu_item = $('#overlay > ul > li');
        $menu_item.click(function () {
            // remove all .active-item and .active
            $menu_item.each(function (index) {
                $(this).removeClass('active-item');
                $(this).children("ul").removeClass('active')
            });

            // add .active-item .active on clicked item
            // $(this).children("a").addClass('active-item');
            $(this).addClass('active-item');
            $(this).children("ul").addClass('active')
        });

        // Mobile menu
        var $burger = $('#back-mobile-navigation a');
        var $mobile_menu = $('#site-navigation');
        var classes = "opened col-12 col-md-12 col-xs-12 col-sm-12";

        var run = function (e) {
            //e.preventDefault();
            if ($burger.hasClass("closed")) {
                $mobile_menu.animate({
                    left: "0",
                }, 50);
            } else {
                $mobile_menu.animate({
                    left: "-100%",
                }, 50);
            }
            $mobile_menu.toggleClass(classes);
            if ($burger.hasClass('closed')) {
                $burger.addClass('opened').removeClass('closed');
            } else {
                if ($burger.hasClass('opened')) {
                    $burger.addClass('closed').removeClass('opened');
                }
            }
        }

        $burger.click(run);

        $(window).resize(function () {
            if ($burger.hasClass('opened')) {
                if ($(window).width() >= 1240) {
                    $burger.click();
                }
            }
        });

        /**
         Remove active class on submit
        **/
        $('form').submit(function (e) {
            /*e.preventDefault();*/
            if ($(this).hasClass('active'))
                $(this).removeClass('active');
        });

        /**
            Show/Hide form inputs
        **/
        $('.search span').click(function (e) {

            var $parent = $(this).parent();

            if (!$parent.hasClass('active')) {

                $parent
                    .addClass('active')
                    .find('input:first')
                    .on('blur', function () {
                        if (!$(this).val().length) $parent.removeClass('active');
                    });

            }
        });

        /**
         * Fade Out Error Message
         */

        $(".exit").on('click', function (e) {
            e.preventDefault();
            $(this).parent().fadeOut("slow");
        });

    });

})(jQuery);
