<main class="container-fluid" id="media-back">
    <div class="row">
        <div class="title col-6">
            <h1>Médias</h1>
            
        </div>
        
        <div class="col-6" id="button">
        <div id="message" class="message info"></div>
            <form id="delete" method="POST" action="">
                    <input type="hidden" value="" name="media_name" class="media_name"/>
                    <input type="hidden" value="" name="media_id" class="media_id"/>
                    <input type="submit" class="button center" id="delete_media" name="delete" value="Supprimer" />
            </form>
            <a href="<?php echo DIRNAME; ?>admin/media/add" class="button right">Ajouter</a>
        </div>
    </div>

    <hr class="separator">

    <div class="row">
        <div class="container-fluid">
            <div class="row">
                <?php foreach($medias as $media): ?>
                    <div class="img">
                        <img id="<?php echo $media->id ?>" class="media_file" src="<?php echo $media->url ?>" title="<?php echo $media->name; ?>" alt="<?php echo $media->name; ?>">
                    </div>
                <?php endforeach; ?>
            <div>
        </div>
    </div>
</main>

