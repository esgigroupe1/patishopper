<main id="single-post" class="container post-<?php echo $data->id; ?>">
    <div class="row">
        <?php if ($data->status == 0 || $data->trash == 1): ?>
        <div id="admin-message" class="col-6 col-xs-12">
            <div class="admin-show message info">
                <span>Vous voyez ce contenu parce que vous êtes un administrateur du site !</span>
            </div>
        </div>
        <?php endif;?>

        <div class="col-9 col-xs-12">
        <?php if ($data->post_type_id == 1): ?>
            <?php $media = $data->getAttachment();?>
            <?php if (!empty($media)): ?>
                <img class="featured-image" src="<?php echo $media->url ?>" title="<?php echo $media->name; ?>">
            <?php endif;?>
        <?php endif;?>

        <?php if(Helper::isAllowedToAdmin()): ?>
            <?php $what = ($data->post_type_id == 1) ? "cet article" : "cette page"; 
                  $post = ($data->post_type_id == 1) ? "article" : "page";
            ?>
            <div style="margin: 25px 0 10px;"><a class="button" href="<?php echo DIRNAME;?>admin/<?php echo $post;?>/edit/<?php echo $data->id;?>">Modifier <?php echo $what; ?></a></div>
        <?php endif; ?>
        </div><!-- end col-9 -->
    </div> <!-- end row -->


    <div class="row">
        <div class="col-12">
            <h1><?php echo $data->title ?></h1>
            <div class="published-date">
                <?php
                    $date = new DateTime($data->created_at);
                    $date = $date->format("d-m-Y") . " à " . $date->format("H:i:s");
                    echo "Publié " . $date;
                ?>
            </div>
        </div> <!-- end col-12 -->
    </div> <!-- end row -->

    <div class="row">
        <div class="col-12">
            <div class="content">
                <?php echo $data->text ?>
            </div>
        </div> <!-- end col-12 -->
    </div> <!-- end row -->


    <?php if ($data->post_type_id == 1): ?>
        <div id="comment-article" class="row">
            <div class="col-12">
                <?php $categories = $data->getCategories(); ?>
                <div class="categories">
                    <?php foreach ($categories as $category): ?>
                        <?php if(!empty($category->slug)):?>
                            <a href="<?php echo DIRNAME . 'articles/' . $category->slug; ?>"><?php echo $category->name; ?></a>
                        <?php endif; ?>
                    <?php endforeach;?>
                </div> <!-- end categories -->
            </div>

            <div id="comments" class="col-7 col-sm-12">
                    <?php
                        $hasShow = 0;
                        $comments = $data->getComments();
                        $scomments = [];
                        Helper::sortByLastUpdated($comments);

                        foreach ($comments as $comment) {
                            $index = is_null($comment->comment_id) ? -1 : $comment->comment_id;
                            $scomments[$index][] = $comment;
                        }

                        //Helper::dump($scomments);
                        ?>
                    <?php if(!empty($comments)): ?>
                    <h2>Les commentaires récents</h2>
                    <div class="lasts">
                        <?php foreach($comments as $comment): ?>
                            <?php if(($comment->status == 1 || (isset($_SESSION['id_user']) && $comment->user_id == $_SESSION['id_user']))
                            && empty($comment->comment_id) ): ?>
                            <?php $hasShow = 1; ?>
                            <div class="single-comment">
                                <?php
                                    $date = new DateTime($comment->created_at);
                                    $date = $date->format("d-m-Y") . " à " . $date->format("H:i:s");
                                    $profil = $comment->getUser()->getAttachment();
                                ?>

                                <div class="user-image">
                                    <img src="<?php echo $profil->url; ?>" alt="<?php echo $profil->name;?>" title="<?php echo $profil->name;?>">
                                </div> <!-- end user-image -->

                                <div class="comment-detail">
                                    <div class="comment-date">
                                        <div><span class="user"><?php echo $comment->getUser()->firstname; ?></span> le <span class="date"><?php echo $date ?></span></div>
                                    </div>
                                    <p class="comment-text"><?php echo Helper::xss($comment->content); ?></p>

                                    <?php
                                        // Response
                                        $scoms = array_key_exists($comment->id, $scomments) ? $scomments[$comment->id] : null;
                                    ?>

                                    <?php if(!is_null($scoms)): ?>
                                    <?php foreach($scoms as $scomment): ?>
                                        <?php
                                            $sdate = new DateTime($scomment->created_at);
                                            $sdate = $sdate->format("d-m-Y") . " à " . $sdate->format("H:i:s");
                                            $sprofil = $scomment->getUser()->getAttachment();
                                        ?>
                                        <div class="response">
                                            <div class="user-image">
                                                <img src="<?php echo $sprofil->url; ?>" alt="<?php echo $sprofil->name;?>" title="<?php echo $sprofil->name;?>">
                                            </div> <!-- end user-image -->

                                            <div class="comment-detail">
                                                <div class="comment-date">
                                                    <div><span class="user"><?php echo $scomment->getUser()->firstname; ?></span> le <span class="date"><?php echo $sdate ?></span></div>
                                                </div>
                                                <p class="comment-text"><?php echo Helper::xss($scomment->content); ?></p>
                                            </div> <!-- end comment-detail scomment-->
                                        </div> <!-- end response -->
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </div> <!-- end comment-detail -->
                            </div> <!-- end single-comment -->
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div> <!-- end last -->
                    <?php endif; ?>
                    <?php if($hasShow == 0): ?>
                    <div id="message-info" class="message info">
                        <span>Aucun commentaire n'a encore été posté ou approuvé pour cet article</span>
                    </div>
                    <?php else: ?>
                        <div class="see-all"><span></span></div>
                    <?php endif; ?>
            </div> <!-- end col-12 -->

            <div class="col-12 col-sm-12">
                <?php if (!Helper::isConnected()): ?>
                <div class="connexion-required">
                    <span>Pour laissez un commentaire, connectez-vous s'il vous plaît en cliquant </span>
                    <a href="<?php echo DIRNAME; ?>authentication">ici</a> <span>!</span>
                </div>
                <?php else: ?>
                <h3>Laissez un commentaire</h3>
                <?php $commentForm = Form::get(Post::configCommentForm()); ?>

                <div id="message-box" class="message info">
                    <span>Les avis publiés seront visibles par tous après validation par nos équipes</span>
                </div>

                <?php echo $commentForm['open']; ?>
                <?php echo $commentForm['comment-content']; ?>
                <input type="hidden" name="article_id" value="<?php echo $data->id ?>" />
                <div class="submit">
                    <?php echo $commentForm['submit']; ?>
                    <img id="loader-perform" src="<?php echo DIRNAME; ?>public/images/icons/loader.gif" alt="Enregistrement du commentaire" title="Enregistrement du commentaire">
                </div>
                <?php echo $commentForm['close']; ?>
                <?php endif;?>
            </div> <!-- end col-12 -->
        </div> <!-- end row -->
    <?php endif;?>
</main>
