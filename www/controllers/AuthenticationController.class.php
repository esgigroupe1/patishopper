<?php
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

require 'vendor/autoload.php';

class AuthenticationController extends Controller
{
    public function indexAction($params)
    {
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $previous_url = parse_url($_SERVER['HTTP_REFERER']);
            if (!empty($previous_url['path'])) {
                $previous_page = $previous_url['path'];
                if (!in_array($previous_page, [DIRNAME . 'authentication', DIRNAME . 'admin/user'])) {
                    $_SESSION['prev_page'] = $previous_page;
                }
            }
        }

        if (empty($_SESSION['id_user'])) {

            if (!empty($params['POST'])) {
                extract($_POST);
                $user = User::findByEmail($email);

                // if user exist
                if (!empty($user) && $user->status > 0) {

                    $hash = $user->password; // its the hashed password from database

                    // check if passwords are same
                    if (password_verify($password, $hash)) {
                        // update his token
                        // $token = substr(sha1("Kgh1yOj4Nbtre" . $user->email . substr(time(), 5) . uniqid() . "5dqd84e98"), 2, 10);
                        $user->setToken();
                        $user->save();

                        // $_SESSION['user'] = $user;

                        // ($user->user_type_id == 3)?$v = new View("index", "front"):$v = new View("home", "back");

                        // create session variable with his firstname, id and picture
                        $_SESSION["firstname"] = $user->firstname;
                        $_SESSION["id_user"] = $user->id;
                        $_SESSION['user_type_id'] = $user->user_type_id;

                        // add his picture
                        $attachment = Attachment::findById($user->attachment_id);
                        if (!empty($attachment)) {
                            $_SESSION["attachment_user"] = $attachment->url;
                        }

                        // if its an admin create a session variable for admin
                        if ($user->status == 1) {
                            $_SESSION["token"] = $user->token;
                            (!empty($_SESSION['prev_page'])) ? header("Location:" . $_SESSION['prev_page']) : header("Location:" . DIRNAME);
                        }

                        else {
                            header("Location:" . DIRNAME);
                        }
                    }
                    // else wrong password and return error 401
                    else {
                        $v = new View("authentication", "front");
                        $v->assign("Erreur 401", "Mot de passe incorrect");
                    }
                }
                // else wrong login and return error 401
                else {
                    if (!empty($user)) {
                        $v = new View("authentication", "front");
                        header('HTTP/1.0 403 Not authorized', true, 403);
                        $v->assign("Erreur 403", "Compte désactivé (vérifiez vos emails)");
                    } else {
                        $v = new View("authentication", "front");
                        header('HTTP/1.0 401 User not logged', true, 401);
                        $v->assign("Erreur 401", "Login incorrect");
                    }

                }
            } else {
                $v = new View("authentication", "front");
            }
        } else {
            header("Location:" . DIRNAME);
        }
    }

    public function changePasswordAction($params)
    {
        $user = new User();
        $form = $user->configFormChangePwd();
        $errors = [];
        $validate = "";
        if (!empty($params["POST"])) {

            $user_verify = User::findById($_SESSION['id_user']);
            $hash = (!empty($user_verify)) ? $user_verify->password : null;

            if ($hash !== null) {

                $errors = Validator::validate($form, $params["POST"]);

                if (strtolower($params['POST']['captcha']) == $_SESSION['captcha']) {

                    if (empty($errors) && password_verify($params['POST']['currentPassword'], $hash)) {
                        $user_verify->setPassword($params["POST"]["password"]);
                        $user_verify->save();

                        $validate = "Votre mot de passe a été modifié avec succès.";
                    } elseif (!password_verify($params['POST']['currentPassword'], $hash)) {
                    $errors['wrong-current-password'] = "mauvais mot de passe actuel";
                    }
                } else {
                    $errors['captcha'] = "Le captcha est incorrect";
                }
            } else {
                $validate = "Vous n'êtes pas connecté.";
            }
        }
        $v = new View("change-password", "front");
        $v->assign("config", $form);
        $v->assign("errors", $errors);
        $v->assign("validate", $validate);
    }

    public function forgotAction($params)
    {

        $errors = [];
        if (!empty($params['URL'][0]) && is_string($params['URL'][0]) && (strlen($params['URL'][0]) > 36)) {
            $verif_token = substr($params['URL'][0], -23, 10);
            $verif_id = substr($params['URL'][0], 13, -23);
            $user = User::findById($verif_id);
            if (!empty($user->id) && $user->token === $verif_token) {

                $form = $user->configFormForgotChangePwd();

                $v = new View("change-password", "front");
                $v->assign("config", $form);

                if (!empty($params['POST']['password'])) {
                    $errors = Validator::validate($form, $params["POST"]);

                    if (empty($errors)) {
                        $user->setPassword($params["POST"]["password"]);
                        $user->setToken();
                        $user->save();

                        $validate = "Votre mot de passe a été modifié avec succès.";
                    }

                }
            } else {
                header("Location:" . DIRNAME);
            }
            $v->assign("errors", $errors);
            $v->assign("validate", $validate);

        } else {
            $errors = [];
            $form = User::configFormForgotPwd();
            $v = new View('forgot-password', 'front');
            $v->assign("config", $form);
            $v->assign("errors", $errors);

            if (!empty($params['POST'])) {

                $user = User::findByEmail($params['POST']['email']);
                $token = (!empty($user)) ? $user->token : null;

                if (!empty($user) && $token !== null) {
                    $mail = new PHPMailer(true); // Passing `true` enables exceptions
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0; // Enable verbose debug output
                        $mail->isSMTP(); // Set mailer to use SMTP
                        $mail->Host = "smtp.gmail.com"; // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true; // Enable SMTP authentication
                        $mail->Username = EMAIL_PATISHOPPER; // SMTP username
                        $mail->Password = PWD_PATISHOPPER; // SMTP password
                        $mail->SMTPSecure = "tls"; // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 587; // TCP port to connect to

                        //Recipients
                        $mail->setFrom(EMAIL_PATISHOPPER, "Patishopper");
                        $mail->addAddress($user->email); // Add a recipient

                        //Content
                        $mail->isHTML(true); // Set email format to HTML
                        $mail->Subject = "Changez votre mot de passe";
                        $mail->Body = '<h1>Cliquez-ici</h1><br><p>Pour changer votre mot de passe <b><a href="' . URL . '/'
                        //.rand(0,999999).uniqid().$user->id.$user->token.'">cliquez sur le lien !</a></b></p>';
                         . uniqid() . $user->id . $user->token . uniqid() . '">cliquez sur le lien !</a></b></p>';
                        $mail->send();

                        $validate = "Un email de modification de votre mot de passe vous a été envoyé.";

                        $v->assign("validate", $validate);

                    } catch (Exception $e) {
                        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                    }
                }
            }
        }
    }
}
