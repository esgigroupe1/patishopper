<?php if(!empty($config['rows'])): ?>
    <table <?php if(!empty($config['id'])) { echo 'id="' . $config['id'] . '"'; } ?> class="table-list">
        <thead>
            <tr>
                <?php foreach ($config['headers'] as $header) : ?>
                    <th><?php echo $header; ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($config['rows'] as $row) : ?>
                <tr>
                    <?php foreach ($row as $column) : ?>
                        <td><?php echo $column; ?></td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <div class="container-fluid">
        <div class="row">
            <div style="display: flex;" class="content col-12">
                <p style="margin: auto;
                         font-size: 2em;
                         margin-top: 20px;"
                         class="empty-list">Cette liste est vide !</p>
            </div>
        </div>
    </div>
<?php endif; ?>
