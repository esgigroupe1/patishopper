<main id="article-comment" class="container-fluid">
    <div class="row">
        <div class="title col-10">
            <h1>Commentaires</h1>
        </div>
        <!-- end col-10 -->
    </div>

    <hr class="separator">

    <form method="POST" id="buttons-comment-actions">
        <?php //$this->addModal("comment-selection-pan", $infos); ?>

        <div class="row">
            <div class="col-12">
                <?php $this->addModal("table", $config); ?>
            </div>
        </div>
    </form>
</main>
