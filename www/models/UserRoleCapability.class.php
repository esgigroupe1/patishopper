<?php

class UserRoleCapability extends BaseSQL {
	protected $id = null;
	protected $can_comment;
	protected $can_order;
	protected $can_edit_attachment;
	protected $can_edit_content;
	protected $can_edit_product;
	protected $can_edit_setting;

	protected $user_type_id;

    public function __construct(){
		parent::__construct();
	}

	public function setId($id){
		$this->id = $id;
	}
}