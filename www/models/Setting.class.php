<?php

class Setting extends BaseSQL
{
    protected $id = null;
    protected $name;
    protected $value;

    protected $relations = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function configFormUpdate()
    {
        //on récupère le nom des pages du site
        $pages = Post::findByPostTypeId(2);
        $size = count($pages);

        $options = [];
        $options['homepage'] = [
            "name" => "homepage",
            "class" => "settings_select",
            "label" => "Page d'accueil du site :",

        ];

        //var_dump(count($options));
        $cpt = 0;
        foreach($pages as $page){
                $options['homepage']['options']["option" . ++$cpt] = [
                    'label' => $page->title,
                    'value' => $page->id
                ];
                $cpt++;
        }

        //on récupère les valeurs par défaut en base
        $settings = Setting::findAll();

        //logo par défaut
        $logo_url = $settings[4]->value;
        $logo_id = Attachment::findByUrl($logo_url);

        if(!empty($logo_id)) {
            $logo_id = $logo_id->id;
        }
        // echo "<pre>";
        // var_dump($logo_id);
        // echo "</pre>";

        //thème par défaut
        $theme = $settings[6]->value;

        switch($theme){
            case "orange":
                $orange_theme = "checked";
                $pink_theme = "";
                $brown_theme = "";
                break;
            case "pink":
                $orange_theme = "";
                $pink_theme = "checked";
                $brown_theme = "";
                break;
            case "brown":
                $orange_theme = "";
                $pink_theme = "";
                $brown_theme = "checked";
        }

		return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => "Mettre à jour",
                "submit_name" => "settings_update",
                "submit_class" => "button center",
            ],

            "input" => [

                "sitename" => [
                    "type" => "text",
                    "label" => "Nom du site",
                    "minString" => 2,
                    "maxString" => 100,
                    "value" => $settings[1]->value
                ],

                "slogan" => [
                    "type" => "text",
                    "label" => "Slogan",
                    "minString" => 2,
                    "maxString" => 100,
                    "value" => $settings[3]->value
                ],

                "logo" => [
                    "type" => "file",
                ],

                "height" => [
                    "type" => "text",
                    "label" => "Hauteur images",
                    "value" => $settings[2]->value
				],

				"width" => [
                    "type" => "text",
                    "label" => "Largeur images",
                    "value" => $settings[0]->value
				]

            ],

            "radio" => [
                "config" => [   "label1_class" => "p_radio",
                                "label1_texte" => "Palette de couleurs : ",
                                "label2_class" => "radio"],
                "options" => [
                    "orange" => [
                        "name" => "theme_back",
                        "checked" => $orange_theme,
                        "link" => "../public/images/back/palette1.png",
                        "alt_img" => "theme1"
                    ],

                    "pink" => [
                        "name" => "theme_back",
                        "checked" => $pink_theme,
                        "link" => "../public/images/back/palette2.png",
                        "alt_img" => "thème2"
                    ],

                    "brown" => [
                        "name" => "theme_back",
                        "checked" => $brown_theme,
                        "link" => "../public/images/back/palette3.png",
                        "alt_img" => "thème3"
                    ]
                ]
            ],

            "select" => $options,

            "img" => [
                "url" => $logo_url,
                "id" => $logo_id
            ]

        ];
    }
}
