<div id="patishopper-user" class="container-fluid collapse">
  <div class="row">
    <div class="container">
      <div id="user-form">
        <div id="form">
          <h1>Inscription</h1>
          <?php if(!empty($errors)) {
                foreach($errors as $key => $value):?>
                    <p class="message error">
                        <span><?php echo $value; ?></span>
                    </p><?php
                endforeach;
                }
                else if(!empty($validate)) : ?>
                    <p class="message success">
                        <span><?php echo $validate; ?></span>
                    </p><?php
                endif; ?>
          <?php $this->addModal("form", $config, $errors);?>
        </div>
        <!-- end form -->
      </div>
      <!-- end user-form -->
    </div>
    <!-- end container -->
  </div>
  <!-- end row -->
</div><!-- end container-fluid -->
