<?php
header("Content-Type: image/png");
$imgWidth = 300;
$imgHeigh = 200;
$image = imagecreate(300, 200);
$background = imagecolorallocate($image, rand(0, 100),rand(0, 100),rand(0, 100));
$char = "abcdefghijklmonpqrstuvwxyz0123456789";
$char = str_shuffle($char);
$length = rand(-8,-6);
$captcha = substr($char, $length);
$fonts = glob("./public/fonts/*.ttf");
$x = rand(5, 10);
for($i=0;$i<strlen($captcha);$i++) {
    $size = rand(20, 25);
    $angle = rand(-20, 20);
    $y = rand(90, $imgHeigh-90);
    $color = imagecolorallocate($image, rand(150, 250),rand(150, 250),rand(150, 250));
    imagettftext($image, $size, $angle, $x, $y, $color, $fonts[rand(0, count($fonts)-1)], $captcha[$i]);
    $x += $size + rand(5, 10);
}
for($j=0;$j<rand(4,6);$j++) {
    $x1 = rand(0, $imgWidth);
    $x2 = rand(0, $imgWidth);
    $y1 = rand(0, $imgHeigh);
    $y2 = rand(0, $imgHeigh);
    $color = imagecolorallocate($image, rand(150, 250),rand(150, 250),rand(150, 250));
    
    switch(rand(0,2)) {
        case 0:
            imageline($image, $x1, $y1, $x2, $y2, $color);
            break;
        case 1:
            imagerectangle($image, $x1, $y1, $x2, $y2, $color);
            break;
        default:
            imageellipse($image, $x1, $y1, $x2, $y2, $color);
            break;
    }
}
imagepng($image);