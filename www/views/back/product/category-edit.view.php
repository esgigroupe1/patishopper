<main id="category-edit" class="container-fluid">
    <div class="row">
        <div class="title col-md-12">
            <h1><?php echo $title; ?></h1>
        </div>
    </div>

    <hr class="separator">

    <?php $form = Form::get($config); ?>

    <?php echo $form['open']; ?>
    <div class="row">

        <div class="col-10">
            <div class="row">
                <?php if(!empty($success)): ?>
                <div class="col-12">
                        <div class="message success">
                            <span><?php echo $success; ?></span>
                            <a href="#" class="exit" class="fade_exit">&times;</a>
                        </div>
                </div>
                <?php endif; ?>

                <div class="col-12 group">
                    <?php  echo $form['name']; ?>

                    <?php if(!empty($errors['title'])): ?>
                        <div class="message error">
                            <span><?php echo $errors['title']; ?></span>
                            <a href="#" class="exit" class="fade_exit">&times;</a>
                        </div>
                    <?php endif; ?>
                </div><!-- end col-12 -->

                <div class="col-12 group">
                    <?php echo $form['slug']; ?>
                </div><!-- end col-12 -->

                <div id="form-content" class="col-12 group">
                    <?php  echo $form['category-content']; ?>
                </div> <!-- end col-12 -->

            </div> <!-- end row -->
        </div> <!-- end col-10 -->

        <div class="col-2">
            <div class="wrapper">
                <?php  echo $form['submit']; ?>
            </div>
        </div> <!-- col-2 -->
    </div><!-- end row -->
    <?php echo $form['close']; ?>
</main>
