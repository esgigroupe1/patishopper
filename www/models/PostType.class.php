<?php

class PostType extends BaseSQL {
    protected $id = null;
    protected $name;

    public function __construct(){
		parent::__construct();
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        return $this->name = $name;
    }
}
