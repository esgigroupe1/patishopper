<?php
$date = new DateTime($order->created_at);
$date = $date->format("d-m-Y") . " à " . $date->format("H:i:s");
?>

<div id="patishopper-order-details" class="container">
   <div class="row">
       <h1>Détails de la commande <?php echo $order->id ?> du  <?php echo $date; ?></h1>
    </div>

    <div class="row details">
        <div class="col-12">
            <?php $total = 0; ?>
            <table id="orders-details">
                <thead>
                    <tr>
                        <th>Noms du produit</th>
                        <th>Quantité commandé</th>
                        <th>Prix</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($transactions as $transaction): ?>
                <?php $product = $transaction->getProduct(); ?>
                <?php $total += $product->price; ?>
                    <tr>
                        <td><?php echo $product->name; ?></td>
                        <td><?php echo $transaction->quantity; ?></td>
                        <td><?php echo $product->price . " €"; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <p class="total">Somme totale de la transaction : <?php echo $total . " €";?></p>
        </div>

        <div class="col-12">
            <a href="<?php echo DIRNAME; ?>account/orders" class="button">Revenir à la liste des commandes</a>
        </div>
    </div>
</div><!-- end container-fluid -->
