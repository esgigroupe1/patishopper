<?php $form = Form::get(User::configFormAccount($config['user'])); ?>

<?php echo $form['open']; ?>
<div class="container-fluid inside-content" id="informations">
    <div class="row">
        <div class="col-12 title">
            <h1>Mes informations</h1>
        </div>
        <div class="col-12">
            <?php if(!empty($config['message_success'])): ?>
            <div id="message-info" class="message success">
                <span><?php echo $config['message_success']; ?></span>
                <a href="#" class="exit" class="fade_exit">&times;</a>
            </div>
            <?php endif; ?>
        </div>

        <div class="col-6 col-sm-12">
            <p><?php echo $form['lastname']; ?></p>
        </div>

        <div class="col-6 col-sm-12">
            <p><?php echo $form['firstname']; ?></p>
        </div>

        <div class="col-12">
            <?php if(!empty($config['errors']['lastname'])): ?>
            <div class="message error">
                <span><?php echo $config['errors']['lastname']; ?></span>
                <a href="#" class="exit" class="fade_exit">&times;</a>
            </div>
            <?php endif; ?>
        </div>

        <div class="col-12">
            <?php if(!empty($config['errors']['firstname'])): ?>
            <div class="message error">
                <span><?php echo $config['errors']['firstname']; ?></span>
                <a href="#" class="exit" class="fade_exit">&times;</a>
            </div>
            <?php endif; ?>
        </div>

        <div class="col-6 col-sm-12">
            <p><?php echo $form['phone']; ?></p>
        </div>

        <div class="col-6 col-sm-12">
            <p><?php echo $form['email']; ?></p>
        </div>

        <div class="col-12">
            <?php if(!empty($config['errors']['email'])): ?>
            <div class="message error">
                <span><?php echo $config['errors']['email']; ?></span>
                <a href="#" class="exit" class="fade_exit">&times;</a>
            </div>
            <?php endif; ?>
        </div>

        <div class="col-6 col-sm-12">
            <p><?php echo $form['password']; ?></p>
        </div>

        <div class="col-6 col-sm-12">
            <p><?php echo $form['passwordConfirm']; ?></p>
        </div>

        <div class="col-12">
            <?php if(!empty($config['errors']['password_error'])): ?>
            <div class="message error">
                <span><?php echo $config['errors']['password_error']; ?></span>
                <a href="#" class="exit" class="fade_exit">&times;</a>
            </div>
            <?php endif; ?>
        </div>

        <div class="col-6 col-sm-12">
            <p><?php echo $form['address']; ?></p>
        </div>

        <div class="col-6 col-sm-12">
            <p><?php echo $form['country']; ?></p>
        </div>

        <div class="col-6 col-sm-12">
            <p><?php echo $form['postal_code']; ?></p>
        </div> <!-- end col-6 -->

        <div class="col-6 col-sm-12">
            <p><?php echo $form['city']; ?></p>
        </div> <!-- end col-6 -->
    </div> <!-- end row -->

    <div class="row">
        <div class="col-12 col-sm-12">
            <p>* Tous les champs sont obligatoires</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-sm-12">
            <?php echo $form['submit']; ?>
        </div>
    </div>
</div><!-- end container -->

<?php echo $form['close']; ?>
