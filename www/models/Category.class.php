<?php

class Category extends BaseSQL
{
    protected $id = null;
    protected $name;
    protected $slug;
    protected $description;
    protected $type;

    protected $category_id;

    protected $created_at = null;
    protected $updated_at = null;

    protected $relations = [
        'products' => [
            'type' => 'ManyToMany',
            'table' => 'product',
            'data' => [],
        ],
        'posts' => [
            'type' => 'ManyToMany',
            'table' => 'post',
            'data' => [],
        ],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all needed posts or products category
     *
     * @param integer $type which type of category to show 1 for articles categories, 2 for products categories
     * @return void
     */
    public static function fetchColumns($type)
    {
        $published_categories = Category::select(['type|OR' => $type, 'type' => 3]);
        return ['published_categories' => $published_categories];
    }

    public static function configForm($category = [])
    {
        $num = !empty($category);

        $suffix_label = '<span class="bar"></span><span class="line-right-left"></span>';

        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => ($num > 0) ? "Modifier la catégorie" : "Créer la catégorie",
                "submit_name" => "",
                "submit_class" => "button",
            ],

            "input" => [

                "name" => [
                    "name" => "name",
                    "type" => "text",
                    "label" => '<p>Titre de la catégorie</p>' . $suffix_label,
                    "placeholder" => "",
                    "required" => "required",
                    "minString" => 1,
                    "maxString" => 50,
                    "id" => "title_category",
                    "value" => ($num) ? $category->name : "",
                ],

                "slug" => [
                    "name" => "slug",
                    "type" => "text",
                    "label" => '<p>Slug</p>' . $suffix_label,
                    "placeholder" => "",
                    "id" => "slug_category",
                    "value" => ($num) ? (empty($category->slug) ? (Helper::uniqueSlug($category->name, $category)) : $category->slug) : "",
                ],
            ],

            "textarea" => [
                "category-content" => [
                    "label" => "<p>Description de la catégorie</p>" . $suffix_label,
                    "class" => "content_category",
                    "name" => "category_description",
                    "value" => ($num) ? $category->description : "",
                    "required" => true,
                    "minString" => 5,
                ],
            ],
        ];
    }

    public static function configTableList($data, $catType)
    {
        // build headers
        $data['headers'] = [
            '<label class="checkbox"><div class="label">Tout</div><input type="checkbox" name="all"><span class="checker"></span></label>',
            "<p>Nom</p>",
            "<p>Description</p>",
        ];

        $type = "undefined";
        if ($catType == 1) {
            $type = 'article';
        } elseif ($catType == 2) {
            $type = 'product';
        }

        // Sort
        if (array_key_exists('categories', $data)) {
            Helper::sortByLastUpdated($data['categories']);
        }

        $rowsData = [];
        foreach ($data['categories'] as $key => $row) {
            $rowsData[$key]['checkbox'] = '<label class="checkbox"><input class="checkboxItem" type="checkbox" name="action_' . $row->id . '" value="' . $row->id . '"><span class="checker"></span></label>';

            // links to modify and delete
            $title = '<span class="category-title">' . $row->name . '</span>';
            $title .= '<span class="category-edit"><a href="' . DIRNAME . 'admin/' . $type . '/category/edit/' . $row->id . '">Modifier</a>';
            $title .= '<a class="category-delete" href="' . DIRNAME . 'admin/' . $type . '/category/delete" data-id="' . $row->id . '">Supprimer définitivement</a></span>';

            $publishedDate = new DateTime($row->created_at); // format date from MYSQL database
            $updatedDate = new DateTime($row->updated_at); // format date from MYSQL database

            $description = ($row->description != 'NULL') ? (Helper::truncate($row->description, 40)) : "Aucune description";

            // set all columns
            $rowsData[$key]['name'] = $title;
            $rowsData[$key]['description'] = $description;
            $rowsData[$key]['publishDate'] = "Le " . $publishedDate->format("d-m-Y") . " à " . $publishedDate->format("H:i:s");
            $rowsData[$key]['updatedDate'] = "Le " . $updatedDate->format("d-m-Y") . " à " . $updatedDate->format("H:i:s");
        }

        // data column after categories
        array_push($data['headers'], "<p>Date de création</p>");
        array_push($data['headers'], "<p>Dernière modification</p>");

        return ['headers' => $data['headers'], 'rows' => $rowsData];
    }
}
