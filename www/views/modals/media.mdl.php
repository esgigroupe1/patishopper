
<div id="mediaModal" class="modal">
    <div class="modal-content">
        <div class="header-modal">
            <span class="close">&times;</span>
            <h2>Choix d'un media</h2>
        </div>
        <p class="p-media"> Veuillez cliquer sur un média pour le séletionner.</p>
        <div class="row">
            <?php foreach ($config["medias"] as $name => $params): ?>
                    <div class="col-3 col-xs-12 media-file">
                        <figure>
                            <img id="<?php echo $params->id ?>" src="<?php echo $params->url ?>" onclick="choose(event)">
                            <figcaption><?php echo $params->name ?></figcaption>
                        </figure>
                    </div>
            <?php endforeach?>
        </div>
        <div class="footer-modal"></div>
    </div>
</div>
