<?php
class View
{
    private $view;
    private $template;
    private $title = "";
    private $data = [];
    private $folder = "";

    public function __construct($view = "default", $template = "front")
    {
        global $a, $c;
        $this->template = $template . ".tpl.php";
        $this->view = $view . ".view.php";

        if ($template == 'back') {
            // Choose folders' structure
            $this->folder = str_replace("Action", "", $a);

        } elseif ($template == 'front') {
            $views = ['error', 'post', 'posts', 'product' , 'products', 'sitemap'];
            if(in_array($view, $views)) {
                $this->folder = "";
            }else {
                $this->folder = strtolower(str_replace("Controller", "", $c));
            }
        } else {
            // no template to load
            $this->folder = "front";
        }

        if (!file_exists("views/templates/" . $this->template)) {
            die("Le template " . $this->template . " n'existe pas");
        }
        // no template to load
        $template = ($template == 'default') ? "" : "/" . $template;
        if (!file_exists("views" . $template . "/" . $this->folder . "/" . $this->view)) {
            die("La vue " . $this->view . " n'existe pas");
        }
    }
    public function assign($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function addModal($modal, $config = [], $errors = [])
    {
        include "views/modals/" . $modal . ".mdl.php";
    }

    public function getAssign()
    {
        return $this->data;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function __destruct()
    {
        global $c, $a, $params;
        extract($this->data);
        include SITE_DIR . "views/templates/" . $this->template;
    }
}
