<?php
abstract class BaseSQL
{
    private static $pdo = null;
    private static $table;
    private $tableName;
    private $columns;

    public function __construct()
    {
        self::initialize();
        //$this->tableName = strtolower(get_called_class());
    }

    private static function initialize()
    {
        if (self::$pdo == null) {
            // connect to database
            try {
                self::$pdo = new PDO(
                    DBDRIVER . ":host=" . DBHOST . ";dbname=" . DBNAME,
                    DBUSER,
                    DBPWD,
                    [
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                    ]
                );
            } catch (Exception $e) {
                die("Erreur SQL :" . $e->getMessage());
            }

        }
        // get table name
        self::$table = Helper::camelCaseToSnakeCase(get_called_class());
    }

    public function getPdo()
    {
        return self::$pdo;
    }

    private function getColumns($object = null)
    {
        if ($object == null) {
            $object = $this;
        }
        $columns = array_diff_key(
            get_object_vars($object),
            get_class_vars(get_class()));

        if (!$columns['id']) {
            unset($columns['id']);
        }

        unset($columns['relations']);
        unset($columns['created_at']);
        unset($columns['updated_at']);

        return $columns;

    }

    /**
     * Retrieve SQL INSERT statement
     *
     * @param mixed $table table in which data will be added
     * @param array $columns insert in which data will be inserted
     * @return array $sql contains query string
     */
    public static function getInsertQuery($table, &$columns)
    {
        //unset($columns['id']);

        $sql = "
                INSERT INTO `" . $table . "` (" . implode(",", array_keys($columns)) . ")
                VALUES (:" . implode(",:", array_keys($columns)) . ")
            ";
        return $sql;
    }

    /**
     * Retrieve SQL UPDATE statement
     * @param $table name of table to update
     * @param $columns fields to set as update values
     */

    /**
     * Retrieve SQL UPDATE statement
     *
     * @param mixed $table
     * @param array $columns
     * @return array $sql contains query string
     */
    public static function getUpdateQuery($table, &$columns)
    {
        $sqlSet = [];
        foreach ($columns as $key => $value) {
            $sqlSet[] = $key . "=:" . $key;
        }
        $queryString = "UPDATE `" . $table . "` SET
                        " . implode(",
                        ", $sqlSet) . " WHERE id=:id ";
        return $queryString;
    }

    public static function getQuery($table, &$columns)
    {
        return (isset($columns['id'])) ? (self::getUpdateQuery($table, $columns)) : (self::getInsertQuery($table, $columns));
    }

    /**
     * Configure and prepare all data needed according to relations between tables
     *
     * @return array $sql all queries and data to execute
     */
    public function configureQuery()
    {
        self::$table = Helper::camelCaseToSnakeCase(get_called_class());

        $this->columns = $this->getColumns();

        $sql['primary'] = [];
        $query_values['query'] = self::getQuery(self::$table, $this->columns);
        $query_values['values'] = $this->columns;
        array_push($sql['primary'], $query_values);
        foreach ($this->relations as &$relation) {
            switch ($relation['type']) {
                case 'OneToOne':{
                        // nothing to do
                        break;
                    }
                case 'OneToMany':{
                        // nothing to do (not sure => check that)
                        break;
                    }
                case 'ManyToOne':{
                        // nothing to do
                        break;
                    }
                case 'ManyToMany':{
                        //Helper::dump($this->getColumns($object));
                        //if (!empty($relation['dataToUpdate'])) {
                        $left_table = self::$table . "_has_" . $relation['table'];
                        $right_table = $relation['table'] . "_has_" . self::$table;
                        $join_table = self::tableExists($left_table) ? $left_table : $right_table;

                        $primary = self::$table . "_id";
                        $secondary = $relation['table'] . "_id";

                        $join_colum[$primary] = -1;
                        $join_colum[$secondary] = -1;

                        // insert all data in joint table
                        $sql['secondary'] = [];
                        $sql['tertiary'] = [];

                        if (isset($relation['dataToUpdate'])) {
                            foreach ((array) $relation['dataToUpdate'] as $object_data) {
                                $values = $this->getColumns($object_data);
                                $query_values['query'] = self::getQuery($relation['table'], $values);
                                $query_values['values'] = $values;
                                array_push($sql['secondary'], $query_values);
                                //$sql['secondary']['values'][] = $values;
                            }
                        }
                        $sqlSet = [];
                        foreach ($join_colum as $key => $value) {
                            $sqlSet[] = $key . "=:" . $key;
                        }
                        // get insert or update query
                        $query_values['query'] = self::getQuery($join_table, $join_colum) . " ON DUPLICATE KEY UPDATE updated_at = NOW()";
                        $query_values['values'] = $join_colum;
                        $sql['tertiary']['insert'] = $query_values;
                        // get delete query

                        $objects_to_delete = [];
                        if (isset($relation['dataToUpdate'])) {
                            $objects_to_delete = array_udiff(
                                $relation['data'], (array) $relation['dataToUpdate'],
                                [$this, 'udiff_assoc_func']
                            );
                        }
                        $ids = array_column($objects_to_delete, 'id');
                        $ids = (count($ids) > 0) ? $ids : [-1];

                        $id_del = -1;
                        if ($this->id) {
                            $id_del = $this->id;
                        }

                        $query_values['query'] = "DELETE FROM " . $join_table . " WHERE " . $primary . " = " . $id_del . " AND " . $secondary . " IN (" . implode(",", $ids) . ")";

                        unset($query_values['values']);
                        $sql['tertiary']['delete'] = $query_values;
                        //}
                        // update data real
                        if (isset($relation['dataToUpdate'])) {
                            $relation['data'] = $relation['dataToUpdate'];
                            unset($relation['dataToUpdate']);
                        }
                        break;
                    }
                default:{
                        break;
                    }
            }
        }

        return $sql;
    }

    /**
     * Saves called object and its associated objects in databases
     *
     * @return void
     */
    public function save()
    {
        $sql = $this->configureQuery();
        // Helper::dump($sql);
        try {
            self::$pdo->beginTransaction();

            $primary_insert_ids = [];
            $secondary_insert_ids = [];
            $query = "";

            $tertiary_insert = "none";
            if (array_key_exists("tertiary", $sql)) {
                $tertiary_insert = $sql['tertiary']; // join table data
                unset($sql['tertiary']);
            }

            // Query primary and secondary table
            foreach ($sql as $key => $data) {
                foreach ($data as $statement) {
                    $query = self::$pdo->prepare($statement['query']);
                    foreach ($statement['values'] as $attribute => &$value) {
                        $query->bindParam(':' . $attribute, $value, PDO::PARAM_STR);
                    }
                    if ($query->execute()) {
                        $query_id = isset($statement['values']['id']) ? $statement['values']['id'] : self::$pdo->lastInsertId();

                        if ($key == 'primary') {
                            $primary_insert_ids[] = $query_id;
                            $this->id = $query_id; // update last for referred object
                        }
                        if ($key == 'secondary') {
                            $secondary_insert_ids[] = $query_id;
                        }
                    } else {
                        throw new Exception("Error inserting");
                    }
                }
            }

            // Query to tertiary table Insert or update
            if ($tertiary_insert != "none" && is_array($tertiary_insert)) {
                $query = self::$pdo->prepare($tertiary_insert['insert']['query']);
                $attributes = array_keys($tertiary_insert['insert']['values']);
                foreach ($secondary_insert_ids as $s_value) {
                    $query->bindParam(':' . $attributes[0], $primary_insert_ids[0], PDO::PARAM_INT);
                    $query->bindParam(':' . $attributes[1], $s_value, PDO::PARAM_INT);
                    $query->execute();
                }

                // Query to tertiary table delete
                $query = self::$pdo->prepare($tertiary_insert['delete']['query']);
                $query->execute();
            }

            if (!self::$pdo->commit()) {
                throw new Exception('Transaction commit failed.');
            }
        } catch (PDOException $ex) { // Something went wrong rollback!
            self::$pdo->rollBack();
            throw $ex;
        }
        return $sql;
    }

    public function isAttributeExists($attribute)
    {
        return property_exists(get_called_class(), $attribute);
    }

    public function getAttribute($attribute)
    {
        return $this->$attribute;
    }

    public function setAttribute($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function __set($attribute, $value)
    {
        if ($this->isAttributeExists($attribute)) {
            return $this->$attribute = $value;
        } else {
            //throw new Exception($attribute . " n'existe pas. Impossible d'assigner " . $value);
        }
    }

    public function __get($attribute)
    {
        if ($this->isAttributeExists($attribute)) {
            return $this->$attribute;
        }
        throw new Exception($attribute . " n'existe pas");
    }

    public function __isset($prop): bool
    {
        // array_column
        return isset($this->$prop);
    }

    public function __call($method, $arguments)
    { // like addCategories ($post->addCategories)
        $method_suffix = Helper::camelCaseToSnakeCase(substr($method, 3));
        $errors = [];
        switch (substr($method, 0, 3)) {
            case 'get':{
                    if (count($arguments) >= 0) {
                        if ($this->isAttributeExists($method_suffix)) {
                            return $this->getAttribute($method_suffix);
                        } else {
                            if (array_key_exists($method_suffix, $this->relations)) {
                                if (array_key_exists('data', $this->relations[$method_suffix]) &&
                                    $this->relations[$method_suffix]['type'] != "OneToOne") {
                                    if (empty($this->relations[$method_suffix]['data'])) {
                                        $class = ucfirst(get_called_class());
                                        $data = $class::getCollections(['id' => $this->id])[0];
                                        return $data->relations[$method_suffix]['data'];
                                    } else {
                                        return $this->relations[$method_suffix]['data'];
                                    }
                                } else {
                                    $table = Helper::snakeCaseToCamelCase($this->relations[$method_suffix]['table']);
                                    $class = ucfirst($table);
                                    $class_attribute = $table . '_id';
                                    return $class::findById($this->$class_attribute);
                                }
                            } else {
                                $errors[] = "GETTER : Cet attribut : " . $method_suffix . " n'existe pas";
                            }
                        }
                    } else {
                        $errors[] = "Trop d'arguments ont été passés à ce setter";
                    }
                    break;
                }
            case 'set':{
                    if (count($arguments) == 1) {
                        if ($this->isAttributeExists($method_suffix)) {
                            $this->setAttribute($method_suffix, $arguments[0]);
                        } else {
                            $errors[] = "SETTER : cet attribut : " . $method_suffix . " n'existe pas";
                        }
                    } else {
                        $errors[] = "Trop d'arguments ont été passés à ce setter";
                    }
                    break;
                }
            case 'add':{
                    $args = count($arguments);
                    if ($args == 1) {
                        $args = [];
                        is_array($arguments[0]) && $args = $arguments[0];
                        !is_array($arguments[0]) && $args[] = $arguments[0];

                        $relations = $this->relations;

                        (!isset($relations[$method_suffix]['dataToUpdate'])) && $relations[$method_suffix]['dataToUpdate'] = [];
                        if (array_key_exists($method_suffix, $relations)) {
                            foreach ($args as $argument) {
                                if (is_a($argument, ucfirst(Helper::snakeCaseToCamelCase($relations[$method_suffix]['table'])))) {
                                    array_push($relations[$method_suffix]['dataToUpdate'], $argument);
                                }
                            }
                            Helper::unique($relations[$method_suffix]['dataToUpdate'], 'id');
                            $this->setRelations($relations);
                        }
                    }
                    return;
                    break;
                }
            default:{

                }
        } // end switch

        if (count($errors) > 0) {
            throw new Exception('La méthode <strong>' . $method . '</strong> a été appelée alors qu\'elle n\'existe pas ! Ses arguments étaient les suivants : <strong>' . implode($arguments, '</strong>, <strong>') . '</strong>');
        }
    }

    public static function __callStatic($method, $args)
    {
        if (substr($method, 0, 6) == "findBy") {
            $method_suffix = Helper::camelCaseToSnakeCase(substr($method, 6));
            $nb_args = count($args);
            array_unshift($args, $method_suffix);
            if ($nb_args == 1) {
                //echo '<pre>', print_r($args, true), '</pre>';
                return call_user_func([get_called_class(), 'findByField'], $args);
            }
            return [];
        }

        if (substr($method, 0, 3) == "get") {
            $method_suffix = Helper::camelCaseToSnakeCase(substr($method, 3));
            $r = static::$relations;
            return $data = ucfirst($r[$method_suffix]['table'])::select(); // snakeCaseToCamelCase
        }

        throw new Exception(sprintf('Aucune méthode statique nommée "%s" dans la classe "%s".', $method, 'BaseSQL'));
    }

    public static function findAll()
    {
        return self::getCollections();
    }

    public static function findByField($args)
    {
        $field = $args[0];
        $value = $args[1];
        $results = self::getCollections([$field => $value]);
        return (count($results) > 1) ? $results : (isset($results[0]) ? $results[0] : []);
    }

    private static function getWhere($conditionsIn)
    {
        /*
        Prepare data
         */

        $connectors = ["AND", "OR", "AND (", ") AND", "( OR", ") OR", ")", "OR ("];
        $operators = ["=", "<>", ">=", "<=", ">", "<", "IS", "IS NOT"];
        $values = array_values($conditionsIn);
        $conditions = [];
        foreach (array_keys($conditionsIn) as $key1 => $condition) {
            $conditions[$key1] = explode('|', $condition);
            foreach ($conditions[$key1] as $key => $value) {
                if (in_array($value, $connectors)) {
                    $conditions[$key1]['connector'] = $value;
                    unset($conditions[$key1][$key]);
                } else if (in_array($value, $operators)) {
                    $conditions[$key1]['operator'] = $value;
                    unset($conditions[$key1][$key]);
                } else {

                    $kWhere = $key1 . '_' . ($key + 1);
                    $kValue = $value . '_' . ($key1 + 1);
                    $conditions[$key1]['expression'] = $value;
                    $conditions[$key1]['alias'] = $kValue; // post_type_id = :post_type_id_1
                    unset($conditions[$key1][$key]);
                    // build data
                    $values[$kValue] = $values[$key1];
                    unset($values[$key1]);
                }

            }
        }

        /*
        Build WHERE statement
         */
        $where = [];
        $total = count($conditions);
        for ($i = 0; $i < $total; $i++) {
            $suffix = (array_key_exists('connector', $conditions[$i]) ? (" " . $conditions[$i]['connector'] . " ") : (($i + 1 < $total) ? " AND " : ""));
            $operator = (array_key_exists('operator', $conditions[$i]) ? " " . $conditions[$i]['operator'] . " " : " = ");
            $where[] = $conditions[$i]['expression'] . $operator . ":" . $conditions[$i]['alias'] . $suffix;
        }
        return ['conditions' => implode("", $where), 'values' => $values];
    }

    private static function getTable($table)
    {
        if ($table == null) {
            return "`" . Helper::camelCaseToSnakeCase(get_called_class()) . "`";
        }
        return "`" . $table . "`";
    }

    public static function select(
        $conditions = [], $table = null, $columns = "*",
        $order = "", $limit = null, $offset = null) {

        self::initialize();

        $table = self::getTable($table);

        /*
        Transform ['id' => 4, 'name' => "Pati"] to [':id' => 4, ':name' => "Pati"]
        specially for SELECT
        */
        $suffix = self::getWhere($conditions);
        $datad = $suffix['values'];
        $data = array_combine(
            array_map(function ($k) {return ':' . $k;}, array_keys($datad)),
            $datad
        );

        // Build Query
        $sql = "SELECT " . $columns . " FROM " . $table;
        $sql .= (($suffix['conditions']) ? " WHERE " . ($suffix['conditions']) . " " : "");
        $sql .= (($order) ? "ORDER BY " . $order . " " : "");
        $sql .= (($limit) ? "LIMIT " . $limit . " " : "");
        $sql .= (($offset && $limit) ? "OFFSET " . $offset : "");
        $query = self::$pdo->prepare($sql);
        // Helper::dump($sql);
        /*
            Helper::dump($data);
            Helper::dump($sql);*/
        $query->execute($data);

        $query->setFetchMode(
            PDO::FETCH_CLASS,
            ucfirst(str_replace("`", "", $table)) // `post`
        ); // OF OBJECT ?

        return $query->fetchAll();
    }

    public static function delete($conditions = [], $table = null)
    {
        self::initialize();

        if (empty($conditions)) {
            die("Aucune condtion n'a été spécifié dans la requête DELETE");
            exit();
        }
        $cond = self::getWhere($conditions);
        // Helper::dump($cond);
        // Build Query
        $sql = "DELETE FROM " . self::getTable($table);
        $sql .= " WHERE " . $cond["conditions"];
        //Helper::dump($sql);
        $query = self::$pdo->prepare($sql);
        $query->execute($cond["values"]);

        // checking if the row was successfully deleted
        $count = $query->rowCount();

        return ($count > 0) ? 1 : 0;
    }

    public static function getCollections($conditions = [], $table = null, $columns = "*", $order = "", $limit = null, $offset = null)
    {
        $objects = self::select($conditions, $table, $columns, $order, $limit, $offset);
        if (!empty($objects[0]) && is_object($objects[0])) {
            foreach ($objects as $object) {
                // echo "Yes it is : " . $object->getid();
                if (!is_array($object->relations)) { // define relations for this objects in class
                    throw new Exception("== Define " . get_called_class() . " relations attributes ==");
                }
                $c = Helper::camelCaseToSnakeCase(get_called_class());

                foreach ($object->relations as &$relation) {
                    $r = $relation['table'];
                    if ($relation['type'] == "ManyToMany") {
                        $t_table = (self::tableExists($c . "_has_" . $r)) ? $c . "_has_" . $r : ($r . "_has_" . $c);
                        $sql = "SELECT " . $r . ".* FROM " . $r . ", " . $t_table . "
                                WHERE " . $c . "_id = :" . $c . "_id
                                        AND id = " . $t_table . "." . $r . "_id";
                        $data = [$c . '_id' => $object->id];
                        $relation['data'] = self::runSelect($sql, $data, Helper::snakeCaseToCamelCase($r));
                    } elseif ($relation['type'] == "OneToMany") {
                        $relation['data'] = self::select([$c . '_id' => $object->id], $relation['table']);
                    } elseif (in_array($relation['type'], ['OneToOne', 'ManyToOne'])) {
                        $table = ucfirst(Helper::snakeCaseToCamelCase($relation['table']));
                        $attribute = $relation['table'] . '_id';
                        $data = $table::findById($object->$attribute);
                        // Helper::dump($data);die();
                        $relation['data'] = $data;
                    }
                }
            }
        }
        return $objects;
    }

    public static function runSelect($sql, $data, $class)
    {
        $query = self::runQuery($sql, $data);
        $query->setFetchMode(PDO::FETCH_CLASS, ucfirst($class));
        return $query->fetchAll();
    }

    public static function runQuery($sql, $data)
    {
        if (empty(self::$pdo)) {
            self::initialize();
        }
        // see if can globalize try and catch query of not on SELECT
        $query = self::$pdo->prepare($sql);
        foreach ($data as $key => &$value) {
            $query->bindParam($key, $value, PDO::PARAM_STR);
        }
        $query->execute();

        return $query;
    }

    public static function remove($table = null, $conditions = [])
    {

        self::initialize();

        if ($table == null) {
            $table = Helper::camelCaseToSnakeCase(get_called_class());
        }

        $where = [];
        $glue = (count($conditions) > 1 ? " AND " : "");
        foreach ($conditions as $key => $value) {
            $where[] = $key . "=:" . $key;
        }
        $conditions_statement = implode($glue, $where);

        $sql = "DELETE FROM " . "`" . $table . "`";
        $sql .= (($conditions) ? " WHERE " . $conditions_statement : "");
        $query = self::$pdo->prepare($sql);
        echo Helper::dump($query);
        echo Helper::dump($conditions_statement);
        $query->execute($data);

        //Suppression réussie
        if ($query) {
            return true;
        }
        //Echec suppression
        else {
            return false;
        }

    }

    /**
     * Check if a table exists in the current database.
     *
     * @param PDO $pdo PDO instance connected to a database.
     * @param string $table Table to search for.
     * @return bool TRUE if table exists, FALSE if no table found.
     */
    public static function tableExists($table)
    {

        // Try a select statement against the table
        // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
        try {
            $result = self::$pdo->query("SELECT 1 FROM $table LIMIT 1");
        } catch (Exception $e) {
            // We got an exception == table not found
            return false;
        }

        // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
        return $result !== false;
    }

    public static function udiff_assoc_func($a, $b)
    {
        return $a->id - $b->id;
    }

    /*
    ** Function that get data to fill dashboard charts
    */
    public static function fillChart($table,$date_attribut)
    {
        $date = date("Y-m-d");
        //$date = "2018-07-03"; //pour les tests
        self::initialize();
        $table = self::getTable($table);
        $data[] = "";

        $sql = "SELECT *, dayofweek(".$date_attribut.") AS jour FROM " . $table . " WHERE week('" . $date . "') = week(".$date_attribut.")";
        $query = self::$pdo->query($sql);

        $query->setFetchMode(PDO::FETCH_ASSOC);
        
        return $query->fetchAll();

    }
}
