<main id="page" class="container-fluid">
    <div class="row">
        <div class="title col-10">
            <h1>Pages</h1>
        </div>
        <!-- end col-10 -->
        <div class="col-2" id="button">
            <a href="<?php echo DIRNAME; ?>admin/page/add" class="button">Ajouter</a>
        </div>
        <!-- end col-2 -->
    </div>

    <hr class="separator">

    <form method="POST" id="buttons-posts-actions">
        <?php if(!empty($config['rows'])):?>
            <?php $this->addModal("post-selection-pan", $infos); ?>
        <?php endif; ?>

        <div class="row">
            <div class="col-12">
                <?php $this->addModal("table", $config); ?>
            </div> <!-- end col-12 -->
        </div><!-- end row -->
    </form>
</main>
