<?php

class IndexController extends Controller
{
    public function indexAction($params)
    {
        $v = new View("home", "front");
        $v->setTitle("Bienvenue sur Patishopper");

        //call to get_visitor fonction
        $this->add_visitor();

        // Begin Home information
        $products = Product::select([], null, "*", "created_at DESC ", 4, null);

        // Helper::dump($products);

        $v->assign('products', $products);
        $articles = Post::select(['post_type_id' => 1], null, "*", "created_at DESC", 4, null);

        // Helper::dump($articles);

        $v->assign('articles', $articles);
        // End Home information
    }


    public function add_visitor()
    {

        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date("Y-m-d H:i:s");
        $data = [$date, $ip];

        //save or update the visitor
        $visitor_new = new Visitor();
        $visitor_new->addVisitor($data);

    }

    public function disconnectAction($params)
    {
        session_destroy();
        header("Location: ".DIRNAME."authentication");
    }
}
