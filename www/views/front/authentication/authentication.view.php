<div id="patishopper-connexion" class="container-fluid collapse">
    <div class="row">
            <div class="col-12">
                <div id="connexion-form">
                    <div id="form">
                    <h2>Connexion</h2>
                    <form method="POST" action="<?php echo DIRNAME; ?>authentication">
                        <p>
                            <input type="text" name="email" placeholder="Email">
                        </p>

                        <p>
                            <input type="password" name="password" placeholder="**********">
                        </p>

                        <p>
                            <?php
                                if($this->getAssign()) {
                                    $value = $this->getAssign();

                                    if(isset($value['Erreur 403'])) { ?>
                                        <p class="message error">
                                            <span><?php echo $value['Erreur 403']; ?></span>
                                        </p>
                                        <?php
                                    }
                                    if(isset($value['Erreur 401'])) {

                                    ?>
                                        <p class="message error">

                                            <span><?php echo $value['Erreur 401']; ?></span>
                                        </p>
                                        <?php
                                    }
                                }
                            ?>
                        </p>

                        <p>
                            <input class="button" type="submit" value="Connexion">
                        </p>

                        <p id="forgot-password"><a href="authentication/forgot">Mot de passe oublié ?</a></p>

                        <a class="button" id="register" href="<?php echo DIRNAME; ?>signup">S'inscrire</a>
                    </form>
                    </div>
                    <!-- end form -->
                </div>
                <!-- end connexion-form -->
            </div>
        <!-- end col-md-12 -->
    </div>
    <!-- end row -->
</div><!-- end container-fluid -->
