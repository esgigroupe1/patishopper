<?php

class Post extends BaseSQL
{
    protected $id = null;
    protected $title;
    protected $slug;
    protected $text;
    protected $publish_at;
    protected $has_comment = 0;
    protected $status = 1;
    protected $trash = 0;
    protected $post_type_id = null;
    protected $user_id = null;
    protected $attachment_id = null;

    protected $created_at = null;
    protected $updated_at = null;

    protected $relations = [
        'categories' => [
            'type' => 'ManyToMany',
            'table' => 'category',
            'data' => [],
        ],
        'comments' => [
            'type' => 'OneToMany',
            'table' => 'comment',
            'data' => [],
        ],
        'post_type' => [
            'type' => 'OneToOne',
            'table' => 'post_type',
        ],
        'user' => [
            'type' => 'OneToOne',
            'table' => 'user',
        ],
        'attachment' => [
            'type' => 'OneToOne',
            'table' => 'attachment',
        ],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public static function fetchColumns($id = 2)
    {
        $all_posts = Post::getCollections(['post_type_id' => $id, 'trash' => 0]);
        $published_posts = Post::getCollections(["post_type_id" => $id, "status" => 1, 'trash' => 0]);
        $draft_posts = Post::getCollections(["post_type_id" => $id, "status" => 0, 'trash' => 0]);
        $trash_posts = Post::getCollections(["post_type_id|AND (" => $id, "trash|OR" => 1, "trash|IS|)" => null]);

        return [
            'all_posts' => $all_posts,
            'published_posts' => $published_posts,
            'draft_posts' => $draft_posts,
            'trash_posts' => $trash_posts,
        ];
    }

    public static function configFormContent($post = [])
    {
        $num = !empty($post);

        $suffix_label = '<span class="bar"></span><span class="line-right-left"></span>';

        $checkboxes = "";

        // gets all attached categories if their exists
        $existing_cats_ids = [];
        if (!empty($post->relations['categories']['data'])) {
            $existing_cats_ids = array_column($post->relations['categories']['data'], 'id'); // // get all cats id
        }
        $categories = Category::select(['type|OR' => 1, 'type' => 3]);
        Helper::sortByLastUpdated($categories);
        foreach ($categories as $key => $category) {
            $checked = (in_array($category->id, $existing_cats_ids)) ? "checked" : "";

            $checkboxes .= '<label class="checkbox"><div class="label">' . $category->name . '</div><input type="checkbox" name="category_' . $category->id . '" value="' . $category->id . '"' . $checked . '>';
            $checkboxes .= '<span class="checker"></span></label>';
        }

        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => ($num > 0) ? "Modifier" : "Créer l'article",
                "submit_name" => "",
                "submit_class" => "button",
            ],

            "input" => [

                "title" => [
                    "name" => "title",
                    "type" => "text",
                    "label" => "<p>Titre</p>" . $suffix_label,
                    "placeholder" => "",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "id" => "title_post",
                    "value" => ($num) ? htmlspecialchars($post->title, ENT_QUOTES, 'UTF-8') : "",
                ],

                "slug" => [
                    "name" => "slug",
                    "type" => "text",
                    "label" => "<p>Slug</p>" . $suffix_label,
                    "placeholder" => "",
                    "minString" => 0,
                    "maxString" => 100,
                    "id" => "slug_post",
                    "value" => ($num) ? (empty($post->slug) ? (htmlspecialchars(Helper::uniqueSlug($post->title, $post, ENT_QUOTES, 'UTF-8'))) : htmlspecialchars($post->slug, ENT_QUOTES, 'UTF-8')) : "",
                ],

                "featured-image" => [
                    "id" => "media-selected",
                    "name" => "media-id",
                    "type" => "hidden",
                    "label" => "",
                    "placeholder" => "",
                    "value" => ($num) ? $post->attachment_id : "-1",
                ],

                // grouped checkbox like categories
                "checkboxes" => [
                    "type" => "checkbox",
                    "values" => $checkboxes,
                ],

                // non grouped checkbox
            ],

            "textarea" => [

                "post-content" => [
                    //"label" => "Contenu de",
                    "class" => "content_post",
                    "value" => ($num) ? $post->text : "",
                ],

            ],
        ];
    }

    public static function configCommentForm() {
        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => "Commenter",
                "submit_name" => "",
                "submit_class" => "button",
                "id" => "article-comment",
            ],

            "textarea" => [
                "comment-content" => [
                    "name" => "text",
                    "class" => "content_product",
                    "placeholder" => "Ecrivez votre commentaire ici !",
                ],
            ],
        ];
    }

    public static function configFormPage($post = [])
    {
        $num = !empty($post);

        $suffix_label = '<span class="bar"></span><span class="line-right-left"></span>';

        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => ($num > 0) ? "Modifier" : "Créer la page",
                "submit_name" => "",
                "submit_class" => "button",
            ],

            "input" => [

                "title" => [
                    "name" => "title",
                    "type" => "text",
                    "label" => '<p>Titre</p>' . $suffix_label,
                    "placeholder" => "",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "id" => "title_post",
                    "value" => ($num) ? (Helper::xss($post->title)) : "",
                ],

                "slug" => [
                    "name" => "slug",
                    "type" => "text",
                    "label" => "<p>Slug</p>" . $suffix_label,
                    "placeholder" => "",
                    "minString" => 0,
                    "maxString" => 100,
                    "id" => "slug_post",
                    "value" => ($num) ? (empty($post->slug) ? (Helper::uniqueSlug($post->title, $post)) : $post->slug) : "",
                ],
            ],

            "textarea" => [
                "post-content" => [
                    //"label" => "Contenu de l'article",
                    "name" => "text",
                    "class" => "content_post",
                    "value" => ($num) ? $post->text : "",
                ],

            ],
        ];
    }

    public static function configTableList($data)
    {
        // prepare data to table
        $rowsData = [];
        $pages = [];

        // build headers
        $data['headers'] = [
            '<label class="checkbox"><div class="label">Tout</div><input type="checkbox" name="all"><span class="checker"></span></label>',
            "<p>Titre</p>",
            "<p>Auteur</p>",
            "<p>Etat</p>",
        ];

        if (!empty($data['pages'])) {

            // for one element build array instead
            if (!is_array($data['pages'])) {
                array_push($pages, $data['pages']);
            } else {
                $pages = $data['pages'];
            }

            // use array_push because of condtion {categorie} format
            $post_type = 'page'; // by default page
            if ($pages[0]->post_type_id == 1) {
                // if we get here post is article
                array_push($data['headers'], "<p>Catégories</p>");
                $post_type = 'article';
            }

            // Sort
            Helper::sortByLastUpdated($pages);

            // build column
            foreach ($pages as $key => $row) {
                $rowsData[$key]['checkbox'] = '<label class="checkbox"><input class="checkboxItem" type="checkbox" name="action_' . $row->id . '" value="' . $row->id . '"><span class="checker"></span></label>';
                //Helper::dump($row);die();
                $i = (int) $row->user_id;
                $user = $data['users'][$i - 1];

                // links to modify, delete, trash
                $title = '<span class="post-title">' . Helper::xss($row->title) . '</span>';
                $title .= '<span class="post-edit">';
                if(!empty($row->slug)) {
                    $title .= '<a target="_blank" href="' . DIRNAME . Helper::xss($row->slug) . '">Afficher</a>';
                }
                $title .= '<a href="' . DIRNAME . "admin/" . $post_type . "/edit/" . $row->id . '">Modifier</a>';

                if ($row->trash == 0) {
                    $title .= '<a class="post-move-to-bin" href="' . DIRNAME . 'admin/' . $post_type . '/move-to-bin" data-id="' . $row->id . '">Mettre à la corbeille</a>';
                }

                if ($row->trash == 1) {
                    $title .= '<a class="post-remove-from-bin" href="' . DIRNAME . 'admin/' . $post_type . '/remove-from-bin" data-id="' . $row->id . '">Restaurer</a>';
                }
                $title .= '<a class="post-delete" href="' . DIRNAME . 'admin/' . $post_type . '/delete" data-id="' . $row->id . '">Supprimer définitivement</a>';
                $title .= '</span>';

                $publishedDate = new DateTime($row->publish_at); // format date from MYSQL database
                $updatedDate = new DateTime($row->updated_at); // format date from MYSQL database

                // set all columns
                $rowsData[$key]['title'] = $title;
                $rowsData[$key]['author'] = Helper::xss($user->firstname . " " . $user->lastname);
                $rowsData[$key]['Etat'] = ($row->status == 0) ? "Non publié" : "Publié";

                // manage categories for articles
                if ($row->post_type_id == 1) {
                    $categories = array_column($row->relations['categories']['data'], 'name');
                    $rowsData[$key]['categories'] = (count($categories) > 0) ? implode(', ', $categories) : "-";
                }
                $rowsData[$key]['publishedDate'] = $publishedDate->format("d-m-Y") . " à " . $publishedDate->format("H:i:s");
                $rowsData[$key]['updatedDate'] = $updatedDate->format("d-m-Y") . " à " . $updatedDate->format("H:i:s");
            }
        }

        // data column after categories
        array_push($data['headers'], "<p>Date de publication</p>", "<p>Dernière mise à jour</p>");

        return ['headers' => $data['headers'], 'rows' => $rowsData, 'id' => $post_type . '-list'];
    }
}
