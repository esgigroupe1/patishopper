<?php

class ProductController extends Controller
{

    public function showAction($params)
    {
        $v = new View("show-product", "front");
    }

    public function commentAction($params)
    {
        (!Helper::isRoute("/product/comment/add")) && $this->errors(404);
        if (!empty($params['POST']['ajaxrequest'])) {
            sleep(1);
            if (empty($params['POST']['text'])) {
                echo json_encode(
                    [
                        'comment' => "",
                        'message' => "Saisissez avis d'abord SVP !",
                    ]
                );
            } else {

                $comment = new Comment();
                $comment->product_id = $params['POST']['product_id'];
                //$comment->setProductId($params['POST']['product_id']);
                $comment->setContent($params['POST']['text']);
                $comment->setUserId($_SESSION['id_user']);
                $comment->save();

                $user = $comment->getUser();
                $date = new DateTime($comment->created_at);
                $date = $date->format("d-m-Y") . " à " . $date->format("H:i:s");
                $text = $comment->content;
                echo json_encode(
                    [
                        'user' => $user->firstname,
                        'date' => $date,
                        'comment' => Helper::xss($text),
                        'message' => "Votre avis été enregistré ! Il sera visible par tous rapidement.",
                    ]
                );
            }
        } else {
            $this->errors(404);
        }
    }
}
