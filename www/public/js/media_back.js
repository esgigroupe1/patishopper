//Variables
var label = document.querySelector("#select-file");
var inputfile = document.querySelector(".browse");
var filename = document.querySelector(".filename");
var delete_button = document.getElementById('delete_media');
var submit_settings = document.getElementById('submit-settings');

//si on est dans media-add
if (inputfile) {
    $('#select-file').click(function () {
        $('.upload .browse').click();
    });

    inputfile.addEventListener("change", function (e) {
        filename.innerHTML = this.value;
    });
}

//delete a media file
$(document).ready(function () {
    $("#delete").submit(function (e) {
        e.preventDefault();
        //put media file's id in input
        var confirmation = window.confirm('Souhaitez-vous vraiment supprimer ce fichier ?');
        var form_data = $(this).serialize();
        
        if(confirmation)
        {
            $.ajax({
                url : '/admin/media/delete',
                type : 'POST',
                data : form_data,
                datatype : "text",
            }).done(function(response){
                window.location.reload();
                $("#message").css("display","block");
                $("#message").append("<p>"+response+"</p>");

            }).fail(function (xhr) {
                console.log(xhr.responseText);
            });
        }

    });

    $(".media_file").click(
        function() 
        {
            var id_media = $(this).attr('id');
            var name_media = $(this).attr('alt');

            if($(this).hasClass("media-chosen"))
            {
                $(this).removeClass("media-chosen");
            }
            else
            {
                $(this).addClass("media-chosen");
                delete_button.style.display = "block";
            }
            $(".media_id").val(id_media);
            $(".media_name").val(name_media);

        }
    );
    // update selected image src on edit or add Model
    $("#mediaModal img").on('click', function (e) {
        var newSrc = $(this).prop('src');
        var $linkToUpdate = $("a#medias");
        var $imgToUpdate = $("img#medias");

        $imgToUpdate.attr('src', newSrc);


        if ($linkToUpdate.length !== 0) {
            $linkToUpdate.remove();
            $('.image-preview').html('<img id="medias" class="image" src="' + newSrc + '" title="">');
        }
    });

    // fix with update dom case
    $(".image-preview").on('click', '#medias', function () {
        $('#mediaModal').css('display', 'block'); // open modal in particular case
    });
});

//Modal media
var modal = document.getElementById('mediaModal');

// button that opens the modal
var btn = document.getElementById("medias");

// <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

    function choose(e) {
        //media's id
        var mediaId = e.target.id;
        var mediaSrc = e.target.src;

        // put the id in the input
        $("#media-selected").val(mediaId);
        //put the link of the selected media on logo's src
        $("#img_modal").attr("src",mediaSrc);

        modal.style.display = "none";
        
    }

if (btn !== null) {
    // onclick on the button, open the modal
    btn.onclick = function () {
        modal.style.display = "block";
    }
}

// onclick on <span> (x), or outside the modal close the modal
if (span != undefined) {
    span.onclick = function () {
        modal.style.display = "none";
    }
}


window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

