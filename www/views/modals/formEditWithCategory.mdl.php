<pre>
<?php //print_r($config);?>
<?php print_r($errors);?>
</pre>


<form method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">

    <?php foreach ($config["input"] as $name => $params):?>

        <?php if($params["type"] == "text" || $params["type"] == "select"):?>

            <?php if($params["type"] == "text"):?>
                <div class="row no-collapse">
                <div class="contain_title">
                    <input
                        type="<?php echo $params["type"];?>"
                        name="<?php echo $name;?>"
                        placeholder="<?php echo $params["placeholder"];?>"
                        class="<?php echo (isset($params["class"]))? $params["class"] : "";?>"
                        id="<?php echo (isset($params["id"]))? $params["id"] : "";?>"
                        <?php echo (isset($params["required"]))?"required='required'":"";?>
                        <?php echo (!empty($params["data"])) ? 'value="'.($params["data"]).'"':"";?>
                    >
                </div>
            <?php elseif($params["type"] == "select"):?>
                <div class="contain_category">
                    <select name="<?php echo $name; ?>" class="<?php echo $params["class"];?>" >
                        <option value="<?php echo $params["data"]; ?>"><?php echo $params["data"]; ?></option>
                    </select>
                </div>
                </div>
            <?php endif;?>
        <?php endif; ?>

        <?php if ($params["type"] == "file"): ?>
            <div class="contain_files">
                <label for="media_files" class="button_files button_font">Ajouter un media</label>
                <input
                    type="<?php echo $params["type"];?>"
                    name="<?php echo $name;?>"
                    class="<?php echo $params["class"];?>"
                    id="<?php echo $params["id"]?>"
                ><br>
            </div>
        <?php endif;?>

        <?php if ($params["type"] == 'tinymce'): ?>
            <textarea name="content" cols="50" rows="15" class="<?php echo $params["class"];?>">
                <?php echo (!empty($params["data"])) ? $params["data"] : ""; ?>
            </textarea>
        <?php endif;?>

    <?php endforeach;?>

    <input type="submit" class="button" value="<?php echo $config["config"]["submit"];?>">

</form>
