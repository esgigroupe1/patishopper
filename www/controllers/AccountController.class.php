<?php

class AccountController extends Controller
{
    public function informationsAction($params)
    {
        (!Helper::canOrder()) && $this->errors(404);
        $v = new View("account", "front");
        $v->assign('page', 'informations');
        $config['user'] = User::findById($_SESSION['id_user']);

        $p = $params['POST'];
        $u = $config['user'];
        $config['errors'] = [];

        if (!empty($p)) {
            if (!empty($p['firstname'])) {
                $u->firstname = $p['firstname'];
                $_SESSION['firstname'] = $p['firstname'];
            } else {
                $config['errors']['lastname'] = "Le nom est obligatoire";
            }
            if (!empty($p['lastname'])) {
                $u->lastname = $p['lastname'];
            } else {
                $config['errors']['lastname'] = "Le prénom est obligatoire";
            }
            if (!empty($p['phone'])) {
                $u->phone = $p['phone'];
            }
            if (!empty($p['email'])) {
                $u->email = $p['email'];

                if (Validator::checkEmail($p['email'])) {
                    $users = User::select(['id|<>' => $_SESSION['id_user'], 'email' => $p['email']]);

                    !(empty($users)) && $config['errors']['email'] = "Cet email est déjà utilisé pour un autre utilisateur";
                }else{
                    $config['errors']['email'] = "Cet champ email est incorrect";
                }

            } else {
                $config['errors']['email'] = "L'email est obligatoire";
            }
            // Password
            if (!empty($p['address'])) {
                $u->address = $p['address'];
            }
            if (!empty($p['city'])) {
                $u->city = $p['city'];
            }
            if (!empty($p['country'])) {
                $u->country = $p['country'];
            }

            if (!empty($p['password']) || !empty($p['passwordConfirm'])) {
                if ($p['password'] == $p['passwordConfirm']) {
                    $u->setPassword($p['password']);
                } else {
                    $config['errors']['password_error'] = "Vos champs de mots de passe ne sont pas identiques";
                }
            }
            if (!empty($p['postal_code'])) {
                $u->postal_code = $p['postal_code'];
            }

            if (empty($config['errors'])) {
                $u->save();
                $config['message_success'] = "Vos informations ont été modifiées";
            }
        }
        $v->assign('config', $config);
    }

    public function ordersAction($params)
    {
        if (!empty($_SESSION['id_user'])) {
            (!Helper::canOrder()) && $this->errors(404);
        } else {
            header("Location: " . DIRNAME . "authentication");
            die();
        }
        if (empty($params['URL'])) {
            $v = new View("account", "front");
            $v->assign('page', 'orders');
            $orders = Order::getCollections(['user_id' => $_SESSION['id_user']]);
            $orders = (!empty($orders)) ? $orders : [];
            $config['orders'] = $orders;
            $v->assign('config', $config);
        } else {
            if (!empty($params['URL'][0]) && is_numeric($params['URL'][0])) {
                (!Helper::isRoute("/account/orders/" . $params['URL'][0])) && $this->errors(404);
                $order_id = $params['URL'][0];
            } else {
                $this->errors(404);
            }
            $v = new View("order-details", "front");
            $transactions = null;
            $transactions = Transaction::select(['order_id' => $order_id]);
            (empty($transactions)) && $this->errors(404);
            $order = Order::findById($order_id);

            $v->assign('transactions', $transactions);
            $v->assign('order', $order);
        }
    }

    public function billsAction($params)
    {
        (!Helper::canOrder()) && $this->errors(404);
        $v = new View("account", "front");
        $v->assign('page', 'bills');
    }

    public function messagesAction($params)
    {
        (!Helper::canOrder()) && $this->errors(404);
        $v = new View("account", "front");
        $v->assign('page', 'messages');
    }
}
