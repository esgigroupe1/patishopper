<main id="error-<?php echo $errorStatus; ?>" class="container">
    <div class="row">
        <h1><?php echo $errorDescription; ?></h1>
    </div> <!-- end row -->

    <div class="row">
        <div class="col-12">
            <p class="error-message"><?php echo $errorMessage; ?></p>
        </div> <!-- end col-12 -->
    </div> <!-- end row -->

    <div class="row">
        <div class="col-12 home-button">
            <a class="button center" href="<?php echo DIRNAME; ?>">Aller à la page d'accueil</a>
        </div> <!-- end col-12 -->
    </div> <!-- end row -->

    <hr>

    <?php if($errorStatus == '404'): ?>
    <main id="products-wrapper">
        <div class="container no-collapse">
            <div class="row">
                <h2 style="margin-left: 10px;">Découvrez nos derniers produits</h2>
            </div> <!-- end row -->
            <div id="all-products" class="row no-collapse">
            <?php foreach($products as $product): ?>
                <div class="col-4 col-lg-6 col-sm-12 product">
                    <a href="<?php echo DIRNAME . $product->slug; ?>" class="image">
                        <?php $media = $product->getAttachment(); ?>
                        <?php if (!empty($media)): ?>
                            <img class="featured-image" src="<?php echo $media->url ?>" title="<?php echo $media->name; ?>">
                        <?php endif;?>
                    </a> <!-- end link -->
                    <h3><?php echo $product->name; ?></h3>
                    <p><?php echo $product->price; ?>€</p>
                </div>
            <?php endforeach; ?>
            </div> <!-- all-products -->
        </div> <!-- end container -->
    </main> <!-- end products-wrapper -->
    <?php endif; ?>
</main>
