<pre>
<?php //Helper::dump($config);?>
<?php //print_r($errors);?>
</pre>

<form method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">

    <?php foreach ($config["input"] as $name => $params):?>
		<?php if($params["type"] == "text" || $params["type"] == "email" || $params["type"] == "password" || $params["type"] == "tel"):?>

			    <div class="control-group">
					<p>
					<input
				    type="<?php echo $params["type"];?>"
				    <?php echo Helper::showIfExists($params, 'name'); ?>
				    <?php echo Helper::showIfExists($params, 'value'); ?>
				    <?php echo Helper::showIfExists($params, 'placeholder'); ?>
                    <?php echo (isset($params["required"]))?"required='required'":"";?>
                    <?php if(!empty($name)):?>
                        <?php $error_desc = !empty($config['input'][$name]['data']) ? $config['input'][$name]['data'] : ""; ?>
                        value="<?php echo (!isset($errors[$name])&&!empty($_POST[$name]))?$_POST[$name]: $error_desc;?>"
                    <?php endif; ?>
				    />
					</p>
			    </div>

            <?php elseif ($params["type"] == 'tinymce'): ?>
				<textarea name="text" cols="50" rows="15">
					<?php echo (!empty($params["data"])) ? $params["data"] : ""; ?>
				</textarea>

        <?php endif;?>

    <?php endforeach;?>

	<?php if(isset($config["select"])) : ?>
	<div class="control-group">
		<label><?php echo $config['select']['config']['label'] ?></label><br/>
		<select name="<?php echo $config['select']['config']['name'] ?>">
			<?php foreach($config["select"][options] as $select => $params): ?>

					<option value="<?php echo $params['value']; ?>"><?php echo $params['value']; ?></option>

			<?php endforeach; ?>
		</select>
	</div>
	<?php endif; ?>

	<?php if(isset($config["radio"])) : ?>
		<div class="radio_div">
			<label class="<?php echo $config['radio']['config']['label1_class'] ?>"><?php echo $config['radio']['config']['label1_texte']; ?></label>
			<?php foreach ($config["radio"]["options"] as $value => $params): ?>
				<label class="<?php echo $config['radio']['config']['label2_class'] ?>">
				<?php if(isset($params["link"])): ?>
					<img src="<?php echo $params["link"];?>" alt="<?php echo $params['alt_img']; ?>" /><br />
				<?php endif ?>
					<input type="radio" name="<?php echo $params['name'];?>" value="<?php echo $value; ?>" <?php echo $params['checked'] ?>/>
				</label>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>

	<?php if(isset($config["img"])) : ?>
		<div class="control-group">
			<?php foreach ($config["img"] as $value => $params): ?>
				<label><?php echo isset($params["label"]) && $params["label"]; ?></label><br/>
				<img id ="<?php echo $params["id"];?>" src="<?php echo $params["src"];?>" alt="<?php echo $params["alt"];?>" /><br />
			<?php endforeach; ?>
		</div>
	<?php endif;?>

	<?php if(isset($config["textarea"])) : ?>
		<?php foreach ($config["textarea"] as $value => $params): ?>
		<textarea rows="<?php $params['rows'] ?>" cols="<?php $params['cols'] ?>" required="<?php echo $params['required']; ?>" placeholder="<?php echo $params['placeholder'] ?>" name="<?php echo $params['name']; ?>"><?php //echo $params['placeholder']; ?></textarea>
		<?php endforeach; ?>
	<?php endif;?>

	<div class="control-group">
		<input type="submit" class="<?php echo isset($config["config"]["class"]) ? $config["config"]["class"] : ''?>" value="<?php echo $config["config"]["submit"];?>" name="<?php echo isset($config["config"]["name"]) ? $config["config"]["name"] : '';?>">
	</div>
</form>
