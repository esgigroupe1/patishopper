<main id="cart-confirmation" class="container">
    <div class="row">
        <div class="col-12">
            <div class="title">
                <h1>Paiement accepté</h1>
                <span><img src="<?php echo DIRNAME; ?>public/images/icons/checked.svg" alt=""></span>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row">
        <div class="col-12">
            <div class="content">
                <p>Votre paiment a été effectué avec succès</p>
                <p>Un mail vous a été envoyé pour confirmer votre commande.</p>
                <p>Vous pouvez retrouver l'historique de vos commandes sur votre espace client.</p>
                <p>Si vous avez besoin d'informations complémentaires, vous pouvez contacter notre service après-vente.</p>
            </div>

            <a href="<?php echo DIRNAME; ?>" class="button center">Revenir à l'accueil</a>
        </div>
    </div>
</main>
