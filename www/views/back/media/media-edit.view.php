<?php $message = $this->getAssign();?>

<main class="container-fluid" id="media-back">
    <div class="row">
        <div class="title col-4">
            <h1>Ajouter un média</h1>
        </div>

        <div class="col-4">
            <?php if(!empty($message['messages'])): ?>
                <div class="message error">
                    <p><?php echo $message['messages']; ?></p>
                </div>
            <?php elseif(!empty($message['messages_success'])): ?>
                <div class="message success">
                    <p><?php echo $message['messages_success']; ?></p>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-1">

        </div>

        <div class="col-3">
            <div class="message info">
                <span><?php echo "Taille maximale des fichiers : 48MB"; ?></span>
            </div>
        </div>
    </div>
    <hr class="separator">

    <div class="row">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <form method="post" action="" enctype="multipart/form-data">
                    <input type="submit" class="button center" name="upload" value="Ajouter les fichiers sélectionnés" />
                    <div class="upload">
                    <label for="upload-media" id="select-file">
                        <span>Cliquer sur la zone pour choisir des fichiers ou glissez-déposer sur la zone</span>
                        <input id="upload-media" type="file" name="upload-media[]" class="browse" value="Choisir des médias" multiple  />
                    </label>
                        <input type="hidden" value="6000000" name="MAX_FILE_SIZE" />
                        <p class="filename"></p>
                    </div>
                    <!-- end upload -->
                    </form>
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end row -->
</main>
