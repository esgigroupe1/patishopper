<?php
header('Content-Type: application/xml');
echo '<?xml version="1.0" encoding="UTF-8" ?>';
echo '<?xml-stylesheet type="text/css" href="/public/css/sitemap.css"?>';

$defaults = Helper::getSitemapData();
$categories = Category::findAll();
$posts = Post::findAll();
$products = Product::findAll();

?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

    <?php foreach ($defaults as $default): ?>
        <url>
            <loc><?php echo $default['loc']; ?></loc>
            <lastmod><?php echo $default['lastmod']; ?></lastmod>
            <priority><?php echo $default['priority']; ?></priority>
        </url>
    <?php endforeach;?>

    <?php foreach ($categories as $category): ?>
        <?php if ($category->type == 2 && !empty($category->slug)): ?>
        <url>
            <loc><?php echo Helper::getSiteUrl() . '/produits/' . $category->slug; ?></loc>
            <lastmod><?php echo (new DateTime($category->updated_at))->format('Y-m-d\TH:i:sP'); ?></lastmod>
            <priority><?php echo '0.5'; ?></priority>
        </url>
        <?php endif;?>
    <?php endforeach;?>

    <?php foreach ($categories as $category): ?>
        <?php if ($category->type == 1 && !empty($category->slug)): ?>
        <url>
            <loc><?php echo Helper::getSiteUrl() . '/articles/' . $category->slug; ?></loc>
            <lastmod><?php echo (new DateTime($category->updated_at))->format('Y-m-d\TH:i:sP'); ?></lastmod>
            <priority><?php echo '0.5'; ?></priority>
        </url>
        <?php endif;?>
    <?php endforeach;?>

    <?php foreach ($posts as $post): ?>
        <?php if (!empty($post->slug)): ?>
        <url>
            <loc><?php echo Helper::getSiteUrl() . '/' . $post->slug; ?></loc>
            <lastmod><?php echo (new DateTime($post->updated_at))->format('Y-m-d\TH:i:sP'); ?></lastmod>
            <priority><?php echo '0.8'; ?></priority>
        </url>
        <?php endif;?>
    <?php endforeach;?>

    <?php foreach ($products as $product): ?>
        <?php if (!empty($product->slug)): ?>
        <url>
            <loc><?php echo Helper::getSiteUrl() . '/' . $product->slug; ?></loc>
            <lastmod><?php echo (new DateTime($product->updated_at))->format('Y-m-d\TH:i:sP'); ?></lastmod>
            <priority><?php echo '0.8'; ?></priority>
        </url>
        <?php endif;?>
    <?php endforeach;?>
</urlset>
