<div id="patishopper-user" class="container-fluid collapse">
  <div class="row">
    <div class="container">
      <div id="user-form">
        <div id="form">
          <h1>Renseignez votre email</h1>
          <?php if(isset($validate)) : ?>
          <b><?php echo $validate; ?></b>
          <?php endif; ?>
            <?php $this->addModal("form", $config, $errors);?>
        </div>
        <!-- end form -->
      </div>
      <!-- end user-form -->
    </div>
    <!-- end container -->
  </div>
  <!-- end row -->
</div><!-- end container-fluid -->
