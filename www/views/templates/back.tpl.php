<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href=<?php echo DIRNAME. "public/images/icons/orange/favicon.png"; ?> type="image/x-icon">
        <link rel="icon" href=<?php echo DIRNAME. "public/images/icons/orange/favicon.png"; ?> type="image/png">


        <title>Administration  <?php echo $this->getTitle(); ?></title>

        <?php $color = ''; ?>
        <!-- Récupération du thème en base -->
        <?php
            $theme = Setting::findByName("theme_back")->value;
            switch($theme) {
                case 'orange':
                $theme_file = 'orange_theme.php';
                $color = 'orange';
                break;
                case 'pink':
                $theme_file = 'pink_theme.php';
                $color = 'pink';
                break;
                case 'brown':
                $theme_file = 'brown_theme.php';
                $color = 'brown';
                break;
                default:
                    $theme_file = 'orange_theme.php';
                    $color = 'orange';
                    break;
            }
            $logo = Setting::findByName("logo")->value;
            ?>
        <!-- Linked -->

        <link href=<?php echo DIRNAME . "public/css/".$theme_file ?> rel="stylesheet">

    </head>

    <body>
        <div id="back-template" class="container-fluid collapse">

            <div id="site-header" class="row">
                <div class="col-12">
                    <header>
                        <div id="back-mobile-navigation">
                            <a href="#" title="Menu" class="closed"></a>
                        </div><!-- end menu -->

                        <div id="site-info">
                            <a href="<?php echo DIRNAME; ?>admin" title="<?php echo (Setting::findByName('site_name'))->value; ?>">
                                <img src="<?php echo $logo; ?>" alt="Patishopper">
                                 <span><?php echo (Setting::findByName('site_name'))->value; ?></span>
                            </a>
                        </div>
                        <!-- site-info -->
                        <div id="informations">
                          <span>Bienvenue <?php echo Helper::xss(ucfirst($_SESSION['firstname'])); ?></span>
                          <a class="profil" href="<?php echo DIRNAME;?>profil/<?php echo $_SESSION['id_user'];?>">
                              <img src="<?php echo Helper::xss($_SESSION['attachment_user']); ?>" alt="">
                          </a>

                          <img src="<?php echo DIRNAME . 'public/images/back/icons/caret-dropdown.svg'; ?>" onclick="dropbtn()" class="dropbtn">
                          <div id="myDropdown" class="dropdown-content">
                            <a href="<?php echo DIRNAME;?>">Voir le site</a>
                            <a href="<?php echo DIRNAME;?>authentication/changePassword">Modifier le mot de passe</a>
                            <a href="<?php echo DIRNAME;?>disconnect">Déconnexion</a>
                          </div>

                        </div>

                        <!-- end informations -->
                    </header>
                </div> <!-- end col-md-12 -->
            </div> <!-- end row -->
            <div id="site-content" class="row">

                    <div id="site-navigation" class="col-2 responsive">
                       <nav>
                            <div id="overlay">
                                <ul>
                                    <li class="<?php echo ($c == "AdminController" && $a == "indexAction") ? "active-item" : "inactive"; ?>">
                                        <a href="<?php echo DIRNAME; ?>admin" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/home_'.$color.'.svg'; ?>" alt=""></i><span>Accueil</span></a>
                                    </li>
                                    <li class="<?php echo ($c == "AdminController" && $a == "articleAction") ? "active-item" : "inactive"; ?>">
                                        <a href="<?php echo DIRNAME; ?>admin/article" >
                                            <i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/article_'.$color.'.svg'; ?>" alt=""></i><span>Articles</span>
                                        </a>
                                        <ul id="dropdown-menu" class='<?php echo ($c == "AdminController" && $a == "articleAction") ? "active" : ""; ?>'>
                                            <li class="<?php echo ($c == "AdminController" && $a == "articleAction" && $params['URL'][0] == "category") ? "active-item" : "inactive"; ?>"><a href="<?php echo DIRNAME; ?>admin/article/category" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/categorie_'.$color.'.svg'; ?>" alt=""></i><span>Catégories</span></a>
                                            </li>
                                            <li class='<?php echo ($c == "AdminController" && $a == "articleAction" && $params['URL'][0] == "comment") ? "active-item" : "inactive"; ?>'><a href="<?php echo DIRNAME; ?>admin/article/comment" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/commentaire_'.$color.'.svg'; ?>" alt=""></i><span>Commentaires</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="<?php echo ($c == "AdminController" && $a == "productAction") ? "active-item" : "inactive"; ?>">
                                        <a href="<?php echo DIRNAME; ?>admin/product" >
                                            <i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/produits_'.$color.'.svg'; ?>" alt=""></i><span>Produits</span>
                                        </a>
                                        <ul id="dropdown-menu" class='<?php echo ($c == "AdminController" && $a == "productAction") ? "active" : ""; ?>'>
                                            <li class="<?php echo ($c == "AdminController" && $a == "productAction" && $params['URL'][0] == "category") ? "active-item" : "inactive"; ?>"><a href="<?php echo DIRNAME; ?>admin/product/category" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/categorie_'.$color.'.svg'; ?>" alt=""></i><span>Catégories</span></a>
                                            </li>

                                            <li class='<?php echo ($c == "AdminController" && $a == "productAction" && $params['URL'][0] == "comment") ? "active-item" : "inactive"; ?>'><a href="<?php echo DIRNAME; ?>admin/product/comment" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/commentaire_'.$color.'.svg'; ?>" alt=""></i><span>Avis</span></a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="<?php echo ($c == "AdminController" && $a == "pageAction") ? "active-item" : "inactive"; ?>"><a href="<?php echo DIRNAME; ?>admin/page" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/page_'.$color.'.svg'; ?>" alt=""></i><span>Pages</span></a></li>
                                    <li class="<?php echo ($c == "AdminController" && $a == "mediaAction") ? "active-item" : "inactive"; ?>"><a href="<?php echo DIRNAME; ?>admin/media" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/medias_'.$color.'.svg'; ?>" alt=""></i><span>Médias</span></a></li>

                                    <?php if (!empty($_SESSION['user_type_id']) && $_SESSION['user_type_id'] == 1) : ?>
                                    <li class="<?php echo ($c == "AdminController" && $a == "userAction") ? "active-item" : "inactive"; ?>"><a href="<?php echo DIRNAME; ?>admin/user" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/utilisateurs_'.$color.'.svg'; ?>" alt=""></i><span>Utilisateurs</span></a></li>
                                    <?php endif; ?>

                                    <li class="<?php echo ($c == "AdminController" && $a == "menuAction") ? "active-item" : "inactive"; ?>"><a href="<?php echo DIRNAME; ?>admin/menu" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/menu_'.$color.'.svg'; ?>" alt=""></i><span>Menus</span></a></li>
                                    <li class="<?php echo ($c == "AdminController" && $a == "settingAction") ? "active-item" : "inactive"; ?>"><a href="<?php echo DIRNAME; ?>admin/setting" ><i><img src="<?php echo DIRNAME . 'public/images/back/nav/'.$color.'/parametres_'.$color.'.svg'; ?>" alt=""></i><span>Paramètres</span></a></li>
                                </ul>
                            </div>
                        </nav>
                   </div>
                   <!-- end col-2 -->

                    <div id="site-view-content" class="col-10">

                        <div id="view-content" class="container-fluid">
                            <!-- Dynamic content -->
                                <?php include "views/back/".$this->folder . "/" . $this->view; ?>
                        </div>
                        <!-- end row -->
                        <!-- End Dynamic content -->
                        <div id="site-footer" class="row">
                            <div class="col-12">
                                <footer>
                                    <p>&copy; Patishopper 2018</p>
                                    <ul>
                                        <li><a href="<?php echo DIRNAME;?>mentions-legales" title="Conditions">Conditions</a></li>
                                        <li><a href="<?php echo DIRNAME;?>contact" title="Support">Support</a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="https://www.facebook.com/ESGIParis/?ref=br_rs" title="Facebook"><i><img src="<?php echo DIRNAME . 'public/images/icons/'.$color.'/facebook_'.$color.'.svg'; ?>" alt="Facebook"></i></a></li>
                                        <li><a href="https://www.linkedin.com/company/esgi-ecole-sup-rieure-de-g-nie-informatique-/?originalSubdomain=fr" title="LinkedIn"><i><img src="<?php echo DIRNAME . 'public/images/icons/'.$color.'/linkedin_'.$color.'.svg'; ?>" alt="LinkedIn"></i></a></li>
                                        <li><a href="https://twitter.com/esgi?lang=fr" title="Twitter"><i><img src="<?php echo DIRNAME . 'public/images/icons/'.$color.'/twitter_'.$color.'.svg'; ?>" alt="Twitter"></i></a></li>
                                    </ul>
                                </footer>
                            </div>
                        </div><!-- end row -->
                    </div> <!-- end col-10 -->
            </div> <!-- end row -->
        </div><!-- end container-fluid -->

        <script>
            function random32bit() {
                let u = new Uint32Array(1);
                window.crypto.getRandomValues(u);
                let str = u[0].toString(16).toUpperCase();
                return '00000000'.slice(str.length) + str;
            }
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <?php if($c == "AdminController" && $a == "indexAction"): ?>
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script src="https://code.highcharts.com/modules/exporting.js"></script>
            <script src="<?php echo DIRNAME . 'public/js/home_stats.js'; ?>"></script>
        <?php endif; ?>

        <script src="<?php echo DIRNAME . 'public/js/media_back.js'; ?>"></script>
        <script src="<?php echo DIRNAME . 'public/js/menu_back.js'; ?>"></script>
        <script src="<?php echo DIRNAME . 'public/js/back_menu.js'; ?>"></script>

        <?php $urls = explode('/', Helper::getURl()); global $a; ?>
        <?php if($a != "userAction" && (in_array('edit', $urls) || in_array('add', $urls)) && !in_array('category', $urls)): ?>

        <script src="<?php echo DIRNAME . 'public/ckeditor/ckeditor.js'; ?>"></script>
        <script type="text/javascript">
           CKEDITOR.replace( 'text', {
               contentsCss: "body {font-size: 20px; font-family: 'Nunito', sans-serif; }",
               language: 'fr',
               height: 350,
            });
        </script>

        <?php endif; ?>

        <script src="<?php echo DIRNAME . 'public/js/view_back.js'; ?>"></script>

        <?php if((in_array($a, ['pageAction', 'articleAction', 'productAction', 'menuAction']))) : ?>
            <script src="<?php echo DIRNAME . 'public/js/post_product_back.js'; ?>"></script>
        <?php endif; ?>

        <?php if(($a == "productAction" || $a == "articleAction")) : ?>
            <script src="<?php echo DIRNAME . 'public/js/category_back.js'; ?>"></script>
        <?php endif; ?>
        <script>
         /**
         * Dropdown Menu
         */
        function dropbtn() {
            document.getElementById("myDropdown").classList.toggle("show");
        }

        // Close the dropdown if the user clicks outside of it
        window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
              }
            }
          }
        }
        </script>
    </body>

</html>
