<?php
class User extends BaseSQL
{

    protected $id = null;
    protected $firstname;
    protected $lastname;
    protected $phone;
    protected $email;
    protected $password;
    protected $address;
    protected $city;
    protected $country;
    protected $postal_code;

    protected $token;
    protected $status = 0;

    protected $user_type_id;
    protected $attachment_id;

    protected $created_at = null;
    protected $updated_at = null;

    protected $relations = [
        'user_type' => [
            'type' => 'OneToOne',
            'table' => 'user_type',
        ],
        'orders' => [
            'type' => 'OneToMany',
            'table' => 'order',
            'data' => [],
        ],
        'posts' => [
            'type' => 'OneToMany',
            'table' => 'post',
            'data' => [],
        ],
        'comments' => [
            'type' => 'OneToMany',
            'table' => 'comment',
            'data' => [],
        ],
        'attachment' => [
            'type' => 'OneToOne',
            'table' => 'attachment',
        ],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function setFirstname($firstname)
    {
        $this->firstname = ucfirst(strtolower(trim($firstname)));
    }
    public function setLastname($lastname)
    {
        $this->lastname = strtoupper(trim($lastname));
    }
    public function setPhone($phone)
    {
        $this->phone = strtolower(trim($phone));
    }
    public function setEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = strtolower(trim($email));
        } else {
            return "L'adresse mail n'est pas valide";
        }
    }
    public function setAddress($address)
    {
        $this->address = strtolower(trim($address));
    }
    public function setCountry($country)
    {
        $this->country = strtolower(trim($country));
    }
    public function setCity($city)
    {
        $this->city = strtolower(trim($city));
    }
    public function setPostalCode($postal_code)
    {
        $this->postal_code = strtolower(trim($postal_code));
    }
    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }
    public function setToken($token = null)
    {
        if ($token) {
            $this->token = $token;
        } else if (!empty($this->email)) {
            $this->token = substr(sha1("GDQgfds4354" . $this->email . substr(time(), 5) . uniqid() . "gdsfd"), 2, 10);
        }
    }

    public static function configFormAdd()
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => "S'inscrire",
                "class" => "button",
            ],

            "input" => [

                "firstname" => [
                    "name" => "firstname",
                    "type" => "text",
                    "placeholder" => "Votre prénom *",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                ],

                "lastname" => [
                    "name" => "lastname",
                    "type" => "text",
                    "placeholder" => "Votre nom *",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                ],

                "phone" => [
                    "name" => "phone",
                    "type" => "tel",
                    "placeholder" => "Votre numéro de téléphone *",
                    "required" => "required",
                ],

                "email" => [
                    "name" => "email",
                    "type" => "email",
                    "placeholder" => "Votre email *",
                    "required" => "required",
                ],

                "emailConfirm" => [
                    "name" => "emailConfirm",
                    "type" => "email",
                    "placeholder" => "Retapez votre email *",
                    "required" => "required",
                    "confirm" => "email",
                ],

                "address" => [
                    "name" => "address",
                    "type" => "text",
                    "placeholder" => "Votre adresse *",
                    "required" => "required",
                    "maxString" => 255,
                    "minString" => 5,
                ],

                "city" => [
                    "name" => "city",
                    "type" => "text",
                    "placeholder" => "Votre ville *",
                    "required" => "required",
                    "maxString" => 45,
                ],

                "country" => [
                    "name" => "country",
                    "type" => "text",
                    "placeholder" => "Votre pays *",
                    "required" => "required",
                    "maxString" => 45,
                ],

                "postal_code" => [
                    "name" => "postal_code",
                    "type" => "text",
                    "placeholder" => "Votre code postal *",
                    "required" => true,
                ],

                "password" => [
                    "name" => "password",
                    "type" => "password",
                    "placeholder" => "Votre mot de passe *",
                    "required" => "required",
                ],

                "passwordConfirm" => [
                    "name" => "passwordConfirm",
                    "type" => "password",
                    "placeholder" => "Retapez votre mot de passe *",
                    "required" => "required",
                    "confirm" => "password",
                ],

                "captcha" => [
                    "name" => "captcha",
                    "type" => "text",
                    "placeholder" => "Valeur du captcha *",
                    "required" => true,
                ],
            ],

            "img" => [
                "captcha" => [
                    "id" => "captcha",
                    "src" => "./captcha.php",
                    "alt" => "captcha",
                ],
            ],
        ];
    }

    public static function configFormForgotPwd()
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => "Valider",
                "class" => "button",
            ],

            "input" => [

                "email" => [
                    "type" => "email",
                    "placeholder" => "Votre email",
                    "name" => "email",
                    "required" => "required",
                ],
            ],
        ];
    }

    public static function configFormForgotChangePwd()
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => "Valider",
                "class" => "button",
            ],

            "input" => [

                "password" => [
                    "type" => "password",
                    "placeholder" => "Nouveau mot de passe",
                    "required" => true,
                ],

                "passwordConfirm" => [
                    "type" => "password",
                    "placeholder" => "Retapez le nouveau mot de passe",
                    "required" => true,
                    "confirm" => "password",
                ],

                "captcha" => [
                    "type" => "text",
                    "name" => "captcha",
                    "placeholder" => "Valeur du captcha",
                    "required" => true,
                ],
            ],

            "img" => [
                "captcha" => [
                    "id" => "captcha",
                    "src" => "/../captcha.php",
                    "alt" => "captcha",
                ],
            ],
        ];
    }

    public static function configFormChangePwd()
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => "Valider",
                "class" => "button",
            ],

            "input" => [
                "currentPassword" => [
                    "name" => "currentPassword",
                    "type" => "password",
                    "placeholder" => "Mot de passe actuel",
                    "required" => true,
                ],

                "password" => [
                    "name" => "password",
                    "type" => "password",
                    "placeholder" => "Nouveau mot de passe",
                    "required" => true,
                ],

                "passwordConfirm" => [
                    "name" => "passwordConfirm",
                    "type" => "password",
                    "placeholder" => "Retapez le nouveau mot de passe",
                    "required" => true,
                    "confirm" => "password",
                ],

                "captcha" => [
                    "name" => "captcha",
                    "type" => "text",
                    "placeholder" => "Valeur du captcha",
                    "required" => true,
                ],
            ],

            "img" => [
                "captcha" => [
                    "id" => "captcha",
                    "src" => "/../captcha.php",
                    "alt" => "captcha",
                ],
            ],
        ];
    }

    public static function configFormAdminAdd()
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => "S'inscrire",
                "class" => "button",
            ],

            "input" => [

                "firstname" => [
                    "name" => "firstname",
                    "type" => "text",
                    "placeholder" => "Prénom *",
                    "required" => true,
                    "minString" => 2,
                    "maxString" => 100,
                ],

                "lastname" => [
                    "name" => "lastname",
                    "type" => "text",
                    "placeholder" => "Nom *",
                    "required" => true,
                    "minString" => 2,
                    "maxString" => 100,
                ],

                "phone" => [
                    "name" => "phone",
                    "type" => "tel",
                    "placeholder" => "Numéro de téléphone *",
                    "required" => true,
                ],

                "email" => [
                    "name" => "email",
                    "type" => "email",
                    "placeholder" => "Email *",
                    "required" => true,
                ],

                "emailConfirm" => [
                    "type" => "email",
                    "placeholder" => "Retapez l'email *",
                    "required" => true,
                    "confirm" => "email",
                ],

                "address" => [
                    "name" => "address",
                    "type" => "text",
                    "placeholder" => "Adresse *",
                    "required" => true,
                    "maxString" => 255,
                    "minString" => 5,
                ],

                "city" => [
                    "name" => "city",
                    "type" => "text",
                    "placeholder" => "Ville *",
                    "required" => true,
                    "maxString" => 45,
                ],

                "country" => [
                    "name" => "country",
                    "type" => "text",
                    "placeholder" => "Pays *",
                    "required" => true,
                    "maxString" => 45,
                ],

                "postal_code" => [
                    "name" => "postal_code",
                    "type" => "text",
                    "placeholder" => "Code postal *",
                    "required" => true,
                    "maxString" => 5,
                ],

                "password" => [
                    "name" => "password",
                    "type" => "password",
                    "placeholder" => "Mot de passe *",
                    "required" => true,
                ],

                "passwordConfirm" => [
                    "type" => "password",
                    "placeholder" => "Retapez le mot de passe *",
                    "required" => true,
                    "confirm" => "password",
                ],
            ],
        ];
    }

    public static function configFormAdminEdit($data)
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => "Modifier",
                "class" => "button",
            ],

            "input" => [

                "firstname" => [
                    "label" => "Prénom",
                    "type" => "text",
                    "placeholder" => "Votre prénom",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "name" => "firstname",
                    "value" => $data->firstname,
                ],

                "lastname" => [
                    "type" => "text",
                    "placeholder" => "Votre nom",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "name" => "lastname",
                    "value" => $data->lastname,
                ],

                "phone" => [
                    "type" => "text",
                    "placeholder" => "Votre numero de téléphone",
                    "required" => false,
                    "minString" => 10,
                    "maxString" => 12,
                    "name" => "phone",
                    "value" => $data->phone,
                ],

                "email" => [
                    "type" => "email",
                    "placeholder" => "Votre email",
                    "required" => "required",
                    "name" => "email",
                    "value" => $data->email,
                ],

                "emailConfirm" => [
                    "type" => "email",
                    "placeholder" => "Retapez votre email",
                    "required" => "required",
                    "confirm" => "email",
                    "name" => "emailConfirm",
                    "value" => $data->email,
                ],

                "address" => [
                    "type" => "text",
                    "placeholder" => "Votre adresse",
                    "required" => "required",
                    "name" => "address",
                    "value" => $data->address,
                ],

                "city" => [
                    "type" => "text",
                    "placeholder" => "Votre ville",
                    "required" => "required",
                    "name" => "city",
                    "value" => $data->city,
                ],

                "country" => [
                    "type" => "text",
                    "placeholder" => "Votre pays",
                    "required" => "required",
                    "name" => "country",
                    "value" => $data->country,
                ],

                "postal_code" => [
                    "type" => "text",
                    "placeholder" => "Votre code postal",
                    "required" => "required",
                    "name" => "postal_code",
                    "value" => $data->postal_code,
                ],
            ],
        ];
    }

    public static function configFormAccount($data)
    {
        return [

            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => "Valider",
                "submit_class" => "button",
                "submit_name" => "submit",
            ],

            "input" => [

                "firstname" => [
                    "class" => 'input',
                    "type" => "text",
                    "placeholder" => "Votre prénom",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "name" => "firstname",
                    "value" => Helper::xss($data->firstname),
                    "label" => "<span>Prénom*</span>",
                ],

                "lastname" => [
                    "class" => 'input',
                    "type" => "text",
                    "placeholder" => "Votre nom",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "name" => "lastname",
                    "value" => Helper::xss($data->lastname),
                    "label" => "<span>Nom*</span>",
                ],

                "phone" => [
                    "class" => 'input',
                    "type" => "text",
                    "placeholder" => "Votre numero de téléphone",
                    "required" => false,
                    "minString" => 10,
                    "maxString" => 12,
                    "name" => "phone",
                    "value" => Helper::xss($data->phone),
                    "label" => "<span>Téléphone*</span>",
                ],

                "email" => [
                    "class" => 'input',
                    "type" => "email",
                    "placeholder" => "Votre email",
                    "required" => "required",
                    "name" => "email",
                    "value" => Helper::xss($data->email),
                    "label" => "<span>Email*</span>",
                ],

                "address" => [
                    "class" => 'input',
                    "type" => "text",
                    "placeholder" => "Votre adresse",
                    "required" => "required",
                    "name" => "address",
                    "value" => Helper::xss($data->address),
                    "label" => "<span>Adresse*</span>",
                ],

                "password" => [
                    "class" => 'input',
                    "name" => "password",
                    "type" => "password",
                    "placeholder" => "Votre mot de passe",
                    "label" => "<span>Mot de passe*</span>",
                ],

                "passwordConfirm" => [
                    "class" => 'input',
                    "type" => "password",
                    "name" => "passwordConfirm",
                    "placeholder" => "Retapez votre mot de passe",
                    "confirm" => "password",
                    "label" => "<span>Confirmation du mot de passe*</span>",
                ],

                "city" => [
                    "class" => 'input',
                    "type" => "text",
                    "placeholder" => "Votre ville",
                    "required" => "required",
                    "name" => "city",
                    "value" => Helper::xss($data->city),
                    "label" => "<span>Ville*</span>",
                ],

                "country" => [
                    "class" => 'input',
                    "type" => "text",
                    "placeholder" => "Votre pays",
                    "required" => "required",
                    "name" => "country",
                    "value" => Helper::xss($data->country),
                    "label" => "<span>Pays*</span>",
                ],

                "postal_code" => [
                    "class" => 'input',
                    "type" => "text",
                    "placeholder" => "Votre code postal",
                    "required" => true,
                    "name" => "postal_code",
                    "value" => Helper::xss($data->postal_code),
                    "label" => "<span>Code postal*</span>",
                ],

                // "user_type" => [
                //     "type" => "checkbox",
                //     "required" => true,
                //     "value" => $data->user_type_id
                // ]
            ],
        ];
    }
}
