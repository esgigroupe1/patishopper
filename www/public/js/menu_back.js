(function ($) {
$(document).ready(function(){
    // Permet aux sections déjà présentes d'utiliser l'accordéon
    sectionToggle();

    //Crée un nouvelle section
    $( "#ajoutSection" ).click(function(e){
        e.preventDefault();
        sectionToggle();
        $( "#navigationMenu" ).append("<li class='subSection'>" +
                                        "<div class='buttonMenu'>Name</div>" +
                                            "<div class='inputSection col-6 group'>" +
                                                "<label><p>Name</p><span class='bar'></span><span class='line-right-left'></span>" +
                                                "<input type='text' class='subTitle name' id='subTitle'></label>" +
                                                "<label><p>Href</p><span class='bar'></span><span class='line-right-left'></span>" +
                                                "<input type='text' class='subTitle href' id='subHref'></label>" +
                                                "<input type='hidden' class='subTitle id' id='subHref'></label>" +
                                                "<div class='delete'>" +
                                                    "<a class='deleteLine section-delete'>Supprimer la section</a>" +
                                                "</div>" +
                                            "</div>" +
                                    "</li>");
        listenFocus();
        sectionToggle();
    });

    // Permet la suppression d'un menu ou d'une section en Ajax
    $('a[class^="menu-"],a[class^="section-"]').on('click', function (e) {
        e.preventDefault();
        var urlTo = $(this).attr('href');
        var idTo = $(this).data('id');
        var nameTo = $(this).data('name');
        var randomValue = random32bit();
        var fade = $(this).closest("li");
        var line = $(this).closest("tr");

        $.ajax({
            url: urlTo + '/' + idTo,
            type: 'POST',
            data: {
                id: idTo,
                name: nameTo,
                ajaxrequest: randomValue
            },
            // data to send in type POST
            // dataType: 'json',
            // what type of data do we expect back from the server
            encode: true,
            beforeSend: function () {

            },
            complete: function () {

            }
        })
            .done(function (response) {
                //Fait disparaitre la ligne du tableau ou la section après sa suppression
                line.fadeOut("slow");
                fade.fadeOut("slow");
                fade.empty();
            })

            // something went wrong
            .fail(function () {
                console.log("Fail");
            })

            // after all this time ?
            .always(function () {
                // event.target.reset();
            });
    });

    function listenFocus() {
        var inputField = $('input[type=text]');
        if (inputField.val()) {
            inputField.parent().addClass("upSection");
        }
        inputField.focus(function () {
            $(this).parent().addClass("upSection");
        }).blur(function () {
            if (!$(this).val()) {
                $(this).parent().removeClass("upSection");
            }
        })
    }

    function sectionToggle(){
        // Donne un name, href, et id unique à chaque section
            $( '#navigationMenu .name' ).each(function (i) {
                var name = "nameSection_" + i;
                $(this).attr('name', name);
            });
            $( '#navigationMenu .href' ).each(function (i) {
                var href = "hrefSection_" + i;
                $(this).attr('name', href);
            });
            $( '#navigationMenu .id' ).each(function (i) {
                var id = "idSection_" + i;
                $(this).attr('name', id);
            });

        // Permet l'accordéon des sections
        var accordion = document.getElementsByClassName("buttonMenu");

        for (i = 0; i < accordion.length; i++) {
            accordion[i].addEventListener("click", function() {
                var content = this.nextElementSibling;
                if ( content.style.display === "block") {
                    content.style.display = "none";
                } else {
                    content.style.display = "block";
                }
            });
        }

        //Permet la suppression des sections
        var deleteLine = document.getElementsByClassName("deleteLine");
        for (i = 0; i < deleteLine.length; i++) {
            deleteLine[i].addEventListener("click", function() {
                var lineDelete = $(this).closest("li");
                lineDelete.fadeOut("slow");
                lineDelete.empty();
            });
        }
    }
});

})(jQuery);
