<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

class SignupController {

	public function testAction() {
		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		try {
			//Server settings
			$mail->SMTPDebug = 2;                                 // Enable verbose debug output
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  					  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = EMAIL_PATISHOPPER;                  // SMTP username
			$mail->Password = PWD_PATISHOPPER;                    // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom(EMAIL_PATISHOPPER, 'Patishopper');
			$mail->addAddress('castelain.jeremy89@gmail.com');     // Add a recipient

			//Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Finaliser l\'inscription';
			$mail->Body    = '<h1>Connectez-vous</h1><br><p>Pour valider votre inscription <b><a href="http://localhost:3000/">cliquez sur le lien !</a></b></p>';
			$mail->AltBody = 'Patishoper2018';

			$mail->send();
			echo 'Message has been sent';
		} catch (Exception $e) {
			echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}
	}

	public function activeAction($params) {
		if(isset($params['URL'][0])) {

			$user = User::findByToken($params['URL'][0]);
			if(!empty($user)) {
				$user->setStatus(1);
				$user->save();
                header('Location:/authentication');
			}
		}
	}

    public function indexAction($params)
    {
        if (empty($_SESSION['id_user'])) {
            $form = User::configFormAdd();
            $validate = "";
            $errors  = [];
            if(!empty($params["POST"])){

                //Verification des saisies
                $errors = Validator::validate($form, $params["POST"]);

                if(!empty($params['POST']['captcha']) && !empty($_SESSION['captcha'])) {
                    if($params['POST']['captcha'] !== $_SESSION['captcha']) {
                        $errors['captcha'] = "captcha invalide";
                    }
                } elseif(empty($params['POST']['captcha']) || empty($_SESSION['captcha'])) {
                    $errors['captcha'] = "captcha invalide";
                }

                if(empty($errors)){

                    $user = new User();
                    $user->setFirstname($params["POST"]["firstname"]);
                    $user->setLastname($params["POST"]["lastname"]);
                    $user->setPhone($params["POST"]["phone"]);
                    $user->setEmail($params["POST"]["email"]);
                    $user->setAddress($params["POST"]["address"]);
                    $user->setCountry($params["POST"]["country"]);
                    $user->setCity($params["POST"]["city"]);
                    $user->setPostalCode($params["POST"]["postal_code"]);
                    $user->setPassword($params["POST"]["password"]);
                    $user->setUserTypeId(USER_PRIVILEGE);
                    $user->setAttachmentId(USER_DEFAULT_PICTURE);
                    $user->setToken();
                    try {
                        $user->save();
                    } catch (PDOException $u) {
                        $errors['user-exception'] = "Le compte existe déjà";
                    }

                    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions

                    //Server settings
                    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                    $mail->isSMTP();                                      // Set mailer to use SMTP
                    $mail->Host = "smtp.gmail.com";  					  // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                    $mail->Username = EMAIL_PATISHOPPER;                  // SMTP username
                    $mail->Password = PWD_PATISHOPPER;                    // SMTP password
                    $mail->SMTPSecure = "tls";                            // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = 587;                                    // TCP port to connect to

                    //Recipients
                    $mail->setFrom(EMAIL_PATISHOPPER, "Patishopper");
                    $mail->addAddress($user->email);     				  // Add a recipient

                    //Content
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = "Finaliser l'inscription";
                    $mail->Body    = '<h1>Connectez-vous</h1><br><p>Pour valider votre inscription <b><a href="'.URL.'/active/'.
                    $user->token.'">cliquez sur le lien !</a></b></p>';

                    if (empty($errors['user-exception'])) {
                        try {
                            $mail->send();
                            $validate = "Un email d'activation vous a été envoyé.";
                        } catch (Exception $m) {
                            $errors['mail-exception'] = $m." | ".$mail->ErrorInfo;
                        }
                    }
                }
            }
            $v = new View("signup", "front");
            $v->assign("config",$form);
            $v->assign("errors",$errors);
            $v->assign("validate",$validate);
        } else {
            header("Location:".DIRNAME);
        }
	}
}
