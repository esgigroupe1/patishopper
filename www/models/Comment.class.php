<?php

class Comment extends BaseSQL
{
    protected $id = null;
    protected $content;
    protected $status = 0;
    protected $user_id;
    protected $post_id = null;
    protected $product_id = null;
    protected $comment_id = null;

    protected $created_at = null;
    protected $updated_at = null;

    protected $relations = [
        'product' => [
            'type' => 'OneToOne',
            'table' => 'product',
        ],
        'post' => [
            'type' => 'OneToOne',
            'table' => 'post',
        ],
        'user' => [
            'type' => 'ManyToOne',
            'table' => 'user',
        ],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public static function fetchColumns($type = 1)
    {
        $all_comments = [];
        if($type == 1) {
            $all_comments = self::select(['post_id|IS NOT' => null, 'comment_id|IS' => null]);
        }elseif($type == 2) {
            $all_comments = self::select(['product_id|IS NOT' => null, 'comment_id|IS' => null]);
        }
        return ['all_comments' => $all_comments];
    }

    public static function configTableList($data, $comType)
    {
        // build headers
        $data['headers'] = [
            // '<label class="checkbox"><div class="label">Tout</div><input type="checkbox" name="all"><span class="checker"></span></label>',
            "<p>Commentaire</p>",
            "<p>Sur</p>",
            "<p>Auteur</p>",
            "<p>Status</p>",
        ];

        $type = "undefined";
        if ($comType == 1) {
            $type = 'article';
        } elseif ($comType == 2) {
            $type = 'product';
        }

        // Sort
        if (array_key_exists('comments', $data)) {
            Helper::sortByLastUpdated($data['comments']);
        }

        $rowsData = [];
        foreach ($data['comments'] as $key => $row) {
            // $rowsData[$key]['checkbox'] = '<label class="checkbox"><input class="checkboxItem" type="checkbox" name="action_' . $row->id . '" value="' . $row->id . '"><span class="checker"></span></label>';

            $user = $row->getUser();
            $article = null; $product = null;
            if($comType == 1) {
                $article = $row->getPost();
            } elseif($comType == 2) {
                $product = $row->getProduct();

            }

            $comment_content = Helper::xss($row->content);

            $author = '<span class="comment-author">' . $user->firstname . " " . $user->lastname . '</span>';

            $publishedDate = new DateTime($row->created_at); // format date from MYSQL database

            $typeObject = "";
            if($row->product_id == NULL) {
                $typeObject = 'article';
            }
            if($row->post_id == NULL) {
                $typeObject = 'product';
            }

            $content = (Helper::truncate($comment_content, 80));
            $cpp = '<span class="comment-edit" data-type="' . $typeObject . '">';
            $cpp .= '<a class="following" href="#following">Afficher</a>';
            if ($row->status == 1 && $typeObject == 'article') {
                $cpp .= '<a data-post-id="' . $row->post_id . '" data-comment-id="' . $row->id . '" class="response" href="#response">Répondre</a>';
            }

            if ($row->status == 0) {
                $cpp .= '<a data-id="' . $row->id . '" class="approve" href="#approve">Approuver</a>';
            }else{
                $cpp .= '<a data-id="' . $row->id . '" class="desapprove" href="#desapprove">Désapprouver</a>';
            }

            $cpp .= '<a data-id="' . $row->id . '" class="delete-comment" href="#delete-comment">Supprimer définitivement</a>';
            $cpp .= '<div class="comment-content-' . $row->id . '">' . htmlentities($comment_content) . '</div>';
            $cpp .= '</span>';

            // set all columns
            $rowsData[$key]['content'] = $content . $cpp;

            if($article != null){
                $rowsData[$key]['on'] = '<a target="_blank" href="' . DIRNAME . $article->slug . '">' . $article->title . '</a>';
            }elseif($product != null){
                $rowsData[$key]['on'] = '<a target="_blank" href="' . DIRNAME . $product->slug . '">' . $product->name . '</a>';
            }

            $rowsData[$key]['author'] = $author;
            $rowsData[$key]['status'] = ($row->status == 1) ? '<span style="color: green; text-align: center;display: block;">Approuvé</span>' : '<span style="color: red; text-align: center;display: block;">En attente d\'approbation</span>';
            $rowsData[$key]['publishDate'] = $publishedDate->format("d-m-Y") . " à " . $publishedDate->format("H:i:s");
        }

        // data column after comments
        array_push($data['headers'], "<p>Envoyé le</p>");

        return ['headers' => $data['headers'], 'rows' => $rowsData, 'id' => 'comment-list'];
    }
}
