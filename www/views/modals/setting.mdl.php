<pre>
<?php //Helper::dump($config);?>
<?php //print_r($errors);?>
</pre>

<form method="<?php echo $config["config"]["method"]?>" action="<?php echo $config["config"]["action"]?>">

	<?php foreach ($config["input"] as $name => $params):?>

		<?php if($params["type"] == "text" || $params["type"] == "email" || $params["type"] == "password" || $params["type"] == "tel"):?>

			    <div class="control-group">
					<p>
					<input
					type="<?php echo $params["type"];?>"
					class=""
				    name="<?php echo $name;?>"
					value="<?php echo $params["value"];?>"
				    placeholder="<?php echo $params["placeholder"];?>"
				    <?php echo (isset($params["required"]))?"required='required'":"";?>
				    />
					</p>
			    </div>

            <?php elseif ($params["type"] == 'tinymce'): ?>
				<textarea name="text" cols="50" rows="15">
					<?php echo (!empty($params["data"])) ? $params["data"] : ""; ?>
				</textarea>

        <?php endif;?>

    <?php endforeach;?>

	<?php if(isset($config["select"])) : ?>
	<div class="control-group">
		<label><?php echo $config['select']['homepage']['label'] ?></label><br/>
		<select name="<?php echo $config['select']['homepage']['name'] ?>">
			<?php foreach($config['select']['homepage']['options'] as $select => $params): ?>

					<option value="<?php echo $params['label']; ?>"><?php echo $params['label']; ?></option>

			<?php endforeach; ?>
		</select>
    </div>
    <?php endif; ?>

	<label class="<?php echo $config['radio']['config']['label1_class'] ?>"><?php echo $config['radio']['config']['label1_texte']; ?></label>
	<div class="radio_div">
		<?php foreach ($config["radio"]["options"] as $value => $params): ?>
			<label class="<?php echo $config['radio']['config']['label2_class'] ?>">
			<?php if(isset($params["link"])): ?>
				<img src="<?php echo $params["link"];?>" alt="<?php echo $params['alt_img']; ?>" /><br />
			<?php endif ?>
				<input type="radio" name="<?php echo $params['name'];?>" value="<?php echo $value; ?>" <?php echo $params['checked'] ?>/>
			</label>
		<?php endforeach; ?>
	</div>


