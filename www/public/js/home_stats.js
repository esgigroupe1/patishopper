$(document).ready(function () {
  
    var xmlhttp = new XMLHttpRequest();
    var xmlhttp2 = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myObj = JSON.parse(this.responseText);
            $.each(myObj, function(key, val){ 
                // if there were no visitors that day set the value to 0 instead of nulls
                if(val == null) {
                    myObj[key] = 0;
                }
            });
            //console.log(myObj);
            
            Highcharts.chart('viewers', {

                title: {
                    text: 'Nombre de visiteurs hebdomadaire'
                },
                credits: {
                    enabled: false
                },
                subtitle: {
                    text: 'Plain'
                },
        
                xAxis: {
                    categories: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
                },
        
                series: [{
                    type: 'column',
                    data: [JSON.parse("[" + myObj.lundi + "]"), JSON.parse("[" + myObj.mardi + "]"), JSON.parse("[" + myObj.mercredi + "]"), JSON.parse("[" + myObj.jeudi + "]"), JSON.parse("[" + myObj.vendredi + "]"), JSON.parse("[" + myObj.samedi + "]"), JSON.parse("[" + myObj.dimanche + "]")],
                    colorByPoint: true,
                    colors: ['#F2994A', '#828282', '#D5D1D1', '#242222', '#F2994A', '#242222', '#D5D1D1'],
                    showInLegend: false
                }],
            });
            
        }
    };
    xmlhttp.open("GET", "/admin/chart_visitor", true);
    xmlhttp.send();

    xmlhttp2.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myObj = JSON.parse(this.responseText);
            $.each(myObj, function(key, val){ 
                // if there were no orders that day set the value to 0 instead of null
                if(val == null) {
                    myObj[key] = 0;
                }
                
            });
            console.log(myObj);
            Highcharts.chart('chart2', {
                chart: {
                    type: 'line'
                },
                colors: ['#F2994A'],
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Nombres de ventes hebdomadaire'
                },
                xAxis: {
                    categories: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
                },
        
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Nombre de ventes',
                    data: [JSON.parse("[" + myObj.lundi + "]"), JSON.parse("[" + myObj.mardi + "]"), JSON.parse("[" + myObj.mercredi + "]"), JSON.parse("[" + myObj.jeudi + "]"), JSON.parse("[" + myObj.vendredi + "]"), JSON.parse("[" + myObj.samedi + "]"), JSON.parse("[" + myObj.dimanche + "]")]
                }]
            });
            
            
        }
    };
    xmlhttp2.open("GET", "/admin/chart_orders", true);
    xmlhttp2.send();


    Highcharts.setOptions({
        lang: {
            printChart: "Imprimer le graphique",
            downloadPNG: "Télécharger le PNG",
            downloadJPEG: "Télécharger le JPEG",
            downloadPDF: "Télécharger le PDF",
            downloadSVG: "Télécharger le SVG",
        }
    });


});
