<main id="category" class="container-fluid">
    <div class="row">
        <div class="title col-10">
            <h1>Catégories</h1>
        </div>
        <!-- end col-10 -->
        <div class="col-2" id="button">
            <a href="<?php echo DIRNAME; ?>admin/product/category/add" class="button right">Ajouter</a>
        </div>
    </div>

    <hr class="separator">

    <form method="POST" id="buttons-category-actions">
        <?php $this->addModal("category-selection-pan", $infos); ?>

        <div class="row">
            <div class="col-12">
                <?php $this->addModal("table", $config); ?>
            </div>
        </div>
    </form>
</main>
