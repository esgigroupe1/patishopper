<?php

class Message extends BaseSQL {
	protected $id = null;
	protected $profession;
	protected $lastname;
	protected $firstname;
	protected $email;
	protected $content;

	protected $relations = [];

    public function __construct(){
		parent::__construct();
	}
}