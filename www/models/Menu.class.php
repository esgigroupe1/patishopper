<?php

class Menu extends BaseSQL{

    protected $id= null;
    protected $title;
    protected $status = 0;

    protected $created_at = null;
    protected $updated_at = null;

    protected $relations = [
        'sections' => [
            'type' => 'OneToMany',
            'table' => 'section',
            'data' => [],
        ],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    //Affiche un menu lors de l'ajout ou toutes les données d'un menu lors d'une modification
    public static function configFormMenu( $menu = [] ){

        $num = !empty($menu);

        $suffix_label = '<span class="bar"></span><span class="line-right-left"></span>';

        $sections = [];
        if(is_a($menu, "Menu")) {
            $all_sections = Section::getCollections(['menu_id' => $menu->id]);

            foreach( $all_sections as $key => $section ) {
                $sections["section"][$key] = [
                    "titleValue" => Helper::xss($section->name),
                    "hrefSection" => Helper::xss($section->href),
                    "id_section" => $section->id
                ];
            }
        }

        return [
            "config" => [
                "method" => "POST",
                "action" => "",
                "submit" => ($num > 0) ? "Modifier" : "Créer le menu",
                "submit_name" => "",
                "submit_class" => "button",
            ],

            "input" => [

                "title" => [
                    "name" => "title",
                    "type" => "text",
                    "label" => "<p>Titre</p>" . $suffix_label,
                    "placeholder" => "",
                    "required" => "required",
                    "minString" => 2,
                    "maxString" => 100,
                    "id" => "title_menu",
                    "value" => ($num) ? $menu->title : "",
                ],
            ],

            "menu" => [
                ($num) ? $sections : ""
            ],


        ];
    }

    //Compte le nombre de section d'un menu
    public static function getNbSections($id) {
        $sections = Section::getCollections(['menu_id' => $id]);
        $nbSections = count($sections);
        return $nbSections;
    }

    // Affiche les tous les menus
    public static function configTableMenuList($data){

        $rowsData = [];
        $items = [];

        $data['headers'] = [
            "<p>Menu</p>",
            "<p>Nombre de sections</p>",
            "<p>Etat</p>",
            "<p>Dernière mise à jour</p>",
        ];

        if (!empty($data['menus'])) {

            // for one element build array instead
            if (!is_array($data['menus'])) {
                array_push($items, $data['menus']);
            } else {
                $items = $data['menus'];
            }

            // Sort
            Helper::sortByLastUpdated($items);

            // build column
            foreach ($items as $key => $row) {
                // links to modify, delete, publish
                $title = '<span class="post-title">' . Helper::xss($row->title) . '</span>';
                $title .= '<span class="post-edit">';
                if($row->status == 0) {
                    $title .= '<a href="' . DIRNAME . "admin/menu/publish/" . $row->id . '">Activer</a>';
                }
                $title .= '<a href="' . DIRNAME . "admin/menu/edit/" . $row->id . '">Modifier</a>';
                $title .= '<a class="menu-delete" href="' . DIRNAME . 'admin/menu/delete" data-id="' . $row->id . '">Supprimer définitivement</a>';
                $title .= '</span>';

                $updatedDate = new DateTime($row->updated_at); // format date from MYSQL database

                // Allow to know if the state of a menu
                $rowsData[$key]['title'] = $title;
                $rowsData[$key]['nbSection'] = self::getNbSections($row->id);
                $rowsData[$key]['Etat'] = ($row->status == 0) ? "Non publié" : "Publié";
                $rowsData[$key]['updatedDate'] = $updatedDate->format("d-m-Y") . " à " . $updatedDate->format("H:i:s");
            }
        }

        return ['headers' => $data['headers'], 'rows' => $rowsData, 'id' => 'menu-list'];

    }
}
