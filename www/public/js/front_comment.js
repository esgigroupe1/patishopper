(function ($) {
    $(document).ready(function () {
        var request;
        var request1;
        // Perform action from actionS apply button
        $('#product-comment').submit(function (e) {
            e.preventDefault();

            // Abort any pending request
            if (request) {
                request.abort();
            }

            var ajax_form_data = $(this).serialize();
            var message_info = $("#message-info span");
            var message_container = $('#message-info');

            ajax_form_data += '&ajaxrequest=' + random32bit();

            request = $.ajax({
                url: "/product/comment/add",
                type: 'POST',
                data: ajax_form_data,
                encode: true,
                beforeSend: function () {
                    $('#product-comment #loader-perform').show();
                    message_container.hide();
                    message_info.hide().text("");
                },
                complete: function () {
                    $('#product-comment #loader-perform').hide();
                    message_container.show();
                }
            })

            request.done(function (response) {
                var response = JSON.parse(response);
                message_info.show().text(response.message);
                setTimeout(function () {
                    message_info.show().text("Les avis publiés seront visibles par tous après validation par nos équipes");
                }, 4000);

                if (response.comment != "") {
                    var comment = response.comment;
                    var message = response.message;

                    comment = `<div class="single-comment">
                                        <p class="comment-date">
                                            </p><div>Laissé par <span class="user">` + response.user + `</span> le <span class="date">` + response.date + `</span></div>
                                        <p></p>
                                        <p class="comment-text">` + response.comment + `</p>
                                    </div>`;

                    // updata numbers data
                    $(".lasts-3").fadeOut(function () {
                        $("#message-infos").css('display', 'none');
                        $(this).prepend(comment).fadeIn();
                    });

                    // $("form#message-info span").prepend(message);

                    // empty message in textarea
                    $('#product-comment textarea').val('');
                }
            })

            // something went wrong
            request.fail(function () {
                console.log("Fail");
            })

            // after all this time ?
            request.always(function () {
                // event.target.reset();
            });
        });

        $('#article-comment').submit(function (e) {
            e.preventDefault();

            // Abort any pending request
            if (request1) {
                request1.abort();
            }

            var ajax_form_data = $(this).serialize();
            var message_info = $("#message-box span");
            var message_container = $('#message-box');

            ajax_form_data += '&ajaxrequest=' + random32bit();

            request1 = $.ajax({
                url: "/article/comment/add",
                type: 'POST',
                data: ajax_form_data,
                encode: true,
                beforeSend: function () {
                    $('#loader-perform').show();
                    message_container.hide();
                    message_info.hide().text("");
                },
                complete: function () {
                    $('#loader-perform').hide();
                    message_container.show();
                }
            })

            request1.done(function (response) {

                var response = JSON.parse(response);
                message_info.show().text(response.message);
                setTimeout(function () {
                    message_info.show().text("Les commentaires postés seront visibles par tous après validation par nos équipes");
                }, 4000);

                comment = "";

                if (!$(".lasts").length) {
                    comment += `<h2>Les commentaires récents</h2>
                                <div class="lasts">`;
                }
                comment += `
                            <div class="single-comment">
                                <div class="user-image">
                                    <img src="` + response.attachment_url + `" alt="` + response.attachment_title + `" title="` + response.attachment_title + `">
                                </div> <!-- end user-image -->

                                <div class="comment-detail">
                                    <div class="comment-date">
                                        <div><span class="user">Admin</span> le <span class="date">` + response.date + `</span></div>
                                    </div>
                                    <p class="comment-text">` + response.comment + `</p>
                                </div> <!-- end comment-detail -->
                            </div>
                        `;

                if (!$(".lasts").length) {
                    comment += `</div><!-- lasts -->`
                }

                if (response.comment != "") {
                    if ($(".lasts").length) {
                        $(".lasts").fadeOut(function () {
                            $("#message-infos").css('display', 'none');
                            $(this).prepend(comment).fadeIn();
                        });
                    }else {
                        $("#comments").fadeOut(function () {
                            $("#message-infos").css('display', 'none');
                            $(this).prepend(comment).fadeIn();
                        });
                    }
                    // empty message in textarea
                    $('#article-comment textarea').val('');
                }
            })

            // something went wrong
            request1.fail(function () {
                console.log("Fail");
            })

            // after all this time ?
            request1.always(function (e) {
                //e.target.reset();
            });
        });
    });

})(jQuery);
