<?php

$slider_data = [
    [
        'title' => "Evènements",
        'description' => "Venez découvrir à les coulisses de nos ateliers de fabrication.",
        'link' => "public/images/slider/slider1.jpg",
        'button' => [
            'text' => 'Je veux y participer',
            'link' => 'contact',
        ],
    ],
    [
        'title' => "Ateliers de fabrication",
        'description' => "Découvrez nos patisseries",
        'link' => "public/images/slider/slider2.jpg",
        'button' => [
            'text' => 'Je veux être de la partie',
            'link' => 'contact',
        ],
    ],
    [
        'title' => "Promo d'automne",
        'description' => "Lorem ispum dolore",
        'link' => "public/images/slider/slider3.jpg",
        'button' => [
            'text' => 'Je souhaite en savoir plus.',
            'link' => 'contact',
        ],
    ],
];

?>

<main id="front-home" class="container no-collapse">
    <div id="home-slider" class="container no-collapse slideshow">
        <div class="slide">
            <?php foreach ($slider_data as $data): ?>
                <div class="slide-element">
                    <img src="<?php echo DIRNAME . $data['link']; ?>"/>
                    <div class="slide-details">
                        <h2><?php echo $data['title'] ?></h2>
                        <p><?php echo $data['description'] ?></p>
                        <a href="<?php echo DIRNAME . $data['button']['link']; ?>" class="button background-light">
                            <?php echo $data['button']['text']; ?>
                        </a>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
        <!-- end row -->
        <div id="controls">
            <a class="control_prev" href="#">
                <i><img src="<?php echo DIRNAME . 'public/images/front/icons/arrow_left.svg'; ?>" alt=""></i>
            </a>
            <a class="control_next" href="#">
                <i><img src="<?php echo DIRNAME . 'public/images/front/icons/arrow_right.svg'; ?>" alt=""></i>
            </a>
        </div>
    </div> <!-- end container -->

    <div id="container-product" class="container no-collapse">
        <h2>Derniers produits</h2>
        <div class="row no-collapse">
            <?php foreach ($products as $product): ?>
                <div class="col-3 col-xs-12 col-sm-6 col-md-3">
                    <a href="<?php echo $product->slug; ?>" class="wrapper">
                        <img src="<?php echo $product->getAttachment()->url; ?>"/>
                        <div class="details">
                            <h3><?php echo Helper::xss($product->name); ?></h3>
                            <p><?php echo Helper::truncate(strip_tags($product->description)); ?></p>
                        </div>
                    </a> <!-- end wrapper -->
                </div> <!-- end col-3 -->
            <?php endforeach;?>
        </div> <!-- end row -->

        <div class="row no-collapse">
            <a href="<?php echo DIRNAME; ?>produits" class="button center">Tous les produits</a>
        </div> <!-- end row -->
    </div> <!-- end container -->

    <div id="container-article" class="container no-collapse">
        <h2>Derniers articles</h2>
        <div class="row no-collapse">
           <?php foreach ($articles as $article): ?>
                <div class="col-3 col-xs-12 col-sm-6 col-md-3">
                   <a href="<?php echo $article->slug; ?>" class="wrapper">
                        <img src="<?php echo $article->getAttachment()->url; ?>"/>
                        <div class="details">
                            <h3><?php echo $article->title ?></h3>
                            <p><?php echo Helper::truncate(strip_tags($article->text)) ?></p>
                        </div>
                   </a> <!-- end wrapper -->
                </div> <!-- end col-3 -->
            <?php endforeach;?>
        </div> <!-- end row -->

        <div class="row">
            <a style="margin-bottom: 50px;" href="<?php echo DIRNAME; ?>articles" class="button center">Tous les articles</a>
        </div> <!-- end row -->
    </div> <!-- end container -->
</main> <!-- end main -->
