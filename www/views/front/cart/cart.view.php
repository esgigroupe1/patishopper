<main id="cart-content" class="container">
    <div class="row">
        <div class="col-10 col-xs-8">
            <h1>Votre panier</h1>
        </div>
        <div class="col-2 col-xs-4 button-right">
            <?php if(!empty($_SESSION['products'])): ?>
                <a class="button no-background center" href="<?php echo DIRNAME . "cart/empty"?>">Vider le panier</a>
            <?php endif; ?>
        </div>
    </div> <!-- end row -->

    <hr>

   <?php if(!empty($products)): ?>
   <?php $total = 0; ?>
    <div class="row cart-content-table">
        <table id="cart-table">
            <thead>
                <tr>
                    <th>Nom du produit</th>
                    <th>Prix unitaire</th>
                    <th>Quantité</th>
                    <th>Prix total</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach($products as $product): ?>
                <tr>
                    <td><?php echo $product['name']; ?></td>
                    <td><?php echo $product['price'] ." €"; ?></td>
                    <td><?php echo $product['quantity']; ?></td>
                    <td><?php echo ($product['price'] * $product['quantity']) . " €"; ?></td>

                    <?php $total += ($product['price'] * $product['quantity']) ;?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div> <!-- end row -->

    <div class="checkout-infos row">
        <div class="col-12">
            <p>Prix : <span><?php echo $total . " €"; ?></span></p>
            <p>TVA : <span><?php echo "10"; ?></span></p>
            <p>Prix TTC : <span><?php echo 10 * $total . " €"; ?></span></p>
        </div>
    </div>

    <div id="button-wrapper" class="row">
        <div class="col-12 col-xs-12 col-sm-12">
            <a class="button center" href="<?php echo DIRNAME . "cart/checkout"?>">Procéder au paiement</a>
        </div> <!-- col-12 -->
    </div> <!-- end row -->
   <?php else: ?>

   <div class="row">
       <div class="col-12 col-xs-12 col-sm-12">
           <div id="message-infos" class="message info">
               <span>Votre panier est vide</span>
           </div> <!-- end message-infos -->
        </div> <!-- end row -->
    </div> <!-- end row -->

    <div id="button-wrapper" class="row">
        <div class="col-12 col-xs-12 col-sm-12">
            <a class="button center" href="<?php echo DIRNAME . "produits"?>">Ajouter des produits</a>
        </div> <!-- col-12 -->
    </div> <!-- end row -->
   <?php endif; ?>
</main>
