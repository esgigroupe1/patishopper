<div class="row selects">
    <div class="col-12">
        <div id="lists">
            <a id="view-published" href="/admin/product">Publiés(<span><?php echo $config['published']; ?></span>)</a>
            <a id="view-trash" href="/admin/product/trash">Corbeille(<span><?php echo $config['trash']; ?></span>)</a>
        </div>

        <div id="actions">
            <div class="wrapper-select">
                <select name="action-perform" id="perform">
                    <option value="group" selected>Opérations groupées</option>
                    <?php $url = Helper::getURl()?>

                    <?php if ($url == "/admin/product/published"): ?>
                        <option value="draft">Mettre au brouillon</option>
                    <?php elseif ($url == "/admin/product/draft"): ?>
                        <option value="publish">Mettre en publication</option>
                    <?php endif;?>

                    <?php if ($url == "/admin/product/trash"): ?>
                        <option value="restore">Restaurer</option>
                    <?php else: ?>
                        <option value="trash">Mettre en corbeille</option>
                    <?php endif;?>

                    <option value="delete">Supprimer définitivement</option>
                </select>
            </div>
            <input class="button" type="submit" data-post-type="product" value="Appliquer">

            <img id="loader-perform" src="<?php echo DIRNAME; ?>public/images/icons/loader.gif" alt="Application des modifications" title="Application des modifications">

            <div id="message-info" class="message info">
                <span></span>
                <a href="#" class="exit" class="fade_exit">&times;</a>
            </div>
        </div>
    </div><!-- end col-12 -->
</div><!-- end row selects -->
