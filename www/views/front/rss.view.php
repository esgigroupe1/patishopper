<?php
header('Content-Type: application/rss+xml');
echo '<?xml version="1.0" encoding="UTF-8" ?>';

// Get the latest posts
$sql = "SELECT * FROM `post` WHERE `slug` <> '' ORDER BY updated_at DESC LIMIT 0,40";
$posts = Post::runSelect($sql, [], 'Post');
// get the site url
$site_url = Helper::getSiteUrl();

// get last modified post
$sql = "SELECT updated_at FROM `post` WHERE `slug` <> '' ORDER BY updated_at DESC LIMIT 0,1";
$lastBuilDate = (Post::runSelect($sql, [], 'Post')[0])->updated_at;
?>

<rss version="2.0">
    <channel>
        <title>Patishopper</title>
        <description>Ceci est le flux RSS du site Patishopper</description>
        <lastBuildDate><?php echo date(DATE_RSS, strtotime($lastBuilDate)); ?></lastBuildDate>
        <link><?php echo $site_url; ?></link>
        <?php foreach ($posts as $post): ?>
            <?php if(!empty($post->slug)): ?>
            <item>
                <title><?php echo $post->title; ?></title>
                <description><?php echo Helper::truncate($post->text, 300); ?></description>
                <pubDate><?php echo date(DATE_RSS, strtotime($post->created_at)); ?></pubDate>
                <link><?php echo $site_url . '/' . $post->slug; ?></link>
                <?php $image = $post->getAttachment(); ?>
                <image>
                    <url><?php echo $site_url . $image->url; ?></url>
                    <link><?php echo $site_url . '/' . $post->slug; ?></link>
                </image>
            </item>
            <?php endif; ?>
        <?php endforeach; ?>
    </channel>
</rss>
