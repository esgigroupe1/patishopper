<?php

abstract class BaseController
{
    /**
     * Handle all type of error and redirect to the right View
     *
     * @param integer $errorType error code to intercept
     * @return void
     */
    public function errors($errorType)
    {
        // verify error type to show the right templates 404, 500, ...
        $v = new View("error", "front");
        switch ($errorType) {
            case '401':{
                    $v->assign("errorStatus", "401");
                    $v->assign("errorDescription", "Identifiant incorrect");
                    break;
                }
            case '404':{
                    header('HTTP/1.0 404 Not Found', true, 404);
                    $v->assign("errorStatus", $errorType);
                    $v->assign("errorDescription", "Cette post n'existe post");
                    break;
                }
            default:{
                    $v->assign("errorStatus", "404");
                    $v->assign("errorDescription", "Cette post n'existe post");
                    break;
                }
        }

        die();
    }

    /**
     * Retrive posts form config with data
     *
     * @param integer $postType whether article or page
     * @param Post $post
     * @return Array posts config with data or empty array
     */
    private function getPostForm($postType, $post = null)
    {
        if ($postType == 1) {
            return Post::configFormContent($post);
        } elseif ($postType == 2) {
            return Post::configFormPage($post);
        }
        return [];
    }

    /**
     * Associate categories to post if they exist
     *
     * @param Post $post the post to which categories will be associated
     * @param Array $params urls POST data
     * @return void
     */
    protected function addCategories(&$post, $params)
    {
        // get all categories in post
        $categories = Helper::preg_grep_keys('/^category_[0-9]+$/', $params['POST']);

        // associate categories to post
        if (!empty($categories)) {
            foreach ($categories as $category_id) {
                $cats[] = Category::findById($category_id);
            }
            $post->addCategories($cats);
        }
    }

    /**
     * Show the edit form for posts (article and pages)
     *
     * @param View $v the view in which to show informations
     * @param Array $params url parameters
     * @param integer $postType determines which posts to show in edit form
     * @return void
     */
    protected function editPost($v, $params, $postType)
    {
        // check whether it is an article or page
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
            $configPost['postTitle'] = "d'un article";

        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
            $configPost['postTitle'] = "d'une page";
        }

        if (!empty($params['URL'][1]) && is_numeric($params['URL'][1])) {
            (!Helper::isValidRoute("/admin/" . $configPost['postType'] . "/edit/" . $params['URL'][1])) && $this->errors(404);
            $post = Post::getCollections([
                'id' => $params['URL'][1],
                'post_type_id' => $postType,
            ])[0];

            if (!empty($post) && is_a($post, 'Post')) {
                if (!empty($params["POST"])) {
                    // Verification des informations modifier
                    $errors = Validator::validate($this->getPostForm($postType), $params["POST"]);

                    if (empty($errors)) {
                        $uniqueSlug = (empty($params["POST"]['slug']) ? (Helper::uniqueSlug($params["POST"]['title'], $post->id)) : Helper::uniqueSlug($params["POST"]['slug'], $post->id));
                        $post->setTitle($params["POST"]['title']);
                        $post->setSlug($uniqueSlug);
                        // $post->setUserId($_SESSION['id_user']);
                        $post->setText($params["POST"]['text']);

                        $this->addCategories($post, $params);
                        $post->save();
                    } else {
                        Helper::dump($errors);
                    }
                }

                $v->assign('title', "Modification " . $configPost['postTitle']);
                $v->assign('post', $post);
                $v->assign("config", $this->getPostForm($postType, $post));
                $v->assign("errors", $errors);
                $v->assign("infos", $infos);
            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404);
        }
    }

    /**
     * Show the new post form for (articles and pages)
     *
     * @param View $v the add form view
     * @param Array $params urls parameters
     * @param integer $postType post type 1 for article or 1 for page
     * @return void
     */
    protected function addPost($v, $params, $postType)
    {
        // check whether it is an article or page
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
            $configPost['postTitle'] = "d'un article";

        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
            $configPost['postTitle'] = "d'une page";
        }

        (!Helper::isValidRoute("/admin/" . $configPost['postType'] . "/add")) && $this->errors(404);
        if (!empty($params["POST"])) {
            // Verification des informations modifier
            $errors = Validator::validate($this->getPostForm($postType), $params["POST"]);
            if (empty($errors)) {
                $post = new Post();

                $uniqueSlug = (empty($params["POST"]['slug']) ? (Helper::uniqueSlug($params["POST"]['title'])) : (Helper::uniqueSlug($params["POST"]['slug'])));

                $post->setTitle($params["POST"]['title']);
                $post->setSlug($uniqueSlug);
                $post->setText($params["POST"]['text']);
                $post->setUserId(1); // user de la session
                $post->setPublishAt((new DateTime())->format('Y-m-d H:i:s'));
                $post->setPostTypeId($postType);

                // retrieve categories if they exists
                $this->addCategories($post, $params);

                $post->save();

                header("Location: /admin/" . $configPost['postType'] . "/edit/" . $post->id);
            } else {
                // Helper::dump($errors);
            }
        }
        $v->assign("title", "Ajout " . $configPost['postTitle']);
        $v->assign("config", $this->getPostForm($postType, $post));
        $v->assign("infos", $infos);
    }

    /**
     * Delete definitively post from database
     *
     * @param Array $params urls parameters
     * @param integer $postType integer $postType post type 1 for article or 1 for page
     * @return void
     */
    protected function deletePost($params, $postType)
    {
        // check whether it is an article or page
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
        }

        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {

            (!Helper::isValidRoute("/admin/" . $configPost['postType'] . "/delete/" . $params['POST']['id'])) && $this->errors(404);
            Post::delete(['id' => $params['POST']['id']]);

            // get all data columns numbers
            extract(Post::fetchColumnsNumbers($postType));
            echo json_encode(
                [
                    'all' => count($all_pages),
                    'published' => count($published_pages),
                    'draft' => count($draft_pages),
                    'trash' => count($trash_pages),
                ]
            );
        } else {
            $this->errors(404);
        }
    }

    /**
     * Move post (article & page) to bin
     *
     * @param Array $params urls parameters
     * @param integer $postType integer $postType post type 1 for article or 1 for page
     * @return void
     */
    protected function movePostToBin($params, $postType)
    {
        // check whether it is an article or page
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
        }

        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {
            (!Helper::isValidRoute("/admin/" . $configPost['postType'] . "/move-to-bin/" . $params['POST']['id'])) && $this->errors(404);
            $post = Post::findById($params['POST']['id']);

            if (is_a($post, 'Post') && !is_array($post)) {
                // update information
                $post->setTrash(1);
                $post->save();

                // get new numbers
                extract(Post::fetchColumnsNumbers($postType));
                echo json_encode(
                    [
                        'all' => count($all_pages),
                        'published' => count($published_pages),
                        'draft' => count($draft_pages),
                        'trash' => count($trash_pages),
                    ]
                );

            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404); // the request is not coming from the right source
        }
    }

    /**
     * Restore posts from bin
     *
     * @param Array $params urls parameters
     * @param integer $postType integer $postType post type 1 for article or 1 for page
     * @return void
     */
    protected function removePostFromBin($params, $postType)
    {
        $configPost = [];
        if ($postType == 1) { // Article
            $configPost['postType'] = "article";
        } elseif ($postType == 2) { // page
            $configPost['postType'] = "page";
        }

        if (!empty($params['POST']['ajaxrequest']) &&
            !empty($params['POST']['id']) &&
            is_numeric($params['POST']['id'])) {
            (!Helper::isValidRoute("/admin/" . $configPost['postType'] . "/remove-from-bin/" . $params['POST']['id'])) && $this->errors(404);
            $post = Post::findById($params['POST']['id']);
            if (!empty($post)) {
                $post->setTrash(0);
                $post->save();

                // get all data columns numbers
                extract(Post::fetchColumnsNumbers($postType));
                echo json_encode(
                    [
                        'all' => count($all_pages),
                        'published' => count($published_pages),
                        'draft' => count($draft_pages),
                        'trash' => count($trash_pages),
                    ]
                );
            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404); // the request is not coming from the right source
        }
    }

    /**
     * Manage grouped action of posts (article and page)
     *
     * @param Array $params urls parameters
     * @param integer $postType integer $postType post type 1 for article or 1 for page
     * @return void
     */
    protected function performSelectionPan($params, $postType)
    {
        if (!empty($params['POST']['ajaxrequest'])) {
            if (!empty($params['POST']['action-perform'])) {
                sleep(1); // wait loader
                // get all CORRECT that matches exactly to [action_4] pattern checkkox values
                $data_actions = Helper::preg_grep_keys('/^action_[0-9]+$/', $params['POST']);
                if (!empty($data_actions)) {
                    switch ($params['POST']['action-perform']) {
                        case "group":{
                                echo json_encode([
                                    'message' => "Aucune action n'a été choisie",
                                ]);
                                break;
                            }
                        case "trash":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `post` SET trash = 1 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumnsNumbers($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_pages),
                                        'published' => count($published_pages),
                                        'draft' => count($draft_pages),
                                        'trash' => count($trash_pages),
                                    ],
                                    'message' => "Toutes les pages sélectionnées ont été mises à la corbeille",
                                ]);

                                break;
                            }
                        case "restore":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `post` SET trash = 0 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumnsNumbers($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_pages),
                                        'published' => count($published_pages),
                                        'draft' => count($draft_pages),
                                        'trash' => count($trash_pages),
                                    ],
                                    'message' => "Toutes les pages sélectionnées ont été rétirées de la corbeille",
                                ]);

                                break;
                            }
                        case "draft":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `post` SET status = 0 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumnsNumbers($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_pages),
                                        'published' => count($published_pages),
                                        'draft' => count($draft_pages),
                                        'trash' => count($trash_pages),
                                    ],
                                    'message' => "Toutes les pages selectionnées ont été mises au brouillon",
                                ]);

                                break;
                            }
                        case "publish":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "UPDATE `post` SET status = 1 WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumnsNumbers($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_pages),
                                        'published' => count($published_pages),
                                        'draft' => count($draft_pages),
                                        'trash' => count($trash_pages),
                                    ],
                                    'message' => "Toutes les pages selectionnées ont été mises en publication",
                                ]);

                                break;
                            }

                        case "delete":{
                                // add prefix to data
                                Helper::array_key_prefix($data_actions, ":");

                                // prepare query
                                $sql = "DELETE FROM `post` WHERE id IN (" . implode(",", array_keys($data_actions)) . ")";

                                // execute query
                                Post::runQuery($sql, $data_actions);

                                extract(Post::fetchColumnsNumbers($postType));
                                echo json_encode([
                                    'data' => [
                                        'all' => count($all_pages),
                                        'published' => count($published_pages),
                                        'draft' => count($draft_pages),
                                        'trash' => count($trash_pages),
                                    ],
                                    'message' => "Toutes les pages selectionnées ont été supprimées définitivement",
                                ]);

                                break;
                                break;
                            }
                        default:{
                                echo json_encode([
                                    'message' => "Une erreur s'est produite",
                                ]);
                                break;
                            }
                    }
                } else {
                    echo json_encode([
                        'message' => "Aucune page n'a été sélectionnée",
                    ]);
                }

            } else {
                $this->errors(404);
            }
        } else {
            $this->errors(404);
        }
    }
}
