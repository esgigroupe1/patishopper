<?php

class ArticleController extends Controller
{
    public function commentAction($params)
    {
        //add
        (!Helper::isRoute("/article/comment/add")) && $this->errors(404);
        if (!empty($params['POST']['ajaxrequest']) && $params['URL'][0] == 'add') {
            if (empty($params['POST']['text'])) {
                echo json_encode(
                    [
                        'comment' => "",
                        'message' => "Saisissez un commentaire d'abord SVP !",
                    ]
                );
            } else {
                $comment = new Comment();
                $comment->post_id = $params['POST']['article_id']; // have to check if it's numeric
                $comment->setContent($params['POST']['text']);
                $comment->setUserId($_SESSION['id_user']); // // have to check if it's numeric
                $comment->setUserId(1);
                $comment->save();

                $user = $comment->getUser();
                $date = new DateTime($comment->created_at);
                $date = $date->format("d-m-Y") . " à " . $date->format("H:i:s");
                $text = $comment->content;

                $attachment = $user->getAttachment();

                echo json_encode(
                    [
                        'user' => $user,
                        'attachment_url' => $attachment->url,
                        'attachment_title' => $attachment->name,
                        'date' => $date,
                        'comment' => Helper::xss($text),
                        'message' => "Votre commentaire été enregistré ! Il sera visible par tous rapidement.",
                    ]
                );
            }
        } else {
            $this->errors(404);
        }
    }
}
