<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href=<?php echo DIRNAME. "public/images/icons/orange/favicon.png"; ?> type="image/x-icon">
        <link rel="icon" href=<?php echo DIRNAME. "public/images/icons/orange/favicon.png"; ?> type="image/png">

        <title><?php echo $this->getTitle(); ?></title>

        <!-- Linked -->
        <link href=<?php echo DIRNAME . "public/css/main.css" ?> rel="stylesheet">

    </head>

    <?php
function getMenu()
{
    ?>
        <ul>
            <li>
                <a href="/" title="Patishopper">
                    <img src="<?php echo DIRNAME . 'public/images/front/icons/icon_home.svg'; ?>" alt="Home">
                </a>
            </li>
            <li><a href="<?php echo DIRNAME; ?>articles">Articles</a></li>
            <li><a href="<?php echo DIRNAME; ?>produits">Produits</a></li>
            <li><a href="#">Recettes</a></li>
            <li><a href="#">Evènements</a></li>
            <li><a href="<?php echo DIRNAME; ?>contact">Contact</a></li>
        </ul>

    <?php

}
?>

    <body>
        <div id="front-template" class="container-fluid collapse">
            <div id="site-header" class="row">
                <div class="col-12">
                    <header class="container-fluid collapse">
                        <div class="container">
                            <div id="site-logo">
                                <a href="/" title="Patishopper">
                                    <img src="<?php echo DIRNAME . 'public/images/front/orange_logo.svg'; ?>" alt="Patishopper">
                                    <span><p><?php echo (Setting::findByName('site_name'))->value; ?></p></span>
                                </a>
                            </div>
                            <!-- site-info -->
                            <div id="infos">

                                <div id="links">
                                    <?php if (empty($_SESSION['id_user'])): ?>
                                        <a href="<?php echo DIRNAME; ?>authentication" title="Connexion">
                                            <img src="<?php echo DIRNAME . 'public/images/front/icons/icon_user.svg'; ?>" alt="Connexion">
                                            <span><p>Connexion</p></span>
                                        </a>
                                    <?php endif;?>
                                    <?php if (!empty($_SESSION['id_user'])): ?>
                                        <span>Bienvenue <?php echo Helper::xss(ucfirst($_SESSION['firstname'])); ?></span>
                                        <?php
                                            $link = "";
                                            if (Helper::isAllowedToAdmin()) {
                                                $link = DIRNAME . "admin/user/" . $_SESSION['id_user'];
                                            } else {
                                                $link = DIRNAME . "account/informations";
                                            }
                                        ?>
                                        <a class="profil" href="<?php echo $link; ?>">
                                            <img src="<?php echo $_SESSION['attachment_user']; ?>" alt="">
                                        </a>
                                        <img src="<?php echo DIRNAME . 'public/images/back/icons/caret-dropdown.svg'; ?>" onclick="dropbtn()" class="dropbtn">

                                        <div id="myDropdown" class="dropdown-content">
                                            <?php if (Helper::isAllowedToAdmin()): ?>
                                            <a href="<?php echo DIRNAME; ?>admin">Administration</a>
                                            <?php endif;?>
                                            <?php if (Helper::canOrder()): ?>
                                            <a href="<?php echo DIRNAME; ?>account/informations">Mon compte</a>
                                            <?php endif;?>
                                            <a href="<?php echo DIRNAME; ?>authentication/changePassword">Modifier le mot de passe</a>
                                            <a href="<?php echo DIRNAME; ?>disconnect">Déconnexion</a>
                                        </div>
                                    <?php endif;?>
                                    <a href="<?php echo DIRNAME; ?>cart/content" title="Panier">
                                        <img src="<?php echo DIRNAME . 'public/images/front/icons/icon_panier.svg'; ?>" alt="Panier">
                                        <span><p>Panier</p></span>
                                        <?php if (!empty($_SESSION['products'])): ?>
                                        <div id="product-number">(<?php echo count($_SESSION['products']); ?>)</div>
                                        <?php else: ?>
                                        <div id="product-number">(0)</div>
                                        <?php endif;?>
                                    </a>
                                </div><!-- end links -->

                                <div id="menu">
                                    <a href="#" title="Menu"></a>
                                </div><!-- end menu -->
                            </div>
                        <!-- end infos -->
                        </div>
                    </header>
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
            <div id="site-navigation" class="row">
                <div class="col-12">
                    <nav>
                        <div class="container">
                            <ul>
                                <li>
                                    <a href="/" title="Patishopper">
                                        <img src="<?php echo DIRNAME . 'public/images/front/icons/icon_home.svg'; ?>" alt="Home">
                                    </a>

                                </li>
                            <?php
                                $menu = Menu::getCollections(['status' => 1 ])[0];
                                $sections = $menu->getSections();
                                foreach($sections as $key => $section):
                            ?>
                                <li><a href="<?php echo DIRNAME.$section->href?>"><?php echo $section->name?></a></li>
                            <?php endforeach;?>
                             </ul>
                        </div>
                        <!-- end informations -->
                    </nav>
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->

            <!-- View inclusion -->
            <div id="site-view-content" class="row">
                <!-- Dynamic content -->

                <div id="view-content" class="container-fluid collapse">
                    <!-- Dynamic content -->
                    <?php include SITE_DIR ."views/front/" . $this->folder . "/" . $this->view;?>
                    <!-- End Dynamic content -->
                </div>
            </div>
            <!-- end View inclusion -->

            <div id="site-footer" class="row">
                <footer class="container">
                    <div class="row">
                        <div class="col-4 col-md-12 col-sm-12 col-xs-12">
                            <div>
                                <div id="message-infos" class="message info">
                                    <span></span>
                                </div>
                                <p>Pour recevoir nos offres <br>spéciales</p>
                                <form id="newsletter">
                                    <p id="newsletter-content">
                                        <input name="email" class="input text" type="text">
                                        <input class="submit button" type="submit" value="S'abonner">
                                        <div id="img">
                                            <img id="loader-perform" src="<?php echo DIRNAME; ?>public/images/icons/loader.gif" alt="Ajout du produit au panier" title="Ajout du produit au panier">
                                        </div>
                                    </p>
                                </form>
                            </div>
                        </div>

                        <div class="col-4 col-md-6 col-sm-12 col-xs-12">
                            <ul>
                                <li><a href="https://www.linkedin.com/company/esgi-ecole-sup-rieure-de-g-nie-informatique-/?originalSubdomain=fr" title="LinkedIn"><i><img src="<?php echo DIRNAME . 'public/images/icons/orange/linkedin_orange.svg'; ?>" alt="LinkedIn"></i></a></li>
                                <li><a href="https://twitter.com/esgi?lang=fr" title="Twitter"><i><img src="<?php echo DIRNAME . 'public/images/icons/orange/twitter_orange.svg'; ?>" alt="Twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/ESGIParis/?ref=br_rs" title="Facebook"><i><img src="<?php echo DIRNAME . 'public/images/icons/orange/facebook_orange.svg'; ?>" alt="Facebook"></i></a></li>
                            </ul>
                            <p>&copy; Company Patishopper 2018</p>
                        </div>
                        <div class="col-4 col-md-6 col-sm-12 col-xs-12">
                            <ul>
                                <li><a href="<?php echo DIRNAME;?>mentions-legales" title="Conditions">Mentions légales</a></li>
                                <li><a href="<?php echo DIRNAME;?>contact" title="Support">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- end row -->
                </footer> <!-- end container -->
            </div><!-- end row -->
        </div> <!-- end container -->

        <script>
            function random32bit() {
                let u = new Uint32Array(1);
                window.crypto.getRandomValues(u);
                let str = u[0].toString(16).toUpperCase();
                return '00000000'.slice(str.length) + str;
            }
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <?php if ($c == "AuthenticationController" && $a == "forgotAction"): ?>
            <script>
                (function ($) {
                    $(document).ready(function(){

                    });
                })(jQuery);
            </script>
        <?php endif;?>

        <script src="<?php echo DIRNAME; ?>public/js/front_comment.js"></script>
        <script src="<?php echo DIRNAME; ?>public/js/front.js"></script>
        <script src="<?php echo DIRNAME; ?>public/js/front_payment.js"></script>


        <script>
         /**
         * Dropdown Menu
         */
        function dropbtn() {
            document.getElementById("myDropdown").classList.toggle("show");
        }

        // Close the dropdown if the user clicks outside of it
        window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
              }
            }
          }
        }
        </script>
    </body>
</html>
