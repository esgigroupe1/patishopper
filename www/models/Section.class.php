<?php

class Section extends BaseSQL{

    protected $id= null;
    protected $rank;
    protected $name;
    protected $href;
    protected $menu_id = null;

    protected $created_at = null;
    protected $updated_at = null;

    protected $relations = [
        'menu' => [
            'type' => 'ManyToOne',
            'table' => 'menu',
        ],
    ];

    public function __construct()
    {
        parent::__construct();
    }
}
