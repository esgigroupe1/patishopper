<?php
/**
 * Here are all view associed to our application backend
 */
class RequestController
{
    public static function render($params)
    {
        global $c, $a;
        $slug = str_replace("Controller", "", $c);
        $action = str_replace("Action", "", $a);
        if (Helper::isAllowedToAdmin()) {
            $data = Post::findBySlug(strtolower($slug));
        } else {
            $data = Post::getCollections(['slug' => strtolower($slug), 'status' => 1, 'trash' => 0]);
            // published and not in Trash

            if (!empty($data)) {
                $data = $data[0];
            }
        }
        $view = "post";
        if (empty($data)) {
            $data = Product::findBySlug($slug);
            $view = "product";
        }

        if (!empty($data)) {
            $v = new View($view, "front");
            if ($view == "product") {
                $v->assign("commentForm", Product::configCommentForm());
            }
            $v->assign("data", $data);
        } else {
            $slug = strtolower($slug);
            if ($slug == "produits") {
                $products = [];
                if ($action != 'index') {
                    $category = Category::select(['slug' => $action, 'type' => 2]);
                    if (!empty($category)) {
                        $products = $category[0]->getProducts();
                    } else {
                        Controller::errors(404);exit();
                    }
                }
                $v = new View("products", "front");
                empty($products) && ($products = Product::findAll());
                Helper::sortByLastUpdated($products);
                $v->assign('products', $products);
                $v->assign('categories', Category::findByType(2)); // 2 = product
            } elseif ($slug == "articles") {
                $posts = [];
                if ($action != 'index') {
                    $category = Category::select(['slug' => $action, 'type' => 1]);
                    if (!empty($category)) {
                        $posts = $category[0]->getPosts();
                    } else {
                        Controller::errors(404);exit();
                    }
                }
                $v = new View("posts", "front");
                empty($posts) && ($posts = Post::getCollections(['post_type_id' => 1, 'status' => 1]));

                Helper::sortByLastUpdated($posts);
                $v->assign('posts', $posts);
                $v->assign('categories', Category::findByType(1)); // 1 for article category | 2 = for product category

            } elseif ($slug == "sitemap.xml" || $slug == "sitemap") {
                $v = new View("sitemap", "default");
            } elseif ($slug == "rss" || $slug == "rss.xml") {
                $v = new View("rss", "default");
            } else {
                Controller::errors(404);
                exit();
            }
        }
    }
}
