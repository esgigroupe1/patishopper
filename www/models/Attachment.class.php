<?php

class Attachment extends BaseSQL
{
    protected $id = null;
    protected $name;
    protected $description;
    protected $url;

    protected $relations = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getId()
    {
        return $this->id;
    }

    public static function makeModal(){
        //tous les medias en BDD
        $medias = Attachment::findAll();

        return [
                "button" => [
                                "class" => "button",
                                "id" => "medias",
                                "text_displayed" => "Ajouter medias"

                ],
                "medias" => $medias
        ];
    }
}
