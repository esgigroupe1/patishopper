<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

class ContactController extends Controller
{
    public function indexAction($params)
    {
        $form = Contact::configFormAdd();
        $validate = "";
        $errors  = [];
        $message = [];
        if($_SERVER['REQUEST_METHOD'] == "POST" && !empty($params["POST"])){
            //send email
            $message = self::mailAction();
            //save message
            $m = new Message();

            $m->profession = $_POST['profession'];
            $m->lastname = $_POST['lastname'];
            $m->firstname = $_POST['firstname'];
            $m->email = $_POST['email'];
            $m->content = $_POST['message'];
            $m->save();
        }
        
        $v = new View("contact", "front");
        $v->assign("config",$form);
        $v->assign("errors",$errors);
        $v->assign("validate",$validate);
        
        //customize message displayed to the user
        switch($message)
        {
            case 'Votre message a bien été envoyé.':
                $v->assign("mail_success",$message);
                break;
            case 'Une erreur est survenue lors de l\'envoi du message.':
                $v->assign("mail_error",$message);
                break;
        }
        
    }

    public function mailAction()
    {
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try 
        {
			//Server settings                          
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output  
			$mail->Host = 'smtp.gmail.com';  					  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = EMAIL_PATISHOPPER;                  // SMTP username
			$mail->Password = PWD_PATISHOPPER;                    // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to
            
			//Recipients
			$mail->setFrom(EMAIL_PATISHOPPER, 'Patishopper');
			$mail->addAddress(EMAIL_PATISHOPPER);     // Add a recipient

			//Contenty
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Prise de contact';
            $mail->Body    = '<h4>Expéditeur : '.$_POST['email'].'</h4>
                              <hr class="separator">
                              <p>Message : <br>
                              '.$_POST['message'].'
                              </p>';
			$mail->AltBody = 'Patishoper2018';

            $mail->send();

            $message = "Votre message a bien été envoyé.";
            return $message;
        } 
        catch (Exception $e) {
            $message = "Une erreur est survenue lors de l'envoi du message.";
            return $message;
		}
    }
}