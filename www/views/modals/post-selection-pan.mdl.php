<div class="row selects">
    <div class="col-12">
        <div id="lists">
            <a id="view-all" href="/admin/<?php echo $config['post_type'];?>">Tout(<span><?php echo $config['all']; ?></span>)</a>
            <a id="view-published" href="/admin/<?php echo $config['post_type'];?>/published">Publiés(<span><?php echo $config['published']; ?></span>)</a>
            <a id="view-draft" href="/admin/<?php echo $config['post_type'];?>/draft">Brouillons(<span><?php echo $config['draft']; ?></span>)</a>
            <a id="view-trash" href="/admin/<?php echo $config['post_type'];?>/trash">Corbeille(<span><?php echo $config['trash']; ?></span>)</a>
        </div>

        <div id="actions">
            <div class="wrapper-select">
                <select name="action-perform" id="perform">
                    <option value="group" selected>Opérations groupées</option>
                    <?php $url = Helper::getURl() ?>

                    <?php if($url == "/admin/". $config['post_type'] . "/published"): ?>
                        <option value="draft">Mettre au brouillon</option>
                    <?php elseif($url == "/admin/" . $config['post_type'] . "/draft"): ?>
                        <option value="publish">Mettre en publication</option>
                    <?php endif; ?>

                    <?php if($url == "/admin/" . $config['post_type'] . "/trash"): ?>
                        <option value="restore">Restaurer</option>
                    <?php else: ?>
                        <option value="trash">Mettre en corbeille</option>
                    <?php endif; ?>

                    <option value="delete">Supprimer définitivement</option>
                </select>
            </div>
            <input class="button" type="submit" data-post-type="<?php echo $config['post_type'];?>" value="Appliquer">

            <img id="loader-perform" src="<?php echo DIRNAME; ?>public/images/icons/loader.gif" alt="Application des modifications" title="Application des modifications">

            <div id="message-info" class="message info">
                <span></span>
                <a href="#" class="exit" class="fade_exit">&times;</a>
            </div>
        </div>
    </div><!-- end col-12 -->
</div><!-- end row selects -->
