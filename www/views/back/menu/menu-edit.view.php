<main class="container-fluid" id="menu-edit">
    <div class="row">
        <div class="title col-10">
            <h1><?php echo $title;?></h1>
        </div>
    </div>

    <hr class="separator">

    <?php $form = Form::get($config); ?>
    <?php echo $form['open'] ?>
    <div class="row">
        <div class="col-10">
            <div class="row">
                <?php if(!empty($success)): ?>
                    <div class="col-12">
                        <div class="message success">
                            <span><?php echo $success; ?></span>
                            <a href="#" class="exit" class="fade_exit">&times;</a>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="col-12 group">
                    <?php if(!empty($errors['title'])): ?>
                        <div class="message error">
                            <span><?php echo $errors['title']; ?></span>
                            <a href="#" class="exit" class="fade_exit">&times;</a>
                        </div>
                    <?php endif; ?>
                    <?php  echo $form['title']; ?>
                </div><!-- end col-12 -->
            </div> <!-- end row -->
            <div class="row">
                <div class="col-12 group titleMenu">
                    <h3 class="title-choice">Section Menu</h3>
                    <div class="container col-10" id="sectionMenu">
                        <ul id="navigationMenu">
                            <?php
                                $tab = $form;
                                unset($tab["title"]);
                                unset($tab["submit"]);
                                echo implode("", $tab)
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div> <!-- end col-10 -->

        <div class="col-2">
            <div class="wrapper">
                <div id="button-actions">
                    <input class="button" id="ajoutSection" type="submit" value="Ajouter une Section">
                    <?php  echo $form['submit']; ?>
                </div>
            </div>
        </div> <!-- col-2 -->
    </div><!-- end row -->
    <?php echo $form['close'] ?>

</main>
